<?php if ($is_login):?>
	<div class="nameBar">ようこそ <?php echo Session::get('login_info.last_name').' '.Session::get('login_info.first_name');?> さん</div>
<?php endif;?>

<div id="contents" class='index'>
	
	<div id='slider'>
		<ul id="slideSpace">

			<?php //休日開けにif文を外す; ?>
			<?php if (Fuel::$env !== Fuel::PRODUCTION): ?>
				<li>
					<a href="<?php echo $base_url;?>/item/index/10034">
						<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/11sp.png" alt="" width="100%" >
					</a>
				</li>
			<?php else: ?>
				<?php
					$dt = new DateTime();
					$dt->setTimeZone(new DateTimeZone('Asia/Tokyo'));
					$current_time = $dt->format('Y-m-d H:i:s');
					$target_time  = '2015-05-30 23:59:59';
				?>
				<?php if (strtotime($target_time) < strtotime($current_time)):?>
					<li>
						<a href="<?php echo $base_url;?>/item/index/10034">
							<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/11sp.png" alt="" width="100%" >
						</a>
					</li>
				<?php endif;?>
			<?php endif;?>

			<li>
				<a href="<?php echo $base_url;?>/item/list?cate=1005">
					<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/10sp.png" alt="ニコペロ好評発売中" width="100%" >
				</a>
			</li>
			<li>
				<a href="<?php echo $base_url;?>/item/list?cate=1006">
					<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/07sp.png" alt="ニコペロ好評発売中" width="100%" >
				</a>
			</li>
			<li>
				<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/01sp.png" alt="" width="100%">
			</li>
			<li class="n8">
				<a href="http://clubt.jp/shop/S0000046853.html" target="_blank">
					<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/08sp.jpg" alt="" width="100%" >
				</a>
			</li>
			<li class="n6">
				<a href="http://make.dmm.com/shop/18122" target="_blank">
					<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/06sp.png" alt="" width="100%">
				</a>
			</li>
			<li>
				<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/04sp.png" alt="" width="100%">
			</li>
		</ul>
		<span class="next nav"></span>
		<span class="prev nav"></span>
	</div>
	<div class="search">
		<div class="searchWord">
<!--
未実装
<form method="post" action="<?php echo $base_url;?>/item/list">
				<label for="search_word">
					検索
				</label>
				<input type="text" name="search_word" value="<?php if(isset($search_word)) echo $search_word;?>" id="search_word">
				<input type="submit" value="検索">
</form>
-->
		</div>
		<h2>キャラクターから探す</h2>
		<ul class="character">
			<?php foreach (ViewHelper::get_chara_data() as $val): ?>
				<li><a href="<?php echo $base_url;?>/item/list?char=<?php echo $val['id'];?>"><img src="<?php echo $assets_path;?>/img/character/<?php echo $val['id'];?>/100_100.png" alt="<?php echo $val['name'];?>"></a></li>
			<?php endforeach;?>
		</ul>
		<h2>カテゴリから探す</h2>
		<ul class="category">
			<?php foreach (ViewHelper::get_side_menu_category_data() as $val):?>
				<li><a href="<?php echo $base_url;?>/item/list?cate=<?php echo $val['id'];?>"><?php echo $val['name'];?></a></li>
			<?php endforeach;?>
		</ul>
	</div>
	
</div>