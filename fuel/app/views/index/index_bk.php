<?php echo Session::get_flash('message'); ?>
<!-- ログインパーツ -->
<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/auth/login">
	<input type="text" name="mail" value="<?php if(isset($login_parts['mail'])): echo $login_parts['mail']; endif;?>">
	<?php if (isset($login_parts['error']['mail'])):?>
		<?php echo $login_parts['error']['mail'];?>
	<?php endif;?>
	<br />
	<input type="text" name="passwd_auth" value="<?php if(isset($login_parts['passwd_auth'])): echo $login_parts['passwd_auth']; endif;?>">
	<?php if (isset($login_parts['error']['passwd_auth'])):?>
		<?php echo $login_parts['error']['passwd_auth'];?>
	<?php endif;?>
	<br />
	<input type="checkbox" name="auto_login" value="1" <?php if(isset($login_parts['auto_login']) && $login_parts['auto_login'] == 1):?> checked <?php endif;?>>自動ログイン
	<?php if (isset($login_parts['error']['auto_login'])):?>
		<?php echo $login_parts['error']['auto_login'];?>
	<?php endif;?>
	<br />
	<input type="submit" value="ログイン">
	<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
</form>

パスワードリセット
<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/auth/reset_passwd">
	<input type="text" name="repwd_mail">
	<br />
	<input type="submit" value="パスワード再発行">
	<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
</form>

<br />

-------商品一覧表示-------<br />

-------ページ番号のリンク-------<br />
<?php for($i=1; $i<= $data['pagenate']['page_count']; $i++): ?>
	<?php echo ($data['pagenate']['page_no'] == $i)? '現在のページ': $i;?>
<?php endfor; ?>
<br /><br />

-------商品の表示-------<br />
<?php for($j=0; $j<count($data['pagenate']['current_page_data']); $j++):?>
	<img src="<?php echo $assets_path;?>/img/item/<?php echo $data['pagenate']['current_page_data'][$j]['id'];?>/152_105.jpg">
	<?php echo $data['pagenate']['current_page_data'][$j]['name'];?>
	<br />
	########################
	<br />
<?php endfor; ?>




<script src="http://maps.googleapis.com/maps/api/js?=AIzaSyDhG5EXTNwUhbuwgLOlZCVnIrFG_vFNO_I&sensor=false" type="text/javascript" charset="UTF-8"></script>
<script type="text/javascript">
var map;
var mark_tmp;

	function initialize (){
		var latlng = new google.maps.LatLng(35.70149853168077, 139.7541618347168);
		var opts = {
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: latlng
		};

		map = new google.maps.Map(document.getElementById("map"), opts);

		//経度と緯度取得fanctionのセット
		console.log(google.maps);
		google.maps.event.addListener(map, 'click', clickMarker);

		//地名から経度/緯度を取得するオブジェクト
		geo = new google.maps.Geocoder();
	}

	//地図上をダブルクリックされた際に経度と緯度を取得するメソッド
	function clickMarker(event) {

		//既にマーカーがあった場合
		if (mark_tmp !== undefined){
			mark_tmp.setMap(null);
		}

		mark_latlng = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());

		var marker = new google.maps.Marker({
			position: mark_latlng,
			map: map,
			title:"小林航のうち",
			icon:'<?php echo $assets_path;?>/marker.png'
		});

		console.log(map);
		//map.openInfoWindowHtml(map.getCenter(),"吹き出してすと!!吹き出してすと!!吹き出してすと!!");

		// 吹き出しを開く
		var myInfoWindow = new google.maps.InfoWindow({
			content:'テストテストテストテストテストテストテストテスト'
		});
		myInfoWindow.open(map, marker);

		mark_tmp = marker;

		document.getElementById("show_lat").innerHTML = event.latLng.lat();
		document.getElementById("show_lng").innerHTML = event.latLng.lng();
	}

	function buttonpress() {
		// GeocoderRequest
		var req = {
			address: document.getElementById("input").value,
		};
		geo.geocode(req, geoResultCallback);
	}

	//地名で経度/緯度を取得するメソッド
	function geoResultCallback(result, status) {
		if (status != google.maps.GeocoderStatus.OK) {
			alert(status);
			return;
		}

		var latlng = result[0].geometry.location;

		map.setCenter(latlng);

		var marker = new google.maps.Marker({position:latlng, map:map, title:latlng.toString(), draggable:true});

		google.maps.event.addListener(marker, 'dragend', function(event){
			marker.setTitle(event.latLng.toString());
		});
		console.log(latlng.toString());
		document.getElementById("latlngtext").innerHTML = document.getElementById("input").value + " : " + latlng.toString();
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
経度<div id="show_lat" style="width:100px; hegiht:100px;"></div>
緯度<div id="show_lng" style="width:100px; hegiht:100px;"></div>
<div id="map" style="width:300px; height:300px"></div>
<input id="input" onsubmit="buttonpress()">
<input type="button" onclick="buttonpress()" value="検索する">
<div id="latlngtext"></div>

<?php //echo Security::js_set_token(); ?>
