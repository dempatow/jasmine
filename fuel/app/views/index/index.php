<div id="contents">
	<div class="newItems">
		<h2><img src="<?php echo $assets_path;?>/img/pages/index/newItem.png" alt="新着アイテム" /></h2>
		<ul>
			<?php for ($i=0; $i<count($data['new_item']); $i++):?>
				<li>
					<?php if (isset($data['new_item'][$i]['external_url']) && !empty($data['new_item'][$i]['external_url'])):?>
						<a href="<?php echo $data['new_item'][$i]['external_url'];?>" target="_blank">
					<?php else:?>
						<a href="<?php echo $base_url;?>/<?php echo $data['new_item'][$i]['name'];?>">
					<?php endif;?>
						<div class="itemPhoto">
							<img src="<?php echo $assets_path.'/img/item/'.$data['new_item'][$i]['id'];?>/120_120.jpg" alt="<?php echo $data['new_item'][$i]['name'];?>">
							<?php if (Func::check_reserve_date($data['new_item'][$i]['selling_date'])):?>
								<img src="<?php echo $assets_path;?>/img/common/parts/itemIcon/icon6.png" alt="予約受付中" class="iconN1">
							<?php elseif (isset($data['new_item'][$i]['new_flg'])):?>
								<!-- 新着 -->
								<img src="<?php echo $assets_path;?>/img/common/parts/itemIcon/icon3.png" alt="新着" class="iconN1">
							<?php elseif(isset($data['new_item'][$i]['discount'])):?>
								<!-- セール -->
								<img src="<?php echo $assets_path;?>/img/common/parts/itemIcon/icon5.png" alt="セール" class="iconN1">
							<?php elseif(isset($data['new_item'][$i]['free_sending'])):?>
								<!-- 送料無料 -->
								<img src="<?php echo $assets_path;?>/img/common/parts/itemIcon/icon4.png" alt="送料無料" class="iconN1">
							<?php elseif(isset($data['new_item'][$i]['reco_flg'])):?>
								<!-- おすすめ -->
								<img src="<?php echo $assets_path;?>/img/common/parts/itemIcon/icon1.png" alt="おすすめ" class="iconN2">
							<?php endif;?>
						</div>
						<p class="name"><?php echo $data['new_item'][$i]['name'];?></p>
						<p class="price">
							<?php if (!isset($data['new_item'][$i]['hide_price_flg']) || !$data['new_item'][$i]['hide_price_flg']):?>
								<?php echo number_format(Func::get_item_value($data['new_item'][$i]));?>円
							<?php endif;?>
						</p>
					</a>
				</li>
			<?php endfor;?>
		</ul>
	</div>
	<div class="rank">
		<h2><img src="<?php echo $assets_path;?>/img/pages/index/rank.png" alt="ランキング" /></h2>
		<ul>
			<?php for ($i=0; $i<count($data['ranking']); $i++):?>
				<?php if (!is_null($data['ranking'][$i]) && $data['ranking'][$i]): ?>
					<li>
						<?php if (isset($data['ranking'][$i]['external_url']) && !empty($data['ranking'][$i]['external_url'])):?>
							<a href="<?php echo $data['ranking'][$i]['external_url'];?>" target="_blank">
						<?php else:?>
							<a href="<?php echo $base_url;?>/<?php echo $data['ranking'][$i]['name'];?>">
						<?php endif;?>
							<div class="itemPhoto">
								<img src="<?php echo $assets_path.'/img/item/'.$data['ranking'][$i]['id'];?>/120_120.jpg" alt="">
								<div class="rankIcon rank-<?php echo $i+1;?>"></div>
							</div>
							<p class="name"><?php echo $data['ranking'][$i]['name'];?></p>
							<p class="price">
								<?php if (!isset($data['ranking'][$i]['hide_price_flg']) || !$data['ranking'][$i]['hide_price_flg']):?>
									<?php echo number_format(Func::get_item_value($data['ranking'][$i]));?>円
								<?php endif;?>
							</p>
						</a>
					</li>
				<?php else:?>
					<li></li>
				<?php endif;?>

			<?php endfor;?>

			<?php if (false):?>
				集計中です
			<?php endif;?>

		</ul>
	</div>
	<div class="news">
		<h2><img src="<?php echo $assets_path;?>/img/pages/index/news.png" alt="ニュース" /></h2>
		<dl>
			<?php for ($i=0;$i<count($data['news']); $i++):?>
				<dt class="newsCategory<?php echo $data['news'][$i]['category']['id'];?>"><?php echo $data['news'][$i]['category']['name'];?></dt>
				<dd>
					<?php if(isset($data['news'][$i]['target_url']) && !empty($data['news'][$i]['target_url'])):?>
						<a href="<?php if ((bool)$data['news'][$i]['is_fqdn']): echo $data['news'][$i]['target_url']; else: echo $base_url.'/'.$data['news'][$i]['target_url']; endif;?>">
							<?php echo substr($data['news'][$i]['open_date'], 0, 10);?> <?php echo $data['news'][$i]['catch'];?>
						</a>
					<?php else:?>
							<?php echo substr($data['news'][$i]['open_date'], 0, 10);?> <?php echo $data['news'][$i]['catch'];?>
					<?php endif;?>
				</dd>
			<?php endfor;?>
		</dl>
	</div>
</div>