<div id="contents" class="reviewSingle">
	<h2>レビュー</h2>
	<form class="formWrap" method="post" action="<?php echo $base_url.'/user/review/form';?>">
		<dl class="form confirm">
			<dt>商品</dt>
			<dd class="itemImg">
				<img src="<?php echo $assets_path;?>/img/item/<?php if (isset($data['dt_purchase']['item_id'])) echo $data['dt_purchase']['item_id'];?>/50_50.jpg" alt="<?php if (isset($data['dt_purchase']['name'])) echo $data['dt_purchase']['name'];?>"><?php if (isset($data['dt_purchase']['name'])) echo $data['dt_purchase']['name'];?>
			</dd>
			<dt>おすすめ度</dt>
			<dd class="hart" id="hartEdit">
				<?php for ($i=0; $i<Controller_User_Review::EVALUATION_MAX+1; $i++):?>
					<label><input type="radio" name="evaluation" value="<?php echo $i;?>" <?php if (isset($data['evaluation']) && (Int)$data['evaluation'] === $i) echo 'checked';?>><?php echo $i;?></label>
				<?php endfor;?>
				<?php if (isset($val) && $val->error('evaluation')):?>
					<span class="errorText">
						<?php echo $val->error('evaluation');?>
					</span>
				<?php endif;?>
			</dd>
			<dt>ニックネーム</dt>
			<dd>
				<input class="size2" type="text" name="contributor" value="<?php if (isset($data['contributor'])) echo $data['contributor'];?>">
				<?php if (isset($val) && $val->error('contributor')):?>
					<span class="errorText">
						<?php echo $val->error('contributor');?>
					</span>
				<?php endif;?>
			</dd>
			<dt>レビューのタイトル</dt>
			<dd>
				<input class="size3" type="text" name="title" value="<?php if (isset($data['title'])) echo $data['title'];?>">
				<?php if (isset($val) && $val->error('title')):?>
					<span class="errorText">
						<?php echo $val->error('title');?>
					</span>
				<?php endif;?>
			</dd>
			<dt>
				レビューの内容
			</dt>
			<dd>
				<textarea name="comment"><?php if (isset($data['comment'])) echo $data['comment'];?></textarea>
				<?php if (isset($val) && $val->error('comment')):?>
					<span class="errorText">
						<?php echo $val->error('comment');?>
					</span>
				<?php endif;?>
			</dd>
		</dl>
		<div class="warning">
			<h3>必ずお読みください</h3>
			<p>
				他人の権利、利益、名誉等をを損ねたり、法令に違反する内容の投稿を禁止しております。<br />
				当ショップが不適切と判断した場合には、予告なく投稿を削除する場合がございます。
			</p>
			<p>
				いただいたコメントは、当サイト内に掲載させていただきますので、予めご了承ください。
			</p>
		</div>
		<input type="hidden" name="purchase_id" value="<?php if (isset($data['dt_purchase']['id'])) echo $data['dt_purchase']['id'];?>">
		<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
		<input type="submit" class="submitBtn" value="投稿">
	</form>
</div>