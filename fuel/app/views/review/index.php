<div id="contents" class="reviewList">
	<h2>レビューを書く</h2>
	<div class="textbox">
		いつも当店をご利用いただき、誠にありがとうございます。<br />
		お客様にお買い上げいただいた商品はいかがでしたか？<br />
		ただ今、お客様からの感想やご意見コメントを募集しております！<br />
		皆さまのご意見を元により良いショップづくりを心がけて参りますのでぜひご意見･ご感想をお願いします！
	</div>
	<ul class="list">
		<?php for($i=0; $i<count($data['record_data']); $i++):?>
			<li>
				<img src="<?php echo $assets_path.'/img/item/';?><?php if (isset($data['record_data'][$i]['name'])) echo $data['record_data'][$i]['item_id'];?>/50_50.jpg" alt="<?php if (isset($data['record_data'][$i]['name'])) echo $data['record_data'][$i]['name'];?>" />
				<div class="text">
					<h3><?php if (isset($data['record_data'][$i]['name'])) echo $data['record_data'][$i]['name'];?></h3>
					<div class="data"><?php if (isset($data['record_data'][$i]['created'])) echo $data['record_data'][$i]['created'];?></div>
				</div>
				<a href="<?php echo $base_url.'/user/review/form/';?><?php if (isset($data['record_data'][$i]['id'])) echo $data['record_data'][$i]['id'];?>" class="write">レビューを書く</a>
			</li>
		<?php endfor;?>
		<?php if (count($data['record_data']) == 0):?>
			<li class="blank">
				レビューを書けるのは発送済みの購入した商品のみとなります。
			</li>
		<?php endif;?>
	</ul>
	<?php if ((Int)$data['page_count']>0):?>
		<div class="pager" id="pager">
			<?php $center  = ceil($data['max_list_cnt']/2);//最大表示ページ中の中央を求める?>
			<ul>
				<li class="prev">
					<?php if ((Int)$data['page_no'] > 1):?>
						<a href="<?php echo $base_url.'/user/review/index/'.($data['page_no']-1)?>" class="btn"></a>
					<?php else: ?>
						<span class="btn"></span>
					<?php endif;?>
				</li>
				<?php $center  = ceil($data['max_list_cnt']/2);//最大表示ページ中の中央を求める?>
				<?php if ((Int)$data['page_no']<$center || (Int)$data['page_count'] ===2):?>
					<!-- 左より -->
					<?php for ($i=1; $i<=$data['max_list_cnt']; $i++):?>
						<?php if ($i > $data['page_count']) break;?>
						<?php if ($i === (Int)$data['page_no']):?>
							<li class="now"><?php echo $i;?></li>
						<?php else:?>
							<li><a href="<?php echo $base_url.'/user/review/index/'.$i?>"><?php echo $i;?></a></li>
						<?php endif;?>
					<?php endfor;?>
				<?php elseif (($data['page_count']-($center-1)) <$data['page_no']):?>
					<!-- 右より -->
					<?php $start = $data['page_count']-($data['max_list_cnt']-1); ?>
					<?php for ($i=0; $i<$data['max_list_cnt']; $i++):?>
						<?php if ($start+$i > $data['page_count']) break;?>
						<?php if ((Int)$start+$i === (Int)$data['page_no']):?>
							<li class="now"><?php echo $start+$i;?></li>
						<?php else:?>
							<li><a href="<?php echo $base_url.'/user/review/index/'.($start+$i)?>"><?php echo $start+$i;?></a></li>
						<?php endif;?>
					<?php endfor;?>
				<?php else:?>
					<!-- 通常 -->
					<?php $start = $data['page_no']+1 - $center;?>
					<?php for ($i=0; $i<$data['max_list_cnt']; $i++):?>
						<?php if ((Int)$start+$i > $data['page_count']) break;?>
						<?php if ((Int)$start+$i === (Int)$data['page_no']):?>
							<li class="now"><?php echo $start+$i;?></li>
						<?php else:?>
							<li><a href="<?php echo $base_url.'/user/review/index/'.($start+$i)?>"><?php echo $start+$i;?></a></li>
						<?php endif;?>
					<?php endfor;?>
				<?php endif;?>
				<li class="next">
					<?php if ((Int)$data['page_no']<(Int)$data['page_count']):?>
						<a href="<?php echo $base_url.'/user/review/index/'.($data['page_no']+1)?>" class="btn"></a>
					<?php else: ?>
						<span class="btn"></span>
					<?php endif;?>
				</li>
			</ul>
		</div>
	<?php endif;?>
</div>