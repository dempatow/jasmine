<div id="contents" class="orderHistorySingle">
	<h2>ご注文詳細</h2>
	<h3>ご注文内容</h3>
	<ul class="item">
		<?php $total_amount = 0;?>
		<?php for ($i=0; $i<count($data['dt_item']); $i++):?>
			<li>
				<img src="<?php echo $assets_path.'/img/item/'.$data['dt_item'][$i]['item_id'].'/50_50.jpg';?>" alt="<?php echo $data['dt_item'][$i]['name'];?>" class="itemImg">
				<div class="itemData">
					<h4><a href="<?php echo $base_url.'/'.$data['dt_item'][$i]['name'] ?>"><?php echo $data['dt_item'][$i]['name'];?></a></h4>
					<span class="price"><?php $unit_value = Func::get_item_value($data['dt_item'][$i]); echo number_format($unit_value);?></span>円
					<?php if (isset($data['dt_item'][$i]['special_select'])):?>
						<ul class="special_select">
							<?php for ($j=0; $j<count($data['dt_item'][$i]['special_select']); $j++):?>
								<li>
									・<?php echo $data['dt_item'][$i]['special_select'][$j]['key'];?>
									<?php echo $data['dt_item'][$i]['special_select'][$j]['value'];?>
									<?php echo $data['dt_item'][$i]['special_select'][$j]['count'];?>
								</li>
							<?php endfor;?>
						</ul>
					<?php endif;?>
					<div class="wrap">
						数量<span><?php echo $data['dt_item'][$i]['count'];?></span>
					</div>
				</div>
				<div class="subtotal">
					<span class="wrap">小計<br /><span class="price"><?php $total_amount+= $tmp_total =$unit_value*$data['dt_item'][$i]['count']; echo number_format($tmp_total);?></span>円</span>
				</div>
			</li>
		<?php endfor;?>
	</ul>
	<div id="total">
		<div class="otherPrice">
			送料 <?php echo $data['dt_purchase']['sending_amount'];?> 円
		</div>
		<?php if (isset($data['dt_purchase']['use_point']) && !empty($data['dt_purchase']['use_point'])):?>
			<div class="otherPrice">
				- <?php echo $data['dt_purchase']['use_point'];?> ポイント
			</div>
		<?php endif;?>
		<?php if (isset($data['dt_purchase']['other_amount']) && !empty($data['dt_purchase']['other_amount']) && (Int)$data['dt_purchase']['other_amount'] != 0):?>
			<div class="otherPrice">
				その他調整額 <?php echo $data['dt_purchase']['other_amount'];?> 円
			</div>
		<?php endif;?>
		合計
		<span class="price"><?php echo number_format($data['dt_purchase']['item_amount'] + $data['dt_purchase']['sending_amount'] - $data['dt_purchase']['use_point'] + $data['dt_purchase']['other_amount']);?></span>円
		<div class="point">
			このご注文で<span><?php echo $data['dt_purchase']['total_point'];?></span>ポイントを獲得しました。
		</div>
	</div>
	<div class="pay">
		<h3>ご注文情報</h3>
		<dl class="form">
			<dt>注文no</dt>
			<dd>
				<?php if(isset($data['dt_purchase']['id'])) echo $data['dt_purchase']['id'];?>
			</dd>
			<dt>お支払い方法</dt>
			<dd>
				<?php if(isset($data['dt_purchase']['payment_name'])) echo $data['dt_purchase']['payment_name'];?>
			</dd>
			<dt>お届け方法</dt>
			<dd>
				<?php if(isset($data['dt_purchase']['sending_name'])) echo $data['dt_purchase']['sending_name'];?>
			</dd>

			<?php if (isset($data['dt_purchase']['date_specified']) && !empty($data['dt_purchase']['date_specified'])):?>
				<dt>お届け希望日</dt>
				<dd><?php echo $data['dt_purchase']['date_specified'];?></dd>
			<?php endif;?>

			<?php if (isset($data['dt_purchase']['time_specified']) && !empty($data['dt_purchase']['time_specified'])):?>
				<dt>お届け希望時間帯</dt>
				<dd><?php echo $data['dt_purchase']['time_specified'];?></dd>
			<?php endif;?>

			<?php if (isset($data['dt_purchase']['status_cd'])):?>
				<dt>処理状態</dt>
				<dd><?php if (isset($data['dt_purchase']['status_name']) && !empty($data['dt_purchase']['status_name'])) echo $data['dt_purchase']['status_name'];?> <?php if ((Int)$data['dt_purchase']['status_cd'] === Purchase::ALREADY_SENDING && isset($data['dt_purchase']['tracking_url']) && !empty($data['dt_purchase']['tracking_url'])):?><a href="<?php echo $data['dt_purchase']['tracking_url'];?><?php if (isset($data['dt_purchase']['tracking_cd']) && !empty($data['dt_purchase']['tracking_cd']))echo $data['dt_purchase']['tracking_cd'];?>" class="check" >配送状況を確認</a> <?php if (isset($data['dt_purchase']['tracking_cd']) && !empty($data['dt_purchase']['tracking_cd']))echo '追跡コード['.(string)$data['dt_purchase']['tracking_cd'].']';?><?php endif;?></dd>
			<?php endif;?>

			<?php if (isset($data['dt_purchase']['free1']) && !empty($data['dt_purchase']['free1'])):?>
				<dt>通信欄</dt>
				<dd><?php echo nl2br($data['dt_purchase']['free1']);?></dd>
			<?php endif;?>
		</dl>
	</div>

	<div class="pay">
		<h3>ご購入者様情報</h3>
		<dl class="form">
			<?php if (isset($data['dt_address'][1]['first_name']) && !empty($data['dt_address'][1]['first_name']) && isset($data['dt_address'][1]['last_name']) && !empty($data['dt_address'][1]['last_name'])):?>
				<dt>お名前</dt>
				<dd><?php echo $data['dt_address'][1]['last_name'].' '.$data['dt_address'][1]['first_name'];?> 様</dd>
			<?php endif;?>

			<?php if (isset($data['dt_address'][1]['corporate_name']) && !empty($data['dt_address'][1]['corporate_name'])):?>
				<dt>法人名・団体名</dt>
				<dd><?php echo $data['dt_address'][1]['corporate_name'];?> 様</dd>
			<?php endif;?>

			<?php if (isset($data['dt_address'][1]['zip']) && !empty($data['dt_address'][1]['zip'])):?>
				<dt>郵便番号</dt>
				<dd><?php echo substr($data['dt_address'][1]['zip'],0,3).'-'.substr($data['dt_address'][1]['zip'],3,6);?></dd>
			<?php endif;?>

			<?php if (isset($data['dt_address'][1]['address1']) && !empty($data['dt_address'][1]['address1']) && isset($data['dt_address'][1]['address2']) && !empty($data['dt_address'][1]['address2'])):?>
				<dt>住所</dt>
				<dd><?php if (isset($data['dt_address'][1]['state_name']) && !empty($data['dt_address'][1]['state_name'])) echo $data['dt_address'][1]['state_name'];?><?php echo $data['dt_address'][1]['address1'];?><br /><?php echo $data['dt_address'][1]['address2'];?> <?php if (isset($data['dt_address'][1]['address3']) && !empty($data['dt_address'][1]['address3'])) echo $data['dt_address'][1]['address3'];?></dd>
			<?php endif;?>

			<?php if (isset($data['dt_address'][1]['tel1']) && !empty($data['dt_address'][1]['tel1']) && isset($data['dt_address'][1]['tel2']) && !empty($data['dt_address'][1]['tel2']) && isset($data['dt_address'][1]['tel3']) && !empty($data['dt_address'][1]['tel3'])):?>
				<dt>お電話番号</dt>
				<dd><?php echo $data['dt_address'][1]['tel1'].'-'.$data['dt_address'][1]['tel2'].'-'.$data['dt_address'][1]['tel3'];?></dd>
			<?php endif;?>

			<?php if (isset($data['dt_address'][1]['mail']) && !empty($data['dt_address'][1]['mail'])):?>
				<dt>メールアドレス</dt>
				<dd><?php echo $data['dt_address'][1]['mail'];?></dd>
			<?php endif;?>
		</dl>
	</div>
	<div class="destination">
		<h3>お届け先情報</h3>
		<dl class="form">
			<?php if (isset($data['dt_address'][2]['first_name']) && !empty($data['dt_address'][2]['first_name']) && isset($data['dt_address'][2]['last_name']) && !empty($data['dt_address'][1]['last_name'])):?>
				<dt>お名前</dt>
				<dd><?php echo $data['dt_address'][2]['last_name'].' '.$data['dt_address'][2]['first_name'];?> 様</dd>
			<?php endif;?>

			<?php if (isset($data['dt_address'][2]['corporate_name']) && !empty($data['dt_address'][2]['corporate_name'])):?>
				<dt>法人名・団体名</dt>
				<dd><?php echo $data['dt_address'][2]['corporate_name'];?> 様</dd>
			<?php endif;?>

			<?php if (isset($data['dt_address'][2]['zip']) && !empty($data['dt_address'][2]['zip'])):?>
				<dt>郵便番号</dt>
				<dd><?php echo substr($data['dt_address'][2]['zip'],0,3).'-'.substr($data['dt_address'][2]['zip'],3,6);?></dd>
			<?php endif;?>

			<?php if (isset($data['dt_address'][2]['address1']) && !empty($data['dt_address'][2]['address1']) && isset($data['dt_address'][2]['address2']) && !empty($data['dt_address'][2]['address2'])):?>
				<dt>住所</dt>
				<dd><?php if (isset($data['dt_address'][2]['state_name']) && !empty($data['dt_address'][2]['state_name'])) echo $data['dt_address'][2]['state_name'];?><?php echo $data['dt_address'][2]['address1'];?><br /><?php echo $data['dt_address'][2]['address2'];?> <?php if (isset($data['dt_address'][2]['address3']) && !empty($data['dt_address'][2]['address3'])) echo $data['dt_address'][2]['address3'];?></dd>
			<?php endif;?>

			<?php if (isset($data['dt_address'][2]['tel1']) && !empty($data['dt_address'][2]['tel1']) && isset($data['dt_address'][2]['tel2']) && !empty($data['dt_address'][2]['tel2']) && isset($data['dt_address'][2]['tel3']) && !empty($data['dt_address'][2]['tel3'])):?>
				<dt>お電話番号</dt>
				<dd><?php echo $data['dt_address'][2]['tel1'].'-'.$data['dt_address'][2]['tel2'].'-'.$data['dt_address'][2]['tel3'];?></dd>
			<?php endif;?>

			<?php if (isset($data['dt_address'][2]['mail']) && !empty($data['dt_address'][2]['mail'])):?>
				<dt>メールアドレス</dt>
				<dd><?php echo $data['dt_address'][2]['mail'];?></dd>
			<?php endif;?>
		</dl>
	</div>

	<?php if (isset($data['back_url'])):?>
	<a href="<?php echo $data['back_url'];?>" class="backBtn">前のページに戻る</a>
	<?php endif;?>

</div>