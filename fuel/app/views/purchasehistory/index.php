<div id="contents" class="orderHistory">
	<h2>注文履歴</h2>
	<form method="post" action="<?php echo $base_url?>/user/purchasehistory">
		<table>
			<tr>
				<th>処理状況</th>
				<td>
					<?php foreach ($data['status_radio_array'] as $key => $value):?>
						<label><input class="" type="radio" name="status_cd" value="<?php echo $value['value'];?>" <?php if (isset($value['checked'])) echo 'checked';?> ><?php echo $value['disp'];?></label>
					<?php endforeach;?>
				</td>
				<?php if (isset($data['max_year'])):?>
					<th>注文した年</th>
					<td class="selectWrap">
						<?php if (isset($data['min_year'])):?>
							<select name="selected_year">
								<option value="">年を指定しない</option>
								<?php for ($year=$data['max_year']; $year>=$data['min_year']; $year--):?>
									<option value="<?php echo $year;?>" <?php if(isset($data['selected_year']) && (Int)$data['selected_year'] === (Int)$year) echo 'selected';?>><?php echo $year;?>年</option>
								<?php endfor;?>
							</select>
						<?php else:?>
							<select>
								<option value="<?php echo $data['max_year'];?>"><?php echo $data['max_year'];?></option>
							</select>
						<?php endif;?>
					</td>
				<?php endif;?>
			</tr>
		</table>
		<input type="submit" value="絞り込み" name="serch" class="searchBtn">
	</form>

	<?php if (isset($data['record_data']) && !empty($data['record_data'])):?>
		<ul class="orderList">
			<li class="title">
				<div class="number">ご注文No</div>
				<div class="data">ご注文確定</div>
				<div class="price">合計金額</div>
				<div class="status">ステータス</div>
				<div class="more"></div>
			</li>
			<?php for ($i=0;$i<count($data['record_data']);$i++):?>
				<li>
					<div class="number"><span></span><?php if (isset($data['record_data'][$i]['id'])) echo $data['record_data'][$i]['id'];?>番</div>
					<div class="data"><span></span><?php if (isset($data['record_data'][$i]['created'])) echo $data['record_data'][$i]['created'] ?></div>
					<div class="price"><span><?php if (isset($data['record_data'][$i]['item_amount']) && isset($data['record_data'][$i]['sending_amount']) && isset($data['record_data'][$i]['use_point'])) echo number_format($data['record_data'][$i]['item_amount'] + $data['record_data'][$i]['sending_amount'] - $data['record_data'][$i]['use_point'] + $data['record_data'][$i]['other_amount'])?></span>円</div>
					<div class="status">
						<span><?php if (isset($data['record_data'][$i]['send_date'])) echo $data['record_data'][$i]['send_date'];?></span><?php if (isset($data['record_data'][$i]['status_cd']['name'])) echo $data['record_data'][$i]['status_cd']['name']?>
					</div>
					<div class="more">
						<a href="<?php echo $base_url.'/user/purchasehistory/detail/'.$data['record_data'][$i]['id']?><?php if (isset($query_string)) echo $query_string;?>	">詳しく見る</a>
					</div>
				</li>
			<?php endfor;?>
		</ul>
	<?php else:?>
		<ul class="orderList">
			<li class="blank">
				買い物履歴がありません
			</li>
		</ul>
	<?php endif;?>

	<?php if ((Int)$data['page_count']>0):?>
		<div class="pager" id="pager">
			<?php $center  = ceil($data['max_list_cnt']/2);//最大表示ページ中の中央を求める?>
			<ul>
				<li class="prev">
					<?php if ((Int)$data['page_no'] > 1):?>
						<a href="<?php echo $base_url.'/user/purchasehistory/index/'.($data['page_no']-1)?><?php if (isset($query_string)) echo $query_string;?>" class="btn"></a>
					<?php else: ?>
						<span class="btn"></span>
					<?php endif;?>
				</li>
				<?php $center  = ceil($data['max_list_cnt']/2);//最大表示ページ中の中央を求める?>
				<?php if ((Int)$data['page_no']<$center || (Int)$data['page_count'] ===2):?>
					<!-- 左より -->
					<?php for ($i=1; $i<=$data['max_list_cnt']; $i++):?>
						<?php if ($i > $data['page_count']) break;?>
						<?php if ($i === (Int)$data['page_no']):?>
							<li class="now"><?php echo $i;?></li>
						<?php else:?>
							<li><a href="<?php echo $base_url.'/user/purchasehistory/index/'.$i?><?php if (isset($query_string)) echo $query_string;?>"><?php echo $i;?></a></li>
						<?php endif;?>
					<?php endfor;?>
				<?php elseif (($data['page_count']-($center-1)) <$data['page_no']):?>
					<!-- 右より -->
					<?php $start = $data['page_count']-($data['max_list_cnt']-1); ?>
					<?php for ($i=0; $i<$data['max_list_cnt']; $i++):?>
						<?php if ($start+$i > $data['page_count']) break;?>
						<?php if ((Int)$start+$i === (Int)$data['page_no']):?>
							<li class="now"><?php echo $start+$i;?></li>
						<?php else:?>
							<li><a href="<?php echo $base_url.'/user/purchasehistory/index/'.($start+$i)?><?php if (isset($query_string)) echo $query_string;?>"><?php echo $start+$i;?></a></li>
						<?php endif;?>
					<?php endfor;?>
				<?php else:?>
					<!-- 通常 -->
					<?php $start = $data['page_no']+1 - $center;?>
					<?php for ($i=0; $i<$data['max_list_cnt']; $i++):?>
						<?php if ((Int)$start+$i > $data['page_count']) break;?>
						<?php if ((Int)$start+$i === (Int)$data['page_no']):?>
							<li class="now"><?php echo $start+$i;?></li>
						<?php else:?>
							<li><a href="<?php echo $base_url.'/user/purchasehistory/index/'.($start+$i)?><?php if (isset($query_string)) echo $query_string;?>"><?php echo $start+$i;?></a></li>
						<?php endif;?>
					<?php endfor;?>
				<?php endif;?>

				<li class="next">
					<?php if ((Int)$data['page_no']<(Int)$data['page_count']):?>
						<a href="<?php echo $base_url.'/user/purchasehistory/index/'.($data['page_no']+1)?><?php if (isset($query_string)) echo $query_string;?>" class="btn"></a>
					<?php else: ?>
						<span class="btn"></span>
					<?php endif;?>
				</li>

			</ul>
		</div>
	<?php endif;?>

</div>