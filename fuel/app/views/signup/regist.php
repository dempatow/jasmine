<div id="contents" class="signup">
	<?php if (isset($form_status) && (Int)$form_status === 1):?>
		<h2>登録が完了しました！</h2>

		<div class="textWrap">
			ご登録いただきありがとうございました、会員登録が完了いたしました！<br />
			右上のログインボタンからログインできます。<br />
			引き続きお買い物をお楽しみください。
		</div>

		<a href="<?php echo $base_url?>" class="goTopBtn">
			トップへ戻る
		</a>
	<?php elseif(isset($form_status) && (Int)$form_status === 2):?>
		<h2>登録情報の変更が完了しました！</h2>

		<div class="textWrap">
			ご登録いただきありがとうございました、会員情報の変更登録が完了いたしました！<br />
			引き続きお買い物をお楽しみください。
		</div>

		<a href="<?php echo $base_url.'/user/mypage'?>" class="goTopBtn">
			マイページへ戻る
		</a>
	<?php endif;?>
</div>