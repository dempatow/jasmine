<?php if ((isset($form_status) && (Int)$form_status === 1) || (isset($form_status) && (Int)$form_status ===2)):?>
	<div id="contents" class="signup">
<?php elseif (isset($form_status) && (Int)$form_status ===3):?>
	<div id="addressFormWrapper" <?php if(isset($open_address_form) && $open_address_form):?>style="display:block;"<?php endif;?>>
<?php endif;?>
	<?php if (isset($form_status) && (Int)$form_status === 1):?>
		<h2 class="pointTitle"><img src="<?php echo $assets_path;?>/img/pages/signup/pointTitle.png" alt="会員登録でお得がいっぱい！" /></h2>
		<ul class="point">
			<li>
				<h4>ポイントが貯まる！</h4>
				<p>商品代金の1％がポイントとして<br />還元！</p>
			</li>
			<li>
				<h4>ご注文がより簡単に！</h4>
				<p>各種情報が登録されるので、毎回入力しなくてOK！<br />お買い物がより簡単に。</p>
			</li>
			<li>
				<h4>会員限定セールにご招待！</h4>
				<p>会員限定のセールやお得な情報を<br />メールなどでお知らせします。</p>
			</li>
		</ul>
	<?php endif;?>

	<?php if (isset($form_status) && (Int)$form_status === 1):?>
		<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/user/signup/form">
	<?php elseif (isset($form_status) && (Int)$form_status === 2):?>
		<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/user/change/form">
	<?php elseif (isset($form_status) && (Int)$form_status === 3):?>
		<h2>
			お届け先を入力してください
		</h2>
		<form class="addressForm" method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/user/address/form">
	<?php endif;?>

		<?php if (isset($form_status) && (Int)$form_status === 1):?>
			<div class="policy">
				<h2>会員規約</h2>
				<div class="textBox">
					＜個人情報の取扱いについて＞<br /><br />

					<?php echo Config::get('custom_config.shop_name');?>の会員登録及び商品の注文にあたり、個人情報の取扱いに関する下記の事項に同意してください。<br /><br />

					1.当サービスを通じて登録されたお客様の個人情報は、エス・ピー広告株式会社（個人情報保護管理者　前田 敏行<img src="<?php echo $assets_path;?>/img/common/parts/mail2.png" alt="メールアドレスはスパム防止のため画像で挿入しております" class="addressImg">）が管理します。<br /><br />

					2.お客様の個人情報は以下の利用目的で利用します。なお、当サービスにおける個人情報の利用目的に変更が発生する場合は、あらかじめお客様に通知し、変更した利用目的について、再度同意を頂いた上で、利用をおこないます。<br />
					(1)注文した商品を発送するため。<br />
					(2)お客様がマイページにログインする際の認証のため。<br />
					(3)メールマガジンを発行するため。<br />
					(4)当社の各種サービスに関してご連絡するため。<br />
					(5)お客様のお問合せに対応するため。<br /><br />

					3. クレジットカード払いを選択されたお客様のクレジットカード情報は、直接決済代行会社（GMOイプシロン株式会社）に送信されるため、当社で取得、保有することはございません。<br /><br />

					4.お客様の個人情報は、お客様の同意なく第三者に提供されることはございません。<br /><br />

					5.当サービスの提供にあたり、データ保守、商品の配送等、個人情報の取り扱いが発生する業務の一部または全部を適切な契約を取り交わした企業に委託する場合がございます。<br /><br />

					6.個人情報の登録は、お客様の任意となりますが、「必須情報」と表示されている情報を登録いただけない場合は、会員登録や商品の注文等ができません。<br /><br />

					7.お客様は、登録したご自身の個人情報について、利用目的の通知・開示、訂正・追加・削除、利用停止・消去・第三者提供停止の請求をおこなうことができます。当該請求をおこなう場合は<img src="<?php echo $assets_path;?>/img/common/parts/mail2.png" alt="メールアドレスはスパム防止のため画像で挿入しております" class="addressImg">までご連絡ください。<br /><br />

					8.当サービスでは、コンテンツ並びにレイアウトの評価及び改善、不正アクセスの防止のため、cookieを通じてユーザーのアクセスログを取得、管理しています。<br /><br />

					以上<br /><br />
				</div>
				<div class="policyCheck">
					<label class="check">
						<input type="checkbox" name="consent" <?php if (isset($data['consent']))echo 'checked';?>>会員規約及び個人情報の取り扱いについて同意する
					</label>
					<?php if (isset($val) && $val->error('consent')) :?>
						<span class="errorText">
							<?php echo $val->error('consent');?>
						</span>
					<?php endif ?>
			</div>
			</div>
		<?php endif;?>

		<?php if (isset($form_status) && (Int)$form_status === 1):?>
			<h2 class="formTitle">登録フォーム入力</h2>
		<?php elseif (isset($form_status) && (Int)$form_status === 2):?>
			<h2 class="formTitle">お客様情報の変更</h2>
		<?php endif;?>

		<dl class="form">
			<dt>
				お名前<span class="requisite">必須</span>
			</dt>
			<dd>
				<input type="text" name="last_name" value="<?php if(isset($data['last_name'])): echo $data['last_name']; endif;?>" class="size2" placeholder="田中">
				<input type="text" name="first_name" value="<?php if(isset($data['first_name'])): echo $data['first_name']; endif;?>" class="size2" placeholder="太郎">
				<?php if (isset($val) && $val->error('last_name')) :?>
					<span class="errorText">
						<?php echo $val->error('last_name');?>
					</span>
				<?php endif ?>
				<?php if (isset($val) && $val->error('first_name')) :?>
					<span class="errorText">
						<?php echo $val->error('first_name');?>
					</span>
				<?php endif ?>
			</dd>
			<dt>
				お名前（フリガナ）
			</dt>
			<dd>
				<input type="text" name="last_name_kana" value="<?php if(isset($data['last_name_kana'])): echo $data['last_name_kana']; endif;?>" class="size2" placeholder="タナカ">
				<input type="text" name="first_name_kana" value="<?php if(isset($data['first_name_kana'])): echo $data['first_name_kana']; endif;?>" class="size2" placeholder="タロウ">
				<?php if (isset($val) && $val->error('last_name_kana')) :?>
					<span class="errorText">
						<?php echo $val->error('last_name_kana');?>
					</span>
				<?php endif ?>
				<?php if (isset($val) && $val->error('first_name_kana')) :?>
					<span class="errorText">
						<?php echo $val->error('first_name_kana');?>
					</span>
				<?php endif ?>
			</dd>

			<dt>法人名・団体名</dt>
			<dd>
				<input type="text" name="corporate_name" value="<?php if(isset($data['corporate_name'])): echo $data['corporate_name']; endif;?>" class="size3">
				<?php if (isset($val) && $val->error('corporate_name')) :?>
					<span class="errorText">
						<?php echo $val->error('corporate_name');?>
					</span>
				<?php endif ?>
			</dd>

			<dt>法人名・団体名（フリガナ）</dt>
			<dd>
				<input type="text" name="corporate_name_kana" value="<?php if(isset($data['corporate_name_kana'])): echo $data['corporate_name_kana']; endif;?>" class="size3">
				<?php if (isset($val) && $val->error('corporate_name_kana')) :?>
					<span class="errorText">
						<?php echo $val->error('corporate_name_kana');?>
					</span>
				<?php endif ?>
			</dd>

			<?php if (isset($form_status) && (Int)$form_status === 1):?>
				<dt>
					メールアドレス<span class="requisite">必須</span>
				</dt>
				<dd>
					<input type="text" name="mail_signup" value="<?php if(isset($data['mail_signup'])): echo $data['mail_signup']; endif;?>" class="size3">
				</dd>
				<dt>
					メールアドレス（確認）<span class="requisite">必須</span>
				</dt>
				<dd>
					<input type="text" name="mail_confirm" value="<?php if(isset($data['mail_confirm'])): echo $data['mail_confirm']; endif;?>" class="size3">
					<?php if (isset($val) && $val->error('mail_signup')) :?>
						<span class="errorText">
							<?php echo $val->error('mail_signup');?>
						</span>
					<?php endif ?>
				</dd>
			<?php elseif (isset($form_status) && (Int)$form_status === 2):?>
				<dt>
					メールアドレス<span class="requisite">必須</span>
				</dt>
				<dd>
					<input type="text" name="mail_change" value="<?php if(isset($data['mail_change'])): echo $data['mail_change']; endif;?>" class="size3">
				</dd>
				<dt>
					メールアドレス（確認）<span class="requisite">必須</span>
				</dt>
				<dd>
					<input type="text" name="mail_change_confirm" value="<?php if(isset($data['mail_change_confirm'])): echo $data['mail_change_confirm']; endif;?>" class="size3">
					<?php if (isset($val) && $val->error('mail_change')) :?>
						<span class="errorText">
							<?php echo $val->error('mail_change');?>
						</span>
					<?php endif ?>
				</dd>
			<?php endif;?>

			<?php if (isset($form_status) && (Int)$form_status === 1):?>
				<dt>パスワード<span class="requisite">必須</span></dt>
				<dd>
					<input type="password" name="passwd" maxlength="10" value="<?php if(isset($data['passwd'])): echo $data['passwd']; endif;?>" class="size2" placeholder="半角英数、6〜10文字" >
					<?php if (isset($val) && $val->error('passwd')) :?>
						<span class="errorText">
							<?php echo $val->error('passwd');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>パスワード（確認）<span class="requisite">必須</span></dt>
				<dd>
					<input type="password" name="passwd_confirm" maxlength="16" value="<?php if(isset($data['passwd_confirm'])): echo $data['passwd_confirm']; endif;?>" class="size2">
					<?php if (isset($val) && $val->error('passwd_confirm')) :?>
						<span class="errorText">
							<?php echo $val->error('passwd_confirm');?>
						</span>
					<?php endif ?>
				</dd>
			<?php endif;?>

			<?php if (isset($form_status) && (Int)$form_status != 3):?>
				<dt>
					生年月日
				</dt>
				<dd>
					<select name="birth_year" class="size1">
						<option value="">年</option>
						<?php for($i=1950; $i<date('Y'); $i++):?>
							<option value="<?php echo $i; ?>" <?php if (isset($data['birth_year']) && $i == $data['birth_year']): echo 'selected'; endif; ?>>
								<?php echo $i; ?>年
							</option>
						<?php endfor;?>
					</select>
					<select name="birth_month" class="size1">
						<option value="">月</option>
						<?php for($i=1; $i<13; $i++):?>
							<option value="<?php echo $i; ?>" <?php if (isset($data['birth_month']) && $i == $data['birth_month']): echo 'selected'; endif; ?>>
								<?php echo $i; ?>月
							</option>
						<?php endfor;?>
					</select>
					<select name="birth_day" class="size1">
						<option value="">日</option>
						<?php for($i=1; $i<32; $i++):?>
							<option value="<?php echo $i; ?>" <?php if (isset($data['birth_day']) && $i == $data['birth_day']): echo 'selected'; endif; ?>>
								<?php echo $i; ?>日
							</option>
						<?php endfor;?>
					</select>
					<?php if (isset($val) && $val->error('birth_year')) :?>
						<span class="errorText">
							<?php echo $val->error('birth_year');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('birth_month')) :?>
						<span class="errorText">
							<?php echo $val->error('birth_month');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('birth_day')) :?>
						<span class="errorText">
							<?php echo $val->error('birth_day');?>
						</span>
					<?php endif ?>
				</dd>
			<?php endif;?>

			<?php if (isset($form_status) && (Int)$form_status != 3):?>
				<dt>
					性別
				</dt>
				<dd>
					<?php for($i=1; $i<=count($data['mt']['sex_cd']); $i++):?>
					<label><input type="radio" name="sex_cd" value="<?php echo $data['mt']['sex_cd'][$i]['id'] ?>" <?php if (isset($data['sex_cd']) && $data['mt']['sex_cd'][$i]['id'] == $data['sex_cd']): echo 'checked'; endif; ?>><?php echo $data['mt']['sex_cd'][$i]['name'] ?></label>
					<?php endfor;?>
					<?php if (isset($val) && $val->error('sex_cd')) :?>
						<span class="errorText">
							<?php echo $val->error('sex_cd');?>
						</span>
					<?php endif ?>
				</dd>
			<?php endif;?>

			<dt>
				郵便番号<span class="requisite">必須</span>
			</dt>
			<dd>
				<input onkeyup="AjaxZip3.zip2addr(this,'','state_cd','address1');" name="zip" size="10" type="text" value="<?php if(isset($data['zip'])): echo $data['zip']; endif;?>" class="size2" placeholder="0000000" />
				<?php if (isset($val) && $val->error('zip')) :?>
					<span class="errorText">
						<?php echo $val->error('zip');?>
					</span>
				<?php endif ?>
			</dd>
			<dt>
				住所<span class="requisite">必須</span>
			</dt>
			<dd>
				<select name="state_cd" class="size2">
					<option value="">都道府県</option>
					<?php for($i=1; $i<=count($data['mt']['state_cd']); $i++):?>
						<option value="<?php echo $data['mt']['state_cd'][$i]['id'] ?>" <?php if (isset($data['state_cd']) && $data['mt']['state_cd'][$i]['id'] == $data['state_cd']): echo 'selected'; endif; ?>>
							<?php echo $data['mt']['state_cd'][$i]['name'] ?>
						</option>
					<?php endfor;?>
				</select>

				<input type="text" name="address1" value="<?php if(isset($data['address1'])): echo $data['address1']; endif;?>" class="size2" placeholder="市区郡"><br />

				<input type="text" name="address2" value="<?php if(isset($data['address2'])): echo $data['address2']; endif;?>" class="size3" placeholder="町村字番地"><br />

				<input type="text" name="address3" value="<?php if(isset($data['address3'])): echo $data['address3']; endif;?>" class="size3" placeholder="建物名">

				<?php if (isset($val) && $val->error('state_cd')) :?>
					<span class="errorText">
						<?php echo $val->error('state_cd');?>
					</span>
				<?php endif ?>
				<?php if (isset($val) && $val->error('address1')) :?>
					<span class="errorText">
					<?php echo $val->error('address1');?>
					</span>
				<?php endif ?>
				<?php if (isset($val) && $val->error('address2')) :?>
					<span class="errorText">
						<?php echo $val->error('address2');?>
					</span>
				<?php endif ?>
				<?php if (isset($val) && $val->error('address3')) :?>
					<span class="errorText">
						<?php echo $val->error('address3');?>
					</span>
				<?php endif ?>
			</dd>
			<dt>
				電話番号<span class="requisite">必須</span>
			</dt>
			<dd>
				<input type="text" name="tel1" value="<?php if(isset($data['tel1'])): echo $data['tel1']; endif;?>" class="size1" placeholder="000">
				<span class="hyphen">-</span>
				<input type="text" name="tel2" value="<?php if(isset($data['tel2'])): echo $data['tel2']; endif;?>" class="size1" placeholder="0000">
				<span class="hyphen">-</span>
				<input type="text" name="tel3" value="<?php if(isset($data['tel3'])): echo $data['tel3']; endif;?>" class="size1" placeholder="0000">
				<?php if (isset($val) && $val->error('tel1')) :?>
					<span class="errorText">
						<?php echo $val->error('tel1');?>
					</span>
				<?php endif ?>
				<?php if (isset($val) && $val->error('tel2')) :?>
					<span class="errorText">
						<?php echo $val->error('tel2');?>
					</span>
				<?php endif ?>
				<?php if (isset($val) && $val->error('tel3')) :?>
					<span class="errorText">
						<?php echo $val->error('tel3');?>
					</span>
				<?php endif ?>
			</dd>
			<dt>
				FAX
			</dt>
			<dd>
				<input type="text" name="fax1" value="<?php if(isset($data['fax1'])): echo $data['fax1']; endif;?>" class="size1" placeholder="000">
				<span class="hyphen">-</span>
				<input type="text" name="fax2" value="<?php if(isset($data['fax2'])): echo $data['fax2']; endif;?>" class="size1" placeholder="0000">
				<span class="hyphen">-</span>
				<input type="text" name="fax3" value="<?php if(isset($data['fax3'])): echo $data['fax3']; endif;?>" class="size1" placeholder="0000">
				<?php if (isset($val) && $val->error('fax1')) :?>
					<span class="errorText">
						<?php echo $val->error('fax1');?>
					</span>
				<?php endif ?>
				<?php if (isset($val) && $val->error('fax2')) :?>
					<span class="errorText">
						<?php echo $val->error('fax2');?>
					</span>
				<?php endif ?>
				<?php if (isset($val) && $val->error('fax3')) :?>
					<span class="errorText">
						<?php echo $val->error('fax3');?>
					</span>
				<?php endif ?>
			</dd>

			<?php if (isset($form_status) && (Int)$form_status != 3):?>
				<dt>
					職業
				</dt>
				<dd>
					<select name="job_cd" class="size2">
						<option value="">選んでください</option>
						<?php for($i=1; $i<=count($data['mt']['job_cd']); $i++):?>
							<option value="<?php echo $data['mt']['job_cd'][$i]['id'] ?>" <?php if (isset($data['job_cd']) && $data['mt']['job_cd'][$i]['id'] == $data['job_cd']): echo 'selected'; endif; ?>>
								<?php echo $data['mt']['job_cd'][$i]['name'] ?>
							</option>
						<?php endfor;?>
					</select>
					<?php if (isset($val) && $val->error('job_cd')) :?>
						<span class="errorText">
							<?php echo $val->error('job_cd');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
					メールマガジン発送の可否<span class="requisite">必須</span>
				</dt>
				<dd>
					<select name="magazine_cd" class="size2">
						<?php for($i=1; $i<=count($data['mt']['magazine_cd']); $i++):?>
							<option value="<?php echo $data['mt']['magazine_cd'][$i]['id'] ?>" <?php if (isset($data['magazine_cd']) && $data['mt']['magazine_cd'][$i]['id'] == $data['magazine_cd']): echo 'selected'; endif; ?>>
								<?php echo $data['mt']['magazine_cd'][$i]['name'] ?>
							</option>
						<?php endfor;?>
					</select>
					<?php if (isset($val) && $val->error('magazine_cd')) :?>
						<span class="errorText">
							<?php echo $val->error('magazine_cd');?>
						</span>
					<?php endif ?>
				</dd>
			<?php endif;?>
		</dl>
		<?php if (isset($form_status) && (Int)$form_status === 3):?>
			<input type="hidden" id="hiddenAddressId" name="address_id" value="<?php if(isset($data['address_id'])) echo $data['address_id'];?>">
		<?php endif;?>
		<?php if (isset($data['r_count']) && isset($data['reserve']) && isset($data['r_item_id'])):?>
			<input type="hidden" name="r_count" value="<?php echo $data['r_count'];?>"/>
			<input type="hidden" name="reserve" value="<?php echo $data['reserve'];?>"/>
			<input type="hidden" name="r_item_id" value="<?php echo $data['r_item_id'];?>"/>
		<?php endif;?>
		<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>" class="submitBtn btnStop">
		<input type="submit" name="送信" class="submitBtn btnStop" value="確認画面へ">
	</form>
</div>