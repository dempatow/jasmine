<!--これは登録完了画面です。-->
<div id="contents" class="regist">
	<?php if (isset($form_status) && (Int)$form_status === 1):?>
		<h2>登録が完了しました！</h2>
		<p>
			ご登録いただきありがとうございました、<br />
			会員登録が完了いたしました！<br />
			上のログインボタンからログインできます。<br />
			引き続きお買い物をお楽しみください。 
		</p> 
		<a href="/" class="btn">
			トップへ戻る
		</a>
	<?php elseif(isset($form_status) && (Int)$form_status === 2):?>
		<h2>登録情報の変更が完了しました！</h2>
		<p>
			ご登録いただきありがとうございました、<br />
			会員情報の変更登録が完了いたしました！<br />
			引き続きお買い物をお楽しみください。
		</p>

		<a href="<?php echo $base_url.'/user/mypage'?>" class="btn">
			マイページへ戻る
		</a>
		
	<?php endif;?>
</div>