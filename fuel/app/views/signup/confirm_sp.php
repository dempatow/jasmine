<div id="contents" class="confirm">
	<h2>入力内容をご確認ください</h2>
	<?php if (isset($form_status) && (Int)$form_status === 1):?>
		<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/user/signup/regist">
	<?php elseif (isset($form_status) && (Int)$form_status === 2):?>
		<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/user/change/regist">
	<?php elseif (isset($form_status) && (Int)$form_status === 3):?>
		<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/user/address/regist">
	<?php endif;?>

		<dl class="form">
			<dt>
				お名前<span class="requisite">必須</span>
			</dt>
			<dd>
				<?php if (isset($data['last_name'])): ?>
					<?php echo $data['last_name']; ?>
					<input type="hidden" name="last_name" value="<?php echo $data['last_name'];?>">
				<?php endif ?>

				<?php if (isset($data['first_name'])): ?>
					<?php echo $data['first_name']; ?>
					<input type="hidden" name="first_name" value="<?php echo $data['first_name'];?>">
				<?php endif ?>
			</dd>

			<dt>
				お名前（フリガナ）
			</dt>
			<dd>
				<?php if (isset($data['last_name_kana'])): ?>
					<?php echo $data['last_name_kana']; ?>
					<input type="hidden" name="last_name_kana" value="<?php echo $data['last_name_kana'];?>">
				<?php endif ?>

				<?php if (isset($data['first_name_kana'])): ?>
					<?php echo $data['first_name_kana']; ?>
					<input type="hidden" name="first_name_kana" value="<?php echo $data['first_name_kana'];?>">
				<?php endif ?>
			</dd>

			<dt>法人名・団体名</dt>
			<dd>
				<?php if (isset($data['corporate_name'])): ?>
					<?php echo $data['corporate_name']; ?>
					<input type="hidden" name="corporate_name" value="<?php echo $data['corporate_name'];?>">
				<?php endif ?>
			</dd>

			<dt>法人名・団体名（フリガナ）</dt>
			<dd>
				<?php if (isset($data['corporate_name_kana'])): ?>
					<?php echo $data['corporate_name_kana']; ?>
					<input type="hidden" name="corporate_name_kana" value="<?php echo $data['corporate_name_kana'];?>">
				<?php endif ?>
			</dd>

			<?php if (isset($form_status) && (Int)$form_status === 1):?>
				<dt>
					メールアドレス<span class="requisite">必須</span>
				</dt>
				<dd>
					<?php if (isset($data['mail_signup'], $data['mail_confirm'])): ?>
						<?php echo $data['mail_signup']; ?>
						<input type="hidden" name="mail_signup" value="<?php echo $data['mail_signup']; ?>">
						<input type="hidden" name="mail_confirm" value="<?php echo $data['mail_confirm']; ?>">
					<?php endif ?>
				</dd>
			<?php elseif (isset($form_status) && (Int)$form_status === 2):?>
				<dt>
					メールアドレス<span class="requisite">必須</span>
				</dt>
				<dd>
					<?php if (isset($data['mail_change'], $data['mail_change_confirm'])): ?>
						<?php echo $data['mail_change']; ?>
						<input type="hidden" name="mail_change" value="<?php echo $data['mail_change']; ?>">
						<input type="hidden" name="mail_change_confirm" value="<?php echo $data['mail_change_confirm']; ?>">
					<?php endif ?>
				</dd>
			<?php endif;?>

			<?php if (isset($form_status) && (Int)$form_status === 1):?>
				<dt>パスワード<span class="requisite">必須</span></dt>
				<dd>
					<?php if (isset($data['passwd_hide'], $data['passwd'], $data['passwd_confirm'])): ?>
						<?php echo $data['passwd_hide']; ?>
						<input type="hidden" name="passwd" value="<?php echo $data['passwd'];?>">
						<input type="hidden" name="passwd_confirm" value="<?php echo $data['passwd_confirm'];?>">
					<?php endif ?>
				</dd>
			<?php endif;?>

			<?php if (isset($form_status) && ((Int)$form_status === 1 || (Int)$form_status === 2)):?>
				<dt>
					生年月日
				</dt>
				<dd>
					<?php if (isset($data['birth_year']) && !empty($data['birth_year']) && isset($data['birth_month']) && !empty($data['birth_month']) && isset($data['birth_day']) && !empty($data['birth_day'])):?>
						<?php echo $data['birth_year']; ?>
						<input type="hidden" name="birth_year" value="<?php echo $data['birth_year'];?>">
						年
						<?php echo $data['birth_month']; ?>
						<input type="hidden" name="birth_month" value="<?php echo $data['birth_month'];?>">
						月
						<?php echo $data['birth_day']; ?>
						<input type="hidden" name="birth_day" value="<?php echo $data['birth_day'];?>">
						日
					<?php endif ?>
				</dd>

				<dt>
					性別
				</dt>
				<dd>
					<?php ?>
					<?php if (isset($data['sex_cd']) && $data['mt']['sex_cd'][$data['sex_cd']]): ?>
						<?php echo $data['mt']['sex_cd'][$data['sex_cd']]['name']; ?>
						<input type="hidden" name="sex_cd" value="<?php echo $data['sex_cd'];?>">
					<?php endif ?>
				</dd>
			<?php endif;?>


			<dt>
				郵便番号<span class="requisite">必須</span>
			</dt>
			<dd>
				<?php if (isset($data['zip'])): ?>
					<?php echo $data['zip']; ?>
					<input type="hidden" name="zip" value="<?php echo $data['zip'];?>">
				<?php endif ?>
			</dd>

			<dt>
				住所<span class="requisite">必須</span>
			</dt>
			<dd>
				<?php if (isset($data['state_cd']) && $data['mt']['state_cd'][$data['state_cd']]): ?>
					<?php echo $data['mt']['state_cd'][$data['state_cd']]['name']; ?>
					<input type="hidden" name="state_cd" value="<?php echo $data['state_cd'];?>">
				<?php endif ?>

				<?php if (isset($data['address1'])): ?>
					<?php echo $data['address1']; ?>
					<input type="hidden" name="address1" value="<?php echo $data['address1'];?>">
				<?php endif ?>
					<br />
				<?php if (isset($data['address2'])): ?>
					<?php echo $data['address2']; ?>
					<input type="hidden" name="address2" value="<?php echo $data['address2'];?>">
				<?php endif ?>

				<?php if (isset($data['address3'])): ?>
					<br />
					<?php echo $data['address3']; ?>
					<input type="hidden" name="address3" value="<?php echo $data['address3'];?>">
				<?php endif ?>
			</dd>
			<dt>
				電話番号<span class="requisite">必須</span>
			</dt>
			<dd>
				<?php if (isset($data['tel1'])): ?>
					<?php echo $data['tel1']; ?>
					<input type="hidden" name="tel1" value="<?php echo $data['tel1'];?>">
				<?php endif ?>

				<?php if (isset($data['tel2'])): ?>
					-
					<?php echo $data['tel2']; ?>
					<input type="hidden" name="tel2" value="<?php echo $data['tel2'];?>">
				<?php endif ?>

				<?php if (isset($data['tel3'])): ?>
					-
					<?php echo $data['tel3']; ?>
					<input type="hidden" name="tel3" value="<?php echo $data['tel3'];?>">
				<?php endif ?>
			</dd>

			<dt>
				FAX
			</dt>
			<dd>
				<?php if (isset($data['fax1']) && !empty($data['fax1']) && isset($data['fax2']) && !empty($data['fax2']) && isset($data['fax3']) && !empty($data['fax3'])):?>
					<?php echo $data['fax1']; ?>
					<input type="hidden" name="fax1" value="<?php echo $data['fax1'];?>">
					-
					<?php echo $data['fax2']; ?>
					<input type="hidden" name="fax2" value="<?php echo $data['fax2'];?>">
					-
					<?php echo $data['fax3']; ?>
					<input type="hidden" name="fax3" value="<?php echo $data['fax3'];?>">
				<?php endif;?>
			</dd>

			<?php if (isset($form_status) && ((Int)$form_status === 1 || (Int)$form_status === 2)):?>
				<dt>
					職業
				</dt>
				<dd>
					<?php if (isset($data['job_cd']) && isset($data['mt']['job_cd'][$data['job_cd']])): ?>
						<?php echo $data['mt']['job_cd'][$data['job_cd']]['name']; ?>
						<input type="hidden" name="job_cd" value="<?php echo $data['job_cd'];?>">
					<?php endif ?>
				</dd>

				<dt>
					メールマガジン利用の有無
				</dt>
				<dd>
					<?php if (isset($data['magazine_cd']) && $data['mt']['magazine_cd'][$data['magazine_cd']]): ?>
						<?php echo $data['mt']['magazine_cd'][$data['magazine_cd']]['name']; ?>
						<input type="hidden" name="magazine_cd" value="<?php echo $data['magazine_cd'];?>">
					<?php endif ?>
				</dd>
			<?php endif;?>

		</dl>
		<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">

		<?php if (isset($form_status) && (Int)$form_status === 3):?>
			<input type="hidden" id="hiddenAddressId" name="address_id" value="<?php if(isset($data['address_id'])) echo $data['address_id'];?>">
		<?php endif;?>

		<?php if (isset($data['r_count']) && isset($data['reserve']) && $data['r_item_id']):?>
			<input type="hidden" name="r_count"   value="<?php echo $data['r_count'];?>">
			<input type="hidden" name="reserve"   value="<?php echo $data['reserve'];?>">
			<input type="hidden" name="r_item_id" value="<?php echo $data['r_item_id'];?>">
		<?php endif;?>

		<input type="submit" name="regist_singnup" value="登録" class="submitBtn btnStop">
		<input type="submit" name="back_singnup" value="戻る" class="backBtn">
	</form>
</div>