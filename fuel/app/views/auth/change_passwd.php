<?php echo Session::get_flash('message'); ?>

<!-- パスワード変更パーツ -->
<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/auth/change_passwd">
	現在のパスワード
	<input type="text" name="old_passwd" value="<?php if(isset($data['current_passwd'])): echo $data['current_passwd']; endif;?>">
	<?php if (isset($val) && $val->error('current_passwd')):?>
		<?php echo $val->error('current_passwd');?>
	<?php endif;?>
	<br />
	新パスワード
	<input type="text" name="new_passwd" value="<?php if(isset($data['new_passwd'])): echo $data['new_passwd']; endif;?>">
	<br />
	新パスワード(確認用)
	<input type="text" name="new_passwd_confirm" value="<?php if(isset($data['new_passwd_confirm'])): echo $data['new_passwd_confirm']; endif;?>">
	<?php if ( isset($val) && $val->error('new_passwd')):?>
		<?php echo $val->error('new_passwd');?>
	<?php endif;?>
	<br />
	<br />
	<input type="submit" value="変更する">
	<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
</form>