<div id="contents" class="point">
	<h2>ポイント使用履歴</h2>
	<div class="currentPoint">
		現在の使用可能ポイント：<span><?php if (isset($current_point)) echo $current_point;?></span>pt
	</div>
	<ul class="list">
		<li class="title">
			<div class="data">日時</div>
			<div class="change">ポイント変動</div>
			<div class="note">備考</div>
		</li>
		<?php if (count($data['point_list'])>0):?>
			<?php for ($i=0; $i<count($data['point_list']); $i++):?>
				<li class="<?php 
				if (isset($data['point_list'][$i]['point']) && (Int)$data['point_list'][$i]['point'] > 0): echo 'plus' ; else: echo 'minus';endif;?>">
					<div class="data"><?php if (isset($data['point_list'][$i]['updated'])) echo $data['point_list'][$i]['updated'];?></div>
					<div class="change"><?php if (isset($data['point_list'][$i]['point'])) echo (String)$data['point_list'][$i]['point']?>pt</div>
					<div class="note"><?php if (isset($data['point_list'][$i]['reason']['name'])) echo $data['point_list'][$i]['reason']['name']?></div>
				</li>
			<?php endfor;?>
		<?php else:?>
				<li class="noData">ポイント履歴がありません。買い物をすることでポイントを獲得できます！</li>
		<?php endif;?>

	</ul>
	<div class="textbox">ポイントの有効期限は最後にポイントを獲得してから<?php echo Config::get('point_config.expiration')/12;?>年間です。</div>

	<?php if ((Int)$data['page_count']>0):?>
		<div class="pager" id="pager">
			<?php $center  = ceil($data['max_list_cnt']/2);//最大表示ページ中の中央を求める?>
			<ul>
				<li class="prev">
					<?php if ((Int)$data['page_no'] > 1):?>
						<a href="<?php echo $base_url.'/user/point/list/'.($data['page_no']-1)?>" class="btn"></a>
					<?php else: ?>
						<span class="btn"></span>
					<?php endif;?>
				</li>
				<?php $center  = ceil($data['max_list_cnt']/2);//最大表示ページ中の中央を求める?>
				<?php if ((Int)$data['page_no']<$center || (Int)$data['page_count'] ===2):?>
					<!-- 左より -->
					<?php for ($i=1; $i<=$data['max_list_cnt']; $i++):?>
						<?php if ($i > $data['page_count']) break;?>
						<?php if ($i === (Int)$data['page_no']):?>
							<li class="now"><?php echo $i;?></li>
						<?php else:?>
							<li><a href="<?php echo $base_url.'/user/point/list/'.$i?>"><?php echo $i;?></a></li>
						<?php endif;?>
					<?php endfor;?>
				<?php elseif (($data['page_count']-($center-1)) <$data['page_no']):?>
					<!-- 右より -->
					<?php $start = $data['page_count']-($data['max_list_cnt']-1); ?>
					<?php for ($i=0; $i<$data['max_list_cnt']; $i++):?>
						<?php if ($start+$i > $data['page_count']) break;?>
						<?php if ((Int)$start+$i === (Int)$data['page_no']):?>
							<li class="now"><?php echo $start+$i;?></li>
						<?php else:?>
							<li><a href="<?php echo $base_url.'/user/point/list/'.($start+$i)?>"><?php echo $start+$i;?></a></li>
						<?php endif;?>
					<?php endfor;?>
				<?php else:?>
					<!-- 通常 -->
					<?php $start = $data['page_no']+1 - $center;?>
					<?php for ($i=0; $i<$data['max_list_cnt']; $i++):?>
						<?php if ((Int)$start+$i > $data['page_count']) break;?>
						<?php if ((Int)$start+$i === (Int)$data['page_no']):?>
							<li class="now"><?php echo $start+$i;?></li>
						<?php else:?>
							<li><a href="<?php echo $base_url.'/user/point/list/'.($start+$i)?>"><?php echo $start+$i;?></a></li>
						<?php endif;?>
					<?php endfor;?>
				<?php endif;?>
				<li class="next">
					<?php if ((Int)$data['page_no']<(Int)$data['page_count']):?>
						<a href="<?php echo $base_url.'/user/point/list/'.($data['page_no']+1)?>" class="btn"></a>
					<?php else: ?>
						<span class="btn"></span>
					<?php endif;?>
				</li>
			</ul>
		</div>
	<?php endif;?>

</div>
