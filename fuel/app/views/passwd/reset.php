<div id="contents" class="password">

	<h2>パスワードのリセット</h2>
	<div class="textBox">
		パスワードをリセットし、再設定します。<br />
		再設定後のパスワードはご登録のメールアドレスに通知されます。<br />
	</div>
	<form method="post" action="<?php echo Config::get('custom_config.url.https_domain').'/user/passwd/reset'?>">
		<dl class="form min">
			<dt>
				メールアドレス<span class="requisite">必須</span>
			</dt>
			<dd>
				<input type="text" name="repwd_mail" value="<?php if (isset($data['repwd_mail'])) echo $data['repwd_mail'];?>">
				<span class="errorText">
					<?php if (isset($val) && $val->error('repwd_mail')) echo $val->error('repwd_mail');?>
				</span>
			</dd>
		</dl>
		<input type="submit" class="submitBtn" id="resetBtn">

		<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
	</form>
</div>