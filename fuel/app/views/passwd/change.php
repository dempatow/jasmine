<div id="contents" class="password">
	<h2>パスワードの変更</h2>
	<div class="textBox">
		パスワードの変更を行います。<br />
		現在のパスワードが不明な場合は、<a href="reset">パスワードの初期化</a>をご利用下さい。
	</div>
	<form method="post" action="<?php echo $base_url.'/user/passwd/change';?>">
		<dl class="form min">
			<dt>
				現在のパスワード<span class="requisite">必須</span>
			</dt>
			<dd>
				<input type="password" name="current_passwd" maxlength="10" value="" placeholder="" >
				<span class="errorText">
					<?php if (isset($val) && $val->error('current_passwd')) echo $val->error('current_passwd');?>
				</span>

			</dd>
		</dl>
		<dl class="form min">
			<dt>
				変更後のパスワード<span class="requisite">必須</span>
			</dt>
			<dd>
				<input type="password" name="new_passwd" maxlength="10" value="" placeholder="半角英数、6〜10文字" >
			</dd>
			<dt>
				変更後のパスワード（確認）<span class="requisite">必須</span>
			</dt>
			<dd>
				<input type="password" name="new_passwd_confirm" maxlength="10" value="" placeholder="" >
				<span class="errorText">
					<?php if (isset($val) && $val->error('new_passwd')) echo $val->error('new_passwd');?>
				</span>
			</dd>
		</dl>
		<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>" class="submitBtn btnStop">
		<input type="submit" value="変更する" class="submitBtn">
	</form>
</div>