<div id="contents" class="withdrawal">
	<h2>退会する</h2>
	<form method="post" action="<?php echo $base_url.'/user/withdrawal';?>">
		<div class="textBox">
			退会手続きに入ります。<br />
			下記注意事項をご確認の上、チェックマークを付け退会手続きを完了させてください。
		</div>
		<ul class="checkList">
			<li class="title">注意事項</li>
			<li>個人情報規定ならびに弊社のセキュリティーシステム上、退会後の会員登録内容の確認はできなくなります。</li>
			<li>お持ちのポイントは無効となります。</li>
			<li>新しい会員登録へのポイント移行はできません。</li>
			<li>
				既にご注文いただきました商品は、退会後も発送を行わせていただきます。<br />
				<span class="warningText">※配送先・お支払い方法・配達日時の変更はできなくなります。</span>
			</li>
			<li class="check">
				<label><input name="consent_withdrawal" type="checkbox" <?php if (isset($data['consent_withdrawal']) && !empty($data['consent_withdrawal'])) echo 'checked';?>/>すべての項目を確認しましたか？</label>
				<?php if (isset($val) && $val->error('consent_withdrawal')):?>
					<span class="errorText">
						<?php echo $val->error('consent_withdrawal');?>
					</span>
				<?php endif;?>
			</li>
		</ul>
		<dl class="password">
			<dt>
				パスワードを入力してください
			</dt>
			<dd>
				<input type="password" name="password_withdrawal" maxlength="10" value="">
				<?php if (isset($val) && $val->error('password_withdrawal')):?>
					<span class="errorText">
						<?php echo $val->error('password_withdrawal');?>
					</span>
				<?php endif;?>
			</dd>
		</dl>
		<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
		<input type="submit" class="submitBtn" value="退会する" />
	</form>
</div>