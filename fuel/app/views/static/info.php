<div id="contents" class="info">
	<div class="infoMessage">
		<h2 id="panpikids"><?php echo Config::get('custom_config.shop_name');?>へようこそ！</h2>
		<div class="logoImg">
			<img src="<?php echo $assets_path;?>/img/pages/info/infoLogo.png" alt="<?php echo Config::get('custom_config.shop_name');?>" />
		</div>
		<div class="textBox">
			<p>
				<?php echo Config::get('custom_config.shop_name');?>では「五感をみがく親子のコミュニケーション」を目指し、個性豊かなキャラクターたちとともに、子どもたちが楽しく遊びながら学べるアイテムを発信する通販サイトです。
			</p>
			<p>
				商品ごとに応じた豊富な配送方法やお支払い方法は代金引換・銀行振込・クレジットカード決済に対応。<br />
				しかも全品ポイント還元いたします。<br />
				<?php if (isset($free_sending)):?>
					配送料は<?php echo number_format($free_sending);?>円以上お買い上げで無料でお届けします。（外部サイトを除く）<br />
				<?php endif;?>
				どうぞお気軽に<?php echo Config::get('custom_config.shop_name');?>でのお買い物をお楽しみください！
			</p>
		</div>
	</div>
	<div class="step">
		<h2 id="process">ご注文の流れ</h2>
		<ul>
			<li>
				<img src="<?php echo $assets_path;?>/img/pages/info/step1.png" alt="">
				<p>
					商品を選んで「カート」ボタンをクリック!
				</p>
			</li>
			<li>
				<img src="<?php echo $assets_path;?>/img/pages/info/step2.png" alt="">
				<p>
					ログイン/会員登録しないで購入のどちらかで手続き開始。<br />
					会員の方はログインをしてお進みください。<br />
					ポイントはつきませんが「会員登録をしないで購入」でもご利用いただけます。<br />
				</p>
			</li>
			<li>
				<img src="<?php echo $assets_path;?>/img/pages/info/step3.png" alt="">
				<p>
					お支払い方法、お届け先を選択して注文手続きを確定。お支払い方法、お届け先、ポイントの使用を選び、注文完了まで手続きに沿ってすすめます。
				</p>
			</li>
			<li>
				<img src="<?php echo $assets_path;?>/img/pages/info/step4.png" alt="">
				<p>
					最短翌日に発送お知らせのメールが届きます。<br />
					発送準備が整いましたらお知らせメールをお送りし、最短で翌日にはお客様のもとにお届けいたします。
				</p>
			</li>
		</ul>
	</div>
	<div class="point">
		<h2 id="member" class="pointTitle"><img src="<?php echo $assets_path;?>/img/pages/signup/pointTitle.png" alt="会員登録でお得がいっぱい！" /></h2>
		<ul>
			<li>
				<h4>ポイントが貯まる！</h4>
				<p>商品代金の1％がポイントとして<br />還元！</p>
			</li>
			<li>
				<h4>ご注文がより簡単に！</h4>
				<p>各種情報が登録されるので、毎回入力しなくてOK！<br />お買い物がより簡単に。</p>
			</li>
			<li>
				<h4>会員限定セールにご招待！</h4>
				<p>会員限定のセールやお得な情報を<br />メールなどでお知らせします。</p>
			</li>
		</ul>
		<a href="<?php echo Config::get('custom_config.url.http_domain').'/user/signup/form';?>" class="btn">会員登録（無料）はこちらから</a>
	</div>
	<div class="pointGuide">
		<h2 id="point">ポイントについて</h2>
		<ul>
			<li>
				<div>
					商品をお買い上げ
				</div>
			</li>
			<li>
				<div>
					自動的に<br />
					ポイントが貯まる
				</div>
			</li>
			<li class="use">
				<div>
					<span>1</span>ポイント＝<span>1</span>円<br />
					でお買い物に使える！
				</div>
			</li>
		</ul>
		<p>
			・会員限定！商品代金から1％のポイントが付与されます。<br />
			・ポイントの有効期間は、最後にポイントを獲得してから<?php echo Config::get('point_config.expiration')/12;?>年間になります。<?php echo Config::get('point_config.expiration')/12;?>年が経過した場合、全てのポイントは消失します。<br />
			・やむを得ない事情でキャンセル処理が発生する場合は、キャンセル処理時にポイントが取り消されます。<br />
			・購入時に発行されるポイントは、仮ポイントのままです。発送完了後に獲得ポイントとなり、それよりご利用になれます。<br />
		</p>
	</div>
	<div class="payGuide">
		<h2 id="payment">お支払いについて</h2>
		<h3>クレジットカード</h3>
		<h4>【ご利用可能なクレジットカードの種類】</h4>
		<p>
			<img src="<?php echo $assets_path;?>/img/pages/info/card.png" alt="" /><br />
			VISA、MASTER、JCB、アメリカン・エキスプレス、ダイナース
		</p>
		<h4>【ご利用可能回数】</h4>
		<p>
			クレジットカードのご利用回数は、「一括」のみご利用いただけます。
		</p>
		<h4>【クレジットカード情報の保管について】</h4>
		<p>
			クレジットカード情報は、直接決済代行会社（GMOイプシロン株式会社）に送信されるため、<br />
			当社で取得、保有することはございません。
		</p>
		<h4>【お引き落とし日】</h4>
		<p>
			決済処理は商品発送の際におこなっており、引き落とし名義は「<?php echo Config::get('custom_config.shop_name');?>」と記載されます。<br />
			お引き落とし日時につきましては、ご利用のクレジットカードの締め日や契約内容により異なりますが、<br />
			通常では翌月または翌々月のご請求となります。<br />
			詳しくはご利用のクレジットカード会社に直接お問合せください。<br />
		</p>
		<h3>代金引換</h3>
		<p class="textBox left">
			商品をお届けの際にドライバーに現金にてお支払いください。<br />
			恐れ入りますが代引き手数料はお客様負担とさせていただきます。
		</p>
		<table class="priceList">
			<thead>
				<tr>
					<th colspan="2">
						代引き手数料（税込）
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>
						1万円未満
					</th>
					<td>
						324円
					</td>
				</tr>
				<tr>
					<th>
						1万円〜3万円未満
					</th>
					<td>
						432円
					</td>
				</tr>
				<tr>
					<th>
						3万円〜10万円未満
					</th>
					<td>
						648円
					</td>
				</tr>
				<tr><th>
						10万円〜30万円未満
					</th>
					<td>
						1,080円
					</td>
				</tr>
			</tbody>
		</table>
		<h3>銀行振込</h3>
		<p class="textBox left">
			恐れ入りますが振込手数料はお客様負担とさせていただきます。
		</p>
		<div class="account">
			<span>振込先</span>
			<p>
				三井住友銀行 銀座支店<br>普通7694562 エスピーコウコク
			</p>
		</div>
	</div>
	<div class="deliver">
		<h2 id="delivery">配送方法・配送料について</h2>
		<h3>宅急便</h3>
		<h4>【送料】</h4>
		<p>
			一律　650円（沖縄・離島を除く）<br />
			※複数地域へ発送される場合1送付先につき送料が発生いたします。
		</p>
		<h4>【到着までのリードタイム】</h4>
		<p>
			通常在庫がある商品につきましては、ご注文から3～5営業日で発送いたします。<br />
			一部出荷が遅れる商品に関してはメールにて納期のご連絡をいたします。<br />
			なお配送は日本国内のみとさせていただきます。<br />
			輸送状況の確認については下記郵便追跡サービスをご利用ください。<br />
			https://trackings.post.japanpost.jp/services/srv/search/
		</p>
		<h3>メール便</h3>
		<h4>【送料】</h4>
		<p>
			一律　200円（沖縄・離島を含む）
			※複数地域へ発送される場合1送付先につき送料が発生いたします。
		</p>

		<h4>【ご注意ください】</h4>
		<p>
			※メール便は一部商品のみ対応しております。<br />
			※お届け先の郵便受けに投函するお届け方法です。 ご不在でもお受け取り可能ですが、商品の保障は致しかねます。<br />
			※メール便を選択した場合は、お届け希望日時は指定できません。<br />
			※代金引換でのお支払いの場合はご利用いただけません。<br />
			※ご発送から到着まで3～4日かかります。<br />
			※メール便対象外の商品をご注文いただいた場合はメール便はご利用いただけません。
		</p>
	</div>
	<div class="cancel">
		<h2 id="cancel">返品・キャンセルについて</h2>
			<p>
				ご注文後の返品・キャンセルはお受けしておりません。<br />
				万一、商品に不備がございましたら速やかにご連絡ください。
			</p>
			<p class="contact">
				<span class="text">
					連絡先
				</span>
				<span class="address">
					<img src="<?php echo $assets_path;?>/img/common/parts/mail.png" alt="メールアドレスはスパム防止のため画像で挿入しております">
				</span>
				<span class="textBox">
				（土日祝の対応はお休みさせていただいております）
				</span>
			</p>
	</div>
	<div class="system">
		<h2 id="system">動作条件</h2>
			<p>
				<?php echo Config::get('custom_config.shop_name');?>は、以下の環境でご利用ください。<br />
				Internet Explorer（8以上）、google chrome（最新版）、firefox（最新版）、safari（7.0以上）<br />
				javascript、cookieを有効にしてください。<br />
				（windows/Macintosh共通）
			</p>
	</div>
</div>