<div class="itemsList">
	<h2>
		検索結果
		<span id="itemCnt">（該当アイテム<?php echo $data['item_cnt'];?>件）</span>
	</h2>
	<ul>
		<?php if (isset($data['page']['current_page_data'])):?>
			<?php for ($i=0; $i< count($data['page']['current_page_data']); $i++):?>
				<li>
					<?php if (isset($data['page']['current_page_data'][$i]['external_url']) && !empty($data['page']['current_page_data'][$i]['external_url'])):?>
						<a href="<?php echo $data['page']['current_page_data'][$i]['external_url'];?>" target="_blank">
					<?php else:?>
						<a href="<?php echo $base_url;?>/<?php echo $data['page']['current_page_data'][$i]['name'];?>">
					<?php endif;?>
						<div class="itemPhoto">
							<img src="<?php echo $assets_path ?>/img/item/<?php echo $data['page']['current_page_data'][$i]['id'];?>/120_120.jpg" alt="<?php if (isset($data['page']['current_page_data'][$i]['name'])) echo $data['page']['current_page_data'][$i]['name'];?>">

							<!-- 左上アイコン -->
							<?php if (isset($data['page']['current_page_data'][$i]['reco_flg'])):?>
								<!-- おすすめ -->
								<img src="<?php echo $assets_path;?>/img/common/parts/itemIcon/icon1.png" alt="" class="iconN2">
							<?php endif;?>

							<!-- 右下アイコン -->
							<?php if (Func::check_reserve_date($data['page']['current_page_data'][$i]['selling_date'])):?>
								<!-- 予約受付中 -->
								<img src="<?php echo $assets_path;?>/img/common/parts/itemIcon/icon6.png" alt="" class="iconN1">
							<?php elseif (isset($data['page']['current_page_data'][$i]['new_flg'])):?>
								<!-- 新着 -->
								<img src="<?php echo $assets_path;?>/img/common/parts/itemIcon/icon3.png" alt="" class="iconN1">
							<?php elseif(isset($data['page']['current_page_data'][$i]['discount'])):?>
								<!-- セール -->
								<img src="<?php echo $assets_path;?>/img/common/parts/itemIcon/icon5.png" alt="" class="iconN1">
							<?php elseif(isset($data['page']['current_page_data'][$i]['free_sending'])):?>
								<!-- 送料無料 -->
								<img src="<?php echo $assets_path;?>/img/common/parts/itemIcon/icon4.png" alt="" class="iconN1">
							<?php endif;?>
						</div>
						<p class="name"><?php echo $data['page']['current_page_data'][$i]['name'];?></p>
						<p class="price">
							<?php if (!isset($data['page']['current_page_data'][$i]['hide_price_flg']) || !$data['page']['current_page_data'][$i]['hide_price_flg']):?>
								<?php echo number_format(Func::get_item_value($data['page']['current_page_data'][$i]));?>円
							<?php endif;?>
						</p>
					</a>
				</li>
			<?php endfor;?>
		<?php else:?>
				<li>対象商品がありません</li>
		<?php endif;?>
	</ul>
</div>

<!--ページャー、可能ならページが複数にならない場合は非表示-->
<?php if ( $data['item_cnt'] > 0 && isset($data['page']['page_no']) && isset($data['page']['page_count'])):?>
	<div id ="pager" data-curent="<?php echo $data['page']['page_no'];?>" class="pager">
		<ul>
			<!-- 戻るボタン -->
			<li class="prev">
				<?php if ($data['page']['page_no']-1>0):?>
					<a class="btn pagerButton" data-button="prev"></a>
				<?php else:?>
					<span class="btn"></span>
				<?php endif;?>
			</li>

			<?php for($i=1; $i<=$data['page']['page_count']; $i++):?>
				<?php if ($i ===(Int)$data['page']['page_no']):?>
					<li class="now"><?php echo $i;?></li>
				<?php else:?>
					<li><a class="pagerButton" data-button="<?php echo $i;?>"><?php echo $i;?></a></li>
				<?php endif;?>
			<?php endfor;?>

			<!-- 進むボタン -->
			<li class="next">
				<?php if ((Int)$data['page']['page_no'] === (Int)$data['page']['page_count']):?>
				<span class="btn"></span>
				<?php else:?>
				<a class="btn pagerButton" data-button="next"></a>
				<?php endif;?>
			</li>
		</ul>
	</div>
	<!--ページャーここまで-->
<?php endif;?>