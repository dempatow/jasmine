<div id="contents" class="item" itemscope itemtype="http://schema.org/Product">
	<script>
		var imgUrl = "<?php echo $assets_path . '/img/item/' . $item['id']; ?>";
	</script>	
	
	
	<div id="itemPhoto" class="photo">
		<div id="photoImg">
			<img itemprop="image" src="<?php echo $assets_path . '/img/item/' . $item['id'];?>/600_600.jpg" data-imgno="1">
		</div>
		
		<?php if (isset($detail['img_cnt']) && (Int) $detail['img_cnt'] > 1): ?>
			<ul class="thumbnail">
				<?php for ($i = 1; $i <= $detail['img_cnt']; $i++): ?>
					<?php if ((Int) $detail['img_cnt'] === 1): ?>
						<li class="on" data-imgno="1"><img src="<?php echo $assets_path . '/img/item/' . $item['id']; ?>/50_50_itemphoto_1.jpg" alt="<?php echo $item['name']; ?>サムネイル1"></li>
					<?php else: ?>
						<li data-imgno="<?php echo $i; ?>"><img src="<?php echo $assets_path . '/img/item/' . $item['id']; ?>/50_50_itemphoto_<?php echo $i; ?>.jpg" alt="<?php echo $item['name']; ?>サムネイル<?php echo $i; ?>"></li>
					<?php endif; ?>
				<?php endfor; ?>
			</ul>
		<?php endif; ?>
	</div>
	
	<span class="itemDescription"><?php if (isset($detail['catch'])) echo $detail['catch']; ?></span>
	<h2 class="title" itemprop="name">
		<?php if (isset($item['name'])) echo $item['name']; ?>
	</h2>
	<div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<span itemprop="price"><?php if (isset($item['price'])) echo number_format(Func::get_item_value($item)); ?></span>円（税込）
	</div>
	
	<div class="rightCont">
		<?php if (isset($desc_img_1) && $desc_img_1): ?>
			<img src="<?php echo $assets_path . '/img/item/' . $item['id']; ?>/desc_sp.png" alt="panpikids ×　パクモグ" class="discImg">
		<?php endif; ?>
		<div class="itemText" itemprop="description">
			<?php if (isset($detail['description1'])) echo $detail['description1']; ?>
		</div>

		<!-- 特殊選択 -->
		<?php if (isset($item['special_select'])):?>
			<div class="optionWrap">
				<?php if (isset($item['special_select']['values'])):?>
				<select class="special-selectbox" name="<?php echo $item['special_select']['id'];?>">
						<option value="0"><?php echo $item['special_select']['name'];?></option>
						<?php for ($i=0; $i<count($item['special_select']['values']); $i++):?>
							<option value="<?php echo $item['special_select']['values'][$i]['value_id'];?>"><?php echo $item['special_select']['values'][$i]['value']; ?></option>
						<?php endfor;?>
					</select>
				<?php endif;?>
			</div>
		<?php endif;?>

		<div class="bottomCont">
		<div class="cart">
			<div>
				<!-- formの開始タグを分岐 -->
				<?php if (Func::check_reserve_date($item['selling_date'])):?>
					<form action="<?php echo Config::get('custom_config.url.https_domain');?>/cart/reserve" method="post">
				<?php else: ?>
					<form />
				<?php endif;?>

					<?php if ($item['stock'] > 0):?>
						<!--商品在庫がある場合-->
						<select name="count" id="count-<?php echo $item['id']; ?>">
							<?php for ($i = 1; $i <= $item['stock']; $i++): ?>
								<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
								<?php if ($i >= Model_Dt_Stock::DISP_MAX_COUNT) break; ?>
							<?php endfor; ?>
						</select>

						<!-- ボタン表示 -->
						<?php if (Func::check_reserve_date($item['selling_date'])):?>
							<!-- 予約受付時 -->
							<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
							<input type="hidden" name="item_id" value="<?php echo $item['id']; ?>">
							<span class="addReserveWrap"><input type="submit" class="addReserve" name="reserve" value="予約する"></span>
						<?php else:?>
							<!-- 通常販売時 -->
							<span class="addCart" data-item-id="<?php echo $item['id']; ?>">カートに入れる</span>
						<?php endif;?>

					<?php else: ?>
						<!--品切れ状態の表示-->
						<select disabled>
							<option value="0">0</option>
						</select>
						<span class="noItem">在庫切れ</span>
					<?php endif; ?>
				</form>
			</div>
		</div>
			
		<?php if (isset($review) && count($review) > 0): ?>
				<div class="review">
					<h3>この商品へのレビュー</h3>
				<?php for ($i = 0; $i < count($review); $i++): ?>
						<div class="wrap">
							<div class="data">
								<p class="title"><?php echo $review[$i]['title']; ?></p>
								<p class="name"><span><?php echo $review[$i]['contributor']; ?></span>様</p>
								<p class="ster n<?php echo $review[$i]['evaluation']; ?>">★</p>
							</div>
							<p class="text"><?php echo nl2br($review[$i]['comment']); ?></p>
						</div>
			<?php endfor; ?>
				</div>
		<?php endif; ?>

		<div class="sns">
			<ul>
				<li class="fb">
					<div id="fb-root"></div>
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0];
							if (d.getElementById(id))
								return;
							js = d.createElement(s);
							js.id = id;
							js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1";
							fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-like" data-width="150" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
				</li>
				<li class="tw">
					<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $base_url . '/item/index/' . $item['id']; ?>" data-count="none">Tweet</a>
					<script>!function(d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
							if (!d.getElementById(id)) {
								js = d.createElement(s);
								js.id = id;
								js.src = p + '://platform.twitter.com/widgets.js';
								fjs.parentNode.insertBefore(js, fjs);
							}
						}(document, 'script', 'twitter-wjs');</script>
				</li>
				<li class="li">
					<span>
						<script type="text/javascript" src="//media.line.me/js/line-button.js?v=20140127" ></script>
						<script type="text/javascript">
						new media_line_me.LineButton({"pc": false, "lang": "ja", "type": "a"});
						</script>
					</span>
				</li>
			</ul>
		</div>
	</div>
	</div>
	<div class="itemData">
		<?php if (isset($spec) && count($spec) > 0): ?>
			<table>
		<?php for ($i = 0; $i < count($spec); $i++): ?>
			<?php if (isset($spec[$i]['name']) && isset($spec[$i]['value'])): ?>
						<tr>
							<th><?php echo $spec[$i]['name']; ?></th>
							<td><?php echo $spec[$i]['value']; ?></td>
						</tr>
		<?php endif; ?>
	<?php endfor; ?>
			</table>
<?php endif; ?>
	</div>

</div>