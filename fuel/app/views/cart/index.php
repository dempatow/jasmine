<div id="contents" class="cart index">
	<h2>
		ショッピングカート
	</h2>

	<h3>
		ご注文内容
	</h3>
	<ul class="cartItem">
		<?php if (isset($cart_dt) && count($cart_dt)>0): ?>
			<!-- カートにある時 -->
			<?php for ($i=0; $i<count($cart_dt); $i++):?>
				<li class="item-<?php echo $cart_dt[$i]['id'];?>">
					<img src="<?php echo $assets_path;?>/img/item/<?php echo $cart_dt[$i]['id'];?>/50_50.jpg" alt="<?php echo $cart_dt[$i]['name'];?>" class="itemImg">
					<div class="itemData">
						<h4>
							<a href="<?php echo $base_url.'/item/index/'.$cart_dt[$i]['id'];?>">
								<?php echo $cart_dt[$i]['name'];?>
							</a>
						</h4>
						<span class="price"><?php echo Func::get_item_value($cart_dt[$i]);?></span>円（税込）

						<!-- 特殊選択 -->
						<br>
						<?php if (isset($cart_dt[$i]['special_select'])):?>
							<ul class="special_select">
								<?php for ($j=0; $j<count($cart_dt[$i]['special_select']); $j++):?>
								<li>
									・<?php echo $cart_dt[$i]['special_select'][$j]['key_name'];?>
									<?php echo $cart_dt[$i]['special_select'][$j]['value_name'];?>
										<?php echo $cart_dt[$i]['special_select'][$j]['count'];?>
								</li>
								<?php endfor;?>
							</ul>
						<?php endif;?>
						<!-- 特殊選択end -->

						<div class="wrap">
							数量
							<?php if (isset($cart_dt[$i]['is_reserve']) || isset($cart_dt[$i]['special_select'])):?>
								<!-- 予約商品がカートに含まれる場合 -->
								<?php echo $cart_dt[$i]['count'];?>
							<?php else:?>
								<!-- 通常表示 -->
								<input class="item-<?php echo $cart_dt[$i]['id'];?> item-count" type="text" value="<?php echo $cart_dt[$i]['count'];?>" maxlength="2" data-item-id="<?php echo $cart_dt[$i]['id'];?>">
							<?php endif;?>
						</div>
					</div>
					<div class="subtotal">
						<div class="wrap">
							小計<span id="price-<?php echo $cart_dt[$i]['id'];?>" class="price"><?php echo Func::get_item_value($cart_dt[$i])* $cart_dt[$i]['count'];?></span><span class="unit">円（税込）</span>
						</div>
					</div>
					<span class="del del_row" data-id="<?php echo $cart_dt[$i]['id'];?>">×</span>
				</li>
			<?php endfor;?>
		<?php else:?>
		<!-- カートにない時 -->
		<li class="blank">
				カートの中に商品がありません
			</li>
		<?php endif;?>
	</ul>

	<div id="total">
		<?php if (isset($total_info['total_price'])):?>
			合計 <span id="total_price" class="price"><?php echo $total_info['total_price'];?></span><span class="unit">円（税込）</span>
		<?php endif;?>

		<?php if (isset($total_info['free_sending']) && gettype($total_info['free_sending']) === "boolean"):?>
			<div id="free_sending" class="free">
				送料無料です！
			</div>
		<?php elseif (isset($total_info['free_sending'])):?>
			<div id="free_sending" class="free">
				あと<span><?php echo $total_info['free_sending'];?></span>円で送料無料です！
			</div>
		<?php endif;?>


		<?php if (isset($total_info['get_point'])):?>
			<div class="point">
				今回のお買い物で<span id="get_point"><?php echo $total_info['get_point'];?></span>ポイントが獲得できます！
			</div>
		<?php endif;?>
	</div>

	<div class="nextWrap">
		<?php if ($is_login):?>

			<?php if (isset($cart_dt) && count($cart_dt)>0):?>
				<a href="<?php echo $base_url.'/user/purchase/purchaser'?>" class="submitBtn next">購入する</a>
			<?php endif;?>

		<?php else: ?>
			<div class="back">
				<h3>ショッピングを続ける</h3>
				<a href="/">トップページへ</a>
			</div>

			<div class="login">
				<h3>会員ログイン</h3>
				<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/auth/login" autocomplete="off">
					<ul>
						<li>
							<input type="text" placeholder="メールアドレス" name="mail" value="<?php if(isset($login_parts['mail'])): echo $login_parts['mail']; endif;?>">
							<?php if (isset($login_parts['error']['mail'])):?>
								<p class="errorText"><?php echo $login_parts['error']['mail'];?></p>
							<?php endif;?>
						</li>
						<li>
							<input type="password" placeholder="パスワード" name="passwd_auth" value="<?php if(isset($login_parts['passwd_auth'])): echo $login_parts['passwd_auth']; endif;?>">
							<?php if (isset($login_parts['error']['passwd_auth'])):?>
								<p class="errorText"><?php echo $login_parts['error']['passwd_auth'];?></p>
							<?php endif;?>
							</li>
							<li>
								<label>
									<input type="checkbox" name="auto_login" value="1" <?php if(isset($login_parts['auto_login']) && $login_parts['auto_login'] == 1):?> checked <?php endif;?>>自動ログイン
								</label>
								<?php if (isset($login_parts['error']['auto_login'])):?>
									<p class="errorText"><?php echo $login_parts['error']['auto_login'];?></p>
								<?php endif;?>
							</li>
							<li>
								<input type="submit" value="ログイン">
								<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
							</li>
					</ul>
				</form>
			</div>
			<?php if (isset($cart_dt) && count($cart_dt)>0) :?>
				<div class="buy">
					<h3>購入する</h3>
					<a href="<?php echo Config::get('custom_config.url.https_domain').'/user/signup/form';?>">会員登録してから購入する</a>
					<a href="/user/purchase/purchaser">会員登録せずに購入する</a>
					<span class="text">※会員登録しないでご購入された場合、<br />ポイントは獲得できません。</span>
				</div>
			<?php endif;?>
		<?php endif;?>
	</div>
</div>