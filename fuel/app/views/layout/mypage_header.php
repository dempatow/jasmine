<ul class="topMinMenu">
	<li class="user"><a href="<?php echo $base_url . '/user/change/form'; ?>">登録情報</a></li>
	<li class="password"><a href="<?php echo $base_url . '/user/passwd/change'; ?>">パスワード</a></li>
	<li class="address"><a href="<?php echo $base_url . '/user/address/form'; ?>">アドレス帳</a></li>
	<li class="point"><a href="<?php echo $base_url . '/user/point/list'; ?>">ポイント確認</a></li>
	<li class="orderhistory"><a href="<?php echo $base_url . '/user/purchasehistory/index'; ?>">注文履歴</a></li>
	<li class="review"><a href="<?php echo $base_url . '/user/review'; ?>">レビュー</a></li>
	<li class="withdrawal"><a href="<?php echo $base_url . '/user/withdrawal'; ?>">退会手続</a></li>
</ul>