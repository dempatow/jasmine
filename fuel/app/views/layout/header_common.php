<div id="header">
	<div class="cont">
		<h1>
			<a href="<?php echo $base_url;?>"><img src="<?php echo $assets_path;?>/img/common/header/logo.png" alt="パンピキッズ"></a>
		</h1>
		<div class="userMenu<?php if ($is_login): echo ' loginMode'; endif;?>">
			<div class="login">
				<div class="welcamText">
					ようこそ<span id="userName"><?php if ($is_login): echo Session::get('login_info.last_name').' '.Session::get('login_info.first_name'); endif;?></span>様
				</div>
				<a href="<?php echo $base_url.'/user/mypage';?>" class="myPageBtn">マイページ</a>
				<a href="<?php echo Config::get('custom_config.url.https_domain');?>/auth/log_out" class="logoutBtn">ログアウト</a>
			</div>
			<div class="logout">
				<a href="/user/signup/form" class="membership">新規登録</a>
				<div id="loginBtn">会員ログイン</div>
				<div id="loginTooltip" <?php if (isset($login_parts['error']) && !isset($login_parts['overlay'])) echo 'style="display: block"';?>>
					<?php if (isset($login_redirect_url) && !empty($login_redirect_url)):?>
						<!-- ログインリダイレクトURLが指定してある時 -->
						<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/auth/login?rd=<?php echo $login_redirect_url;?>">
					<?php else:?>
						<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/auth/login">
					<?php endif;?>
						<ul>
							<li>
								<input type="text" placeholder="メールアドレス" name="mail" value="<?php if(isset($login_parts['mail'])): echo $login_parts['mail']; endif;?>" autocomplete="off" >
								<?php if (isset($login_parts['error']['mail'])):?>
									<p class="errorText"><?php echo $login_parts['error']['mail'];?></p>
								<?php endif;?>
							</li>
							<li>
								<input type="password" placeholder="パスワード" name="passwd_auth" value="<?php if(isset($login_parts['passwd_auth'])): echo $login_parts['passwd_auth']; endif;?>" autocomplete="off" maxlength="10">
								<?php if (isset($login_parts['error']['passwd_auth'])):?>
									<p class="errorText"><?php echo $login_parts['error']['passwd_auth'];?></p>
								<?php endif;?>
							</li>
							<li>
								<label>
									<input type="checkbox" name="auto_login" value="1" <?php if(isset($login_parts['auto_login']) && $login_parts['auto_login'] == 1):?> checked <?php endif;?>>自動ログイン
								</label>
								<?php if (isset($login_parts['error']['auto_login'])):?>
									<p class="errorText"><?php echo $login_parts['error']['auto_login'];?></p>
								<?php endif;?>
							</li>
							<li>
								<input type="submit" value="ログイン">
								<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
							</li>
							<li>
								<a href="/user/passwd/reset">パスワードを忘れた方はこちら</a>
							</li>
						</ul>
					</form>
				</div>
			</div>
		</div>
		<div class="cart">
			<h2>現在のカート</h2>
			<span id="cartAmount"><?php echo Func::get_cart_info_sum(Session::get('cart_dt', false))['total_amount']; ?></span>円
			<?php if (Func::get_cart_info_sum(Session::get('cart_dt', false))['total_count']>0):?>
				<a id="cartLink" href="<?php echo Config::get('custom_config.url.https_domain');?>/cart/index" class="check">カートの中身を見る</a>
			<?php endif;?>
		</div>
	</div>
</div>
