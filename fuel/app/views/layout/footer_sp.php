<footer>
	<div id="goTopBtn" class="goTopBtn">
		top
	</div>
	<ul>
		<li><a href="/info">ショッピングガイド</a></li>
		<li><a href="/contact">お問い合わせ</a></li>
		<li><a href="/law">特定商取引法</a></li>
		<li><a href="http://www.sp-k.co.jp/privacy/">プライバシーポリシー</a></li>
		<li><a href="http://www.panpikids.com/">PanpiKids公式サイト</a></li>
	</ul>
	<div class="copyRight">
		<img src="<?php echo $assets_path;?>/img/common/footer/copyRight.png" class="logo" width="184" />
	</div>
</footer>