<header>
	<h1>
		<a href="/"><img src="<?php echo $assets_path;?>/img/common/header/logo_sp.png" alt="panpikids" width="300"></a>
	</h1>
	<ul class="menu">
		<?php if ($is_login):?>
			<li class="logout"><a href="/auth/log_out">ログアウト</a></li>
		<?php else:?>
			<li class="membership"><a href="/user/signup/form">会員登録</a></li>
		<?php endif;?>
		<li class="cart">
			<a href="/cart/index">カート</a>
			<?php
				$cart = Session::get('cart_dt');
				if (!empty($cart)):
			?>
				<div class="icon">
					<?php echo count($cart);?>
				</div>
			<?php endif;?>
		</li>

		<?php if (!$is_login):?>
			<li class="login"><a href="#">ログイン</a></li>
		<?php else:?>
			<li class="myPage"><a href="<?php echo Config::get('custom_config.url.https_domain'); ?>/user/mypage">マイページ</a></li>
		<?php endif;?>

		<li class="search"><a href="<?php echo Config::get('custom_config.url.https_domain'); ?>/item/list">検索</a></li>
	</ul>
	<div class="loginWrap">
		<form method="post" action="<?php echo Config::get('custom_config.url.https_domain'); ?>/auth/login?overlay_login=true">
			<ul>
				<li>
					<input type="text" placeholder="メールアドレス" name="mail" value="<?php if(isset($login_parts['mail'])): echo $login_parts['mail']; endif;?>" autocomplete="off" >
					<?php if (isset($login_parts['error']['mail'])):?>
						<p class="errorText"><?php echo $login_parts['error']['mail'];?></p>
					<?php endif;?>
				</li>
				<li>
					<input type="password" placeholder="パスワード" name="passwd_auth" value="<?php if(isset($login_parts['passwd_auth'])): echo $login_parts['passwd_auth']; endif;?>" autocomplete="off" maxlength="10">
					<?php if (isset($login_parts['error']['passwd_auth'])):?>
						<p class="errorText"><?php echo $login_parts['error']['passwd_auth'];?></p>
					<?php endif;?>
				</li>
				<li>
					<input type="checkbox" id="headr_auto_login" name="auto_login" value="1" <?php if(isset($login_parts['auto_login']) && $login_parts['auto_login'] == 1):?> checked <?php endif;?>>
					<label for="headr_auto_login">
						自動ログインを設定する
					</label>
					<?php if (isset($login_parts['error']['auto_login'])):?>
						<p class="errorText"><?php echo $login_parts['error']['auto_login'];?></p>
					<?php endif;?>
				</li>
				<li>
					<input type="submit" value="ログイン">
					<input type="hidden" name="overlay" value="true">
					<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
				</li>
				<li>
					<a href="/user/signup/form" class="member">新規登録</a>
					<a href="/user/passwd/reset">パスワードを忘れた方はこちら</a>
				</li>
			</ul>
		</form>
		<span class="close">閉じる</span>
	</div>
</header>