<div id="mainMenu">
	<ul id="dropMenu" class="clearfix">
		<li class="allItem"><a href="<?php echo $base_url;?>/item/list" class="menuText">商品一覧</a>
		</li>
		<li class="category"><a class="menuText">カテゴリから探す</a>
			<div class="wrap">
				<div class="imgSpace"></div>
				<ul>
					<?php foreach (ViewHelper::get_side_menu_category_data() as $val): ?>
						<li><a href="<?php echo $base_url;?>/item/list?cate=<?php echo $val['id'];?>"><img src="<?php echo $assets_path ?>/img/category/<?php echo $val['id'];?>/135_45.png" alt="<?php echo $val['name'];?>" ></a></li>
					<?php endforeach;?>
				</ul>
			</div>
		</li>
		<li class="character"><a class="menuText">キャラクターで探す</a>
			<div class="wrap">
				<div class="imgSpace"></div>
				<ul>
					<?php foreach (ViewHelper::get_chara_data() as $val): ?>
						<li><a href="<?php echo $base_url;?>/item/list?char=<?php echo $val['id'];?>"><img src="<?php echo $assets_path;?>/img/character/<?php echo $val['id'];?>/100_100.png" alt="<?php echo $val['name'];?>"></a></li>
					<?php endforeach;?>
				</ul>
			</div>
		</li>
		<li class="info">
			<a href="<?php echo $base_url;?>/info" class="menuText">ご利用案内</a>
		</li>
		<li class="special"><a href="#" class="menuText">特集</a>
			<div class="wrap">
				<div class="imgSpace"></div>
				<ul>
					<li><a href="http://clubt.jp/shop/S0000046853.html" target="_blank"><img src="<?php echo $assets_path ?>/img/common/mainMenu/special/001_1.png" alt="" ></a></li>
					<li><a href="http://www.upsold.com/dshop/original/panpikids/" target="_blank"><img src="<?php echo $assets_path ?>/img/common/mainMenu/special/001_2.png" alt="" ></a></li>
					<li><a href="http://www.ttrinity.jp/shop/panpikids/" target="_blank"><img src="<?php echo $assets_path ?>/img/common/mainMenu/special/001_3.png" alt="" ></a></li>
					<li><a href="http://make.dmm.com/shop/18122" target="_blank"><img src="<?php echo $assets_path ?>/img/common/mainMenu/special/002.png" alt="" ></a></li>
				</ul>
			</div>
		</li>
	</ul>
</div>