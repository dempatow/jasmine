<?php if (isset($overlay_message) && !empty($overlay_message)):?>
	<div id="overlay" class="on message">
		<div class="window">
			<div class="messageText"><?php echo $overlay_message;?></div>
			<div class="loginForm">
				<form method="post" action="<?php echo Config::get('custom_config.url.https_domain'); ?>/auth/login?overlay_login=true">
					<h1>ログイン、または新規登録</h1>
					<ul>
						<li>
							<input type="text" placeholder="メールアドレス" name="mail" value="<?php if(isset($login_parts['mail'])): echo $login_parts['mail']; endif;?>" autocomplete="off" >
							<?php if (isset($login_parts['error']['mail'])):?>
								<p class="errorText"><?php echo $login_parts['error']['mail'];?></p>
							<?php endif;?>
						</li>
						<li>
							<input type="password" placeholder="パスワード" name="passwd_auth" value="<?php if(isset($login_parts['passwd_auth'])): echo $login_parts['passwd_auth']; endif;?>" autocomplete="off" maxlength="10">
							<?php if (isset($login_parts['error']['passwd_auth'])):?>
								<p class="errorText"><?php echo $login_parts['error']['passwd_auth'];?></p>
							<?php endif;?>
						</li>
						<li>
							<input type="checkbox" id="auto_login" name="auto_login" value="1" <?php if(isset($login_parts['auto_login']) && $login_parts['auto_login'] == 1):?> checked <?php endif;?>>
							<label for="auto_login">
								自動ログインを設定する
							</label>
							<?php if (isset($login_parts['error']['auto_login'])):?>
								<p class="errorText"><?php echo $login_parts['error']['auto_login'];?></p>
							<?php endif;?>
						</li>
						<li>
							<input type="submit" value="ログイン">
							<input type="hidden" name="overlay" value="true">
							<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
							<span class="close">閉じる</span>
						</li>
						<li>
							<a href="/user/signup/form" class="member">新規登録</a>
							<a href="/user/passwd/reset">パスワードを忘れた方はこちら</a>
						</li>
					</ul>
				</form>
			</div>
			<div id="questionYes">yes</div>
			<div id="questionNo">no</div>
			<span class="close">閉じる</span>
		</div>
	</div>
<?php elseif ((isset($overlay_login) && !empty($overlay_login)) || isset($login_parts['overlay'])):?>
	<div id="overlay" class="on login">
		<div class="window">
			<div class="messageText"></div>
			<div class="loginForm">
				<form method="post" action="<?php echo Config::get('custom_config.url.https_domain'); ?>/auth/login?overlay_login=true">
					<h1>ログイン、または新規登録</h1>
					<ul>
						<li>
							<input type="text" placeholder="メールアドレス" name="mail" value="<?php if(isset($login_parts['mail'])): echo $login_parts['mail']; endif;?>" autocomplete="off" >
							<?php if (isset($login_parts['error']['mail'])):?>
								<p class="errorText"><?php echo $login_parts['error']['mail'];?></p>
							<?php endif;?>
						</li>
						<li>
							<input type="password" placeholder="パスワード" name="passwd_auth" value="<?php if(isset($login_parts['passwd_auth'])): echo $login_parts['passwd_auth']; endif;?>" autocomplete="off" maxlength="10">
							<?php if (isset($login_parts['error']['passwd_auth'])):?>
								<p class="errorText"><?php echo $login_parts['error']['passwd_auth'];?></p>
							<?php endif;?>
						</li>
						<li>
							<input type="checkbox" id="auto_login" name="auto_login" value="1" <?php if(isset($login_parts['auto_login']) && $login_parts['auto_login'] == 1):?> checked <?php endif;?>>
							<label for="auto_login">
								自動ログインを設定する
							</label>
							<?php if (isset($login_parts['error']['auto_login'])):?>
								<p class="errorText"><?php echo $login_parts['error']['auto_login'];?></p>
							<?php endif;?>
						</li>
						<li>
							<input type="submit" value="ログイン">
							<input type="hidden" name="overlay" value="true">
							<span class="close">閉じる</span>
							<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
							<?php if (isset($login_parts['count'])):?>
								<input type="hidden" name="count"   value="<?php echo $login_parts['count'];?>">
							<?php endif;?>
							<?php if (isset($login_parts['item_id'])):?>
								<input type="hidden" name="item_id" value="<?php echo $login_parts['item_id'];?>">
							<?php endif;?>
						</li>
						<li>
							<?php if (isset($login_parts['count']) && $login_parts['item_id']):?>
								<a href="/user/signup/form?reserve=true&r_item_id=<?php echo $login_parts['item_id'];?>&r_count=<?php echo $login_parts['count'];?>" class="member">新規登録</a>
							<?php else:?>
								<a href="/user/signup/form" class="member">新規登録</a>
							<?php endif;?>
							<a href="/user/passwd/reset">パスワードを忘れた方はこちら</a>
						</li>
					</ul>
				</form>
			</div>
			<div id="questionYes">yes</div>
			<div id="questionNo">no</div>
			<span class="close">閉じる</span>
		</div>
	</div>
<?php else:?>
	<div id="overlay">
		<div class="window">
			<div class="messageText"></div>
			<div class="loginForm">
				<form method="post" action="<?php echo Config::get('custom_config.url.https_domain'); ?>/auth/login?overlay_login=true">
					<h1>ログイン、または新規登録</h1>
					<ul>
						<li>
							<input type="text" placeholder="メールアドレス" name="mail" value="<?php if(isset($login_parts['mail'])): echo $login_parts['mail']; endif;?>" autocomplete="off" >
							<?php if (isset($login_parts['error']['mail'])):?>
								<p class="errorText"><?php echo $login_parts['error']['mail'];?></p>
							<?php endif;?>
						</li>
						<li>
							<input type="password" placeholder="パスワード" name="passwd_auth" value="<?php if(isset($login_parts['passwd_auth'])): echo $login_parts['passwd_auth']; endif;?>" autocomplete="off" maxlength="10">
							<?php if (isset($login_parts['error']['passwd_auth'])):?>
								<p class="errorText"><?php echo $login_parts['error']['passwd_auth'];?></p>
							<?php endif;?>
						</li>
						<li>
							<input type="checkbox" id="auto_login" name="auto_login" value="1" <?php if(isset($login_parts['auto_login']) && $login_parts['auto_login'] == 1):?> checked <?php endif;?>>
							<label for="auto_login">
								自動ログインを設定する
							</label>
							<?php if (isset($login_parts['error']['auto_login'])):?>
								<p class="errorText"><?php echo $login_parts['error']['auto_login'];?></p>
							<?php endif;?>
						</li>
						<li>
							<input type="submit" value="ログイン">
							<input type="hidden" name="overlay" value="true">
							<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
							<span class="close">閉じる</span>
						</li>
						<li>
							<a href="/user/signup/form" class="member">新規登録</a>
							<a href="/user/passwd/reset">パスワードを忘れた方はこちら</a>
						</li>
					</ul>
				</form>
			</div>
			<div id="questionYes">yes</div>
			<div id="questionNo">no</div>
			<span class="close">閉じる</span>
		</div>
	</div>
<?php endif;?>