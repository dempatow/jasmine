<div id="footer">
	<div class="cont">
		<a href="http://www.panpikids.com/" class="goSite" target="_blank">
			<img src="<?php echo $assets_path;?>/img/common/footer/logoImg.png" class="logo" />
		</a>
		<div class="guid">
			<h2>ショッピングガイド</h2>
			<ul>
				<li><a href="<?php echo $base_url;?>/info#panpikids">パンピキッズとは</a></li>
				<li><a href="<?php echo $base_url;?>/info#payment">お支払い方法について</a></li>
				<li><a href="<?php echo $base_url;?>/info#process">ご注文の流れ</a></li>
				<li><a href="<?php echo $base_url;?>/info#delivery">配送方法・配送方法について</a></li>
				<li><a href="<?php echo $base_url;?>/info#member">新規会員と登録</a></li>
				<li><a href="<?php echo $base_url;?>/info#cancel">返品・キャンセルについて</a></li>
				<li><a href="<?php echo $base_url;?>/info#system">動作条件</a></li>
			</ul>
		</div>
		<div class="info">
			<h2>
				インフォメーション
			</h2>
			<ul>
				<li><a href="<?php echo $base_url;?>/law">特定商取引法</a></li>
				<li><a href="http://www.sp-k.co.jp/privacy/" target="_blank">プライバシーポリシー</a></li>
				<li><a href="<?php echo $base_url;?>/contact">お問い合わせ</a></li>
				<!--<li><a href="#">サイトマップ</a></li>-->
			</ul>
		</div>
	</div>
	<div class="copyRight">
<!--		<div class="link">
			<a href="http://www.sp-k.co.jp/privacy/">個人情報の取り扱いについて</a>
			<a href="<?php echo $base_url;?>/law">特定商取引法に関する表示</a>
		</div>-->
		<img src="<?php echo $assets_path;?>/img/common/footer/footerLogo.png" class="logo">
	</div>
</div>