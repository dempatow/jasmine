<div id="sideMenu">
	<div class="searchCharacter">
		<h2>キャラクターから探す</h2>
		<ul>
			<?php foreach (ViewHelper::get_chara_data() as $val): ?>
				<li><a href="<?php echo $base_url;?>/item/list?char=<?php echo $val['id'];?>"><img src="<?php echo $assets_path;?>/img/character/<?php echo $val['id'];?>/50_50.png" alt="<?php echo $val['name'];?>"></a></li>
			<?php endforeach;?>
		</ul>
	</div>
	<div class="searchCategory">
		<h2>カテゴリから探す</h2>
		<ul>
			<?php foreach (ViewHelper::get_side_menu_category_data() as $val):?>
				<li><a href="<?php echo $base_url;?>/item/list?cate=<?php echo $val['id'];?>"><?php echo $val['name'];?></a></li>
			<?php endforeach;?>
		</ul>
	</div>
	<div class="searchKeyword">
		<form method="post" action="<?php echo $base_url;?>/item/list">
			<input type="text" name="search_word" value="<?php if(isset($search_word)) echo $search_word;?>">
			<input type="submit" value="検索">
		</form>
	</div>
	<a href="<?php echo $base_url;?>/item/list?cate=1005" class="banner">
		<img src="<?php echo $assets_path;?>/img/common/sidemenu/banner_20141120.png" alt="">
	</a>
	<a href="<?php echo $base_url;?>/item/list?cate=1006" class="banner">
		<img src="<?php echo $assets_path;?>/img/common/sidemenu/banner_pakumogu.png" alt="最新コラボパペット、絶賛発売中！">
	</a>
	<div class="calenderWrapper">
		<div id="calender">
			<?php echo render('calender/calender');?>
		</div>
		<div class="description">
			<span>●</span>＝定休日
		</div>
	</div>
	<div class="twitter">
		<a class="twitter-timeline"  href="https://twitter.com/search?q=from%3Apanpikids+OR+from%3Apanpikids_ku"  height="150px" width="178px" data-widget-id="479576791078535168" data-chrome="noheader nofooter noborders transparent">from:panpikids OR from:panpikids_ku に関するツイート</a>
		<script>!function(d,s,id){
			var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
			if(!d.getElementById(id)){js=d.createElement(s);
				js.id=id;js.src=p+"://platform.twitter.com/widgets.js";
				fjs.parentNode.insertBefore(js,fjs);}
			}
			(document,"script","twitter-wjs");
		</script>
	</div>

</div>