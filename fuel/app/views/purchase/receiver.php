<div id="contents" class="receiver">

	<?php if ($is_login):?>
		<h2>お届け先の設定</h2>
		<div class="destinationList">
			<ul>
				<?php if (count($data['address_data'])>0): ?>
					<?php for ($i=0; $i<count($data['address_data']); $i++):?>
						<li>
							<div class="name">
								<span><?php if(isset($data['address_data'][$i]['corporate_name'])) echo $data['address_data'][$i]['corporate_name']?></span>
								<h3><?php if(isset($data['address_data'][$i]['last_name']) && isset($data['address_data'][$i]['first_name'])) echo $data['address_data'][$i]['last_name'].''.$data['address_data'][$i]['first_name']?></h3>
							</div>
							<div class="data">
								<span class="zip"><?php if (isset($data['address_data'][$i]['zip'])) echo substr($data['address_data'][$i]['zip'], 0, 3).'-'.substr($data['address_data'][$i]['zip'], 3, 4);?></span>
								<span class="address">
									<?php if (isset($data['address_data'][$i]['state_cd']) && isset($data['mt']['state_cd'][$data['address_data'][$i]['state_cd']])) echo $data['mt']['state_cd'][$data['address_data'][$i]['state_cd']]['name']?>
									<?php if (isset($data['address_data'][$i]['address1'])) echo $data['address_data'][$i]['address1'];?><br />
									<?php if (isset($data['address_data'][$i]['address2'])) echo $data['address_data'][$i]['address2'];?>
									<?php if (isset($data['address_data'][$i]['address3'])) echo $data['address_data'][$i]['address3'];?>
								</span>
							</div>
							<div class="edit">
								<span class="editBtn" data-id="<?php if (isset($data['address_data'][$i]['id'])) echo $data['address_data'][$i]['id'];?>">使用する</span>
							</div>
						</li>
					<?php endfor; ?>
				<?php else:?>
						<li>登録済み住所がありません</li>
				<?php endif;?>
			</ul>
		</div>
	<?php endif;?>
	<div class="editForm">
		<h2>
			お届け先の情報を入力してください
		</h2>
		<form class="formWrap" method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/user/purchase/receiver">
			<dl class="form">
				<dt>
				お名前<span class="requisite">必須</span>
				</dt>
				<dd>
					<input type="text" name="last_name" value="<?php if(isset($data['last_name'])): echo $data['last_name']; endif;?>" class="size2" placeholder="田中">
					<input type="text" name="first_name" value="<?php if(isset($data['first_name'])): echo $data['first_name']; endif;?>" class="size2" placeholder="太郎">
					<?php if (isset($val) && $val->error('last_name')) :?>
						<span class="errorText">
							<?php echo $val->error('last_name');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('first_name')) :?>
						<span class="errorText">
							<?php echo $val->error('first_name');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
				お名前（フリガナ）
				</dt>
				<dd>
					<input type="text" name="last_name_kana" value="<?php if(isset($data['last_name_kana'])): echo $data['last_name_kana']; endif;?>" class="size2" placeholder="タナカ">
					<input type="text" name="first_name_kana" value="<?php if(isset($data['first_name_kana'])): echo $data['first_name_kana']; endif;?>" class="size2" placeholder="タロウ">
					<?php if (isset($val) && $val->error('last_name_kana')) :?>
						<span class="errorText">
							<?php echo $val->error('last_name_kana');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('first_name_kana')) :?>
						<span class="errorText">
							<?php echo $val->error('first_name_kana');?>
						</span>
					<?php endif ?>
				</dd>

				<dt>法人名・団体名</dt>
				<dd>
					<input type="text" name="corporate_name" value="<?php if(isset($data['corporate_name'])): echo $data['corporate_name']; endif;?>" class="size3">
					<?php if (isset($val) && $val->error('corporate_name')) :?>
						<span class="errorText">
							<?php echo $val->error('corporate_name');?>
						</span>
					<?php endif ?>
				</dd>

				<dt>法人名・団体名（フリガナ）</dt>
				<dd>
					<input type="text" name="corporate_name_kana" value="<?php if(isset($data['corporate_name_kana'])): echo $data['corporate_name_kana']; endif;?>" class="size3">
					<?php if (isset($val) && $val->error('corporate_name_kana')) :?>
						<span class="errorText">
							<?php echo $val->error('corporate_name_kana');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
				郵便番号<span class="requisite">必須</span>
				</dt>
				<dd>
					<input onkeyup="AjaxZip3.zip2addr(this,'','state_cd','address1');" name="zip" size="10" type="text" value="<?php if(isset($data['zip'])): echo $data['zip']; endif;?>" class="size2" placeholder="0000000" maxlength="7"/>
					<?php if (isset($val) && $val->error('zip')) :?>
						<span class="errorText">
							<?php echo $val->error('zip');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
				住所<span class="requisite">必須</span>
				</dt>
				<dd>
					<select name="state_cd" class="size2">
						<option>選んでください</option>
						<?php for($i=1; $i<=count($data['mt']['state_cd']); $i++):?>
							<option value="<?php echo $data['mt']['state_cd'][$i]['id'] ?>" <?php if (isset($data['state_cd']) && $data['mt']['state_cd'][$i]['id'] == $data['state_cd']): echo 'selected'; endif; ?>>
								<?php echo $data['mt']['state_cd'][$i]['name'] ?>
							</option>
						<?php endfor;?>
					</select>
					<input type="text" name="address1" value="<?php if(isset($data['address1'])): echo $data['address1']; endif;?>" class="size2" placeholder="市区郡">
					<br />
					<input type="text" name="address2" value="<?php if(isset($data['address2'])): echo $data['address2']; endif;?>" class="size3" placeholder="町村字番地">
					<br />
					<input type="text" name="address3" value="<?php if(isset($data['address3'])): echo $data['address3']; endif;?>" class="size3" placeholder="建物名">					<?php if (isset($val) && $val->error('address1')) :?>
						<span class="errorText">
							<?php echo $val->error('address1');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('state_cd')) :?>
						<span class="errorText">
							<?php echo $val->error('state_cd');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('address2')) :?>
						<span class="errorText">
							<?php echo $val->error('address2');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('address3')) :?>
							<span class="errorText">
								<?php echo $val->error('address3');?>
							</span>
					<?php endif ?>
				</dd>
				<dt>
				電話番号<span class="requisite">必須</span>
				</dt>
				<dd>
					<input type="text" name="tel1" value="<?php if(isset($data['tel1'])): echo $data['tel1']; endif;?>" class="size1" placeholder="000">
					<span class="hyphen">-</span>>
					<input type="text" name="tel2" value="<?php if(isset($data['tel2'])): echo $data['tel2']; endif;?>" class="size1" placeholder="0000">
					<span class="hyphen">-</span>
					<input type="text" name="tel3" value="<?php if(isset($data['tel3'])): echo $data['tel3']; endif;?>" class="size1" placeholder="0000">
					<?php if (isset($val) && $val->error('tel1')) :?>
						<span class="errorText">
							<?php echo $val->error('tel1');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
				FAX
				</dt>
				<dd>
					<input type="text" name="fax1" value="<?php if(isset($data['fax1'])): echo $data['fax1']; endif;?>" class="size1" placeholder="000">
					<span class="hyphen">-</span>>
					<input type="text" name="fax2" value="<?php if(isset($data['fax2'])): echo $data['fax2']; endif;?>" class="size1" placeholder="0000">
					<span class="hyphen">-</span>
					<input type="text" name="fax3" value="<?php if(isset($data['fax3'])): echo $data['fax3']; endif;?>" class="size1" placeholder="0000">
					<?php if (isset($val) && $val->error('fax1')) :?>
						<span class="errorText">
							<?php echo $val->error('fax1');?>
						</span>
					<?php endif ?>
				</dd>
			</dl>
			<div class="clearWrap">
				<span id="clearForm">formをクリアする</span>
			</div>
			<input type="submit" value="次へ" name="check_receiver" class="submitBtn overlay">
			<input type="submit" name="back" value='戻る' class="backBtn overlay">
			<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
		</form>
	</div>
</div>