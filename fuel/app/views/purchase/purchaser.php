<div id="contents" class="purchaser">
	<!--非会員のみの表示-->
	<?php if (!$is_login):?>
		<div class="warning">
			<h2>「会員登録をしないで購入」では、ポイント還元はございません。</h2>
			<div class="textWrap">
				今回のお買い物で取得できるポイントはこのまま消えてしまいます。<br />
				会員登録をして頂ければ、「1ポイント=1円」として次回以降のお買い物の際にお使い頂けます！（一部商品を除く）
				<a href="<?php echo $base_url;?>/user/signup/form">会員登録してから購入</a>
			</div>
		</div>
	<?php endif;?>
	<!--/ここまで-->
	<h2>ご注文内容</h2>
	<ul class="cartItem">
		<?php $total_value = 0;?>
		<?php for ($i=0; $i<count($data['cart_dt']); $i++):?>
			<?php $unit_value = Func::get_item_value($data['cart_dt'][$i]); ?>
			<li>
				<img src="<?php echo $base_url.'/assets/img/item/'.$data['cart_dt'][$i]['id'];?>/50_50.jpg" alt="<?php echo $data['cart_dt'][$i]['name'];?>" class="itemImg">
				<div class="itemData">
					<h4>
						<a href="<?php echo $base_url.'/item/index/'.$data['cart_dt'][$i]['id'];?>">
							<?php echo $data['cart_dt'][$i]['name'];?>
						</a>
					</h4>
					<span class="price"><?php echo $unit_value;?></span>円

					<!-- 特殊選択 -->
						<br>
						<?php if (isset($data['cart_dt'][$i]['special_select'])):?>
						<ul class="special_select">
							<?php for ($j=0; $j<count($data['cart_dt'][$i]['special_select']); $j++):?>
							<li>
								・<?php echo $data['cart_dt'][$i]['special_select'][$j]['key_name'];?>
								<?php echo $data['cart_dt'][$i]['special_select'][$j]['value_name'];?>
								[<?php echo $data['cart_dt'][$i]['special_select'][$j]['count'];?>]
							</li>
							<?php endfor;?>
						</ul>
						<?php endif;?>
					<!-- 特殊選択end -->

					<div class="wrap">
						数量
						[<span class='q'><?php echo $data['cart_dt'][$i]['count'];?></span>]
					</div>
				</div>
				<div class="subtotal">
					<div class="wrap">
						小計<span class="price"><?php $total_value += $sub_total = $unit_value*$data['cart_dt'][$i]['count']; echo $sub_total;?></span><span class="unit">円（税込）</span>
					</div>
				</div>
			</li>
		<?php endfor;?>
	</ul>
	<div id="total">
		合計 <span id="total_price" class="price"><?php echo $total_value;?></span>円(税込)
		<?php $free_data = Func::get_cart_total_info($data['cart_dt']);?>
		<?php if (isset($free_data['free_sending']) && gettype($free_data['free_sending']) === "boolean"):?>
			<div id="free_sending" class="free">
				送料無料です！
			</div>
		<?php endif;?>
		<?php if ($is_login):?>
			<!--非会員非表示-->
			<div class="point">
				今回のお買い物で<span id="get_point"><?php echo Func::get_total_point($data['cart_dt'],  Model_Mt_Pointrate::select_rate()); ?></span>ポイントが獲得できます！
			</div>
			<!--/非会員非表示-->
		<?php endif;?>
	</div>
	<form method="post" action="<?php echo Config::get('custom_config.url.https_domain').'/user/purchase/purchaser';?>">
		<?php if (!$is_login):?>
			<!--会員非表示-->
			<div class="policy">
				<h2>個人情報の取扱いについて</h2>
				<div class="textBox">
					＜個人情報の取扱いについて＞<br /><br />

					<?php echo Config::get('custom_config.shop_name');?>の会員登録及び商品の注文にあたり、個人情報の取扱いに関する下記の事項に同意してください。<br /><br />

					1.当サービスを通じて登録されたお客様の個人情報は、エス・ピー広告株式会社（個人情報保護管理者　前田 敏行<img src="<?php echo $assets_path;?>/img/common/parts/mail2.png" alt="メールアドレスはスパム防止のため画像で挿入しております" class="addressImg">）が管理します。<br /><br />

					2.お客様の個人情報は以下の利用目的で利用します。なお、当サービスにおける個人情報の利用目的に変更が発生する場合は、あらかじめお客様に通知し、変更した利用目的について、再度同意を頂いた上で、利用をおこないます。<br />
					(1)注文した商品を発送するため。<br />
					(2)お客様がマイページにログインする際の認証のため。<br />
					(3)メールマガジンを発行するため。<br />
					(4)当社の各種サービスに関してご連絡するため。<br />
					(5)お客様のお問合せに対応するため。<br /><br />

					3. クレジットカード払いを選択されたお客様のクレジットカード情報は、直接決済代行会社（GMOイプシロン株式会社）に送信されるため、当社で取得、保有することはございません。<br /><br />

					4.お客様の個人情報は、お客様の同意なく第三者に提供されることはございません。<br /><br />

					5.当サービスの提供にあたり、データ保守、商品の配送等、個人情報の取り扱いが発生する業務の一部または全部を適切な契約を取り交わした企業に委託する場合がございます。<br /><br />

					6.個人情報の登録は、お客様の任意となりますが、「必須情報」と表示されている情報を登録いただけない場合は、会員登録や商品の注文等ができません。<br /><br />

					7.お客様は、登録したご自身の個人情報について、利用目的の通知・開示、訂正・追加・削除、利用停止・消去・第三者提供停止の請求をおこなうことができます。当該請求をおこなう場合は<img src="<?php echo $assets_path;?>/img/common/parts/mail2.png" alt="メールアドレスはスパム防止のため画像で挿入しております" class="addressImg">までご連絡ください。<br /><br />

					8.当サービスでは、コンテンツ並びにレイアウトの評価及び改善、不正アクセスの防止のため、cookieを通じてユーザーのアクセスログを取得、管理しています。<br /><br />

					以上<br /><br />
				</div>
				<div class="policyCheck">
					<label class="check">
						<input type="checkbox" name="consent" <?php if (isset($data['consent']))echo 'checked';?>>会員規約及び個人情報の取り扱いについて同意する
					</label>
					<?php if (isset($val) && $val->error('consent')) :?>
						<span class="errorText">
							<?php echo $val->error('consent');?>
						</span>
					<?php endif ?>
				</div>
			</div>
			<!--/会員非表示-->
		<?php endif;?>
		<div class="">
			<h2>お客様(購入者様)情報</h2>
			<dl class="form">
				<dt>
				お名前<span class="requisite">必須</span>
				</dt>
				<dd>
					<input type="text" name="last_name" value="<?php if(isset($data['last_name'])): echo $data['last_name']; endif;?>" class="size2" placeholder="田中">
					<input type="text" name="first_name" value="<?php if(isset($data['first_name'])): echo $data['first_name']; endif;?>" class="size2" placeholder="太郎">
					<?php if (isset($val) && $val->error('last_name')) :?>
						<span class="errorText">
							<?php echo $val->error('last_name');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('first_name')) :?>
						<span class="errorText">
							<?php echo $val->error('first_name');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
				お名前（フリガナ）
				</dt>
				<dd>
					<input type="text" name="last_name_kana" value="<?php if(isset($data['last_name_kana'])): echo $data['last_name_kana']; endif;?>" class="size2" placeholder="タナカ">
					<input type="text" name="first_name_kana" value="<?php if(isset($data['first_name_kana'])): echo $data['first_name_kana']; endif;?>" class="size2" placeholder="タロウ">
					<?php if (isset($val) && $val->error('last_name_kana')) :?>
						<span class="errorText">
							<?php echo $val->error('last_name_kana');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('first_name_kana')) :?>
						<span class="errorText">
							<?php echo $val->error('first_name_kana');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>法人名・団体名</dt>
				<dd>
					<input type="text" name="corporate_name" value="<?php if(isset($data['corporate_name'])): echo $data['corporate_name']; endif;?>" class="size3">
					<?php if (isset($val) && $val->error('corporate_name')) :?>
						<span class="errorText">
							<?php echo $val->error('corporate_name');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>法人名・団体名（フリガナ）</dt>
				<dd>
					<input type="text" name="corporate_name_kana" value="<?php if(isset($data['corporate_name_kana'])): echo $data['corporate_name_kana']; endif;?>" class="size3">
					<?php if (isset($val) && $val->error('corporate_name_kana')) :?>
						<span class="errorText">
							<?php echo $val->error('corporate_name_kana');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
				メールアドレス<span class="requisite">必須</span>
				</dt>
				<dd>
					<input type="text" name="mail_purchase" value="<?php if(isset($data['mail_purchase'])): echo $data['mail_purchase']; endif;?>" class="size3">
				</dd>
				<dt>
				メールアドレス（確認）<span class="requisite">必須</span>
				</dt>
				<dd>
					<input type="text" name="mail_purchase_confirm" value="<?php if(isset($data['mail_purchase_confirm'])): echo $data['mail_purchase_confirm']; endif;?>" class="size3">
					<?php if (isset($val) && $val->error('mail_purchase')) :?>
						<span class="errorText">
							<?php echo $val->error('mail_purchase');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
				郵便番号<span class="requisite">必須</span>
				</dt>
				<dd>
					<input onkeyup="AjaxZip3.zip2addr(this,'','state_cd','address1');" name="zip" size="10" type="text" value="<?php if(isset($data['zip'])): echo $data['zip']; endif;?>" class="size2" placeholder="0000000" maxlength="7"/>
					<?php if (isset($val) && $val->error('zip')) :?>
						<span class="errorText">
							<?php echo $val->error('zip');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
				住所<span class="requisite">必須</span>
				</dt>
				<dd>
					<select name="state_cd" class="size2">
						<option>選んでください</option>
						<?php for($i=1; $i<=count($data['mt']['state_cd']); $i++):?>
							<option value="<?php echo $data['mt']['state_cd'][$i]['id'] ?>" <?php if (isset($data['state_cd']) && $data['mt']['state_cd'][$i]['id'] == $data['state_cd']): echo 'selected'; endif; ?>>
								<?php echo $data['mt']['state_cd'][$i]['name'] ?>
							</option>
						<?php endfor;?>
					</select>
					<input type="text" name="address1" value="<?php if(isset($data['address1'])): echo $data['address1']; endif;?>" class="size2" placeholder="市区郡"><br />
					<input type="text" name="address2" value="<?php if(isset($data['address2'])): echo $data['address2']; endif;?>" class="size3" placeholder="町村字番地"><br />
					<input type="text" name="address3" value="<?php if(isset($data['address3'])): echo $data['address3']; endif;?>" class="size3" placeholder="建物名">
						<?php if (isset($val) && $val->error('state_cd')) :?>
						<span class="errorText">
							<?php echo $val->error('state_cd');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('address1')) :?>
						<span class="errorText">
							<?php echo $val->error('address1');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('address2')) :?>
						<span class="errorText">
							<?php echo $val->error('address2');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('address3')) :?>
						<span class="errorText">
							<?php echo $val->error('address3');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
				電話番号<span class="requisite">必須</span>
				</dt>
				<dd>
					<input type="text" name="tel1" value="<?php if(isset($data['tel1'])): echo $data['tel1']; endif;?>" class="size1" placeholder="000">
					<span class="hyphen">-</span>
					<input type="text" name="tel2" value="<?php if(isset($data['tel2'])): echo $data['tel2']; endif;?>" class="size1" placeholder="0000">
					<span class="hyphen">-</span>
					<input type="text" name="tel3" value="<?php if(isset($data['tel3'])): echo $data['tel3']; endif;?>" class="size1" placeholder="0000">
					<?php if (isset($val) && $val->error('tel1')) :?>
						<span class="errorText">
							<?php echo $val->error('tel1');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('tel2')) :?>
						<span class="errorText">
							<?php echo $val->error('tel2');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('tel3')) :?>
						<span class="errorText">
							<?php echo $val->error('tel3');?>
						</span>
					<?php endif ?>
				</dd>
				<dt>
				FAX
				</dt>
				<dd>
					<input type="text" name="fax1" value="<?php if(isset($data['fax1'])): echo $data['fax1']; endif;?>" class="size1" placeholder="000">
					<span class="hyphen">-</span>
					<input type="text" name="fax2" value="<?php if(isset($data['fax2'])): echo $data['fax2']; endif;?>" class="size1" placeholder="0000">
					<span class="hyphen">-</span>
					<input type="text" name="fax3" value="<?php if(isset($data['fax3'])): echo $data['fax3']; endif;?>" class="size1" placeholder="0000">
					<?php if (isset($val) && $val->error('fax1')) :?>
						<span class="errorText">
							<?php echo $val->error('fax1');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('fax2')) :?>
						<span class="errorText">
							<?php echo $val->error('fax2');?>
						</span>
					<?php endif ?>
					<?php if (isset($val) && $val->error('fax3')) :?>
						<span class="errorText">
							<?php echo $val->error('fax3');?>
						</span>
					<?php endif ?>
				</dd>
			</dl>
			<dl class="form">
				<dt>
					お届け先
				</dt>
				<dd>
					<label><input type="radio" name="receiver" value="own" <?php if (!isset($data['receiver']) || $data['receiver'] == 'own')echo 'checked';?>>購入者の住所で送る</label>
					<label><input type="radio" name="receiver" value="other" <?php if (isset($data['receiver']) && $data['receiver'] == 'other')echo 'checked';?>>違う住所を入力/登録した住所を使用</label>
				</dd>
				<dt>
					お支払い方法
				</dt>
				<dd class="option">

				<?php foreach ($data['mt']['payment_cd'] as $key => $value):?>
					<label><input type="radio" name="payment_cd" value="<?php echo $key;?>" <?php if (isset($data['payment_cd']) && (Int)$value['id'] === (Int)$data['payment_cd'])echo 'checked';?>><?php echo $value['name'];?><p class="description"><?php if (isset($payment_detail[$key]))echo $payment_detail[$key]['description'];?></p></label>
					<!-- カード決済 -->
					<?php if ((Int)$key == 1):?>

						<?php if ($is_login && isset($data['is_card_used']) && $data['is_card_used']):?>
							<p class="description attention">
								<span>前回利用したカードを利用します。</span><br />
								違うカードを利用する場合、前回利用カードが不明な場合などは<a href="<?php echo $base_url;?>/user/change/card">こちら</a>からご変更下さい。
							</p>
							<input type="hidden" name="payment_process_cd" value="<?php echo Payment::$PROSESS_REGISTERED; ?>">
						<?php else:?>
							<input type="hidden" name="payment_process_cd" value="<?php echo Payment::$PROSESS_FIRST; ?>">
						<?php endif;?>
						<?php if (isset($val) && $val->error('payment_process_cd')) :?>
							<span class="errorText"><?php echo $val->error('payment_process_cd');?></span>
						<?php endif ?>

					<?php endif;?>
				<?php endforeach;?>
				<?php if (isset($val) && $val->error('payment_cd')) :?>
					<span class="errorText"><?php echo $val->error('payment_cd');?></span>
				<?php endif ?>
				</dd>
			</dl>
		</div>
		<input type="submit" class="submitBtn overlay" value="次へ" name="check_purchaser"/>
		<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
	</form>
</div>