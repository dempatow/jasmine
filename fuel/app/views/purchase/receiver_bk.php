<h1>カートに入っているアイテム</h1>
<?php for ($i=0; $i<count($data['cart_dt']); $i++):?>
	<br />------------------------------------------------<br />
	商品名:<?php echo $data['cart_dt'][$i]['name'];?><br />
	単価:<?php echo $data['cart_dt'][$i]['price'];?><br />
	単価(税込み):<?php echo Func::get_tax_inclueded_price($data['cart_dt'][$i]['price']);?><br />
	一つあたりの獲得ポイント:<?php //echo $data['cart_dt'][$i]['point'];?><br />
	カートに入っている個数:<?php echo $data['cart_dt'][$i]['count'];?><br />
	<br />------------------------------------------------<br />
<?php endfor;?>
合計金額:<?php echo $data['sum']['total_amount'];?><br />
合計獲得ポイント:<?php echo $data['sum']['total_point'];?><br />
合計個数:<?php echo $data['sum']['total_count'];?><br /><br />

<br />
<br />

<br />
<h1>決済方法</h1>
<br />

<?php echo $data['mt']['payment']['name'];?>
<br />
<br />
<?php Debug::dump($data);?>

<h1>送り先情報</h1><br />

<form method="post" action="<?php echo Config::get('custom_config.url.https_domain');?>/user/purchase/receiver">

	corporate_name
	<input type="text" name="corporate_name" value="<?php if(isset($data['corporate_name'])): echo $data['corporate_name']; endif;?>">
	<?php if (isset($val) && $val->error('corporate_name')) :?>
		<?php echo $val->error('corporate_name');?>
	<?php endif ?>
	<br />

	corporate_name_kana
	<input type="text" name="corporate_name_kana" value="<?php if(isset($data['corporate_name_kana'])): echo $data['corporate_name_kana']; endif;?>">
	<?php if (isset($val) && $val->error('corporate_name_kana')) :?>
		<?php echo $val->error('corporate_name_kana');?>
	<?php endif ?>
	<br />

	last_name
	<input type="text" name="last_name" value="<?php if(isset($data['last_name'])): echo $data['last_name']; endif;?>">
	<?php if (isset($val) && $val->error('last_name')) :?>
		<?php echo $val->error('last_name');?>
	<?php endif ?>
	<br />

	first_name
	<input type="text" name="first_name" value="<?php if(isset($data['first_name'])): echo $data['first_name']; endif;?>">
	<?php if (isset($val) && $val->error('first_name')) :?>
		<?php echo $val->error('first_name');?>
	<?php endif ?>
	<br />

	last_name_kana
	<input type="text" name="last_name_kana" value="<?php if(isset($data['last_name_kana'])): echo $data['last_name_kana']; endif;?>">
	<?php if (isset($val) && $val->error('last_name_kana')) :?>
		<?php echo $val->error('last_name_kana');?>
	<?php endif ?>
	<br />

	first_name_kana
	<input type="text" name="first_name_kana" value="<?php if(isset($data['first_name_kana'])): echo $data['first_name_kana']; endif;?>">
	<?php if (isset($val) && $val->error('first_name_kana')) :?>
		<?php echo $val->error('first_name_kana');?>
	<?php endif ?>
	<br />

	zip
	<input onkeyup="AjaxZip3.zip2addr(this,'','state_cd','address1');" name="zip" size="10" type="text" value="<?php if(isset($data['zip'])): echo $data['zip']; endif;?>"/>
	<?php if (isset($val) && $val->error('zip')) :?>
		<?php echo $val->error('zip');?>
	<?php endif ?>
	<br />

	state_cd
	<select name="state_cd">
		<option>選んでください</option>
		<?php for($i=1; $i<=count($data['mt']['state_cd']); $i++):?>
			<option value="<?php echo $data['mt']['state_cd'][$i]['id'] ?>" <?php if (isset($data['state_cd']) && $data['mt']['state_cd'][$i]['id'] == $data['state_cd']): echo 'selected'; endif; ?>>
				<?php echo $data['mt']['state_cd'][$i]['name'] ?>
			</option>
		<?php endfor;?>
	</select>
	<?php if (isset($val) && $val->error('state_cd')) :?>
		<?php echo $val->error('state_cd');?>
	<?php endif ?>
	<br />

	address1
	<input type="text" name="address1" value="<?php if(isset($data['address1'])): echo $data['address1']; endif;?>">
	<?php if (isset($val) && $val->error('address1')) :?>
		<?php echo $val->error('address1');?>
	<?php endif ?>
	<br />

	address2
	<input type="text" name="address2" value="<?php if(isset($data['address2'])): echo $data['address2']; endif;?>">
	<?php if (isset($val) && $val->error('address2')) :?>
		<?php echo $val->error('address2');?>
	<?php endif ?>
	<br />

	address3
	<input type="text" name="address3" value="<?php if(isset($data['address3'])): echo $data['address3']; endif;?>">
	<?php if (isset($val) && $val->error('address3')) :?>
		<?php echo $val->error('address3');?>
	<?php endif ?>
	<br />

	tel1
	<input type="text" name="tel1" value="<?php if(isset($data['tel1'])): echo $data['tel1']; endif;?>">
	<?php if (isset($val) && $val->error('tel1')) :?>
		<?php echo $val->error('tel1');?>
	<?php endif ?>
	<br />

	tel2
	<input type="text" name="tel2" value="<?php if(isset($data['tel2'])): echo $data['tel2']; endif;?>">
	<?php if (isset($val) && $val->error('tel2')) :?>
		<?php echo $val->error('tel2');?>
	<?php endif ?>
	<br />

	tel3
	<input type="text" name="tel3" value="<?php if(isset($data['tel3'])): echo $data['tel3']; endif;?>">
	<?php if (isset($val) && $val->error('tel3')) :?>
		<?php echo $val->error('tel3');?>
	<?php endif ?>
	<br />

	fax1
	<input type="text" name="fax1" value="<?php if(isset($data['fax1'])): echo $data['fax1']; endif;?>">
	<?php if (isset($val) && $val->error('fax1')) :?>
		<?php echo $val->error('fax1');?>
	<?php endif ?>
	<br />

	fax2
	<input type="text" name="fax2" value="<?php if(isset($data['fax2'])): echo $data['fax2']; endif;?>">
	<?php if (isset($val) && $val->error('fax2')) :?>
		<?php echo $val->error('fax2');?>
	<?php endif ?>
	<br />

	fax3
	<input type="text" name="fax3" value="<?php if(isset($data['fax3'])): echo $data['fax3']; endif;?>">
	<?php if (isset($val) && $val->error('fax3')) :?>
		<?php echo $val->error('fax3');?>
	<?php endif ?>
	<br />

	<br />
	<input type="submit" name="back" value='戻る'>
	<input type="submit" name="check_receiver" value='次へ'>
	<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
</form>
<?php Debug::dump($data);?>
