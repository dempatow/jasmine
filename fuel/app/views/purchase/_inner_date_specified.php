<tr id="dateTimeTr">
	<th><?php if (isset($error_date) && $error_date && isset($error_time) && $error_time):?>お届け希望日時<?php elseif (isset($error_date) && $error_date):?>お届け希望日<?php endif;?></th>
	<td>
		<!-- 日にち指定 -->
		<?php if (isset($error_date) && $error_date):?>
			<select name="date_specified">
				<?php for ($i=0; $i<count($error_date); $i++):?>
					<option value="<?php echo $error_date[$i]['value'];?>" <?php if (isset($date_specified) && (Int)$date_specified === (Int)$error_date[$i]['value']) echo 'selected'; ?>><?php echo $error_date[$i]['disp'];?></option>
				<?php endfor;?>
			</select>
		<?php endif;?>
		<!-- 時間帯指定 -->
		<?php if (isset($error_time) && $error_time):?>
			&nbsp;
			<select name="time_specified">
				<?php for ($i=0; $i<count($error_time); $i++):?>
					<option value="<?php echo $error_time[$i]['id'];?>" <?php if (isset($time_specified) && (Int)$time_specified === (Int) $error_time[$i]['id']) echo 'selected';?>><?php echo $error_time[$i]['name'];?></option>
				<?php endfor;?>
			</select>
		<?php endif;?>
		<?php if (isset($val) && $val->error('date_specified')):?>
			<span class="errorText">
				<?php echo $val->error('date_specified');?>
			</span>
		<?php endif;?>
	</td>
</tr>