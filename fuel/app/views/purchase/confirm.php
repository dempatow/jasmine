<div id="contents" class="confirm">
	<form action="<?php echo $base_url.'/user/purchase/confirm';?>" method="post">
		<div class="orderItm">
			<h2>ご注文内容</h2>
			<ul class="cartItem">
				<?php for ($i=0; $i<count($data['cart_dt']); $i++):?>
					<li>
						<img src="<?php echo $base_url.'/assets/img/item/'.$data['cart_dt'][$i]['id'];?>/50_50.jpg" alt="<?php echo $data['cart_dt'][$i]['name'];?>" class="itemImg">
						<div class="itemData">
							<h4>
								<a href="<?php echo $base_url.'/item/index/'.$data['cart_dt'][$i]['id'];?>"><?php echo $data['cart_dt'][$i]['name'];?></a>
							</h4>
							<span class="price">
								<?php 
									$unit_price = Func::get_item_value($data['cart_dt'][$i]);
									echo $unit_price;
								?>
							</span>円(税込)

							<!-- 特殊選択 -->
								<?php if (isset($data['cart_dt'][$i]['special_select'])):?>
								<ul class="special_select">
									<?php for ($j=0; $j<count($data['cart_dt'][$i]['special_select']); $j++):?>
									<li>
										・<?php echo $data['cart_dt'][$i]['special_select'][$j]['key_name'];?>
										<?php echo $data['cart_dt'][$i]['special_select'][$j]['value_name'];?>
										[<?php echo $data['cart_dt'][$i]['special_select'][$j]['count'];?>]
										</li>
									<?php endfor;?>
								</ul>
								<?php endif;?>
							<!-- 特殊選択end -->

							<div class="wrap">
								数量[<span><?php echo $data['cart_dt'][$i]['count'];?></span>]
							</div>
						</div>
						<div class="subtotal">
							<span class="wrap">小計<span class="price"><?php echo $unit_price * $data['cart_dt'][$i]['count'];?></span><span class="unit">円(税込)</span></span>
						</div>
					</li>
				<?php endfor;?>
				<li class="other line">
					アイテム金額合計<span class="price" id="itemTotalPrice"><?php if (isset($data['sum']['total_amount'])) echo $data['sum']['total_amount'];?></span>
					<span class="unit">円(税込)</span>
				</li>
				<?php if ($is_login):?>
					<li class="other">
						<div class="block">
							使えるポイント<span class="price"><?php if (isset($data['has_point'])): echo $data['has_point']; else: echo '0'; endif;?></span>
							<span class="unit">pt</span>
						</div>
						<div class="block">
							使用予定ポイント
							<span class="price"><?php if (isset($data['use_point'])): echo $data['use_point']; else: echo '0'; endif;?></span>
							<span class="unit">pt</span>
						</div>
						<div class="block">
								<input id="inputPoint" type="text" name="use_point" value="<?php if (isset($data['use_point'])): echo $data['use_point']; else: echo '0'; endif;?>">
								<input type="hidden" name="use_point_hidden" value="<?php if (isset($data['use_point'])): echo $data['use_point']; else: echo '0'; endif;?>">
							<span class="unit">
								<input type="submit" name="use_point_button" value="使用"/>
							</span>
							<?php if (isset($val_point) && $val_point->error('use_point')) :?>
								<span class="errorText">
									<?php echo $val_point->error('use_point');?>
								</span>
							<?php endif ?>
						</div>
					</li>
				<?php endif;?>
				<li class="other">
					
					<div class="block">
						送料
						<span class="price"><?php if (isset($data['sending_info']['postage'])) echo $data['sending_info']['postage'];?></span>
						<span class="unit">円</span>
					</div>
				</li>
			</ul>
			<div id="total">
				合計 <span id="total_price" class="price"><?php if (isset($data['total_amount'])) echo $data['total_amount'];?></span><span class="unit">円(税込)</span>
				<?php if ($is_login):?>
					<div>
						ポイントが<?php if(isset($data['total_point'])) echo $data['total_point'];?>ポイント獲得できます
					</div>
				<?php endif;?>
				<div class="commission">
					※代引き手数料が別途必要になります<br />
					※振込の際の手数料はお客様のご負担になります
				</div>
			</div>
			<a href="<?php echo $base_url.'/cart/index';?>" class="btn">変更</a>
		</div>
		<div class="user">
			<h2>ご購入者様情報</h2>
			<dl>
				<dt>お名前</dt>
				<dd><?php if (isset($data['purchase_info']['last_name'])) echo $data['purchase_info']['last_name'];?> <?php if (isset($data['purchase_info']['first_name'])) echo $data['purchase_info']['first_name'];?> 様</dd>
				<?php if (isset($data['purchase_info']['corporate_name']) && !empty($data['purchase_info']['corporate_name'])):?>
					<dt>法人名・団体名</dt>
					<dd><?php echo $data['purchase_info']['corporate_name']?> 様</dd>
				<?php endif;?>
				<dt>郵便番号</dt>
				<dd>
					<?php if (isset($data['purchase_info']['zip'])) echo substr($data['purchase_info']['zip'],0,3).'-'.substr($data['purchase_info']['zip'],3,6);?>
				</dd>
				<dt>住所</dt>
				<dd>
					<?php if (isset($data['purchase_info']['state']['name'])) echo $data['purchase_info']['state']['name'];?> <?php if (isset($data['purchase_info']['address1'])) echo $data['purchase_info']['address1']; ?><br />
					<?php if (isset($data['purchase_info']['address2'])) echo $data['purchase_info']['address2']; ?> <?php if (isset($data['purchase_info']['address3'])) echo $data['purchase_info']['address3']; ?>
				</dd>
				<dt>メールアドレス</dt>
				<dd><?php if (isset($data['purchase_info']['mail_purchase'])) echo $data['purchase_info']['mail_purchase'];?></dd>
				<dt>お電話番号</dt>
				<dd>
					<?php if (isset($data['purchase_info']['tel1']) && isset($data['purchase_info']['tel2']) && isset($data['purchase_info']['tel3'])); echo $data['purchase_info']['tel1'].'-'.$data['purchase_info']['tel2'].'-'.$data['purchase_info']['tel3']?>
				</dd>
				<?php if (isset($data['purchase_info']['fax1']) && isset($data['purchase_info']['fax2']) && isset($data['purchase_info']['fax3']) && !empty($data['purchase_info']['fax1']) && !empty($data['purchase_info']['fax2']) && !empty($data['purchase_info']['fax3'])):?>
					<dt>FAX</dt>
					<dd><?php echo $data['purchase_info']['fax1'].'-'.$data['purchase_info']['fax2'].'-'.$data['purchase_info']['fax3'];?></dd>
				<?php endif;?>
			</dl>
			<span  data-url="<?php echo $base_url.'/user/purchase/purchaser'?>" class="btn pageMoveBtn">変更</span>
		</div>
		<div class="destination">
			<h2>お届け先情報</h2>
				<?php if (isset($data['purchase_info']['receiver']) && $data['purchase_info']['receiver'] === 'own'):?>
					<dl>
						<!--購入者と同じ場合に表示-->
						<dt>お届け先</dt>
						<dd>ご購入者様と同じ住所にお届け</dd>
					</dl>
					<span data-url="<?php if (true)echo $base_url.'/user/purchase/purchaser'?>#" class="btn pageMoveBtn">変更</span>
				<?php else:?>
					<dl>
						<!--購入者と同じ場合に表示ここまで-->
						<dt>お名前</dt>
						<dd><?php if (isset($data['receiver_info']['last_name'])) echo $data['receiver_info']['last_name'];?> <?php if (isset($data['receiver_info']['first_name'])) echo $data['receiver_info']['first_name'];?> 様</dd>
						<?php if (isset($data['receiver_info']['corporate_name']) && !empty($data['receiver_info']['corporate_name'])):?>
							<dt>法人名・団体名</dt>
							<dd><?php echo $data['receiver_info']['corporate_name'].' 様'?></dd>
						<?php endif;?>
						<dt>郵便番号</dt>
						<dd>
							<?php if (isset($data['receiver_info']['zip'])) echo substr($data['receiver_info']['zip'],0,3).'-'.substr($data['receiver_info']['zip'],3,6);?>
						</dd>
						<dt>住所</dt>
						<dd>
							<?php if (isset($data['receiver_info']['state']['name'])) echo $data['receiver_info']['state']['name'];?> <?php if (isset($data['receiver_info']['address1'])) echo $data['purchase_info']['address1']; ?><br />
							<?php if (isset($data['receiver_info']['address2'])) echo $data['receiver_info']['address2']; ?> <?php if (isset($data['receiver_info']['address3'])) echo $data['receiver_info']['address3']; ?>
						</dd>
						<dt>お電話番号</dt>
						<dd>
							<?php if (isset($data['receiver_info']['tel1']) && isset($data['receiver_info']['tel2']) && isset($data['receiver_info']['tel3'])) echo $data['receiver_info']['tel1'].'-'.$data['receiver_info']['tel2'].'-'.$data['receiver_info']['tel3'];?>
						</dd>
						<?php if (isset($data['receiver_info']['fax1']) && isset($data['receiver_info']['fax2']) && isset($data['receiver_info']['fax3']) && !empty($data['receiver_info']['fax1']) && !empty($data['receiver_info']['fax2']) && !empty($data['receiver_info']['fax3'])):?>
							<dt>FAX</dt>
							<dd><?php echo $data['receiver_info']['fax1'].'-'.$data['receiver_info']['fax2'].'-'.$data['receiver_info']['fax3'];?></dd>
						<?php endif;?>
					</dl>
						<span data-url="<?php if (true)echo $base_url.'/user/purchase/receiver'?>#" class="btn pageMoveBtn">変更</span>
				<?php endif;?>
		</div>
		<div class="othe">
			<h2>配達について</h2>
			<dl>
				<dt>配送方法</dt>
				<dd><?php if (isset($data['sending_info']['name'])) echo $data['sending_info']['name']?></dd>
				<?php if (isset($data['purchase_info']['date_specified_name']) && !empty($data['purchase_info']['date_specified_name'])):?>
					<dt>お届け希望日</dt>
					<dd>
						<?php echo $data['purchase_info']['date_specified_name'];?>
					</dd>
				<?php endif;?>
				<?php if (isset($data['purchase_info']['time_specified_name']) && !empty($data['purchase_info']['time_specified_name'])):?>
					<dt>お届け時間帯</dt>
					<dd>
						<?php echo $data['purchase_info']['time_specified_name'];?>
					</dd>
				<?php endif;?>
				<?php if (isset($data['purchase_info']['free1']) && !empty($data['purchase_info']['free1'])):?>
					<dt>その他</dt>
					<dd>
						<?php echo nl2br($data['purchase_info']['free1']);?>
					</dd>
				<?php endif;?>
			</dl>
			<span data-url="<?php echo $base_url.'/user/purchase/sending'?>" class="btn pageMoveBtn">変更</span>
		</div>
		<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
		<input type="submit" name="check_confirm" class="submitBtn overlay" value="この内容で注文する" />
	</form>
</div>