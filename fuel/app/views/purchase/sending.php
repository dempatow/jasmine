<div id="contents" class="cart sending">
	<h2>配送方法の選択</h2>

	<?php if (isset($sending_message)):?>
		<div class="warning">
			<h3>ご注意ください</h3>
			<ul>
				<?php foreach ($sending_message as $ms):?>
					<li><?php echo $ms;?></li>
				<?php endforeach;?>
			</ul>
		</div>
	<?php endif;?>

	<div class="textBox">お届け方法を選択してください。</div>

	<div class="formWrap">
		<form method="post" action ="<?php echo Config::get('custom_config.url.https_domain').'/user/purchase/sending';?>">
			<table>
				<?php for ($i=0; $i<count($data['mt']['sending']['postages']); $i++):?>
					<tr>
						<th>
							<label>
								<input
									class="sendingWay"
									type="radio"
									name="sending_cd"
									value="<?php echo $data['mt']['sending']['postages'][$i]['id'];?>"
									<?php if (isset($data['mt']['sending']['postages'][$i]['date_specified']) && $data['mt']['sending']['postages'][$i]['date_specified']):?>data-sending-cd="<?php echo $data['mt']['sending']['postages'][$i]['id'];?>"data-area-cd="<?php if (isset($data['mt']['sending']['area_cd'])) echo $data['mt']['sending']['area_cd'];?>"<?php endif;?> 
									<?php if (isset($data['sending_cd']) && (Int)$data['mt']['sending']['postages'][$i]['id'] === (Int)$data['sending_cd']) echo 'checked';?>>
								<?php if (isset($data['mt']['sending']['postages'][$i]['name'])) echo $data['mt']['sending']['postages'][$i]['name'];?>
							</label>
						</th>
						<td class="price">
							送料
							<span>
								<?php if (isset($data['mt']['sending']['is_free']) && $data['mt']['sending']['is_free']):?>
									<!-- 送料無料フラグが立っていた時 -->
									0
								<?php elseif (isset($data['mt']['sending']['postages'][$i]['postage'])):?>
									<!-- 通常価格 -->
									<?php echo $data['mt']['sending']['postages'][$i]['postage'];?>
								<?php endif;?>
							</span>円
						</td>
						<td>
							<?php if (isset($sending_detail[$data['mt']['sending']['postages'][$i]['id']]['description'])) echo $sending_detail[$data['mt']['sending']['postages'][$i]['id']]['description'];?>
						</td>
					</tr>
				<?php endfor;?>
			</table>
			<?php if (isset($val) && $val->error('sending_cd_first')):?>
				<span class="errorText">
					<?php echo $val->error('sending_cd_first');?>
				</span>
			<?php endif;?>
			<table>
				<tbody id="otherData">
					<?php if (isset($_inner_date_specified)) echo $_inner_date_specified;?>
					<tr>
						<th>
							自由欄
						</th>
						<td>
							<textarea name ="free1" placeholder="都合の悪い日や梱包についてなど、気になる点がございましたらご記入下さい。" maxlength="500" ><?php if (isset($data['free1'])) echo $data['free1'];?></textarea>
							<?php if (isset($val_purchase) && $val_purchase->error('free1')):?>
								<span class="errorText">
									<?php echo $val_purchase->error('free1');?>
								</span>
							<?php endif;?>
						</td>
					</tr>
				</tbody>
			</table>
			<input type="submit" class="submitBtn overlay" name="check_sending" value="次へ" >
			<input type="submit" class="backBtn overlay" name="back" value="戻る" >
		</form>
	</div>
</div>