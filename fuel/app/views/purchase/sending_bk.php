<h1>カートに入っているアイテム</h1>
<?php for ($i=0; $i<count($data['cart_dt']); $i++):?>
	<br />------------------------------------------------<br />
	商品名:<?php echo $data['cart_dt'][$i]['name'];?><br />
	単価:<?php echo $data['cart_dt'][$i]['price'];?><br />
	単価(税込み):<?php echo Func::get_tax_inclueded_price($data['cart_dt'][$i]['price']);?><br />
	一つあたりの獲得ポイント:<?php //echo $data['cart_dt'][$i]['point'];?><br />
	カートに入っている個数:<?php echo $data['cart_dt'][$i]['count'];?><br />
	<br />------------------------------------------------<br />
<?php endfor;?>

合計金額:<?php echo $data['sum']['total_amount'];?><br />
合計獲得ポイント:<?php echo $data['sum']['total_point'];?><br />
合計個数:<?php echo $data['sum']['total_count'];?><br /><br />

<h1>------------発送方法選択------------</h1><br />

<form method='post' action="<?php echo Config::get('custom_config.url.https_domain');?>/user/purchase/sending">
	<?php for ($i=0; $i<count($data['mt']['sending']['postages']); $i++):?>

		<?php if (isset($data['mt']['sending']['highest_cd'])):?>
		<!-- 発送方法優先が適用される時 -->

			<?php if ($data['mt']['sending']['highest_cd'] != $data['mt']['sending']['postages'][$i]['id']):?>
				<!-- 選択出来ない発送方法 -->
				<input type="radio" disabled>
				<?php echo $data['mt']['sending']['postages'][$i]['name'];?>
				<?php if (isset($data['mt']['sending']['postages'][$i]['area_name'])) echo '('.$data['mt']['sending']['postages'][$i]['area_name'].')';?>:
				<?php if (isset($data['mt']['sending']['is_free']) && $data['mt']['sending']['is_free']):?> 0<?php else:?> <?php echo $data['mt']['sending']['postages'][$i]['postage'];?><?php endif;?><br />

			<?php else: ?>
				<!-- 強制指定発送方法 -->
				<input type="radio"
					name="sending_cd"
					value="<?php echo $data['mt']['sending']['postages'][$i]['id'];?>" checked>
					<?php echo $data['mt']['sending']['postages'][$i]['name'] ;?>
					<?php if (isset($data['mt']['sending']['postages'][$i]['area_name'])) echo '('.$data['mt']['sending']['postages'][$i]['area_name'].')';?>:
					<?php if (isset($data['mt']['sending']['is_free']) && $data['mt']['sending']['is_free']):?> 0<?php else:?> <?php echo $data['mt']['sending']['postages'][$i]['postage'];?><?php endif;?><br />
					(同封商品の制限の為、この発送方法のみご利用が可能です)<br />
			<?php endif;?>

		<?php else: ?>
		<!-- されない時 -->

			<input type="radio" name="sending_cd" 
				value="<?php echo $data['mt']['sending']['postages'][$i]['id'];?>"
				<?php if (isset($data['sending_cd']) && $data['sending_cd'] == $data['mt']['sending']['postages'][$i]['id']) echo 'checked';?>>
				<?php echo $data['mt']['sending']['postages'][$i]['name'] ;?>
				<?php if (isset($data['mt']['sending']['postages'][$i]['area_name'])) echo '('.$data['mt']['sending']['postages'][$i]['area_name'].')';?>:
				<?php if (isset($data['mt']['sending']['is_free']) && $data['mt']['sending']['is_free']):?> 0<?php else:?> <?php echo $data['mt']['sending']['postages'][$i]['postage'];?><?php endif;?><br />
		<?php endif;?>

		<!-- エラーメッセージ -->
		<?php if (isset($val) && $val->error('sending_cd')):?>
			<?php echo $val->error('sending_cd');?>
		<?php endif;?>


		<!-- 日時指定 -->
		<?php if (isset($data['mt']['sending']['postages'][$i]['specified']['choice_date']) && count($data['mt']['sending']['postages'][$i]['specified']['choice_date'])>0):?>
			日時指定:
			<select name="date_specified">
				<?php for ($j=0; $j<count($data['mt']['sending']['postages'][$i]['specified']['choice_date']); $j++):?>
					<option value="<?php echo $data['mt']['sending']['postages'][$i]['specified']['choice_date'][$j]['value'];?>" <?php if (isset($data['purchase_info']['date_specified']) && (Int)$data['purchase_info']['date_specified'] === (Int)$data['mt']['sending']['postages'][$i]['specified']['choice_date'][$j]['value'])echo 'selected';?>><?php echo $data['mt']['sending']['postages'][$i]['specified']['choice_date'][$j]['disp'];?></option>
				<?php endfor;?>
			</select>
			<br />

			<!-- 時間帯指定 -->
			<?php if (isset($data['mt']['sending']['postages'][$i]['specified']['time_zone']) && count($data['mt']['sending']['postages'][$i]['specified']['time_zone'])>0):?>
				時間帯指定:
				<select name="time_specified">
					<?php for ($x=0; $x<count($data['mt']['sending']['postages'][$i]['specified']['time_zone']); $x++):?>
						<option value="<?php echo $data['mt']['sending']['postages'][$i]['specified']['time_zone'][$x]['id']; ?>" <?php if(isset($data['purchase_info']['time_specified']) && (Int)$data['purchase_info']['time_specified'] === (Int)$data['mt']['sending']['postages'][$i]['specified']['time_zone'][$x]['id'])echo 'selected';?>><?php echo $data['mt']['sending']['postages'][$i]['specified']['time_zone'][$x]['name']; ?></option>
					<?php endfor;?>
				</select>
				<br />
			<?php endif;?>

			<!-- エラーメッセージ -->
			<?php if (isset($val) && $val->error('date_specified')):?>
				<?php echo $val->error('date_specified');?><br />
			<?php endif;?>

		<?php endif;?>

	<?php endfor;?>

	<!-- 送料無料メッセージ -->
	<?php if (isset($data['mt']['sending']['free_amount']) && !empty($data['mt']['sending']['free_amount'])):?>
		商品金額が<?php echo $data['mt']['sending']['free_amount']?>円以上なので送料無料になります
	<?php elseif (isset($data['mt']['sending']['free_item']) && $data['mt']['sending']['free_item']):?>
		発送料金無料対象商品の為無料です
	<?php endif;?>

	<br /><br />

	<h1>------------購入者情報------------</h1><br />

	mail
	<?php if(isset($data['purchase_info']['mail_purchase'])): echo $data['purchase_info']['mail_purchase']; endif;?>
	<br />

	mail_confirm
	<?php if(isset($data['purchase_info']['mail_purchase_confirm'])): echo $data['purchase_info']['mail_purchase_confirm']; endif;?>
	<br />

	corporate_name
	<?php if(isset($data['purchase_info']['corporate_name'])): echo $data['purchase_info']['corporate_name']; endif;?>
	<br />

	corporate_name_kana
	<?php if(isset($data['purchase_info']['corporate_name_kana'])): echo $data['purchase_info']['corporate_name_kana']; endif;?>
	<br />

	last_name
	<?php if(isset($data['purchase_info']['last_name'])): echo $data['purchase_info']['last_name']; endif;?>
	<br />

	first_name
	<?php if(isset($data['purchase_info']['first_name'])): echo $data['purchase_info']['first_name']; endif;?>
	<br />

	last_name_kana
	<?php if(isset($data['purchase_info']['last_name_kana'])): echo $data['purchase_info']['last_name_kana']; endif;?>
	<br />

	first_name_kana
	<?php if(isset($data['purchase_info']['first_name_kana'])): echo $data['purchase_info']['first_name_kana']; endif;?>
	<br />

	zip
	<?php if(isset($data['purchase_info']['zip'])): echo $data['purchase_info']['zip']; endif;?>
	<br />

	state
		<?php for($i=1; $i<=count($data['mt']['state_cd']); $i++):?>
			<?php if (isset($data['purchase_info']['state_cd']) && $data['mt']['state_cd'][$i]['id'] == $data['purchase_info']['state_cd']): echo $data['mt']['state_cd'][$i]['name']; endif; ?>
		<?php endfor;?>
	<br />

	address1
	<?php if(isset($data['purchase_info']['address1'])): echo $data['purchase_info']['address1']; endif;?>
	<br />

	address2
	<?php if(isset($data['purchase_info']['address2'])): echo $data['purchase_info']['address2']; endif;?>
	<br />

	address3
	<?php if(isset($data['purchase_info']['address3'])): echo $data['purchase_info']['address3']; endif;?>
	<br />

	tel1
	<?php if(isset($data['purchase_info']['tel1'])): echo $data['purchase_info']['tel1']; endif;?>
	<br />

	tel2
	<?php if(isset($data['purchase_info']['tel2'])): echo $data['purchase_info']['tel2']; endif;?>
	<br />

	tel3
	<?php if(isset($data['purchase_info']['tel3'])): echo $data['purchase_info']['tel3']; endif;?>
	<br />

	fax1
	<?php if(isset($data['purchase_info']['fax1'])): echo $data['purchase_info']['fax1']; endif;?>
	<br />

	fax2
	<?php if(isset($data['purchase_info']['fax2'])): echo $data['purchase_info']['fax2']; endif;?>
	<br />

	fax3
	<?php if(isset($data['purchase_info']['fax3'])): echo $data['purchase_info']['fax3']; endif;?>
	<br />

	<br />
	<h1>------------送り先------------</h1>
	<br />
	<?php if (isset($data['purchase_info']['receiver']) && $data['purchase_info']['receiver'] == 'own'): ?>
		<!-- 購入者と同一 -->
		<h1>購入者様と同一です</h1>
		<br />
	<?php else: ?>

		<!-- 購入者と異なる場合 -->
		mail
		<?php if(isset($data['receiver_info']['mail_purchase'])): echo $data['receiver_info']['mail_purchase']; endif;?>
		<br />

		mail_confirm
		<?php if(isset($data['receiver_info']['mail_purchase_confirm'])): echo $data['receiver_info']['mail_purchase_confirm']; endif;?>
		<br />

		corporate_name
		<?php if(isset($data['receiver_info']['corporate_name'])): echo $data['receiver_info']['corporate_name']; endif;?>
		<br />

		corporate_name_kana
		<?php if(isset($data['receiver_info']['corporate_name_kana'])): echo $data['receiver_info']['corporate_name_kana']; endif;?>
		<br />

		last_name
		<?php if(isset($data['receiver_info']['last_name'])): echo $data['receiver_info']['last_name']; endif;?>
		<br />

		first_name
		<?php if(isset($data['receiver_info']['first_name'])): echo $data['receiver_info']['first_name']; endif;?>
		<br />

		last_name_kana
		<?php if(isset($data['receiver_info']['last_name_kana'])): echo $data['receiver_info']['last_name_kana']; endif;?>
		<br />

		first_name_kana
		<?php if(isset($data['receiver_info']['first_name_kana'])): echo $data['receiver_info']['first_name_kana']; endif;?>
		<br />

		zip
		<?php if(isset($data['receiver_info']['zip'])): echo $data['receiver_info']['zip']; endif;?>
		<br />

		state
		<?php for($i=1; $i<=count($data['mt']['state_cd']); $i++):?>
			<?php if (isset($data['receiver_info']['state_cd']) && $data['mt']['state_cd'][$i]['id'] == $data['receiver_info']['state_cd']): echo $data['mt']['state_cd'][$i]['name']; endif; ?>
		<?php endfor;?>
		<br />

		address1
		<?php if(isset($data['receiver_info']['address1'])): echo $data['receiver_info']['address1']; endif;?>
		<br />

		address2
		<?php if(isset($data['receiver_info']['address2'])): echo $data['receiver_info']['address2']; endif;?>
		<br />

		address3
		<?php if(isset($data['receiver_info']['address3'])): echo $data['receiver_info']['address3']; endif;?>
		<br />

		tel1
		<?php if(isset($data['receiver_info']['tel1'])): echo $data['receiver_info']['tel1']; endif;?>
		<br />

		tel2
		<?php if(isset($data['receiver_info']['tel2'])): echo $data['receiver_info']['tel2']; endif;?>
		<br />

		tel3
		<?php if(isset($data['receiver_info']['tel3'])): echo $data['receiver_info']['tel3']; endif;?>
		<br />

		fax1
		<?php if(isset($data['receiver_info']['fax1'])): echo $data['receiver_info']['fax1']; endif;?>
		<br />

		fax2
		<?php if(isset($data['receiver_info']['fax2'])): echo $data['receiver_info']['fax2']; endif;?>
		<br />

		fax3
		<?php if(isset($data['receiver_info']['fax3'])): echo $data['receiver_info']['fax3']; endif;?>
		<br />

	<?php endif; ?>

	<br />
	<h1>決済方法</h1>
	<br />
	<h2><?php echo $data['payment'];?></h2>
	<br />
	<input type="submit" name="back" value='戻る'>
	<input type="submit" name="check_sending" value='次へ'>
	<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
</form>