<div id="contents" class="cart completion">
	<h2>ありがとうございました！</h2>
	<div class="textBox">
		<p class="thanks">ご注文を承りました！</p>
		<?php if (isset($data['payment_cd']) && (Int)$data['payment_cd'] === 2):?>
			<p class="account">振込先は<span>【三井住友銀行 銀座支店　普通7694562 エスピーコウコク】</span>です。</p>
		<?php elseif (isset($data['payment_cd']) && (Int)$data['payment_cd'] === 3):?>
			
		<?php endif;?>
		<p>
			ご登録いただいたメールアドレス宛にご注文の確認メールを<br />お送りいたしますので、ご注文内容を再度ご確認ください。
		</p>
		<p>
			またのご利用をお待ちしております！
		</p>
	</div>

	<?php if (!isset($is_login) || !$is_login):?>
		<div class="singup">
			<h3>メルマガ会員になりませんか？</h3>
			新商品情報、セール情報など会員様限定のお得が満載です！
			<a href="<?php echo $base_url.'/user/signup/form';?>" class="submitBtn">会員登録はこちらから</a>
		</div>
	<?php endif;?>

	<?php if (isset($data['is_other_address']) &&  $data['is_other_address']):?>
		<form method="post" action="<?php echo $base_url.'/user/address/form';?>">
			<div class="addAddress">
				<h3>今回使ったお届け先をまた使いますか？</h3>
				お届け先リストに登録しておけば、次回も同じお届け先をそのまま使えます！
				<input type="submit" name="regist_address" value="登録する" class="submitBtn">
			</div>
			<?php if (isset($data['last_name']) && !empty($data['last_name'])):?>
				<input type="hidden" name="last_name" value="<?php echo $data['last_name'];?>">
			<?php endif;?>
			<?php if (isset($data['first_name']) && !empty($data['first_name'])):?>
				<input type="hidden" name="first_name" value="<?php echo $data['first_name'];?>">
			<?php endif;?>
			<?php if (isset($data['last_name_kana']) && !empty($data['last_name_kana'])):?>
				<input type="hidden" name="last_name_kana" value="<?php echo $data['last_name_kana'];?>">
			<?php endif;?>
			<?php if (isset($data['corporate_name']) && !empty($data['corporate_name'])):?>
				<input type="hidden" name="corporate_name" value="<?php echo $data['corporate_name'];?>">
			<?php endif;?>
			<?php if (isset($data['corporate_name_kana']) && !empty($data['corporate_name_kana'])):?>
				<input type="hidden" name="corporate_name_kana" value="<?php echo $data['corporate_name_kana'];?>">
			<?php endif;?>
			<?php if (isset($data['zip']) && !empty($data['zip'])):?>
				<input type="hidden" name="zip" value="<?php echo $data['zip'];?>">
			<?php endif;?>
			<?php if (isset($data['state_cd']) && !empty($data['state_cd'])):?>
				<input type="hidden" name="state_cd" value="<?php echo $data['state_cd'];?>">
			<?php endif;?>
			<?php if (isset($data['address1']) && !empty($data['address1'])):?>
				<input type="hidden" name="address1" value="<?php echo $data['address1'];?>">
			<?php endif;?>
			<?php if (isset($data['address2']) && !empty($data['address2'])):?>
				<input type="hidden" name="address2" value="<?php echo $data['address2'];?>">
			<?php endif;?>
			<?php if (isset($data['address3']) && !empty($data['address3'])):?>
				<input type="hidden" name="address3" value="<?php echo $data['address3'];?>">
			<?php endif;?>
			<?php if (isset($data['tel1']) && !empty($data['tel1'])):?>
				<input type="hidden" name="tel1" value="<?php echo $data['tel1'];?>">
			<?php endif;?>
			<?php if (isset($data['tel2']) && !empty($data['tel2'])):?>
				<input type="hidden" name="tel2" value="<?php echo $data['tel2'];?>">
			<?php endif;?>
			<?php if (isset($data['tel3']) && !empty($data['tel3'])):?>
				<input type="hidden" name="tel3" value="<?php echo $data['tel3'];?>">
			<?php endif;?>
			<?php if (isset($data['fax1']) && !empty($data['fax1'])):?>
				<input type="hidden" name="fax1" value="<?php echo $data['fax1'];?>">
			<?php endif;?>
			<?php if (isset($data['fax2']) && !empty($data['fax2'])):?>
				<input type="hidden" name="fax2" value="<?php echo $data['fax2'];?>">
			<?php endif;?>
			<?php if (isset($data['fax3']) && !empty($data['fax3'])):?>
				<input type="hidden" name="fax3" value="<?php echo $data['fax3'];?>">
			<?php endif;?>
		</form>
	<?php //else:?>
	<!--
		<div class="singup">
			<h3>メルマガ会員になりませんか？</h3>
			新商品情報、セール情報など会員様限定のお得が満載です！
			<a href="/user/signup/form" class="submitBtn">会員登録はこちらから</a>
		</div>
	-->
	<?php endif;?>
</div>