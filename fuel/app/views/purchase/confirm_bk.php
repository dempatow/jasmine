<?php Debug::dump('=====>',$data);?>

<h1>カートに入っているアイテム</h1>
<?php for ($i=0; $i<count($data['cart_dt']); $i++):?>
	<br />------------------------------------------------<br />
	商品名:<?php echo $data['cart_dt'][$i]['name'];?><br />
	単価:<?php echo $data['cart_dt'][$i]['price'];?><br />
	単価(税込み):<?php echo Func::get_tax_inclueded_price($data['cart_dt'][$i]['price']);?><br />
	カートに入っている個数:<?php echo $data['cart_dt'][$i]['count'];?><br />
	<br />------------------------------------------------<br />
<?php endfor;?>
	
合計金額:<?php echo $data['sum']['total_amount'];?><br />
合計獲得ポイント(商品ごとについているポイントの合計):<?php echo $data['sum']['total_point'];?><br />
合計個数:<?php echo $data['sum']['total_count'];?><br /><br />

<form method='post' action="<?php echo Config::get('custom_config.url.https_domain');?>/user/purchase/confirm">

<?php if (CustomAuth::get_login_info()):?>
	<h1>------------合計獲得ポイント(レート含む)------------</h1><br />
	<h2><?php echo $data['total_point'];?> ポイント</h2>

	<?php if (isset($data['has_point']) && $data['has_point']>0):?>
		<h1>------------保持ポイント------------</h1><br />
		<h2><?php if (isset($data['has_point']) && $data['has_point']>0): echo $data['has_point']; endif;?> ポイント</h2>
		ポイントを使用する<br />
	<?php endif;?>
<?php endif;?>

	<h1>------------発送方法------------</h1><br />
	<h2>送付方法 : <?php if (isset($data['sending_info']['name'])): echo $data['sending_info']['name']; endif;?></h2>
	<h2>送付金額 : <?php if (isset($data['sending_info']['postage'])): echo $data['sending_info']['postage']; endif;?>円</h2>
	<?php if (isset($data['sending_info']['area_name'])): echo '<h2>発送エリア : '.$data['sending_info']['area_name'].'</h2>'; endif;?>

	<!- 送料無料メッセージ ->
	<?php if (isset($data['sending_info']['is_free']) && isset($data['sending_info']['free_amount'])):?>
		<h2>商品金額が<?php echo $data['sending_info']['free_amount']?>円以上なので送料無料になります</h2>
	<?php elseif ($data['sending_info']['free_item']):?>
		<h2>送料無料商品なので送料無料です</h2>
	<?php endif;?>

	<br />

	<h1>------------購入者情報------------</h1><br />

	mail
	<?php if(isset($data['purchase_info']['mail_purchase'])): echo $data['purchase_info']['mail_purchase']; endif;?>
	<br />

	mail_confirm
	<?php if(isset($data['purchase_info']['mail_purchase_confirm'])): echo $data['purchase_info']['mail_purchase_confirm']; endif;?>
	<br />

	corporate_name
	<?php if(isset($data['purchase_info']['corporate_name'])): echo $data['purchase_info']['corporate_name']; endif;?>
	<br />

	corporate_name_kana
	<?php if(isset($data['purchase_info']['corporate_name_kana'])): echo $data['purchase_info']['corporate_name_kana']; endif;?>
	<br />

	last_name
	<?php if(isset($data['purchase_info']['last_name'])): echo $data['purchase_info']['last_name']; endif;?>
	<br />

	first_name
	<?php if(isset($data['purchase_info']['first_name'])): echo $data['purchase_info']['first_name']; endif;?>
	<br />

	last_name_kana
	<?php if(isset($data['purchase_info']['last_name_kana'])): echo $data['purchase_info']['last_name_kana']; endif;?>
	<br />

	first_name_kana
	<?php if(isset($data['purchase_info']['first_name_kana'])): echo $data['purchase_info']['first_name_kana']; endif;?>
	<br />

	zip
	<?php if(isset($data['purchase_info']['zip'])): echo $data['purchase_info']['zip']; endif;?>
	<br />

	state
	<?php if (isset($data['purchase_info']['state']['name'])) : echo $data['purchase_info']['state']['name']; endif;?>
	<br />

	address1
	<?php if(isset($data['purchase_info']['address1'])): echo $data['purchase_info']['address1']; endif;?>
	<br />

	address2
	<?php if(isset($data['purchase_info']['address2'])): echo $data['purchase_info']['address2']; endif;?>
	<br />

	address3
	<?php if(isset($data['purchase_info']['address3'])): echo $data['purchase_info']['address3']; endif;?>
	<br />

	tel1
	<?php if(isset($data['purchase_info']['tel1'])): echo $data['purchase_info']['tel1']; endif;?>
	<br />

	tel2
	<?php if(isset($data['purchase_info']['tel2'])): echo $data['purchase_info']['tel2']; endif;?>
	<br />

	tel3
	<?php if(isset($data['purchase_info']['tel3'])): echo $data['purchase_info']['tel3']; endif;?>
	<br />

	fax1
	<?php if(isset($data['purchase_info']['fax1'])): echo $data['purchase_info']['fax1']; endif;?>
	<br />

	fax2
	<?php if(isset($data['purchase_info']['fax2'])): echo $data['purchase_info']['fax2']; endif;?>
	<br />

	fax3
	<?php if(isset($data['purchase_info']['fax3'])): echo $data['purchase_info']['fax3']; endif;?>
	<br />

	<br />
	<h1>------------送り先------------</h1>
	<br />
	<?php if (isset($data['purchase_info']['receiver']) && $data['purchase_info']['receiver'] === 'own'): ?>
		<h2>購入者様と同じ</h2>
	<?php else: ?>

		corporate_name
		<?php if(isset($data['receiver_info']['corporate_name'])): echo $data['receiver_info']['corporate_name']; endif;?>
		<br />

		corporate_name_kana
		<?php if(isset($data['receiver_info']['corporate_name_kana'])): echo $data['receiver_info']['corporate_name_kana']; endif;?>
		<br />

		last_name
		<?php if(isset($data['receiver_info']['last_name'])): echo $data['receiver_info']['last_name']; endif;?>
		<br />

		first_name
		<?php if(isset($data['receiver_info']['first_name'])): echo $data['receiver_info']['first_name']; endif;?>
		<br />

		last_name_kana
		<?php if(isset($data['receiver_info']['last_name_kana'])): echo $data['receiver_info']['last_name_kana']; endif;?>
		<br />

		first_name_kana
		<?php if(isset($data['receiver_info']['first_name_kana'])): echo $data['receiver_info']['first_name_kana']; endif;?>
		<br />

		zip
		<?php if(isset($data['receiver_info']['zip'])): echo $data['receiver_info']['zip']; endif;?>
		<br />

		state
		<?php if(isset($data['receiver_info']['state'])): echo $data['receiver_info']['state']['name']; endif;?>
		<br />

		address1
		<?php if(isset($data['receiver_info']['address1'])): echo $data['receiver_info']['address1']; endif;?>
		<br />

		address2
		<?php if(isset($data['receiver_info']['address2'])): echo $data['receiver_info']['address2']; endif;?>
		<br />

		address3
		<?php if(isset($data['receiver_info']['address3'])): echo $data['receiver_info']['address3']; endif;?>
		<br />

		tel1
		<?php if(isset($data['receiver_info']['tel1'])): echo $data['receiver_info']['tel1']; endif;?>
		<br />

		tel2
		<?php if(isset($data['receiver_info']['tel2'])): echo $data['receiver_info']['tel2']; endif;?>
		<br />

		tel3
		<?php if(isset($data['receiver_info']['tel3'])): echo $data['receiver_info']['tel3']; endif;?>
		<br />

		fax1
		<?php if(isset($data['receiver_info']['fax1'])): echo $data['receiver_info']['fax1']; endif;?>
		<br />

		fax2
		<?php if(isset($data['receiver_info']['fax2'])): echo $data['receiver_info']['fax2']; endif;?>
		<br />

		fax3
		<?php if(isset($data['receiver_info']['fax3'])): echo $data['receiver_info']['fax3']; endif;?>
		<br />
	<?php endif;?>

	<br />
	<h1>決済方法</h1>
	<br />
	<h2><?php echo $data['payment'];?></h2>
	<br />
	<input type="submit" name="back" value='戻る'>
	<input type="submit" name="check_confirm" value='次へ'>
	<input type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">
</form>