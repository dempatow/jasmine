<div id="contents" class="error">
	<div class="textBox">
		<h2>ERROR</h2>
		<p>
			<?php if (isset($error_message)):?>
				<?php echo $error_message;?>
			<?php else:?>
				処理に失敗しました。<br />しばらく時間を置いてから、再度お試し下さい。
			<?php endif;?>
		</p>
	</div>
	<a href="<?php echo $base_url;?>" class="btn">
		トップページへ
	</a>
</div>