<!DOCTYPE HTML>
<html lang="ja">
	<head>
		<title><?php echo @$title; ?></title>
		<meta name="keywords" content="<?php echo @$keywords; ?>">
		<meta name="description" content="<?php echo @$description; ?>">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $assets_path; ?>/favicon.ico" />
		<script type="text/javascript" src="<?php echo $assets_path; ?>/js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="<?php echo $assets_path; ?>/js/common_sp.js"></script>

		<!-- if script added -->
		<?php echo Asset::render('add_js'); ?>
		<?php echo Asset::render('add_css'); ?>

		<!-- if protocol https -->
		<?php if (isset($is_ssl)): ?>
		<?php endif; ?>

		<!-- google analytics -->
		<?php echo render('google/tracking');?>

	</head>
	<body>

	<!-- over lay file -->
	<?php echo View::forge('layout/overlay_sp'); ?>
	<!-- header -->
	<?php echo $header; ?>
	<!-- content -->
	<?php echo $content; ?>
	<!-- footer -->
	<?php echo $footer; ?>

	</body>
</html>