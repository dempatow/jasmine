<!DOCTYPE HTML>
<html lang="ja">
	<head>
		<title><?php echo @$title; ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="<?php echo @$keywords;?>">
		<meta name="description" content="<?php echo @$description;?>">
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $assets_path; ?>/favicon.ico" />
		<!--[if lt IE 9]><script src="<?php echo $assets_path;?>/js/html5shiv.js"></script><![endif]-->
		<link type="text/css" rel="stylesheet" href="<?php echo $assets_path;?>/css/common.css">
		<script type="text/javascript" src="<?php echo $assets_path;?>/js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="<?php echo $assets_path;?>/js/easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo $assets_path;?>/js/common.js"></script>
		<script type="text/javascript" src="<?php echo $assets_path;?>/js/calender.js"></script>

		<!-- if script added -->
		<?php echo Asset::render('add_js'); ?>
		<?php echo Asset::render('add_css'); ?>

		<!-- if protocol https -->
		<?php if (isset($is_ssl)):?>
		<?php endif; ?>

		<!-- google analytics -->
		<?php echo render('google/tracking');?>

	</head>
	<body>
		<!-- over lay file -->
		<?php echo $overlay;?>

		<div id="wrapper">
		<!--header -->
		<?php echo $header; ?>
		
		<!--mainmenu -->
		<?php echo $menu; ?>

		<div id="mainCont">
			<!--left -->
			<?php echo $left; ?>

			<!-- mypage_header -->
			<?php if (isset($mypage_header)) echo $mypage_header;?>

			<!--content -->
			<?php echo $content; ?>
		</div>

		<!--footer -->
		<?php echo $footer; ?>
		</div>
	</body>
</html>