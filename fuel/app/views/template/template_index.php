<!DOCTYPE HTML>
<html lang="ja">
	<head>
		<title><?php echo @$title; ?></title>
		<meta name="keywords" content="<?php echo @$keywords;?>">
		<meta name="description" content="<?php echo @$description;?>">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="google-site-verification" content="sTbz0BJYnieXlpWdDxPadT_92RFxpsVWOQCCMjdpA-E" />
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $assets_path; ?>/favicon.ico" />
		<!--[if lt IE 9]><script src="<?php echo $assets_path;?>/js/html5shiv.js"></script><![endif]-->
		<link type="text/css" rel="stylesheet" href="<?php echo $assets_path;?>/css/common.css">
		<link type="text/css" rel="stylesheet" href="<?php echo $assets_path;?>/css/index.css">
		<script type="text/javascript" src="<?php echo $assets_path;?>/js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="<?php echo $assets_path;?>/js/easing.1.3.js"></script>
		<script type="text/javascript" src="<?php echo $assets_path;?>/js/common.js"></script>
		<script type="text/javascript" src="<?php echo $assets_path;?>/js/calender.js"></script>
		<script type="text/javascript" src="<?php echo $assets_path;?>/js/index/slider.js"></script>

		<!-- if script added -->
		<?php echo Asset::render('add_js'); ?>
		<?php echo Asset::render('add_css'); ?>

		<!-- if protocol https -->
		<?php if (isset($is_ssl)):?>
		<?php endif; ?>

		<!-- google analytics -->
		<?php echo render('google/tracking');?>

	</head>
	<body class="index">

		<!-- over lay file -->
		<?php echo $overlay;?>

		<?php if (isset($overlay_message) && !empty($overlay_message)):?>
			<div id="overlay" class="on message">
				<img src="<?php echo $assets_path;?>/img/common/parts/loading.gif" alt="ロード中です..." class="lordIcon" />
				<div id="messageBox">
						<span id="messageText"><?php echo $overlay_message;?></span>
						<span class="close">閉じる</span>
				</div>
			</div>
		<?php else:?>
			<div id="overlay">
				<img src="<?php echo $assets_path;?>/img/common/parts/loading.gif" alt="ロード中です..." class="lordIcon" />
				<div id="messageBox">
						<span id="messageText"></span>
						<span class="close"></span>
				</div>
			</div>
		<?php endif;?>
		<div id="wrapper">
		<!--header -->
		<?php echo $header; ?>

		<!--mainmenu -->
		<?php echo $menu; ?>

		<div id="mainCont">
			<div  id="slider">
				<ul id="slideSpace">
					<?php //休日開けにif文を外す; ?>
					<?php if (Fuel::$env !== Fuel::PRODUCTION): ?>
						<li>
							<a href="<?php echo $base_url;?>/item/index/10034">
								<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/11.png" alt="" >
							</a>
						</li>
					<?php else: ?>
						<?php
							$dt = new DateTime();
							$dt->setTimeZone(new DateTimeZone('Asia/Tokyo'));
							$current_time = $dt->format('Y-m-d H:i:s');
							$target_time  = '2015-05-30 23:59:59';
						?>
						<?php if (strtotime($target_time) < strtotime($current_time)):?>
							<li>
								<a href="<?php echo $base_url;?>/item/index/10034">
									<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/11.png" alt="" >
								</a>
							</li>
						<?php endif;?>
					<?php endif;?>

					<li>
						<a href="<?php echo $base_url;?>/item/list?cate=1005">
							<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/10.png" alt="コラボポスター、発売中です！" >
						</a>
					</li>
					<li>
						<a href="<?php echo $base_url;?>/item/list?cate=1006">
							<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/07.png" alt="ニコペロ好評発売中" >
						</a>
					</li>
					<li>
						<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/01.png" alt="" >
					</li>
					<li class="n8">
						<a href="http://clubt.jp/shop/S0000046853.html" target="_blank">
							<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/08.jpg" alt="" >
						</a>
					</li>
					<li class="n6">
						<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/06.png" alt="" >
						<a href="http://make.dmm.com/shop/18122" target="_blank"><img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/06_1.png" alt="" ></a>
					</li>
					<li>
						<img src="<?php echo $assets_path; ?>/img/pages/index/mainImg/04.png" alt="" >
					</li>
				</ul>
				<div id="slidePrev"></div>
				<div id="slideNext"></div>
				<ul id="slideNav">
				</ul>
			</div>
			<!--left -->
			<?php echo $left; ?>
			<!--content -->
			<?php echo $content; ?>
		</div>

		<!--footer -->
		<?php echo $footer; ?>
		</div>
	</body>
</html>