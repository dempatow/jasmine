<?php echo Security::js_fetch_token();?>
<?php if ($is_login):?>
	<div class="nameBar">ようこそ <?php echo Session::get('login_info.last_name').' '.Session::get('login_info.first_name');?> さん</div>
<?php endif;?>

<div id="contents" class="myPageIndex">
	<?php if (isset($data['reserve'])):?>
		<div class="reservation">
			<h3>予約商品があります</h3>
			<ul class="itemList">
				<?php for ($i=0; $i<count($data['reserve']); $i++):?>
					<li id="reserve-<?php echo $data['reserve'][$i]['id'];?>">
						<img src="<?php Config::get('custom_config.url.https_domain');?>/assets/img/item/<?php echo $data['reserve'][$i]['item_id'];?>/120_120.jpg" alt="" />
						<div class="item">
							<span class="reserveId">注文ID:<?php echo $data['reserve'][$i]['id'];?></span>
							<span class="releaseDate"><?php echo date("Y年m月d日",strtotime($data['reserve'][$i]['selling_date']));?>〜販売開始</span>
							<a href="<?php echo Config::get('custom_config.url.https_domain').'/'.$data['reserve'][$i]['name'];?>"><?php echo $data['reserve'][$i]['name'];?></a>
						</div>
						<?php if (Func::check_reserve_date($data['reserve'][$i]['selling_date'])):?>
							<div class="submitBtn off">カートへ</div>
						<?php elseif (isset($data['reserve'][$i]['in_cart']) && $data['reserve'][$i]['in_cart']):?>
							<div class="submitBtn off">カートへ</div>
						<?php else:?>
							<div class="submitBtn purchaseBtn" data-id="<?php echo $data['reserve'][$i]['id'];?>">カートへ</div>
						<?php endif;?>
						<div class="quantity">
							数量/<?php echo $data['reserve'][$i]['count'];?>
						</div>

						<?php if (!(isset($data['reserve'][$i]['in_cart']) && $data['reserve'][$i]['in_cart'])):?>
							<!-- 削除ボタン -->
							<div id="delBtn-<?php echo $data['reserve'][$i]['id'];?>" class="delBtn purchaseCancelBtn" data-id="<?php echo $data['reserve'][$i]['id'];?>">削除</div>
						<?php endif;?>

					</li>
				<?php endfor;?>
			</ul>
		</div>
	<?php endif;?>
	
	<h2>マイページ</h2>
	<ul class="myPageMenu">
		<li class="user"><a href="/user/change/form">登録情報</a></li>
		<li class="password"><a href="/user/passwd/change">パスワード</a></li>
		<li class="address"><a href="/user/address/form">アドレス帳</a></li>
		<li class="point"><a href="/user/point/list">ポイント確認</a></li>
		<li class="orderhistory"><a href="/user/purchasehistory/index">注文履歴</a></li>
		<li class="review"><a href="/user/review">レビュー</a></li>
		<li class="withdrawal"><a href="/user/withdrawal">退会手続</a></li>
	</ul>
</div>