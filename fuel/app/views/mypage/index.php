<?php echo Security::js_fetch_token();?>
<?php if (isset($data['reserve'])):?>
	<div class="reservation">
		<h2>予約商品があります</h2>
		<ul class="itemList">
			<?php for ($i=0; $i<count($data['reserve']); $i++):?>
				<li id="reserve-<?php echo $data['reserve'][$i]['id'];?>">
					<img src="<?php echo Config::get('custom_config.url.ssl_assets_url');?>/img/item/<?php echo $data['reserve'][$i]['item_id'];?>/50_50.jpg" alt="" />
					<div class="item">
						<div class="reservationId">予約No.<?php echo $data['reserve'][$i]['id'];?></div>
						<div class="reservationData"><?php echo date("Y年m月d日",strtotime($data['reserve'][$i]['selling_date']));?>〜販売開始</div>
						<a href="<?php echo Config::get('custom_config.url.https_domain').'/'.$data['reserve'][$i]['name'];?>"><?php echo $data['reserve'][$i]['name'];?></a>
						<div id="ms-<?php echo $data['reserve'][$i]['id'];?>" class="message">
							<?php if (Func::check_reserve_date($data['reserve'][$i]['selling_date'])): ?>
								<!-- 予約期間中 -->
								販売開始までお持ちください！
							<?php elseif (isset($data['reserve'][$i]['in_cart']) && $data['reserve'][$i]['in_cart']): ?>
								<!-- カートに入っている -->
								カートに入っています
							<?php endif; ?>
						</div>
					</div>

					<div class="quantity">
						数量/<?php echo $data['reserve'][$i]['count'];?>
					</div>

					<?php if (Func::check_reserve_date($data['reserve'][$i]['selling_date'])):?>
						<!-- 予約期間中 -->
						<div class="submitBtn non">カートへ</div>
					<?php elseif (isset($data['reserve'][$i]['in_cart']) && $data['reserve'][$i]['in_cart']):?>
						<!-- カートに入っている -->
						<div class="submitBtn non">カートへ</div>
					<?php else:?>
						<!-- 販売期間中 -->
						<div class="submitBtn purchaseBtn" data-id="<?php echo $data['reserve'][$i]['id'];?>">カートへ</div>
					<?php endif;?>

					<?php if (!isset($data['reserve'][$i]['in_cart'])):?>
						<!-- キャンセルボタン -->
						<div id="del-<?php echo $data['reserve'][$i]['id'];?>" class="delBtn purchaseCancelBtn" data-id="<?php echo $data['reserve'][$i]['id'];?>">×</div>
					<?php endif;?>

				</li>
			<?php endfor;?>
		</ul>
	</div>
<?php endif;?>
<div id="contents" class="myPageTop">
	<h2>
		お客様情報
	</h2>
	<dl class="form">
		<dt>
			お名前
		</dt>
		<dd>
			<?php if (isset($data['last_name'])) echo $data['last_name'];?>&nbsp;<?php if (isset($data['first_name'])) echo $data['first_name'];?>
		</dd>
		<dt>
			法人名・団体名
		</dt>
		<dd>
			<?php if (isset($data['corporate_name'])) echo $data['corporate_name'];?>
		</dd>
		<dt>
			郵便番号
		</dt>
		<dd>
			<?php if (isset($data['zip'])) echo substr($data['zip'],0,3).'-'.substr($data['zip'],3,6);?>
		</dd>
		<dt>
			住所
		</dt>
		<dd>
			<?php if (isset($data['state_cd'])) echo $data['state_cd'];?>
			<?php if (isset($data['address1'])) echo $data['address1'];?><br /><?php if (isset($data['address2'])) echo $data['address2'];?>
			<?php if (isset($data['address3'])) echo $data['address3']; ?>
		</dd>
		<dt>
		お電話番号
		</dt>
		<dd>
			<?php if (isset($data['tel1']) && isset($data['tel2']) && isset($data['tel3'])) echo $data['tel1'].'-'.$data['tel2'].'-'.$data['tel3'];?>
		</dd>
		<dt>
			メールアドレス
		</dt>
		<dd>
			<?php if (isset($data['mail'])) echo $data['mail'];?>
		</dd>
		<?php if (isset($data['is_use_card']) && $data['is_use_card']):?>
			<dt>
				クレジットカード
			</dt>
			<dd>
				登録済（情報を変更する場合は<a href="<?php echo $base_url;?>/user/change/card">こちら</a>）
			</dd>
		<?php endif;?>
	</dl>
</div>
<input id="token" type="hidden" name="<?php echo \Config::get('security.csrf_token_key');?>" value="<?php echo \Security::fetch_token();?>">