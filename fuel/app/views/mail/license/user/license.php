<?php echo $company_name."\n"; ?>
<?php echo $charge_name; ?>様

この度はパンピキッズへお問い合わせ下さいまして、誠にありがとうございます。
以下の内容で承りましたので、ご確認を宜しくお願い申し上げます。
確認次第、担当者よりご連絡いたします。

----------
貴社名:<?php echo $company_name; ?>
ご担当者お名前:<?php echo $charge_name; ?>
サイトURL<?php echo $url; ?>
電話番号:<?php echo $tel1; ?><?php echo $tel2; ?><?php echo $tel3; ?>
メールアドレス:<?php echo $contact_mail; ?>
お問い合わせ内容<?php echo $detail; ?>
----------


■□-------------------------------------------------------------
■　キャラと絵本のパンピキッズ
http://www.panpikids.com/
東京都文京区後楽1-4-14　後楽森ビル8階
お問い合せ: contact@panpikids.com