注文が入ったよ(´･ヮ･`)

<?php if (isset($data['purchase_info']['last_name']) && isset($data['purchase_info']['first_name'])) echo $data['purchase_info']['last_name'].' '.$data['purchase_info']['first_name'];?> 様

この度は、<?php echo Config::get('custom_config.shop_name');?>をご利用いただきまして誠にありがとうございます。

お客様のご注文を下記の内容で承りましたのでご連絡申し上げます。
ご不明な点ご質問等ございましたら、お気軽にお問合せ下さいますよう
お願い申し上げます。

■ご注文内容 -------------------------------------------------------
   ◆ ご注文番号 <?php if (isset($data['purchase_info']['purchase_id'])) echo $data['purchase_info']['purchase_id']; echo "\n";?>
   ◆ ご注文日時 <?php if (isset($data['date'])) echo $data['date']; echo "\n";?>
<?php $item_value = 0;?>
   ——————————————————————
<?php for ($i=0; $i<count($data['cart_dt']); $i++):?>
    品名:<?php echo $data['cart_dt'][$i]['name']."\n";?>
<?php if (isset($data['cart_dt'][$i]['special_select'])):?>
<?php for ($j=0; $j<count($data['cart_dt'][$i]['special_select']); $j++):?>
    ・<?php echo $data['cart_dt'][$i]['special_select'][$j]['key'].'  '.$data['cart_dt'][$i]['special_select'][$j]['value'].'  '.$data['cart_dt'][$i]['special_select'][$j]['count']."\n";?>
<?php endfor;?>
<?php endif;?>
    価格:<?php echo Func::get_item_value($data['cart_dt'][$i])."\n";?>
    数量:<?php echo $data['cart_dt'][$i]['count']."\n";?>
<?php $item_value += $value_tmp = Func::get_item_value($data['cart_dt'][$i]) * $data['cart_dt'][$i]['count'];?>
    小計:<?php echo $value_tmp."円\n";?>
   ——————————————————————
<?php endfor;?>
   商品合計 :<?php echo $item_value;?>円
   ——————————————————————
   送料 :<?php if (isset($data['sending_info']['postage'])): $item_value += $data['sending_info']['postage'];echo $data['sending_info']['postage'];endif;?>円
<?php if (isset($data['is_login']) && $data['is_login']):?>
   ——————————————————————
   ポイント使用 :<?php if (isset($data['purchase_info']['use_point']) && !empty($data['purchase_info']['use_point'])): $item_value -= (Int)$data['purchase_info']['use_point'];echo ' - '.$data['purchase_info']['use_point']; else: echo '0'; endif;?>point
<?php endif;?>
   ——————————————————————
   合計金額 : <?php echo number_format($item_value);?>円


■ご注文者 ---------------------------------------------------------
<?php if (isset($data['purchase_info']['corporate_name']) && !empty($data['purchase_info']['corporate_name'])):?>
	[法人名・団体名] <?php echo $data['purchase_info']['corporate_name']."\n";?>
<?php endif;?>
<?php if (isset($data['purchase_info']['corporate_name_kana']) && !empty($data['purchase_info']['corporate_name_kana'])):?>
	[法人名・団体名(かな)] <?php echo $data['purchase_info']['corporate_name_kana']."\n";?>
<?php endif;?>
<?php if (isset($data['purchase_info']['last_name']) && !empty($data['purchase_info']['last_name']) && isset($data['purchase_info']['first_name']) && !empty($data['purchase_info']['first_name'])):?>
	[お名前] <?php echo $data['purchase_info']['last_name'].' '.$data['purchase_info']['first_name']."\n";?>
<?php endif;?>
<?php if (isset($data['purchase_info']['last_name_kana']) && !empty($data['purchase_info']['last_name_kana']) && isset($data['purchase_info']['first_name_kana']) && !empty($data['purchase_info']['first_name_kana'])):?>
	[お名前(かな)] <?php echo $data['purchase_info']['last_name_kana'].' '.$data['purchase_info']['first_name_kana']."\n";?>
<?php endif;?>
<?php if (isset($data['purchase_info']['zip']) && !empty($data['purchase_info']['zip'])):?>
	[郵便番号] <?php echo substr($data['purchase_info']['zip'],0,3).'-'.substr($data['purchase_info']['zip'],3,6)."\n";?>
<?php endif;?>
<?php if (isset($data['purchase_info']['address1']) && !empty($data['purchase_info']['address1']) && isset($data['purchase_info']['address2']) && !empty($data['purchase_info']['address2'])):?>
	[ご住所]<?php echo Model_Mt_State::get_state()[$data['purchase_info']['state_cd']]['name'].' '.$data['purchase_info']['address1'].' '.$data['purchase_info']['address2'].' ';if (isset($data['purchase_info']['address3']) && !empty($data['purchase_info']['address3'])):echo $data['purchase_info']['address3'];else:echo "\n";endif;?>
<?php endif;?> 
<?php if (isset($data['purchase_info']['tel1']) && !empty($data['purchase_info']['tel1']) && isset($data['purchase_info']['tel2']) && !empty($data['purchase_info']['tel2']) && isset($data['purchase_info']['tel3']) && !empty($data['purchase_info']['tel3'])):?>
	[お電話番号] <?php echo $data['purchase_info']['tel1'].'-'.$data['purchase_info']['tel2'].'-'.$data['purchase_info']['tel3']."\n";?>
<?php endif;?>
<?php if (isset($data['purchase_info']['fax1']) && !empty($data['purchase_info']['fax1']) && isset($data['purchase_info']['fax2']) && !empty($data['purchase_info']['fax2']) && isset($data['purchase_info']['fax3']) && !empty($data['purchase_info']['fax3'])):?>
	[FAX番号] <?php echo $data['purchase_info']['fax1'].'-'.$data['purchase_info']['fax2'].'-'.$data['purchase_info']['fax3']."\n";?>
<?php endif;?>
<?php if (isset($data['purchase_info']['mail_purchase']) && !empty($data['purchase_info']['mail_purchase'])):?>
	[メールアドレス] <?php echo $data['purchase_info']['mail_purchase']."\n";?>
<?php endif;?>

■お届け先 ---------------------------------------------------------
<?php if (isset($data['purchase_info']['receiver']) && $data['purchase_info']['receiver'] === 'own'):?>
	購入者様と同一です
<?php else:?>
<?php if (isset($data['receiver_info']['corporate_name']) && !empty($data['receiver_info']['corporate_name'])):?>
	[法人名・団体名] <?php echo $data['receiver_info']['corporate_name']."\n";?>
<?php endif;?>
<?php if (isset($data['receiver_info']['last_name']) && !empty($data['receiver_info']['last_name']) && isset($data['receiver_info']['first_name']) && !empty($data['receiver_info']['first_name'])):?>
	[お名前] <?php echo $data['receiver_info']['last_name'].' '.$data['receiver_info']['first_name']."\n";?>
<?php endif;?>
<?php if (isset($data['receiver_info']['last_name_kana']) && !empty($data['receiver_info']['last_name_kana']) && isset($data['receiver_info']['first_name_kana']) && !empty($data['receiver_info']['first_name_kana'])):?>
	[お名前(かな)] <?php echo $data['receiver_info']['last_name_kana'].' '.$data['receiver_info']['first_name_kana']."\n";?>
<?php endif;?>
<?php if (isset($data['receiver_info']['zip']) && !empty($data['receiver_info']['zip'])):?>
	[郵便番号] <?php echo substr($data['receiver_info']['zip'],0,3).'-'.substr($data['receiver_info']['zip'],3,6)."\n";?>
<?php endif;?>
<?php if (isset($data['receiver_info']['address1']) && !empty($data['receiver_info']['address1']) && isset($data['receiver_info']['address2']) && !empty($data['receiver_info']['address2'])):?>
	[ご住所]<?php echo Model_Mt_State::get_state()[$data['receiver_info']['state_cd']]['name'].' '.$data['receiver_info']['address1'].' '.$data['receiver_info']['address2'].' ';if (isset($data['receiver_info']['address3']) && !empty($data['receiver_info']['address3'])):echo $data['receiver_info']['address3'];else:echo "\n";endif;?>
<?php endif;?> 
<?php if (isset($data['receiver_info']['tel1']) && !empty($data['receiver_info']['tel1']) && isset($data['receiver_info']['tel2']) && !empty($data['receiver_info']['tel2']) && isset($data['receiver_info']['tel3']) && !empty($data['receiver_info']['tel3'])):?>
	[お電話番号] <?php echo $data['receiver_info']['tel1'].'-'.$data['receiver_info']['tel2'].'-'.$data['receiver_info']['tel3']."\n";?>
<?php endif;?>
<?php if (isset($data['receiver_info']['fax1']) && !empty($data['receiver_info']['fax1']) && isset($data['receiver_info']['fax2']) && !empty($data['receiver_info']['fax2']) && isset($data['receiver_info']['fax3']) && !empty($data['receiver_info']['fax3'])):?>
	[FAX番号] <?php echo $data['purchase_info']['fax1'].'-'.$data['purchase_info']['fax2'].'-'.$data['purchase_info']['fax3']."\n";?>
<?php endif;?>
<?php endif;?>

■発送方法    -----------------------------------------------------
	<?php if (isset($data['sending_info']['name'])) echo $data['sending_info']['name']."\n";?>
<?php if (isset($data['sending_info']['date_specified']) && $data['sending_info']['date_specified']):?>
	日にち指定:<?php echo $data['sending_info']['date_specified']."\n";?>
<?php endif;?>
<?php if (isset($data['sending_info']['time_specified'])):?>
	時間帯指定:<?php echo $data['sending_info']['time_specified']."\n";?>
<?php endif;?>

■お支払い方法 -----------------------------------------------------
	<?php echo Model_Mt_Payment::select_payment()[$data['purchase_info']['payment_cd']]['name']."\n";?>
<?php if ((Int)$data['purchase_info']['payment_cd'] === 1): ?>
<?php //カード支払い ?>
<?php elseif ((Int)$data['purchase_info']['payment_cd'] === 2): ?>
<?php //銀行振込 ?>
	【振込口座】 三井住友銀行銀座支店　普通7694562　エスピーコウコク

	当店指定の口座へお振り込みください。
	おそれいりますが、振込手数料は、
	ご負担くださいますようお願いいたします。

	入金確認後、商品を発送いたします。
<?php elseif ((Int)$data['purchase_info']['payment_cd'] === 3): ?>
	商品お届けの際、運送会社のドライバーに直接お支払いください。（現金のみ）
	※代引き手数料はお客様でご負担ください。
<?php endif;?>

<?php if (isset($data['purchase_info']['free1']) && !empty($data['purchase_info']['free1'])):?>
■その他 ----------------------------------------------------------
<?php echo $data['purchase_info']['free1']."\n";?>
-----------------------------------------------------------------
<?php endif;?>

発送の準備が済み次第、商品を送らせていただきます。
商品の発送後、改めてご連絡いたしますので、
今しばらくお待ちください。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
<?php echo Config::get('custom_config.shop_name');?>
http://shop.panpikids.com/

〒112-0004　東京都文京区後楽1-4-14　後楽森ビル8階
お問い合せ: contact@panpikids.com
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━