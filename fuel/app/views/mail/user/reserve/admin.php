予約が入りました!
内容は以下の通りです。

<?php if (isset($data['last_name']) && isset($data['first_name'])) echo $data['last_name'].' '.$data['first_name'];?> 様

この度は、<?php echo Config::get('custom_config.shop_name');?>をご利用いただきまして誠にありがとうございます。

商品のご予約を下記の内容で承りましたのでご連絡申し上げます。
ご不明な点ご質問等ございましたら、お気軽にお問合せ下さいますよう
お願い申し上げます。

■ご予約内容 -------------------------------------------------------
   ◆ ご予約番号 <?php if (isset($data['reserve_id'])) echo $data['reserve_id']; echo "\n";?>
   ◆ ご予約日時 <?php if (isset($data['date'])) echo $data['date']; echo "\n";?>
   ◆ 販売開始日 <?php if (isset($data['selling_date'])) echo date("Y年m月d日",strtotime($data['selling_date'])); echo "\n";?>
   ——————————————————————
    品名:<?php echo $data['name']."\n";?>
    価格:<?php echo number_format(Func::get_item_value($data['item_id']))."円\n";?>
    数量:<?php echo $data['count']."\n";?>
-----------------------------------------------------------------

ご予約頂いた商品は販売開始日以降に、サイトマイページよりご購入頂く事が出来ます。
<?php echo Config::get('custom_config.url.https_domain').'/user/mypage';?>


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
<?php echo Config::get('custom_config.shop_name');?>
http://shop.panpikids.com/

〒112-0004　東京都文京区後楽1-4-14　後楽森ビル8階
お問い合せ: contact@panpikids.com
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━