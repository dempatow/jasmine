<?php echo $data['last_name'].' '.$data['first_name'].' ';?> 様

この度は、<?php echo Config::get('custom_config.shop_name');?>の会員にご登録いただきまして、
誠にありがとうございます。
お客様の登録が完了いたしましたのでご連絡申し上げます。

会員ID：<?php echo $data['mail']."\n";?>
パスワード：<?php echo $data['passwd'];?>

会員情報を確認する場合は、当店のトップページ上部にある
「ログイン」からご確認ください。

今後とも<?php echo Config::get('custom_config.shop_name');?>をよろしくお願いいたします。

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
<?php echo Config::get('custom_config.shop_name');?>
http://shop.panpikids.com/

〒112-0004　東京都文京区後楽1-4-14　後楽森ビル8階
お問い合せ: contact@panpikids.com
━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━