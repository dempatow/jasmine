<span class="changeCal first" data-monthyear="<?php echo date('Ym', mktime(0, 0, 0, $month , 1, $year - 1));?>">&lt;&lt;</span><!--
--><span class="changeCal prev" data-monthyear="<?php echo date('Ym', mktime(0, 0, 0, $month - 1 , 1, $year));?>">&lt;</span><!--
--><h2><?php echo $year . '年' . $month . '月';?></h2><!--
--><span class="changeCal next" data-monthyear="<?php echo date('Ym', mktime(0, 0, 0, $month + 1 , 1, $year));?>">&gt;</span><!--
--><span class="changeCal last" data-monthyear="<?php echo date('Ym', mktime(0, 0, 0, $month , 1, $year + 1));?>">&gt;&gt;</span>


<table>
	<tbody>

		<!-- 曜日 -->
		<tr>
			<?php $i = 0; ?>
			<?php while ($i <= 6):?>
				<td class="week"> <?php echo $weekday[$i];?></td>
				<?php $i++;?>
			<?php endwhile;?>
		</tr>

		<!-- 日 -->
		<tr>
			<!-- 始めのblank -->
			<?php for($i=0; $i<$first_blank; $i++):?>
				<td></td>
			<?php endfor;?>
			<?php $i=$first_blank;?>

			<?php for ($days = 1; checkdate($month, $days, $year); $days++):?>

				<!-- row(tr)の終了タグと開始タグ -->
				<?php if ($i > 6): ?>
					</tr>
					<tr>
					<?php $i = 0;?>
				<?php endif;?>

				<!-- 日によってバックグラウンドを変更する -->
				<td <?php echo CalenderHelper::get_day_class($days, $i);?>><?php echo $days;?></td>
					<?php $i++;?>
			<?php endfor;?>

			<!-- 表示月のうち日がない日 -->
			<?php while ($i < 7):?>
				<td></td>
				<?php $i++;?>
			<?php endwhile;?>
		</tr>

	</tbody>
</table>
