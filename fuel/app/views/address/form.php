<div id="contents" class="destinationEdit">
	<h2>
		お届け先の変更・登録
	</h2>
	<div class="destinationList">
		<ul id="parentLi">
			<?php for ($i=0; $i<count($data['address']); $i++):?>
				<li id="record-<?php echo $data['address'][$i]['id'];?>" class="" >
					<div class="name">
						<span><?php if (isset($data['address'][$i]['corporate_name']))echo $data['address'][$i]['corporate_name'];?></span>
						<h3><?php if (isset($data['address'][$i]['last_name']) && isset($data['address'][$i]['first_name']))echo $data['address'][$i]['last_name'].' '.$data['address'][$i]['first_name'];?> </h3>
					</div>
					<div class="data">
						<span class="zip">
							<?php if (isset($data['address'][$i]['zip'])) echo substr($data['address'][$i]['zip'],0,3).'-'.substr($data['address'][$i]['zip'],3,6)?>
							<?php if (isset($data['address'][$i]['state_cd']) && isset($data['mt']['state_cd'][$data['address'][$i]['state_cd']]['name'])) echo $data['mt']['state_cd'][$data['address'][$i]['state_cd']]['name'];?>
							<?php if (isset($data['address'][$i]['address1']))echo $data['address'][$i]['address1'];?>
						</span>
						<span class="address">
							<?php if (isset($data['address'][$i]['address2']))echo $data['address'][$i]['address2'];?>
							<?php if (isset($data['address'][$i]['address3']))echo $data['address'][$i]['address3'];?>

						</span>
					</div>
					<div class="edit">
						<span class="delBtn" data-id="<?php echo $data['address'][$i]['id'];?>">×</span>
						<span class="editBtn cangeBtn" data-id="<?php echo $data['address'][$i]['id'];?>">変更する</span>
					</div>
				</li>
			<?php endfor;?>

			<?php if (count($data['address']) === 0):?>
				<li class="blank">登録されているおと届け先データはありません。右下のボタンから登録することができます！</li>
			<?php endif;?>

		</ul>
		<?php if(count($data['address'])<=4):?>
			<span class="addData cangeBtn" data-id="new">
				新しい連絡先を追加
			</span>
		<?php elseif (count($data['address']) >= Model_Dt_Receiveraddress::DISP_COUNT):?>
			<span class="errorText">連絡先の登録上限です</span>
		<?php endif;?>
	</div>

	<?php if (isset($_inner_form))echo $_inner_form;?>

</div>