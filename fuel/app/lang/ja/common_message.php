<?php

/**
 * 共通メッセージ
 * common controllerでロードしています
 */

return array(

	/**
	 *正常系
	 *
	 */
	'success_test' => '正常系テストメッセージ',
	'success_test2' => 'name正常系テストメッセージその2',

	/**
	 *異常系
	 *
	 */
	'error_test' => '異常系テストメッセージ',
	'error_false_db' => 'データベース処理に失敗しました' ,
);