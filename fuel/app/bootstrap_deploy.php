<?php

// Load in the Autoloader
require COREPATH.'classes'.DIRECTORY_SEPARATOR.'autoloader.php';
class_alias('Fuel\\Core\\Autoloader', 'Autoloader');

// Bootstrap the framework DO NOT edit this
require COREPATH.'bootstrap.php';

Autoloader::add_classes(
	array(
		// Add classes you want to override here
		'Common'         => APPPATH.'classes/spcore/common.php',
		'CustomAuth'     => APPPATH.'classes/spcore/customauth.php',
		'Device'         => APPPATH.'classes/spcore/device.php',
		'Func'           => APPPATH.'classes/spcore/func.php',
		'DbHelper'       => APPPATH.'classes/spcore/dbhelper.php',
		'RedisHelper'    => APPPATH.'classes/spcore/redishelper.php',
		'CustomAuth'     => APPPATH.'classes/spcore/customauth.php',
		'Validation'     => APPPATH.'classes/model/validation.php',
		'UploadImage'    => APPPATH.'classes/spcore/uploadimage.php',
		'FacebookAPI'    => APPPATH.'classes/spcore/facebook/facebookapi.php',
		'TwitterAPI'     => APPPATH.'classes/spcore/twitter/twitterapi.php',
		'GoogleAPI'      => APPPATH.'classes/spcore/google/googleapi.php',
		'CalenderHelper' => APPPATH.'classes/spcore/calenderhelper.php',
		'Payment'        => APPPATH.'classes/spcore/payment.php',
		'Purchase'       => APPPATH.'classes/spcore/purchase.php',
		'Point'          => APPPATH.'classes/spcore/point.php',
		'ViewHelper'     => APPPATH.'classes/spcore/viewhelper.php',
		'SearchHelper'   => APPPATH.'classes/spcore/searchhelper.php',
		'SiteMapHelper'  => APPPATH.'classes/spcore/sitemaphelper.php',
	)
);

// Register the autoloader
Autoloader::register();

/**
 * Your environment.  Can be set to any of the following:
 *
 * Fuel::DEVELOPMENT
 * Fuel::TEST
 * Fuel::STAGE
 * Fuel::PRODUCTION
 * 環境設定はapachceのhttpd.confの一番下に書く事にしました。
 * ex) SetEnv FUEL_ENV DEVELOPMENT
 * SetEnv FUEL_ENV STAGE
 */
Fuel::$env = (isset($_SERVER['FUEL_ENV']) ? $_SERVER['FUEL_ENV'] : 'development');

// Initialize the framework with the config file.
Fuel::init('config.php');
