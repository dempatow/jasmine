<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Searchword Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_View_Searchword extends \Orm\Model
{
	protected static $table_name = 'v_searchword';

	protected static $_properties = array(
	);

	/**
	 * select_item_id_by_word
	 */
	public static function select_item_id_by_word ($words_array)
	{
		$query = DB::select ('id')
					->distinct(true)
					->from(self::$table_name);

		for ($i=0; $i<count($words_array); $i++)
		{
			$query->or_where('word', 'like', "%$words_array[$i]%");
		}
		return DbHelper::query_exec($query);
	}
}