<?php
/**
 * Validation
 *
 * 独自Validationクラスです
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package  app
 * @extends  Validation
 */

//parent::instance()->input('sms_code');

class Validation extends \fuel\Core\Validation
{
//----------------------dempa 検証済みメソッド----------------------

	/**
	 * checkdrawalpasswd
	 * 
	 * 退会時のpasswdを検査するメソッド
	 * 
	 * @param Strign $val
	 * @return bool
	 */
	public static function _validation_checkwithdrawalpasswd ($val)
	{
		$reuslt = false;
		$user = CustomAuth::get_login_info();

		if (!empty($user) && isset($user['id']))
		{
			$query = Model_Dt_User::get_query_select_user_by_userid($user['id'], 0);
			$db_user = DbHelper::query_exec($query,true);

			if (
				$db_user &&
				isset($db_user['passwd']) &&
				Auth::instance()->hash_password((string)$val) === (string)$db_user['passwd']
			)
				$reuslt = true;
		}
		return $reuslt;
	}

	/**
	 * checkdatespecified
	 * 
	 * 日時指定の検証を行うメソッド
	 * 
	 * @return bool
	 */
	public function _validation_checkdatespecified ($val)
	{
		$post_time_specified = $this->input('time_specified');//時間指定入力値
		/******************
		 * どちらも入力選択がされていない場合(指定なしの場合)
		 ******************/
		if (
			(!isset($val)|| empty($val)) &&
			(!isset($post_time_specified) || empty($post_time_specified))
		)
			return true;

		//発送先郵便番号
		$receiver_zip        = Session::get('receiver_info.zip', false)? Session::get('receiver_info.zip'): Session::get('purchase_info.zip',false);
		//発送先都道府県番号
		$receiver_state_cd   = Session::get('receiver_info.state_cd', false)? Session::get('receiver_info.state_cd'): Session::get('purchase_info.state_cd',false);
		$receiver_area_cd    = (Int)Model_Mt_Area::get_area_by_state($receiver_state_cd, $receiver_zip)['id'];
		$specified_end_day   = Model_Mt_Maxdatespecified::get_day();
		$sending_cd          = $this->input('sending_cd');    //発送法コード

		//発送方法とエリアコードをキーに日付マスタを取得する
		$mt_date = Model_Mt_Datespecified::select_by_sending_cd_and_area_cd($sending_cd, $receiver_area_cd);

		if (!$mt_date)
			return false;

		/******************
		 * 日付指定のチェック
		 ******************/
		if (isset($val) && !empty($val))
		{
			$date_list = CalenderHelper::get_designated_date_array($mt_date['from_day'], $specified_end_day);

			$is_OK = false;
			for ($i=0; $i<count($date_list); $i++)
			{
				if ($val === $date_list[$i]['value'])
				{
					$is_OK = true;
					break;
				}
			}
			if (!$is_OK)
				return false;
		}
		/******************
		 * 時間帯指定のチェック
		 ******************/
		if (isset($post_time_specified) && !empty($post_time_specified))
		{
			$time_mt = Model_Dt_Timespecified::select_by_date_id_join_mt ($mt_date['date_id']);
			if (!$time_mt)
				return false;
			$is_OK = false;
			for ($i=0; $i<count($time_mt); $i++)
			{
				if ((Int)$time_mt[$i]['id'] ===(Int)$post_time_specified)
				{
					$is_OK = true;
					break;
				}
			}
			if (!$is_OK)
				return false;
		}
		return true;//全て通過した場合はtrueを返却する
	}

	/**
	 * checkusepoint
	 * 
	 * 使用ポイント検証メソッド
	 */
	public function _validation_checkusepoint ($val, $is_null = false)
	{
		$result = false;

		//入力なしOKかつ入力が無い場合はそのまま返す
		if ($is_null && empty($val))
			return true;

		/**
		 * 必要パラメタチェック
		 */
		$cart_dt = Session::get('cart_dt', false);
		$sending_info = Session::get('sending_info', false);
		$sending_cd   = Session::get('sending_cd', false);
		if (count($cart_dt)> 0 && CustomAuth::get_login_info() && $sending_info && $sending_cd)
		{
			$total_amount = 0;
			//商品合計金額
			$sum_data = Func::get_cart_info_sum($cart_dt);
			$total_amount += (Int)$sum_data['total_amount'];

			//送料計算
			if (!(isset($sending_info['is_free']) && $sending_info['is_free']))
			{
				//送料無料でなかった場合
				for ($i=0; $i<count($sending_info['postages']); $i++)
				{
					if ((Int)$sending_info['postages'][$i]['id'] === (Int)$sending_cd)
						$total_amount+=(Int)$sending_info['postages'][$i]['postage'];
				}
			}
		}

		//使用ポイントが(商品金額+送料)より少ない かつ 自分の所持ポイントより少ない
		if ($total_amount >= (Int)$val && Point::get_point() >= (Int)($val))
		{
			$result = true;
		}
		return $result;
	}

	/**
	 * checkpaymentprocess
	 * 
	 * 有効な　payment_process_cdであるか検査します
	 */
	public function _validation_checkpaymentprocess ($val)
	{
		$result = false;
		$payment_cd = $this->input('payment_cd');
		$login_info = CustomAuth::get_login_info();

		if (
			!empty($payment_cd) &&
			(Int)$payment_cd === Model_Mt_Payment::CARD_PAYMENT_ID
		)
		{
			//カードを選択されているのにprocess_cdが空の場合はエラーを返す
			if (empty($val))
				return false;

			/**********************
			 * カード決済が選択された時
			 **********************/
			if ($login_info)
			{
				/*****************
				 * ログインユーザー
				 *****************/
				$purchase_cnt = count(Model_Dt_Purchasehistory::select_credit_data($login_info['id']));

				if (
					($purchase_cnt > 0) &&
					(Int)$val === (Int)Payment::$PROSESS_REGISTERED
				)
				{
					//登録済み課金
					$result = true;
				}
				else if ($purchase_cnt === 0 && (Int)$val === (Int)Payment::$PROSESS_FIRST)
				{
					//初回課金
					$result = true;
				}
			}
			else if ((Int)$val === (Int)Payment::$PROSESS_FIRST)
			{
				/*****************
				 * 非ログインユーザー
				 *****************/
				//初回課金
				$result = true;
			}
		}
		else
		{
			//カード課金が選択されていない時
			$result = true;
		}
		return $result;
	}

	/**
	 * checkavailsending
	 * 
	 * 有効なsenidng_cdであるか検査します
	 */
	public static function _validation_checkavailsending ($val)
	{
		$result = false;
		$sending_cd        = $val;
		$cart_dt           = Session::get('cart_dt', false);     //cart session
		$avail_sendings    = Session::get('sending_info', false);//使用可能な発送方法データを格納する
		$receiver_state_cd = ''; //お届け先state_cd
		$receiver_zip = '';      //お届け先郵便番号

		if ((bool)Session::get('purchase_info.receiver',false))
		{
			//購入者住所を送り先と指定している場合
			$receiver_state_cd = Session::get('purchase_info.state_cd', false);
			$receiver_zip      = Session::get('purchase_info.zip', false);
		}
		else
		{
			//購入者住所と送り先が違う場合
			$receiver_state_cd = Session::get('receiver_info.state_cd', false);
			$receiver_zip      = Session::get('receiver_info.zip', false);
		}

		try
		{
			//--- 各種sessionが取得出来なかったらfalse
			if (!$cart_dt || !$receiver_state_cd || !$receiver_zip)
				throw new Exception('sessionなし');

			//--- 使用可能な発送方法データ取得
			if (!$avail_sendings)
				throw new Exception('sessionなし[sending_info]');

			if (isset($avail_sendings['highest_cd']) && !empty($avail_sendings['highest_cd']))
			{
				//---優先発送の場合
				(Int)$avail_sendings['highest_cd'] === (Int)$sending_cd and $result = true;
			}
			else
			{
				//---優先発送以外
				for ($i=0; $i<count($avail_sendings['postages']); $i++)
				{
					$avail_sendings['postages'][$i]['id'] === $val and $result = true;
				}
			}
		}
		catch (Exception $e)
		{
			$result = false;
			Func::write_exception_log($e,true);
		}
		return $result;
	}

	/**
	 * checkcurrentpasswd
	 * 
	 * ログイン中でないと使用出来ません
	 * @param  val
	 * @return bool
	 */
	public static function _validation_checkcurrentpasswd($val)
	{
		$uid         = Session::get('login_info.id', false);
		$hash_passwd = Auth::instance()->hash_password($val);
		return (count(Model_Dt_User::select_user_by_uid_and_passwd($uid, $hash_passwd))>0)?true: false;
	}

	/**
	 * checklogin
	 * 
	 * @param val
	 * @return bool
	 */
	public static function _validation_checklogin($val)
	{
		return (count(Session::get('login_info',null))>0)? true: false;
	}

	/**
	 * check_mtvalue
	 * 
	 * @param val
	 * @return bool
	 */
	public static function _validation_checkmtvalue($val, $table_name, $is_emty = false)
	{
		$result = true;

		//空許容かつからの場合はtrue
		if ($is_emty && empty($val))
		{
			//Debug::dump($result);exit;
			return $result;
		}

		$mt = RedisHelper::get_master($table_name);
		if (!isset($mt[$val]))
		{
			$result = false;
			//Debug::dump($result);exit;
		}

		//Debug::dump($result);exit;

		return $result;
	}

	/**
	 * chechZip
	 *   @param
	 *   $val == form入力値（郵便番号）3348794
	 * 
	 *   @return
	 *   bool
	 */
	public static function _validation_checkZip($val)
	{
		return ( preg_match('/^-?[0-9]+$/',$val) && strlen($val) == 7 )? true: false;
	}

	/**
	 * checkcharNumJp
	 * バイト数で文字数を検査します
	 *
	 *   @param  form入力値
	 *   @return bool
	 */
	public static function _validation_checkCharNumJp($val, $limit)
	{
		mb_internal_encoding("UTF-8");
		$num = mb_strlen($val);
		return ($num <= $limit)? true: false;
	}

	/**
	 * checkRangeCharNumJp
	 * バイト数で文字数の範囲を検査します
	 *
	 *   @param  form入力値
	 *   @return bool
	 */
	public static function _validation_checkRangeCharNumJp($val, $min, $max)
	{
		Debug::dump('in charNumJp');exit;
		mb_internal_encoding("UTF-8");
		$num = mb_strlen($val);
		return ($min <= $num && $num <= $max)? true: false;
	}

	/**
	 * checkisSame
	 * 
	 * $val と モデルに渡された値が同一であるか比較します
	 * 
	 * @param $val $other_filed
	 * @return boolean
	 *
	 */
	public function _validation_checkIsSame($val, $other_filed,$other_filed_jp)
	{
		$original = $this->input($other_filed);
		//Debug::dump($val,$original);exit;

		return ($val == $original)? true: false;
	}

	/**
	 * 半角英数字検証メソッド
	 *   半角英数字かどうかを検証します
	 *   それ以外の値はそのまま返します
	 *   @param  int || string
	 *   @return bool
	 */
	public static function _validation_checkAlphaNumeric($val)
	{
		return (preg_match( '/^[a-zA-Z0-9]+$/', $val))?true: false;
	}

	/**
	 * checkEmail
	 *
	 * @param   
	 * @return boolean
	 *
	 */
	public static function _validation_checkEmail($val)
	{
		return (preg_match('/^[-._a-zA-Z0-9\/]+@[-._a-z0-9]+\.[a-z]{2,4}$/', $val))?true:false;
	}

	/**
	 * checkChangeMailDb
	 *
	 * @parma string mail
	 * @return boolean
	 *
	 */
	public static function _validation_checkChangeMailDb($val)
	{
		$result = false;
		$login_info = CustomAuth::get_login_info();

		if (!isset($login_info['mail']) || !isset($login_info['id']))
			return $result;

		$query = Db::select('id')
				->from(Model_Dt_User::$table_name)
				->where('id', '<>', $login_info['id'])
				->and_where('del_flg', '=',0)
				->and_where('mail', '=', $val);

		if (
			$login_info['mail'] === $val ||
			count(DbHelper::query_exec($query)) === 0
		)
			$result = true;

		return $result;
	}

	/**
	 * checkUniquUserMail
	 *
	 * @param $denial_flg default null
	 * //該当するレコードがあるかどうか判定する時はflgをたてる
	 * 
	 * 会員用メールアドレス重複チェックメソッド
	 * (del_flg!=1)
	 * @return boolean
	 *
	 */
	public static function _validation_checkUniquUserMail($val,$denial_flg = null)
	{
		$result = false;
		$query = Model_Dt_User::get_query_select_user_by_mail($val, 0);

		$result = (count(DbHelper::query_exec($query,true))>0)? false: true;

		if (!is_null($denial_flg))
			$result = !$result;

		return $result;
	}

	/**
	 * checkisnumeric
	 *
	 * 数字かどうかの検査
	 * @param   $val
	   @return  exists sama value false else true
	 */
	public static function _validation_checkisnumeric ($val, $is_null = false)
	{
		if ($is_null && empty($val))
			return true;

		return is_numeric($val);
	}

	/**
	 * 
	 * checkdate
	 * 
	 * 日付が実在するかどうかの検査
	 * 
	 * @param $val, $year, $month, $day
	 * @return bool
	 */
	public function _validation_checkdate ($val, $year_nm, $month_nm, $day_nm, $is_empty = false)
	{
		$month = isset($this->input[$month_nm])?$this->input[$month_nm]:'';
		$day   = isset($this->input[$day_nm])?$this->input[$day_nm]: '';
		$year  = isset($this->input[$year_nm])?$this->input[$year_nm]:'';

		return (
			(!empty($month) && !empty($day) && !empty($year) && checkdate($month,$day,$year)) ||
			($is_empty && empty($day) && empty($month) && empty($year))
		)
		? true: false;
	}

	/**
	 * checkflg
	 * 
	 * @param $val, $is_null(空の許可)
	 * @return bool
	 */
	public function _validation_checkflg ($val, $row_nm, $is_null = false)
	{
		$flg = $this->input[$row_nm];
		//null許可の場合
		if ($is_null && empty($flg))
			return true;

		if((Int)$flg === (Int)1 || (Int)$flg === (Int)0)
		{
			return true;
		}
		return false;
	}

	/**
	 * checktel
	 *
	 * @param   $val, $table, $column, $status = null
	   @return  exists sama value false else true
	 */
	public function _validation_checktel ($val, $row1_nm, $row2_nm, $row3_nm , $is_null = false)
	{
		$tel1 = '';
		$tel2 = '';
		$tel3 = '';

		isset($this->input[$row1_nm]) and $tel1 = $this->input[$row1_nm];
		isset($this->input[$row2_nm]) and $tel2 = $this->input[$row2_nm];
		isset($this->input[$row3_nm]) and $tel3 = $this->input[$row3_nm];

		//null許可の場合
		if (
			$is_null && empty($tel1) && empty($tel2) && empty($tel3) &&
			!($tel1 || '0' && $tel2  || '0' || $tel3 == '0')
		)
			return true;

		//必須の場合
		return (
			is_numeric($tel1) &&
			is_numeric($tel2) &&
			is_numeric($tel3) &&
			(strlen($tel1) <= 4 && strlen($tel1) >= 2) &&
			(strlen($tel2) <= 4 && strlen($tel1) >= 2) &&
			(strlen($tel3) <= 4 && strlen($tel1) >= 2) &&
			strlen($tel1.$tel2.$tel3) <= 12
		)
		? true: false;
	}

	/**
	 * checkreqiredmult 
	 * 
	 * @param $val_array チェックする要素名を配列で渡す
	 * @return bool
	 */
	public function _validation_checkrequiredmult ($val, $val_array, $is_empty = false)
	{
		for ($i=0; $i<count($val_array); $i++)
		{
			$value = $this->input($val_array[$i]);

			if (!$is_empty && empty($value))
				return false;
		}
		return true;
	}


//----------------------dempa 検証済みメソッドend-------------------
	/**
	 * checkwithdrawalreason
	 *
	 * @param   $val, $table, $column, $status = null
	 * @return  exists sama value false else true
	 */
	public static function _validation_checkwithdrawalreason ($val)
	{
		$db_value = DbHelper::query_exec(Model_Mt_Withdrawal::get_query_select_id_without_1());
		$reason_key_array = array();

		foreach ($db_value as $value)
		{
				$reason_key_array[$value['id']] = $value['id'];
		}
		return (isset($reason_key_array[$val]))? true: false;
	}

	/**
	 * Minimum string length
	 *
	 * @param   string
	 * @param   int
	 * @return  bool
	 */
	public static function _validation_minlength($val, $length)
	{
		return (strlen($val) >= $length)?true: false;
	}

	/**
	 * Maximum string length
	 *
	 * @param   string
	 * @param   int
	 * @return  bool
	 */
	public static function _validation_maxlength($val, $length)
	{
		return (strlen($val) <= $length)? true: false;
	}

	/**
	 * checkSameValue
	 *
	 * @param   $val, $table, $column, $status = null
	   @return  exists sama value false else true
	 * if status true return boolean reversal
	 */
/*
	public static function _validation_checkSameValue ($val, $table, $column, $status = false)
	{
		$result = DB::select('mail')->from($table)
									 ->where($column, '=', $val)
									 ->and_where('del_flg', '<>', '1')
									 ->execute()->as_array();
		if ($status)
		{
			//同じ値があったらtrue
			return (count($result) > 0)? true: false;
		}
		else
		{
			//同じ値があったらfalse
			return (count($result) > 0)? false: true;
		}
	}
*/

	/**
	 * checknotSame
	 *
	 * @param   
	 * @return boolean
	 *
	*/
	public static function _validation_checknotSame($val, $target)
	{
		return ($val != $target)? true: false;
	}

	/**
	 * URL検証メソッド
	 * URLが適正な値であるかどうか検証します
	 * fuelphp　デフォでありますが、スルーしてしまうものがあるので、作成しました。
	 *
	 *   @param  int or string
	 *   @return bool
	 */
	public static function _validation_checkUrl( $val )
	{
		if( preg_match('/^(?:http|https):\/\/[\w,.:;&=+*%$#!?@()~\'\/-]+$/', $val) )
		{
			return true;
		}
		else 
		{
			return false;
		}
	}

	/**
	 * mysql date 検証メソッド
	 * databaseに入れるdate が形式にあっているか確認するメソッド
	 *
	 *   @param  int or string
	 *   @return bool
	 */
	public static function _validation_checkDbDate ($val)
	{
		return (preg_match( '/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}\s[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}$/', $val))?true: false;
	}
	
	/**
	 *db date 検証メソッド
	 * databaseに入れるdate が形式にあっているか確認するメソッド
	 *
	 *   @param  int or string
	 *   @return bool
	 */
	public static function _validation_checkLessThan ($val, $data)
	{
		return ($data[0] < $data[1])?true: false;
	}

	/**
	 * id検証メソッド
	 * ajaxでidが渡されたかどうか検証するメソッド
	 *
	 *   @param  int or string
	 *   @return bool
	 */
	public static function _validation_checkAjaxId ($val)
	{
		return (!empty($val) && !is_null($val) && is_numeric($val))?true: false;
	}
}