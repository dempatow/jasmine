<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Itemspeckeyvalue Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Itemspeckeyvalue extends \Orm\Model
{
	protected static $table_name = 'dt_itemspeckeyvalue';

	protected static $_properties = array(
	);

	/**
	 * select
	 * 
	 * @param none
	 */
	public static function select()
	{
		$query = DB::select('item_id', 'key_id', 'value_id')
					->from(self::$table_name)
					->and_where('del_flg', '=', 0);

		return DbHelper::query_exec($query);
	}

	/**
	 * select_by_item_id
	 * 
	 * @param int item_id
	 * @return mix array
	 */
	public static function select_by_item_id ($item_id)
	{
		$result = array();
		$redis_key = 'itemspec_'.$item_id;
		$result = RedisHelper::get_master($redis_key);

		if (!is_array($result) && !$result)
		{
			$dt = self::select();
			$dt_cnt = count($dt);

			for ($i=0; $i<$dt_cnt; $i++)
			{
				if ($dt[$i]['item_id'] == (Int)$item_id)
				{
					$key_mt = Model_Mt_Itemspeckey::select_by_id($dt[$i]['key_id']);
					$value_mt = Model_Mt_Itemspecvalue::select_by_id($dt[$i]['value_id']);
					if ($key_mt && $value_mt)
					{
						$result[] =  array(
							'name' => $key_mt['name'],
							'value' =>$value_mt['value']
						);
					}
				}
				unset($dt[$i]);
			}

			if (!$result || count($result)=== 0)
				$result = array();

			RedisHelper::set_master ($redis_key, $result);
		}
		return $result;
	}
}