<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Specialselectstock Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Specialselectstock extends \Orm\Model
{
	const DISP_MAX_COUNT = 20;
	protected static $table_name = 'dt_specialselectstock';

	protected static $_properties = array(
	);

	/**
	 * 在庫更新(減算)
	 * subtraction_stock
	 */
	public static function subtraction_stock ($item_id, $count)
	{
		$query = DB::query(
			'update 
				`dt_stock`
			set
				`count`= `count`-'.$count.',
				`updated`="'.date('Y-m-d H:i:s').'"
			where
				`item_id` ='. $item_id .'
			 and 
				`del_flg` = 0'
		);
		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * 在庫更新(加算)
	 * subtraction_stock
	 */
	public static function add_stock ($item_id, $count)
	{
		$query = DB::query(
			'update 
				`dt_stock`
			set
				`count`= `count`+'.$count.',
				`updated`="'.date('Y-m-d H:i:s').'"
			where
				`item_id` ='. $item_id .'
			 and 
				`del_flg` = 0'
		);
		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * select_stock_by_value_id
	 * 
	 * 商品在庫数を取得します。
	 * 
	 * @param $item_id
	 * @return queryObject
	 */
	public static function select_stock_by_value_id($value_id)
	{
		$result = array();
		$query = DB::select('count')
					->from(self::$table_name)
					->and_where('id', '=', $value_id)
					->and_where('del_flg', '=', 0);
		$result = DbHelper::query_exec($query, true, false, 'master');
		return isset($result['count'])? (Int)$result['count']: 0;
	}

	/**
	 * check_stock_special_select
	 * 
	 * 特殊表示アイテム在庫チェックメソッド
	 * (カート追加時)
	 */
	public static function check_stock_special_select ($item_id, $select_dt, $count, $special_select_dt)
	{
		$item_mt = array();
		$item_mt = Model_Mt_Item::select_item_by_id($item_id, true);
		$result['result']  = true;

		if (!empty($item_mt) && isset($item_mt['special_select']['id']) && isset($select_dt[$item_mt['special_select']['id']]))
		{
			//既にカートに入っている商品個数を取得する
			$current_count = 0;
			for ($i=0; $i<count($special_select_dt); $i++)
			{
				if ((Int)$special_select_dt[$i]['key'] === (Int)$item_mt['special_select']['id'] && (Int)$special_select_dt[$i]['value'] === (Int)$select_dt[$item_mt['special_select']['id']])
					$current_count+=$special_select_dt[$i]['count'];
			}

			//在庫数の確認
			$dt_stock = Model_Dt_Specialselectstock::select_stock_by_value_id((Int)$select_dt[(Int)$item_mt['special_select']['id']]);

			if (Model_Dt_Stock::DISP_MAX_COUNT<(Int)$count)
			{
				$result['result']  = false;
				$result['message'] = '一度にカートに入れられる件数は'.Model_Dt_Stock::DISP_MAX_COUNT.'までです。';
			}
			elseif ($dt_stock<$current_count+$count)
			{
				$result['result']  = false;
				$result['message'] = '在庫不足の為、カートに追加出来ませんでした。';
			}
		}
		else
		{
			$result['result'] = false;
			$result['message'] = '処理に失敗しました。再度時間をあけてお試しください。';
		}
		return $result;
	}

	/*
	[tmp]

	public static function check_stock_special_select ($item_id, $select_dt, $count, $special_select_dt)
	{
		$item_mt = array();
		$item_mt = Model_Mt_Item::select_item_by_id($item_id, true);
		$result  = true;

		if (!empty($item_mt) && isset($item_mt['special_select']['id']) && isset($select_dt[$item_mt['special_select']['id']]))
		{
			//既にカートに入っている商品個数を取得する
			$current_count = 0;
			for ($i=0; $i<count($special_select_dt); $i++)
			{
				if ((Int)$special_select_dt[$i]['key'] === (Int)$item_mt['special_select']['id'] && (Int)$special_select_dt[$i]['value'] === (Int)$select_dt[$item_mt['special_select']['id']])
					$current_count+=$special_select_dt[$i]['count'];
			}

			//在庫数の確認
			$dt_stock = Model_Dt_Specialselectstock::select_stock_by_value_id((Int)$select_dt[(Int)$item_mt['special_select']['id']]);

			if (Model_Dt_Stock::DISP_MAX_COUNT<(Int)$count || $dt_stock<$current_count+$count)
				$result = false;
		}
		else
		{
			$result = false;
		}
		return $result;
	}
	 */

	/**
	 * check_stock_special_select_purchase
	 * 
	 * 特殊表示アイテム在庫チェックメソッド
	 * 
	 * (購入処理時)
	 * 
	 * @param type $item_id
	 * @param type $special_select_dt
	 * @return boolean
	 */
	public static function check_stock_special_select_purchase ($item_id, $special_select_dt)
	{
		$result = true;

		foreach ($special_select_dt as $val)
		{
			if ((Int)$item_id === (Int)$val['item_id'])
			{
				$stock = Model_Dt_Specialselectstock::select_stock_by_value_id($val['value']);
				if ((Int)$val['count'] > (Int)$stock || (Int)$val['count'] === 0)
				{
					$result = false;
					break;
				}
			}
		}
		return $result;
	}
}