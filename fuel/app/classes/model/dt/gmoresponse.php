<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The GmoResponse Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Gmoresponse extends \Orm\Model
{
	protected static $table_name = 'dt_gmoresponse';
	protected static $_properties = array(
	);

	/**
	 * insert
	 * 
	 * @param insert data array
	 * @return reslut
	 */
	public static function insert ($insert_array)
	{
		$query = DB::insert(self::$table_name)
			->set($insert_array);

		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * update_by_purchase_id
	 * 
	 * @update_array
	 * @param purchase_id
	 * @return result
	 */
	public static function update_by_purchase_id ($update_array, $purchase_id)
	{
		$query = DB::update(self::$table_name)
					->set($update_array)
					->where('purchase_id', '=', $purchase_id)
					->and_where('del_flg', '=', 0);

		return DbHelper::query_exec($query, false, false, 'master');
	}
}