<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * Dt_Specialselect.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Specialselect extends \Orm\Model
{
	protected static $table_name = 'dt_specialselect';

	protected static $_properties = array(
	);

	/**
	 * select_join_mt_by_key_id
	 * 
	 * dt_specialとmt_specialvalueをjoinする
	 * 
	 * @param  int key_id
	 * @return mix array
	 */
	public static function select_join_mt_by_key_id ($key_id)
	{
		$query = DB::select(
						'value_id',
						'value'
					)
				->from(array('dt_specialselect','dt'))
				->join(array('mt_specialselectvalue', 'value'), 'left')
				->on('dt.value_id', '=', 'value.id')
				->where('dt.key_id', '=', $key_id)
				//期限チェック
				->and_where('value.open_date', '<=', date("Y-m-d H:i:s"))
				->and_where('value.close_date', '>=', date("Y-m-d H:i:s"))
				//削除flgチェック
				->and_where('dt.del_flg', '=', 0)
				->and_where('value.del_flg', '=', 0);

		return DbHelper::query_exec($query, false, false, 'master');
	}
}