<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Purchaseitemhistory Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Purchaseitemhistory extends \Orm\Model
{
	public static $table_name = 'dt_purchaseitemhistory';

	protected static $_properties = array(
		'id' => array(
			'label' => 'name',
			'validation' => array(
				'required',
				'checkRangeCharNumJp' => array('3','10'),
			),
		),
	);

	/**
	 * get_query_insert
	 * 
	 * @param type $data insert data array
	 * @return query object
	 */
	public static function get_query_insert ($data)
	{
		return $query = DB::insert(self::$table_name)->set($data);
	}

	/**
	 * select_by_uid_and_payment_cd
	 * 
	 * uidとpayment_cdをキーにしてselectします
	 * 
	 */
	public static function select_by_uid_and_payment_cd ($uid, $payment_cd)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('uid', '=', $uid)
					->and_where('payment_cd', '=', $payment_cd)
					->and_where('del_flg', '<>', 1);

		return DbHelper::query_exec($query);
	}

	/**
	 * update_by_purchase_id
	 * 
	 * @update_array
	 * @param purchase_id
	 * @return result
	 */
	public static function update_by_purchase_id ($update_array, $purchase_id)
	{
		$query = DB::update(self::$table_name)
					->set($update_array)
					->where('purchase_id', '=', $purchase_id)
					->and_where('del_flg', '=', 0);

		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * select_by_purchase_id
	 * 
	 * @param purchase_id
	 * @param del_flg default null
	 * @return data array
	 */
	public static function select_by_purchase_id ($purchase_id, $del_flg = null)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('purchase_id', '=', $purchase_id);

		if (!is_null($del_flg))
			$query->and_where('del_flg', '=', $del_flg);

		return DbHelper::query_exec($query);
	}

	/**
	 * select_for_review_list
	 * 
	 * レビュー一覧に使用する
	 * 
	 * @param $uid
	 * @param $review_flg
	 * @param $is_count
	 */
	public static function get_query_select_for_review_list ($uid, $review_flg)
	{
		$query = DB::select(
							'item.id',
							'item.item_id',
							'item.name',
							'item.review_flg',
							'purchase.created'
						)
						->from(array('dt_purchasehistory','purchase'))
						->join(array(self::$table_name,'item'), 'LEFT')
						->on('purchase.id', '=', 'item.purchase_id')
						->where('purchase.uid', '=', $uid)
						->and_where('item.review_flg', '=', $review_flg)
						->and_where('item.del_flg', '=', 0)
						->and_where('purchase.del_flg', '=', 0)
						->and_where('purchase.status_cd', '=', Purchase::ALREADY_SENDING);
		return $query;
	}

	/**
	 * select_for_review_list
	 * 
	 * レビュー一覧に使用するカウントをとる
	 * 
	 * @param $uid
	 * @param $review_flg
	 * @param $is_count
	 */
	public static function select_count_for_review_list ($uid, $review_flg)
	{
		$query = DB::select(DB::expr('COUNT(*) as count'))
						->from(array('dt_purchasehistory','purchase'))
						->join(array(self::$table_name,'item'), 'LEFT')
						->on('purchase.id', '=', 'item.purchase_id')
						->where('purchase.uid', '=', $uid)
						->and_where('item.review_flg', '=', $review_flg)
						->and_where('item.del_flg', '=', 0)
						->and_where('purchase.del_flg', '=', 0)
						->and_where('purchase.status_cd', '=', Purchase::ALREADY_SENDING);

		return (Int)DbHelper::query_exec($query,true)['count'];
	}

	/**
	 * update_by_id
	 * 
	 * @update_array
	 * @param purchase_id
	 * @return result
	 */
	public static function update_by_id ($update_array, $id)
	{
		$query = DB::update(self::$table_name)
					->set($update_array)
					->where('id', '=', $id)
					->and_where('del_flg', '=', 0);

		return DbHelper::query_exec($query, false, false, 'master');
	}
}