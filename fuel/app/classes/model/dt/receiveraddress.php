<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Receiveraddress Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Receiveraddress extends \Orm\Model
{
	protected static $table_name = 'dt_receiveraddress';
	protected static $_properties = array(
	);
	const DISP_COUNT = 5;

	/**
	 * select_by_uid
	 * 
	 * uidをキーにselectします
	 * 
	 * @param  int uid
	 * @return mix address data array
	 */
	public static function select_by_uid ($uid)
	{
		$query = DB::select(
					'id',
					'uid',
					'corporate_name',
					'corporate_name_kana',
					'first_name',
					'last_name',
					'first_name_kana',
					'last_name_kana',
					'zip',
					'state_cd',
					'address1',
					'address2',
					'address3',
					'tel1',
					'tel2',
					'tel3',
					'fax1',
					'fax2',
					'fax3'
				)
				->from(self::$table_name)
				->where('uid', '=', $uid)
				->and_where('del_flg', '=', '0');

		return DbHelper::query_exec($query);
	}
	
	/**
	 * select_by_id_and_uid
	 * 
	 * uidと主キーでselectします(セキュリティーの為)
	 */
	public static function select_by_id_and_uid ($id, $uid)
	{
		$query = DB::select(
					'id',
					'corporate_name',
					'corporate_name_kana',
					'first_name',
					'last_name',
					'first_name_kana',
					'last_name_kana',
					'zip',
					'state_cd',
					'address1',
					'address2',
					'address3',
					'tel1',
					'tel2',
					'tel3',
					'fax1',
					'fax2',
					'fax3'
				)
				->from(self::$table_name)
				->where('id','=', $id)
				->and_where('uid', '=', $uid)
				->and_where('del_flg', '=', 0);
		$result = DbHelper::query_exec($query, true);
		return (count($result)>0)? $result: false;
	}

	/**
	 * update_by_id_and_uid
	 * 
	 * セキュリティー対策の為,uidをキーに使用する
	 * 
	 * @param mix array update array
	 * @param int id
	 * @param int uid
	 */
	public static function get_query_update_by_id_and_uid($update_array, $id, $uid)
	{
		return DB::update(self::$table_name)
					->set($update_array)
					->where('id', '=', $id)
					->and_where('uid', '=', $uid);
	}

	/**
	 * get_query_insert
	 * 
	 * 新規登録userのsql
	 * 
	 * @param mix array insert data
	 * @return queryObject
	 */
	public static function get_query_insert ($data)
	{
		return $query = DB::insert(self::$table_name)->set($data);
	}
}