<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The User Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Withdrawal extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'reason_id' => array(
			'label' => '退会理由',
			'validation'=> array(
				'required',
				'checkwithdrawalreason',
			),
		),
		'other_comment' => array(
			'label' => 'その他理由',
			'validation' => array(
				'checkcharNumJp'=> array('200'),
			),
		),
		'del_flg' => array(
			'skip' => true,
		),
		'updated' => array(
			'skip' => true,
		),
		'created' => array(
			'skip' => true,
		),
	);

	/**
	 * get_query_insert_user
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_insert_withdrawal($data)
	{
		return $query = DB::insert('dt_withdrawal')
							->set(
								array(
									'reason_id' => $data['reason_id'],
									'other_comment' => $data['other_comment'],
								)
							);
	}
}