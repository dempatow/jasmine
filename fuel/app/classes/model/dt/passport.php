<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Passport Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Passport extends \Orm\Model
{

	protected static $table_name = 'dt_passport';

	protected static $_properties = array(
		'name' => array(
			'label' => 'name',
			'validation' => array(
				'required',
				'checkRangeCharNumJp' => array('3','10'),
			),
		),
	);


	/**
	 * get_query_select_availdata_by_passport
	 * パスポートチェックの為に使います
	 * 
	 * @param type $name Description
	 * @return Query Object
	 * 
	 */
	public static function get_query_select_availdata_by_passport ($passport, $expiration)
	{
		$effective_date = date(
				'Y-m-d H:i:s',
				mktime(
						date('H'),
						date('i'),
						date('s') - (Int)$expiration,
						date('m'),
						date('d'),
						date('Y')
						)
				);

		$query = DB::select('*')
						->from(self::$table_name)
						->where('passport', '=', $passport)
						->and_where('updated', '>=', $effective_date)
						->and_where('del_flg', '<>', 1)
						->limit(1);
		return $query;
	}

	/**
	 * set_del_flg
	 * 
	 * uidをキーにパスポートを削除します
	 * 
	 * @param int $uid
	 * @return bool
	 * 
	 */
	public static function set_del_flg_by_uid($uid)
	{
		$query = DB::update(self::$table_name)
				->set(array('del_flg' => 1))
				->where('uid', '=', $uid);

		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * delete_by_del_flg_1
	 * 
	 * del_flgに1が立っているレコードをdeleteします。
	 * 夜間定期バッチで使用しています。
	 */
	public static function delete_by_del_flg_1 ()
	{
		/************************************************
		 * 
		 *  注意!! このメソッドはレコードを物理削除します
		 * 
		 ************************************************/
		$query = DB::delete(self::$table_name)->where('del_flg', '=', 1);
		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * get_query_insert_passport
	 * インサートqueryを返却します。
	 * 
	 * @param $data insert data array
	 * @prama Query Object
	 */
	public static function get_query_insert_passport ($data)
	{
		return $query = DB::insert(self::$table_name)->set($data);
	}

	/**
	 * get_query_insert_user
	 * 
	 * 新規登録userのsql
	 * 
	 * @param
	 * @return queryObject
	 */
	public static function get_query_update_login_flg ($id ,$column, $value)
	{
		return $query = DB::update('mt_options')
					->set(
							array(
								$column => $value,
								)
							)
					->where('id', '=', $id);
	}

	/**
	 * get_query_select_user_by_userid
	 * 
	 * userid を キーにしてuserデータを取得します。
	 * 
	 * @access public
	 * @param $userid , del_flg = delfault 0
	 * @return queryObject
	 */
	public static function get_query_select_user_by_userid($userid, $del_flg = 0)
	{
		return $query = DB::select('*')
						->from(self::$table_name)
						->where('userid', '=', $userid)
						->and_where('del_flg', '=', $del_flg);
	}
}