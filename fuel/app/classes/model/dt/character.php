<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Character Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Character extends \Orm\Model
{
	protected static $table_name = 'dt_character';

	protected static $_properties = array(
	);

	/**
	 * select
	 *
	 * 一度DBから取得したものはredisから取得します
	 *
	 * @access public
	 * @return data array
	 */
	public static function select()
	{
		$redis_key = 'dt_character';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select(
							'character_id',
							'item_id'
						)
						->from(self::$table_name)
						->and_where('del_flg', '=', 0);
			$result = DbHelper::query_exec($query);
			RedisHelper::set_master($redis_key, $result);

		}
		return $result;
	}

	/**
	 * select_by_item_id
	 * 
	 * @param int character_id
	 * @return mix array
	 */
	public static function select_by_item_id ($item_id)
	{
		$result = array();
		$dt_character = self::select();
		$dt_cnt = count($dt_character);

		for ($i=0; $i<$dt_cnt; $i++)
		{
			if ((Int)$dt_character[$i]['item_id'] === (Int)$item_id)
				$result[$dt_character[$i]['character_id']] = $dt_character[$i];
		}
		return count($result)>0? $result: false;
	}

	/**
	 * select_item_id_by_chara_id
	 * 
	 * @param int chara_id
	 * @return mix array
	 */
	public static function select_item_id_by_chara_id ($chara_id)
	{
		$result = array();
		$dt_character = self::select();
		$dt_cnt = count($dt_character);

		for ($i=0; $i<$dt_cnt; $i++)
		{
			if ((Int)$dt_character[$i]['character_id'] === (Int)$chara_id)
				$result[] = (Int)$dt_character[$i]['item_id'];

		}

		return count($result)>0? $result: false;
	}
}