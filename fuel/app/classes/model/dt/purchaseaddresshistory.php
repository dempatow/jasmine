<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Purchaseaddresshisotory Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Purchaseaddresshistory extends \Orm\Model
{
	public static $table_name = 'dt_purchaseaddresshistory';

	protected static $_properties = array(
	);

	/**
	 * get_query_insert
	 * 
	 * @param type $data insert data array
	 * @return query object
	 */
	public static function get_query_insert ($data)
	{
		return $query = DB::insert(self::$table_name)->set($data);
	}

	/**
	 * update_by_purchase_id
	 * 
	 * @update_array
	 * @param purchase_id
	 * @return result
	 */
	public static function update_by_purchase_id ($update_array, $purchase_id)
	{
		$query = DB::update(self::$table_name)
					->set($update_array)
					->where('purchase_id', '=', $purchase_id)
					->and_where('del_flg', '=', 0);

		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * select_by_purchase_id
	 * 
	 */
	public static function select_by_purchase_id ($purchase_id)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('purchase_id', '=', $purchase_id)
					->and_where('del_flg', '=', 0);
		return DbHelper::query_exec($query, 'type_cd');
	}
}