<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Readmessage Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Readmessage extends \Orm\Model
{
	protected static $table_name = 'dt_readmessage';

	protected static $_properties = array(
	);

	/**
	 * select_by_uid
	 * 
	 * uidとmessage_idをキーにして
	 * 存在するレコードのmessage_idを取得します
	 * 
	 * @param $uid $message_array
	 * @return message_id array
	 */
	public static function select_by_uid ($uid, $message_array)
	{
		$ms_ids = array();
		for ($i=0; $i<count($message_array); $i++)
		{
			$ms_ids[] = $message_array[$i]['id'];
		}

		$query = DB::select('message_id')
					->from(self::$table_name)
					->where('message_id', 'in', $ms_ids)
					->and_where('uid','=', $uid)
					->and_where('del_flg','=', 0);
		return DbHelper::query_exec($query);
	}

	/**
	 * insert_dt
	 * 
	 * 既読レコードをinsertします
	 * 
	 * @param uid message_id
	 */
	public static function insert_dt ($uid, $message_id)
	{
		$query = DB::insert(self::$table_name)
					->set(
						array(
							'uid' => $uid,
							'message_id' => $message_id
						)
					);
		DbHelper::query_exec($query, false, false, 'master');
	}
}