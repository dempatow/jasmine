<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Purchasehistory Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Purchasehistory extends \Orm\Model
{
	public static $table_name   = 'dt_purchasehistory';
	const GMO_EXPIRATION_MINUTE = 30; //GMO画面遷移後確定するまでのタイムリミット(分)

	protected static $_properties = array(
		'id' => array(
			'label' => 'name',
			'validation' => array(
				'required',
				'checkRangeCharNumJp' => array('3','10'),
			),
		),
		'free1' => array(
			'label' => '自由欄',
			'validation' => array(
				'checkCharNumJp' => array(500),
			),
		),
	);

	/**
	 * get_query_insert
	 * 
	 * @param type $data insert data array
	 * @return query object
	 */
	public static function get_query_insert ($data)
	{
		return $query = DB::insert(self::$table_name)->set($data);
	}

	/**
	 * select_by_uid_and_payment_cd
	 * 
	 * uidとpayment_cdをキーにしてselectします
	 * 
	 */
	public static function select_by_uid_and_payment_cd ($uid, $payment_cd)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('uid', '=', $uid)
					->and_where('payment_cd', '=', $payment_cd)
					->and_where('del_flg', '<>', 1);

		return DbHelper::query_exec($query);
	}

	/**
	 * select_credit_data
	 * 
	 * カード決済を行ったレコードを返却します
	 */
	public static function select_credit_data ($uid)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('uid', '=', $uid)
					->and_where('payment_cd', '=', Model_Mt_Payment::CARD_PAYMENT_ID)
					->and_where('payment_status', '<>', Purchase::ZERO_YEN_PAYMENT)
					->and_where('payment_status', '<>', 0)
					->and_where('del_flg', '<>', 1);

		return DbHelper::query_exec($query);
	}
	
	
	//select_by_uid_and_payment_cd

	/**
	 * select_by_id
	 * 
	 * purchase_idをキーにselectします
	 */
	public static function select_by_id ($purchase_id)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('id', '=', $purchase_id)
					->and_where('del_flg', '=', 0);
		return DbHelper::query_exec($query, true, false);
	}

	/**
	 * select_by_id_and_uid
	 * 
	 * purchase_idをキーにselectします
	 */
	public static function select_by_id_and_uid ($purchase_id, $uid)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('id', '=', $purchase_id)
					->and_where('del_flg', '=', 0)
					->and_where('uid', '=', $uid);
		return DbHelper::query_exec($query, true, false);
	}

	/**
	 * update_by_purchase_id
	 * 
	 * @update_array
	 * @param purchase_id
	 * @return result
	 */
	public static function update_by_purchase_id ($update_array, $purchase_id)
	{
		$query = DB::update(self::$table_name)
					->set($update_array)
					->where('id', '=', $purchase_id)
					->and_where('del_flg', '=', 0);
		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * get_ranking_data
	 * 
	 * 購入数ランキングを返します
	 * 
	 * @param $rank, $from_date, $to_date, $limit
	 * @return queryObject
	 */
	public static function get_ranking_data ()
	{

		$rank_limit = 5;
		$redis_key = 'item_purchase_ranking';
		$result = array();
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$mt_ranking = Model_Mt_Ranking::select();
			$db_data    = null;

			if (!is_null($mt_ranking)|| !empty($mt_ranking))
			{
				$query = DB::select(
									'item.item_id',
									DB::expr('sum(1) as total')
								)
								->from(array(self::$table_name,'history'))
								->join(array(Model_Dt_Purchaseitemhistory::$table_name,'item'), 'LEFT')
								->on('history.id', '=', 'item.purchase_id')
								->and_where('history.created', '>=', $mt_ranking['from'])
								->and_where('history.created', '<=', $mt_ranking['to'])
								->and_where('history.del_flg', '<>' ,1)
								->and_where('item.del_flg', '<>' ,1)
								->group_by('item.item_id')
								->order_by('total','DESC')
								->limit($rank_limit);

				$db_data = DbHelper::query_exec($query);
			}

			!$db_data and $db_data = null;
			$set_cnt = 0;
			for ($i=0; $i<count($db_data); $i++)
			{
				$result[] = Model_Mt_Item::select_item_by_id($db_data[$i]['item_id'], true);
			}

			while ($i<$rank_limit)
			{
				$result[] = null;
				$i++;
			}
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * get_query_select_count
	 * 
	 * uidとstatus_cdをキーにレコードのカウントを取得します
	 * 
	 * //一般ユーザー向けデータはstatus_cdが100未満
	 * 
	 * @param int uid
	 * @return query object
	 */
	public static function select_count_by_uid ($uid, $status_cd = false, $is_admin = false)
	{
		$query = DB::select(DB::expr('COUNT(*) as count'))
					->from(self::$table_name)
					->where('uid', '=', $uid)
					->and_where('del_flg', '=', 0);

		/************
		 * status指定
		 ************/
		if ($status_cd)
			$query->and_where('status_cd', '=', $status_cd);

		/******************************
		 *　ロールごとのstatus_cd fillter
		 ******************************/
		if ($is_admin)
		{
			//管理者の場合
		}
		else
		{
			//一般ユーザーの場合
			$query->and_where('status_cd', '<', 100);
		}
		return (Int)DbHelper::query_exec($query,true)['count'];
	}

	/**
	 * select_by_history_base_data
	 * 
	 * uidとstatus_cdをキーにレコードのカウントと
	 * 購入日(最大)と購入日(最小)を取得します
	 * 
	 * //一般ユーザー向けデータはstatus_cdが100未満
	 * 
	 * @param int uid
	 * @return query object
	 */
	public static function select_by_history_base_data ($uid, $status_cd = false, $is_admin = false)
	{
		$query = DB::select(
					DB::expr('COUNT(*) as count'),
					DB::expr('MAX(created) as max'),
					DB::expr('MIN(created) as min'))
					->from(self::$table_name)
					->where('uid', '=', $uid)
					->and_where('del_flg', '=', 0);

		/************
		 * status指定
		 ************/
		if ($status_cd)
			$query->and_where('status_cd', '=', $status_cd);

		/******************************
		 *　ロールごとのstatus_cd fillter
		 ******************************/
		if ($is_admin)
		{
			//管理者の場合
		}
		else
		{
			//一般ユーザーの場合
			$query->and_where('status_cd', '<', 100);
		}
		return DbHelper::query_exec($query, true);
	}

	/**
	 * get_query_select_count_for_searched_history
	 * 
	 * uidとstatus_cdと日付をキーにレコードのカウントと
	 * 
	 * //一般ユーザー向けデータはstatus_cdが100未満
	 * 
	 * @param int uid
	 * @return query object
	 */
	public static function get_query_select_count_for_searched_history ($uid, $is_admin = false)
	{
		$query = DB::select(DB::expr('COUNT(*) as count'))
					->from(self::$table_name)
					->where('uid', '=', $uid)
					->and_where('del_flg', '=', 0);

		/******************************
		 *　ロールごとのstatus_cd fillter
		 ******************************/
		if ($is_admin)
		{
			//管理者の場合
		}
		else
		{
			//一般ユーザーの場合
			$query->and_where('status_cd', '<', 100);
		}
		return $query;
	}

	/**
	 * get_query_select_by_uid_and_status_cd
	 * 
	 * uidとstatus_cdをキーにレコードを取得します
	 * 
	 * @param type $uid
	 * @param type $status_cd
	 * @param type $is_admin
	 * @return type
	 */
	public static function get_query_select_by_uid ($uid, $is_admin = false)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('uid', '=', $uid)
					->and_where('del_flg', '=', 0);

		/******************************
		 *　ロールごとのstatus_cd fillter
		 ******************************/
		if ($is_admin)
		{
			//管理者の場合
		}
		else
		{
			//一般ユーザーの場合
			$query->and_where('status_cd', '<', 100);
		}
		return $query;
	}

	/**
	 * get_expiration_gmo_user
	 * 
	 * GMO画面遷移期間切れユーザー取得メソッド
	 */
	public static function get_expiration_gmo_user ()
	{
		//今から定数'GMO_EXPIRATION_MINUTE'分前の日時を取得
		$effective_date = date(
					'Y-m-d H:i:s',
					mktime(
						date('H'),
						date('i') - self::GMO_EXPIRATION_MINUTE,
						date('s'),
						date('m'),
						date('d'),
						date('Y')
					)
				);

		$query = DB::select('*')
					->from(self::$table_name)
					->where('status_cd', '=', Purchase::SCREEN_TRANSITION_GMO)
					->and_where('updated', '<',$effective_date)
					->and_where('del_flg', '<>', 1);
		return DbHelper::query_exec($query);
	}
}