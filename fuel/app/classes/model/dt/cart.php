<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Template Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Template extends \Orm\Model
{
	protected static $table_name = 'dt_template';

	protected static $_properties = array(
		'id' => array(
			'label' => 'name',
			'validation' => array(
				'required',
				'checkRangeCharNumJp' => array('3','10'),
			),
		),
	);

	/**
	 * get_query_id
	 * 
	 * idレコードをupdateするqueryオブジェクトを返します
	 * 
	 * @param
	 * @return queryObject
	 */
	public static function get_query_update_by_id ($user_data, $id)
	{

	}
}