<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Timespecified Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Timespecified extends \Orm\Model
{
	protected static $table_name = 'dt_timespecified';

	protected static $_properties = array(
	);

	/**
	 * select
	 * 
	 * dt_timespecifiedをセレクトします
	 * 
	 * @return type
	 */
	public static function select ()
	{
		$query = DB::select('date_id', 'time_id')
					->from(self::$table_name)
					->where('del_flg', 0);

		$result = DbHelper::query_exec($query);
		return $result;
	}

	/**
	 * select_by_dete_id
	 * 
	 * dt_timespecifiedをセレクトします
	 * 
	 * @return type
	 */
	public static function select_by_dete_id ($date_id)
	{
		$query = DB::select('date_id', 'time_id')
					->from(self::$table_name)
					->where('del_flg', 0)
					->and_where('date_id', '=', $date_id);

		$result = DbHelper::query_exec($query);
		return $result;
	}

	/**
	 * select_by_date_id_join_mt
	 * 
	 * dt_timespecifiedをセレクトしマスタとjoinした状態で返却します
	 * 
	 * @param int date_id
	 * 
	 */
	public static function select_by_date_id_join_mt ($date_id)
	{
		$result = false;
		$dt = self::select_by_dete_id($date_id);

		for ($i=0; $i<count($dt); $i++)
		{
			$mt_tmp = Model_Mt_Timespecified::select_by_id($dt[$i]['time_id']);
			$mt_tmp and $result[] = $mt_tmp;
		}
		return $result;
	}

}