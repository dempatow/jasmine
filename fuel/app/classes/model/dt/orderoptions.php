<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The order options Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Orderoptions extends \Orm\Model
{

	protected static $_properties = array(

		'id',
		'password' => array(
			'data_type' => 'varchar',
			'label' => 'PASS',
			'validation'=> array(
				'required',
				'checkAlphaNumeric',
				'min_length' => array('4'),
				'max_length' => array('20'),
			),
		),
		'mail' => array(
			'data_type' => 'varchar',
			'label' => 'MAIL',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('100'),
			),
		),
		'del_flg' => array(
			'skip' => true,
		),
		'updated' => array(
			'skip' => true,
		),
		'created' => array(
			'skip' => true,
		),
		//----------validation only-----------
		'consent' => array(
			'label' => '同意',
			'validation' => array(
				'required',
			),
		),
		'detail' => array(
			'label' => '問い合わせ内容',
			'validation' => array(
				'required',
				'checkcharNumJp'=> array('500'),
			),
		),
		'contact_mail' => array(
			'data_type' => 'varchar',
			'label' => 'メールアドレス',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('100'),
			),
		),
		'company_name' => array(
			'data_type' => 'varchar',
			'label' => '会社名',
			'validation'=> array(
				'required',
				'checkcharNumJp'=> array('30'),
			),
		),
		'charge_name' => array(
			'data_type' => 'varchar',
			'label' => 'ご担当者名',	
			'validation'=> array(
				'required',
				'checkcharNumJp'=> array('20'),
			),
		),
		'url' => array(
			'data_type' => 'varchar',
			'label' => '貴社サイトURL',
			'validation'=> array(
				'required',
				'max_length' => array('100'),
				'checkUrl',
			),
		),
		'withdrawal_mail' => array(
			'label' => 'メールアドレス',
			'validation' => array(
				'required',
				'checkSameValue' => array('dt_users', 'mail', true),
				'checkEmail',
				
			),
		),
	);
		
	public static function get_options_join_mt_by_order_id($order_id)
	{
		$query = DB::select(
							'mt.name',
							'mt.price'
							)
					->from(array('dt_order_options', 'dt'))
					->join(array('mt_options', 'mt'))
					->on('dt.options_id', '=', 'mt.id')
					->where('dt.del_flg', '<>', 1)
					->and_where('dt.order_id', '=', $order_id);
		return DbHelper::query_exec($query);
	}

	/**
	 * get_query_insert_order_options
	 * 
	 * order_commodity tableにインサートする
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_insert_order($order_id, $options_id)
	{
		return $query = DB::insert('dt_order_options')
							->set(
								array(
									'order_id' => $order_id,
									'options_id' => (Int)$options_id
								)
							);
	}


	/**
	 * get_query_select_users_login
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_select_users_login($mail)
	{
		return $query = DB::select(
							'user.uid',
							'user.mail',
							'user.password',
							'user.last_name',
							'user.first_name',
							'user.role',
							'un.name'
						)
						->from(array('dt_users','user'))
						->join(array('mt_unit','un'), 'LEFT')
						->on('user.unit', '=', 'un.id')
						->where('user.mail', '=', $mail)
						->and_where('user.del_flg', '<>', 1)
						->and_where('un.del_flg', '<>', 1);
	}

	/**
	 * get_query_select_users
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_select_users_by_mail($mail)
	{
		return $query = DB::select('*')->from('dt_users')
					->where('mail', '=', $mail)
					->and_where('del_flg', '<>', 1);
	}

	/**
	 * get_query_select_malmaga_user
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_select_malmaga_user($mail)
	{
		//現在から一日前を算出
		$effective_date = date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') - 1, date('Y')));
		return $query = DB::select('*')->from('dt_users')
					->where('mail', '=', $mail)
					->and_where('del_flg', '<>', 1)
					->and_where('status', '=', Config::get('custom_config.user.status.interim_registration'))
					->and_where('created', '>=', $effective_date);
	}

	/**
	 * get_query_update_malmaga_user
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_update_user_status($id, $status = 1)
	{
		//現在から一日前を算出
		$effective_date = date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') - 1, date('Y')));
		return $query = DB::update('dt_users')
					->set(array('status' => $status))
					->where('id', '=', $id)
					->and_where('del_flg', '<>', 1)
					->and_where('status', '=', 0)
					->and_where('created', '>=', $effective_date);
	}

	/**
	 * get_query_insert_user
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_insert_user($data)
	{
		return $query = DB::insert('dt_users')
							->set(
								array(
									'name' => $data['name'],
									'mail' => $data['mail'],
									'status' => $data['status'],
								)
							);
	}

	/**
	 * get_query_update_del_user
	 *
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_update_del_user($mail)
	{
		return $query = DB::update('dt_users')
					->set(array('del_flg' => 1))
					->where('mail', '=', $mail);
	}

	/**
	 * get_query_update_status
	 * 
	 * 仮登録ユーザーかつ有効期限切れに対してdel_flgをたてる
	 * 
	 * @access public
	 * @param status
	 * @return queryObject
	 */
	public static function get_query_update_del_expiration_user()
	{
		//現在から1日前を算出
		$effective_date = date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') - 1, date('Y')));
		return $query = DB::update('dt_users')
					->set(array('del_flg' => 1))
					->where('del_flg', '<>', 1)
					->and_where('status', '=', Config::get('custom_config.user.status.interim_registration'))
					->and_where('created', '<', $effective_date);
	}
}