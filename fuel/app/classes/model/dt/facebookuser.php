<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Facebook User Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Facebookuser extends \Orm\Model
{
	protected static $table_name = 'dt_facebookuser';

	protected static $_properties = array(

		'id' => array(
			'label' => 'name',
			'validation' => array(
				'required',
				'checkRangeCharNumJp' => array('3','10'),
			),
		),
		'name' => array(
			'label' => 'name',
			'validation' => array(
				'required',
				'checkRangeCharNumJp' => array('3','10'),
			),
		),
		'email' => array(
			'data_type' => 'varchar',
			'label' => 'mail',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('256'),
				'checkUniquUserMail',
			),
		),
		'access_token' => array(
			'label' => 'token',
			'validation' => array(
				'required',
			)
		),

		//----------validation only-----------
		'password1' => array(
			'data_type' => 'varchar',
			'label' => '新パスワード',
			'validation'=> array(
				'required',
				'checkAlphaNumeric',
				'min_length' => array('4'),
				'max_length' => array('20'),
				'checkisSame' => array('password2')
			),
		),
		'password2' => array(
			'data_type' => 'varchar',
			'label' => '新パスワード(確認用)',
			'validation'=> array(
				'required',
				'checkAlphaNumeric',
				'min_length' => array('4'),
				'max_length' => array('20'),
			),
		),
		'contact_mail' => array(
			'data_type' => 'varchar',
			'label' => 'メールアドレス',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('100'),
			),
		),
		'company_name' => array(
			'data_type' => 'varchar',
			'label' => '会社名',
			'validation'=> array(
				'required',
				'checkcharNumJp'=> array('30'),
			),
		),
		'charge_name' => array(
			'data_type' => 'varchar',
			'label' => 'ご担当者名',	
			'validation'=> array(
				'required',
				'checkcharNumJp'=> array('20'),
			),
		),
		'url' => array(
			'data_type' => 'varchar',
			'label' => '貴社サイトURL',
			'validation'=> array(
				'required',
				'max_length' => array('100'),
				'checkUrl',
			),
		),
		'withdrawal_mail' => array(
			'label' => 'メールアドレス',
			'validation' => array(
				'required',
				'checkSameValue' => array('dt_users', 'mail', true),
				'checkEmail',
				
			),
		),
		'cahange_pass_mail' => array(
			'data_type' => 'varchar',
			'label' => 'MAIL',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('100'),
				'checkSameValue' => array ('dt_users', 'mail', true),
			),
		)
	);

	/**
	 * get_query_select_facebookuser_by_id
	 * 
	 * facebook id をキーにして facebookuserをselect する
	 * 
	 * @param id (facebookのユーザーid)
	 * @return Query object
	 */
	public static function get_query_select_facebookuser_by_facebook_id ($facebook_id, $del_flg = 0)
	{
		return $query = DB::select('*')
						->from(self::$table_name)
						->where('facebook_id', '=', $facebook_id)
						->and_where('del_flg', '=', $del_flg);
	}

	/**
	 * get_query_select_user_by_mail_and_smscode
	 * 
	 * mailとsms_codeをキーにしてuserを取得します
	 * 
	 * @access 
	 * @param $mail, sms_code = 0, $del_flg = 0
	 * @return queryObject
	 */
	public static function get_query_select_user_by_mail_and_smscode($mail, $sms_code = 0, $del_flg = 0)
	{
		return $query = DB::select('*')
						->from(self::$table_name)
						->where('mail', '=', $mail)
						->and_where('sms_code', '=', $sms_code)
						->and_where('del_flg', '=', $del_flg);
	}

	/**
	 * get_query_insert_facebookuser
	 * 
	 * facebookユーザーのsql
	 * 
	 * @param
	 * @return queryObject
	 */
	public static function get_query_insert_facebookuser ($user_data)
	{
		return $query = DB::insert(self::$table_name)
					->set($user_data);
	}

	/**
	 * get_query_update_facebookuser_by_id
	 * 
	 * facebookユーザーのupdate sql
	 * 
	 * @param
	 * @return queryObject
	 */
	public static function get_query_update_facebookuser_by_id ($user_data, $id)
	{
		return $query = DB::update(self::$table_name)
					->set($user_data)
					->where('id', '=', $id);
	}
}