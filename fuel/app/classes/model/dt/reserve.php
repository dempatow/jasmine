<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Reserve Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Reserve extends \Orm\Model
{
	protected static $table_name = 'dt_reserve';

	//status cd
	const RESERVE           = 1; //予約中
	const PURCHASE_COMPLATE = 2; //購入完了
	const CANCEL_USER       = 3; //ユーザーによる取り消し
	const EXPIRED           = 4; //有効期限切れ
	const DELTE_SYSTEM_USER = 5; //システムユーザーによる取り消し

	protected static $_properties = array();

	/**
	 * reserve_item
	 * 商品予約処理
	 * 
	 * dt_reserveへinsertとdt_stockをupdateします
	 * 
	 * @param  int     $uid
	 * @param  int     $item_id
	 * @param  int     $count
	 * 
	 * @return boolean $result
	 */
	public static function reserve_item ($uid, $item_id, $count)
	{
		$result = true;

		/*******************
		 * 在庫の減算
		 *******************/
		$db_result = Model_Dt_Stock::subtraction_stock($item_id, $count);
		if (empty($db_result) || count($db_result) <= 0)
			$result = false;

		/*******************
		 * 予約レコードinsert
		 *******************/
		$insert_array = array(
			'uid'       => $uid,
			'item_id'   => $item_id,
			'count'     => $count,
			'status_cd' => self::RESERVE,
			'del_flg'   => 0,
		);
		if ($result)
		{
			$query     = DB::insert(self::$table_name)->set($insert_array);
			$db_result = DbHelper::query_exec($query, false, false, 'master');
			$result    = isset($db_result['insert_id'])? $db_result['insert_id']: $db_result;
		}
		if (!$result)
			Func::error_log ('予約処理時にエラーが発生しました uid:['.$uid.']', true);

		return $result;
	}

	/**
	 * get_reserve_tiems
	 * 予約商品取得メソッド
	 * 
	 * @param int $uid
	 */
	public static function get_reserve_items ($uid , $status_cd = null)
	{
		$reserve_dt = array();
		$query = DB::select(
					'id',
					'item_id',
					'count',
					'status_cd',
					'created'
				)
				->from(self::$table_name)
				->where('del_flg', '<>', 1)
				->and_where('uid', '=', $uid);

		if (!is_null($status_cd))
			$query->and_where('status_cd', '=', $status_cd);

		$reserve_dt = DbHelper::query_exec($query, false, false);

		for ($i=0; $i<count($reserve_dt); $i++)
		{
			$item_mt = Model_Mt_Item::select_item_by_id($reserve_dt[$i]['item_id'], true);
			$reserve_dt[$i]['name'] = $item_mt['name'];
			$reserve_dt[$i]['selling_date'] = $item_mt['selling_date'];
		}
		return $reserve_dt;
	}

	/**
	 * update_del_flg
	 * 
	 * @parma int $id
	 * @param int $status_cd
	 */
	public static function update_del_flg ($id, $status_cd)
	{
		$query = DB::update(self::$table_name)
					->set(
						array(
							'del_flg' => 1,
							'status_cd' => $status_cd
						)
					)
					->where('id', '=', $id);
		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * select_ids_count
	 * 
	 * @param mix $ids (合計したいレコードのidが入った配列)
	 * @return int count
	 */
	public static function select_ids_count ($ids_array)
	{
		$query = \DB::select('SUM("count") as count')->from(self::$table_name)->where('id', 'in', $ids_array);
		return (Int)DbHelper::query_exec($query, true, false, 'master')['count'];
	}

	/**
	 * get_reserve_tiems
	 * 予約商品取得メソッド
	 * 
	 * @param int $uid
	 */
	public static function select_by_id_and_uid ($id, $uid)
	{
		$result = array();
		$query = DB::select(
					'*'
				)
				->from(self::$table_name)
				->where('del_flg', '<>', 1)
				->and_where('id', '=', $id)
				->and_where('uid', '=', $uid);
		return DbHelper::query_exec($query, true);
	}

	/**
	 * select_reserve_db
	 * sessionデータにセットされている予約idに該当するレコードを
	 * 取得します
	 * 
	 * @param mix array (session reserve_dt)
	 * @return mix array $result
	 */
	public static function select_by_session_dt ($session_dt)
	{
		$merged_array = array();
		foreach ($session_dt as $val)
		{
			$merged_array = array_merge($merged_array, $val);
		}
		unset($val);
		$query = DB::SELECT('*')
			->from(self::$table_name)
			->where('id', 'in', $merged_array);
		return DbHelper::query_exec($query, 'id');
	}

	/**
	 * validation_reserve_cancel
	 * 予約取り消し用validation
	 * 
	 * @param int uid
	 * @param mix input data
	 */
	public static function validation_reserve_cancel($uid, $input_data)
	{
		$result = true;

		/*********************
		 * レコードの存在チェック
		 *********************/



		//$input_data

		/**************
		 * 対象商品チェック
		 **************/
		$item_data = Model_Mt_Item::select_item_by_id($input_data['item_id'], true);
		if (
			!$result ||
			!(
				isset($item_data['selling_date']) &&
				!empty($item_data['selling_date']) &&
				Func::check_reserve_date($item_data['selling_date'])
			)
		)
			$result = false;

		/**************
		 * 在庫チェック
		 **************/
		if (
			!$result ||
			Model_Dt_Stock::select_stock_by_item_id($input_data['item_id']) < $input_data['count']
		)
			$result = false;

		return $result;
	}

	/**
	 * update_by_ids
	 * 
	 * 複数のidをキーにupdateします
	 * 
	 * @param mix update columns
	 * @param mix ids array
	 */
	public static function update_by_ids ($columns, $ids)
	{
		$query = DB::update(self::$table_name)->set($columns)->where('id', 'in', $ids);
		return DbHelper::query_exec($query, false, false, 'master');
	}
}