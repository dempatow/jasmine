<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Category Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Category extends \Orm\Model
{
	protected static $table_name = 'dt_category';

	protected static $_properties = array(
	);

	/**
	 * select_category_by_item_id
	 * 
	 * 商品idをキーにしてcategoryデータを取得します
	 * キーをcategory_idにします
	 * 
	 * @param item_id
	 * @return category data array
	 */
	public static function select_category_by_item_id ($item_id)
	{
		$redis_key = 'dt_category_'.$item_id;
		$result = RedisHelper::get_master($redis_key);

		if (!is_array($result) && !$result)
		{
			$query = DB::select('category_id')
						->from(self::$table_name)
						->where('item_id', '=', $item_id)
						->and_where('del_flg', '=', 0);
			$result = DbHelper::query_exec($query, 'category_id');

			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
}