<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Point Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Point extends \Orm\Model
{
	protected static $table_name = 'dt_point';

	protected static $_properties = array(
	);

	/**
	 * update_point_by_uid
	 * 
	 * @param add_point
	 * @param uid
	 * @retun result data
	 */
	public static function update_point_by_uid ($add_point, $uid)
	{
		$query = DB::query(
			'update `'.self::$table_name.'` set `point`=`point`+('.$add_point.'), `updated`="'.date('Y-m-d H:i:s').'" where `uid`= '.$uid
		);

		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * select_point_by_uid
	 * 
	 * @param uid
	 * @parama is_exits_record default = null
	 * @return point
	 */
	public static function select_point_by_uid ($uid, $is_exits_record = null)
	{
		$query = Db::select('point')
			->from(self::$table_name)
			->where('uid', '=', $uid)
			->and_where('del_flg', '=', 0);

		$result = DbHelper::query_exec($query, true);

		if (is_null($is_exits_record))
		{
			//レコードが無い場合は0を返却します
			return (isset($result['point']))? $result['point']: 0;
		}
		else
		{
			//レコードの有無をbooleanで返却します
			return (isset($result['point']))? true: false;
		}
	}

	/**
	 * insert
	 * 
	 * @param insert data array
	 * @return insert result
	 */
	public static function insert ($insert_array)
	{
		$query = Db::insert(self::$table_name)
				->set($insert_array);
		return DbHelper::query_exec($query, false, false, 'master');
	}
}