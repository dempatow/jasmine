<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Purchasespecialselect Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Purchasespecialselect extends \Orm\Model
{
	public static $table_name = 'dt_purchasespecialselect';

	protected static $_properties = array(
	);

	/**
	 * get_query_insert
	 * 
	 * @param type $data insert data array
	 * @return query object
	 */
	public static function get_query_insert ($data)
	{
		return $query = DB::insert(self::$table_name)->set($data);
	}

	/**
	 * select_by_purchase_id
	 * 
	 * @param int $purchase_id
	 */
	public static function select_by_purchase_id ($purchase_id)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('purchase_id', '=', $purchase_id)
					->and_where('del_flg', '<>', 1);

		return DbHelper::query_exec($query, false, false);
	}

	/**
	 * select_by_purchase_id_and_item_id
	 * 
	 * @param int $purchase_id
	 */
	public static function select_by_purchase_id_and_item_id ($purchase_id, $item_id)
	{
		$query = DB::select(
						'purchase_id',
						'history_id',
						'key_id',
						'value_id',
						'key',
						'value',
						'item_id',
						'count'
					)
					->from(self::$table_name)
					->where('purchase_id', '=', $purchase_id)
					->and_where('item_id', '=', $item_id)
					->and_where('del_flg', '<>', 1);

		return DbHelper::query_exec($query, false, false);
	}

	/**
	 * get_query_select_by_purchase_id_and_item_id
	 * 
	 * @param int $purchase_id
	 */
	public static function get_query_select_by_purchase_id_and_item_id ($purchase_id, $item_id)
	{
		$query = DB::select(
						'purchase_id',
						'history_id',
						'key_id',
						'value_id',
						'key',
						'value',
						'item_id',
						'count'
					)
					->from(self::$table_name)
					->where('purchase_id', '=', $purchase_id)
					->and_where('item_id', '=', $item_id)
					->and_where('del_flg', '<>', 1);

		return $query;
	}

	/**
	 * get_query_update_del_flg_by_id
	 * 
	 * del_flg更新クエリ取得メソッド
	 * 
	 * @param type $id
	 */
	public static function get_query_update_del_flg_by_id ($id, $del_flg = 1)
	{
		$query = DB::update(self::$table_name)
					->set(
						array(
							'del_flg' => $del_flg
						)
					);

		//idが配列(複数)か単独か
		if (is_array($id))
		{
			$query->where('id', 'in', $id);
		}
		else
		{
			$query->where('id', '=', $id);
		}	

		return $query;
	}
}