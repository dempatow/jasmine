<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Changepasswd Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Changepasswd extends \Orm\Model
{

	protected static $_properties = array(

		'id',
		'password' => array(
			'data_type' => 'varchar',
			'label' => 'PASS',
			'validation'=> array(
				'required',
				'checkAlphaNumeric',
				'min_length' => array('4'),
				'max_length' => array('20'),
			),
		),
		'mail' => array(
			'data_type' => 'varchar',
			'label' => 'MAIL',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('100'),
			),
		),
		'del_flg' => array(
			'skip' => true,
		),
		'updated' => array(
			'skip' => true,
		),
		'created' => array(
			'skip' => true,
		),
		//----------validation only-----------
		'consent' => array(
			'label' => '同意',
			'validation' => array(
				'required',
			),
		),
		'detail' => array(
			'label' => '問い合わせ内容',
			'validation' => array(
				'required',
				'checkcharNumJp'=> array('500'),
			),
		),
		'contact_mail' => array(
			'data_type' => 'varchar',
			'label' => 'メールアドレス',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('100'),
			),
		),
		'company_name' => array(
			'data_type' => 'varchar',
			'label' => '会社名',
			'validation'=> array(
				'required',
				'checkcharNumJp'=> array('30'),
			),
		),
		'charge_name' => array(
			'data_type' => 'varchar',
			'label' => 'ご担当者名',	
			'validation'=> array(
				'required',
				'checkcharNumJp'=> array('20'),
			),
		),
		'url' => array(
			'data_type' => 'varchar',
			'label' => '貴社サイトURL',
			'validation'=> array(
				'required',
				'max_length' => array('100'),
				'checkUrl',
			),
		),
		'withdrawal_mail' => array(
			'label' => 'メールアドレス',
			'validation' => array(
				'required',
				'checkSameValue' => array('dt_users', 'mail', true),
				'checkEmail',
				
			),
		),
	);
	
	
	/**
	 * get_query_insert_changepasswd
	 * 
	 * changepasswd tableにインサートするquery
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_insert_changepasswd($uid, $hash)
	{
		$query = DB::insert('dt_changepasswd')
							->set(
								array(
									'uid' => $uid,
									'hash' => $hash,
								)
							);
		return $query;
	}

	/**
	 * get_query_update_changepasswd_status
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_update_changepasswd_status($uid, $status = 0)
	{
		return $query = DB::update('dt_changepasswd')
					->set(array('status' => $status))
					->where('uid', '=', $uid);
	}

	/**
	 * get_query_update_changepasswd_del_flg
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_update_changepasswd_del_flg($id, $del_flg = 1)
	{
		return $query = DB::update('dt_changepasswd')
					->set(array('status' => $status))
					->where('id', '=', $id);
	}

	/**
	 * get_query_select_changepasswd_by_uid
	 * 
	 * @access public
	 * @param uid, status = 0
	 * @return queryObject
	 */
	public static function get_query_select_changepasswd_by_uid($uid ,$status = 0)
	{
		return $query = DB::select(
							'id',
							'uid',
							'hash'
						)
						->from('dt_changepasswd')
						->where('uid', '=', $uid)
						->and_where('del_flg', '<>', 1)
						->and_where('status', '=', 0);
	}
}