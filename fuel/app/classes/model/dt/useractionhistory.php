<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The UserActionHistory Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Useractionhistory extends \Orm\Model
{
	protected static $table_name = 'dt_useractionhistory';

	protected static $_properties = array(
	);

	/**
	 * insert_history
	 * 
	 * ユーザー行動履歴をinsertします
	 * 
	 * @param $action_cd $uid = null
	 */
	public static function insert_history ($action_cd, $uid = null, $memo = null)
	{
		$insert_data = array();
		$insert_data['action_cd'] = $action_cd;

		if (!is_null($uid))
			$insert_data['uid'] = $uid;

		if (!is_null($memo))
			$insert_data['memo'] = $memo;

		$query = DB::insert(self::$table_name)->set($insert_data);
		DbHelper::query_exec($query, false, false, 'master');
	}
}