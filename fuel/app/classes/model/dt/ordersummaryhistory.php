<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The order summary history Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Ordersummaryhistory extends \Orm\Model
{
	private static $table_name = 'dt_order_summary_history';
	
	protected static $_properties = array(
		'id',
	);

	/**
	 * get_query_update_status
	 * 
	 * order tableにインサートする
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_update_status( $summary_id, $status)
	{
		return $query = DB::update('dt_order_summary_history')
					->set(array('status' => $status))
					->where('id', '=', $summary_id);
	}

	/**
	 * get_query_insert_order_commodity
	 * 
	 * summary_idを取得する
	 * @param status 
	 *   0 → statusが0かつ最新のもの、ない場合はインサートする
	 *   1 → statusが1かつ最新のもの
	 *
	 * @access public
	 * @return summary_id
	 */
	public static function get_current_summary_id ($status = 0)
	{
		//-----社内からデータが確定されたもの
		if ($status == 1)
		{
			//$query = DB::select('*')
			$query = DB::select(DB::expr('max(id) as id'))
					->from(self::$table_name)
					->where('status', '=', $status)
					->and_where('del_flg', '<>' ,1);
			$summary_id = DbHelper::query_exec($query, true);
			return (isset($summary_id['id'])? $summary_id['id']: false);
		}
		else
		{
		//-----データが未確定のもの(注文受付中)
			$query = DB::select('id')
						->from(self::$table_name)
						->where('del_flg', '<>', 1)
						->and_where('status', '=', $status);
			$db_data = DbHelper::query_exec($query, true);

			if($db_data)
			{
				//select したデータを素直にreturn
				return (isset($db_data['id']))? $db_data['id']:false;
			}
			else
			{
				//未確定のsummary_idがない場合は新規でインサートする
				$db_data = DbHelper::query_exec(DB::insert(self::$table_name)->set(array('status' => $status)));
				return (isset($db_data['insert_id']))? $db_data['insert_id']: false;
			}
		}
	}
}