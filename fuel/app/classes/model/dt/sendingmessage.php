<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Sendingmessage Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Sendingmessage extends \Orm\Model
{
	protected static $table_name = 'dt_sendingmessage';
	protected static $_properties = array();

	/**
	 * get_holiday
	 * 
	 * @access public
	 * @return array ex)20140212
	 */
	public static function select_by_item_id($item_id)
	{
		$now   = date("Y-m-d H:i:s");
		$query = DB::select('*')
					->from(self::$table_name)
					->where('item_id', '=', $item_id)
					->and_where('del_flg','<>', 1);

		$result = DbHelper::query_exec($query);
		return $result;
	}

	/**
	 * 
	 */
	public static function select_message_ids_by_item_id ($item_id)
	{
		$result = array();

		$now    = date("Y-m-d H:i:s");
		$query  = DB::select('*')
					->from(self::$table_name)
					->where('item_id', '=', $item_id)
					->and_where('del_flg','<>', 1);
		$db     = DbHelper::query_exec($query);

		for ($i=0; $i<count($db); $i++)
			$result[] = $db[$i]['ms_id'];

		return $result;
	}
}