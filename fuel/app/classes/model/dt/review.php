<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Review Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Review extends \Orm\Model
{
	protected static $table_name = 'dt_review';

	protected static $_properties = array(
		'evaluation' => array(
			'label' => 'おすすめ度',
			'validation'=> array(
				'required',
				'checkisnumeric',
				'numeric_min' => array(0),
				'numeric_max' => array(Controller_User_Review::EVALUATION_MAX),
			),
		),
		'contributor' => array(
			'label' => 'ニックネーム',
			'validation'=> array(
				'required',
				'checkCharNumJp' => array(10),
			),
		),
		'title' => array(
			'label' => 'レビューのタイトル',
			'validation'=> array(
				'required',
				'checkCharNumJp' => array(30),
			),
		),
		'comment' => array(
			'label' => 'レビューの内容',
			'validation'=> array(
				'required',
				'checkCharNumJp' => array(300),
			),
		),
	);

	/**
	 * select_review_by_item_id
	 * 
	 * item_idをキーにreviewを取得します
	 * 
	 * @param item_id
	 * @return review data array
	 */
	public static function select_review_by_item_id ($item_id)
	{
		$query = DB::select(
					'contributor',
					'title',
					'evaluation',
					'comment'
				)
					->from(self::$table_name)
					->where('item_id', '=',$item_id)
					->and_where('del_flg', '=', 0)
					->and_where('approval_flg', '=', 1);
		return DbHelper::query_exec($query);
	}

	/**
	 * insert
	 * 
	 * @parma insert_array
	 */
	public static function insert ($insert_array)
	{
		$query = DB::insert(self::$table_name)->set($insert_array);
		return DbHelper::query_exec($query, false, false, 'master');
	}
}