<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Stock Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Stock extends \Orm\Model
{
	const DISP_MAX_COUNT = 20;
	protected static $table_name = 'dt_stock';

	protected static $_properties = array(
	);

	/**
	 * 在庫更新(減算)
	 * subtraction_stock
	 */
	public static function subtraction_stock ($item_id, $count)
	{
		$query = DB::query(
			'update 
				`dt_stock`
			set
				`count`= `count`-'.$count.',
				`updated`="'.date('Y-m-d H:i:s').'"
			where
				`item_id` ='. $item_id .'
			 and 
				`del_flg` = 0'
		);
		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * 在庫更新(加算)
	 * subtraction_stock
	 */
	public static function add_stock ($item_id, $count)
	{
		$query = DB::query(
			'update 
				`dt_stock`
			set
				`count`= `count`+'.$count.',
				`updated`="'.date('Y-m-d H:i:s').'"
			where
				`item_id` ='. $item_id .'
			 and 
				`del_flg` = 0'
		);
		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * select_stock_by_item_id
	 * 
	 * 商品在庫数を取得します。
	 * 
	 * @param $item_id
	 * @return queryObject
	 */
	public static function select_stock_by_item_id($item_id)
	{
		//なるべくリアルタイムなデータが欲しいのでマスターから取得する
		$query = DB::select('count')
					->from('dt_stock')
					->and_where('item_id', '=', $item_id)
					->and_where('del_flg', '=', 0);
		return (Int)DbHelper::query_exec($query,true, false, 'master')['count'];
	}

	/**
	 * check_stock
	 * 
	 * cart_dtの商品在庫をチェックし、
	 * 在庫が足りない商品はその配列要素から除いて返却する
	 * 不足データがある場合はカートセッションをセットし直す
	 * dataがない場合はfalseを返却する
	 * 
	 * @param cart data
	 * @return cart data or false
	 */
	public static function check_stock($cart_dt, $special_select_dt = array())
	{
		$result_array  = array(
			'result' => true, //返却用結果
		);
		$checked_array   = array();
		$diff_array      = array();
		$is_delete       = false; //削除したカートアイテムがあるかどうか
		$reserve_session = Session::get('reserve_dt', null); //予約商品カート情報取得

		for ($i=0; $i<count($cart_dt); $i++)
		{
			$stock = self::select_stock_by_item_id($cart_dt[$i]['id']);
			if (!empty($reserve_session) && isset($reserve_session[$cart_dt[$i]['id']]))
				$stock += Model_Dt_Reserve::select_ids_count($reserve_session[$cart_dt[$i]['id']]);

			if(
				Func::check_integrity_item_and_special_session($cart_dt[$i]['id'], $special_select_dt, $cart_dt[$i]['count']) && //商品マスタと特殊選択セッションの整合性をチェックする
				!empty($stock)   &&
				!is_null($stock) &&
				(Int)$cart_dt[$i]['count'] <= (Int)$stock &&
				Model_Dt_Specialselectstock::check_stock_special_select_purchase($cart_dt[$i]['id'], $special_select_dt)
			)
			{
				$checked_array[] = $cart_dt[$i];
			}
			else
			{
				/********************
				 * 在庫不足を発見した場合
				 ********************/

				//購入予約カートセッションを削除する
				if (!empty($reserve_session) && isset($reserve_session[$cart_dt[$i]['id']]))
					Session::delete('reserve_dt.'.$cart_dt[$i]['id']);

				//item_idに紐づいたspecial_select sessionを削除する
				Func::delete_special_select_by_item_id($cart_dt[$i]['id']);

				$result_array['result'] = false;
				$is_delete = true;
				$diff_array[] = $cart_dt[$i];
			}
		}

		//cart session reset
		if(count($checked_array) === 0)
		{
			//cart情報がなくなった時
			$checked_array = false;
			Session::delete('cart_dt');
			Session::delete('reserve_dt');
			Session::delete('special_select_dt');
		}
		else if ($is_delete)
		{
			//削除された値がある時はsessionをreset
			Session::set('cart_dt', $checked_array);
		}

		count($diff_array)>0 and $result_array['diff'] = $diff_array;
		return $result_array;
	}

/**
予約機能実装前
退避中
	public static function check_stock($cart_dt)
	{
		$result_array  = array('result' => true,);
		$checked_array = array();
		$diff_array    = array();

		for ($i=0; $i<count($cart_dt); $i++)
		{
			$is_delete = false;
			$stock = self::select_stock_by_item_id($cart_dt[$i]['id']);
			if(
				!empty($stock) &&
				!is_null($stock) &&
				(Int)$cart_dt[$i]['count'] <= (Int)$stock
			)
			{
				$checked_array[] = $cart_dt[$i];
			}
			else
			{
				//在庫不足を発見した場合
				$result_array['result'] = false;
				$is_delete = false;
				$diff_array[] = $cart_dt[$i];
			}
		}

		//cart session reset
		if(count($checked_array) == 0)
		{
			//cart情報がなくなった時
			$checked_array = false;
			Session::delete('cart_dt');
		}
		else if (!$is_delete)
		{
			//削除された値がある時はsessionをreset
			Session::set('cart_dt', $checked_array);
		}

		count($diff_array)>0 and $result_array['diff'] = $diff_array;

		return $result_array;
	}
 */
}