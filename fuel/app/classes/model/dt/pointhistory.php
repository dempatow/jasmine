<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Pointhistory Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Pointhistory extends \Orm\Model
{
	public static $table_name = 'dt_pointhistory';

	protected static $_properties = array(
		//確認画面のポイント使用押下時
		'use_point' => array(
			'label' => '使用するポイント',
			'validation' => array(
				'checkisnumeric' => array(true),
				'checkusepoint' => array(true),
			),
		),
		//確認画面の確定ボタン押下時
		'use_point_hidden' => array(
			'label' => '使用するポイント',
			'validation' => array(
				'checkisnumeric' => array(true),
				'checkusepoint' => array(true),
			),
		),
	);

	/**
	 * get_query_insert
	 * 
	 * @param type $data insert data array
	 * @return query object
	 */
	public static function get_query_insert ($data)
	{
		return $query = DB::insert(self::$table_name)->set($data);
	}

	/**
	 * update_by_purchase_id
	 * 
	 * @param update_array
	 * @param purchase_id
	 * @param avail_flg = default null
	 * @param dela_flg  = default null
	 * @return query Object
	 */
	public static function update_by_purchase_id ($update_array,$purchase_id, $avail_flg = null, $del_flg = null)
	{
		$query = DB::update(self::$table_name)
			->set($update_array)
			->where('purchase_id', '=', $purchase_id);

		if (!is_null($avail_flg))
			$query->and_where('avail_flg', '=', $avail_flg);

		if (!is_null($del_flg))
			$query->and_where('del_flg', '=', $del_flg);

		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * select_point_by_purchase_id
	 * 
	 * @param purchase_id
	 * @param avail_flg default null
	 * @param del_flg default null
	 * @return point
	 */
	public static function select_by_purchase_id ($purchase_id, $avail_flg=null, $del_flg=null)
	{
		$query = DB::select('*')
			->from(self::$table_name)
			->where('purchase_id', '=', $purchase_id);

		if (!is_null($avail_flg))
			$query->and_where('avail_flg', '=', $avail_flg);

		if (!is_null($del_flg))
			$query->and_where('del_flg', '=', $del_flg);

		$result = DbHelper::query_exec($query);

		return ($result>0)? $result: false;
	}

	/**
	 * select_new_invalidation
	 * 
	 * 新しく失効するポイントレコードを取得します
	 * (既に失効したポイントは取得しません)
	 * 
	 * @param uid user_id
	 * @param set_time 有効期間
	 */
	public static function select_new_invalidation ($uid, $set_time)
	{
		$set_time = date(
			'Y-m-d H:i:s',
			mktime(
				date('H'),
				date('i'),
				date('s'),
				date('m') -$set_time,
				date('d'),
				date('Y')
			)
		);

		$query = DB::select('*')
			->from(self::$table_name)
			->where('uid', '=', $uid)
			->and_where('point', '>', 0)
			->and_where('invalidation_flg', '=', 0)
			->and_where('avail_flg', '=', 1)
			->and_where('updated', '<', $set_time)
			->and_where('del_flg', '=', 0);

		return DbHelper::query_exec($query);
	}

	/**
	 * update_by_ids
	 * 
	 * where in句を使ってupdateします
	 * 
	 * @param update_data
	 * @param ids_array
	 * 
	 * return update result
	 */
	public static function update_by_ids ($update_data, $ids_array)
	{
		$query = DB::update(self::$table_name)
					->set($update_data)
					->where('id', 'in', $ids_array);

		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * select_current_point_by_uid
	 * 
	 * 有効なポイントレコードのポイントを合計して返却します
	 * 
	 * @param  uid
	 * @retrun point
	 */
	public static function select_current_point_by_uid ($uid)
	{
		$query = DB::select(DB::expr('sum(point) as point'))
					->from(self::$table_name)
					->where('avail_flg', '=', 1)
					->and_where('uid', '=', $uid)
					->and_where('del_flg', '=', 0);
		$result = DbHelper::query_exec($query, true);
		return (isset($result['point']))? (Int)$result['point']: 0;
	}

	/**
	 * get_query_select_avail_by_uid
	 * 
	 * uidをキーに有効なレコードを全て取得します
	 * 
	 * @parma int uid
	 * @retun query object
	 */
	public static function get_query_select_avail_by_uid ($uid)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('uid', '=', $uid)
					->and_where('avail_flg', '=', 1)
					->and_where('del_flg','=', 0)
					->order_by('created', 'desc');
		return $query;
	}

	/**
	 * get_query_select_count_avail_by_uid
	 * 
	 * uidをキーに有効なレコードのカウントを取得します
	 * 
	 * @param int uid
	 * @return query object
	 */
	public static function get_query_selec_count_avail_by_uid ($uid)
	{
		$query = DB::select(DB::expr('COUNT(*) as count'))
					->from(self::$table_name)
					->where('uid', '=', $uid)
					->and_where('avail_flg', '=', 1)
					->and_where('del_flg','=', 0);
		return $query;
	}

	/**
	 * selct_count_avail_by_uid
	 * 
	 * uidをキーにavail_flg = 1 and del_flg =0 の
	 * 合計ポイントを返却します
	 * 
	 * @param int $uid
	 * @return int total avail point
	 * 
	 */
	public static function select_count_avail_by_uid ($uid)
	{
		$query = self::get_query_selec_count_avail_by_uid($uid);
		return DbHelper::query_exec($query, true);
	}
}