<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The ItemAvailSending Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_Itemavailsending extends \Orm\Model
{

	protected static $table_name = 'dt_itemavailsending';
	protected static $_properties = array(
	);

	/**
	 * select_by_item_id
	 * 
	 * @param item_id
	 * @return sending_cds_array
	 */
	public static function select_by_item_id ($item_id)
	{
		$query = DB::select('sending_cd', 'priority_flg')
					->from(self::$table_name)
					->and_where('item_id', '=', $item_id)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->and_where('del_flg', '=', 0);
		return DbHelper::query_exec($query, 'sending_cd');
	}

	/**
	 * select_sending
	 * 
	 * @param $item_id
	 * @return dt_itemavailsending join mt_sending
	 * 
	 * dt_itemavailsendingとmt_sendingをjoinしたデータを返却します
	 */
	public static function select_sending ($item_id)
	{
		$query = DB::select(
						'mt.id',
						'mt.name'
					)
					->from(array(self::$table_name,'dt'))
					//--発送方法マスタ
					->join(array('mt_sending', 'mt'),'left')
					->on('dt.sending_cd', '=', 'mt.id')
					//商品id
					->and_where('dt.item_id', '=', $item_id)
					//期限チェック
					->and_where('dt.open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('dt.close_date', '>=', date("Y-m-d H:i:s"))
					->and_where('mt.open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('mt.close_date', '>=', date("Y-m-d H:i:s"))
					//削除flgチェック
					->and_where('mt.del_flg', '=', 0)
					->and_where('dt.del_flg', '=', 0);
		return DbHelper::query_exec($query);
	}

	/**
	 * get_query_insert_user
	 * 
	 * 新規登録userのsql
	 * 
	 * @param
	 * @return queryObject
	public static function get_query_insert_user ($data)
	{
		return $query = DB::insert('dt_user')->set($data);
	}
	 */

	/**
	 * get_query_insert_user
	 * 
	 * 新規登録userのsql
	 * 
	 * @param
	 * @return queryObject
	public static function get_query_update_login_flg ($id ,$column, $value)
	{
		//return $query = DB::insert('dt_user')->set($data);

		return $query = DB::update('mt_options')
					->set(
							array(
								$column => $value,
								)
							)
					->where('id', '=', $id);
	}
	 * 
	 */

	/**
	 * get_query_select_user_by_userid
	 * 
	 * userid を キーにしてuserデータを取得します。
	 * 
	 * @access public
	 * @param  $uid, $del_flg = 0, $auto_login = null, $onetime_flg = null, $role_cd = null
	 * @return queryObject
	public static function get_query_select_user_by_userid($uid, $del_flg = 0, $auto_login = null, $onetime_flg = null, $role_cd = null)
	{
		return $query = DB::select('*')
						->from(self::$table_name)
						->where('id', '=', $uid)
						->and_where('del_flg',     '=', $del_flg);

		if (!is_null($role_cd))
			$query->and_where('role_cd', '=', $role_cd);

		if (!is_null($auto_login))
			$query->and_where('auto_login', '=', $auto_login);

		if (!is_null($onetime_flg))
			$query->and_where('onetime_flg', '=', $onetime_flg);
	}
	*/

	/**
	 * get_query_select_user_by_mail
	 * 
	 * @return queryObject

	public static function get_query_select_user_by_mail($mail)
	{
		return $query = DB::select('*')
				->from(self::$table_name)
				->where('mail', '=', $mail)
				->and_where('del_flg', '<>', 1)
				->and_where('onetime_flg', '<>', 1);
	}
	 */

	/**
	 * get_query_select_user_by_mail
	 * 
	 * mailをキーにuserデータを引きます
	 * @param $mail, $del_flg = 0, $auto_login = null, $onetime_flg = null, $role_cd = null
	 * @return queryObject
	public static function get_query_select_user_by_mail($mail, $del_flg = 0, $auto_login = null, $onetime_flg = null, $role_cd = null)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('mail', '=', $mail)
					->and_where('del_flg','=', $del_flg);

		if (!is_null($role_cd))
			$query->and_where('role_cd', '=', $role_cd);

		if (!is_null($auto_login))
			$query->and_where('auto_login', '=', $auto_login);

		if (!is_null($onetime_flg))
			$query->and_where('onetime_flg', '=', $onetime_flg);

		return $query;
	}
	 * 
	 */

	/**
	 * get_login_query
	 * メールとパスワード入力でのログイン時に使用する
	 * @return login query
	public static function get_login_query($mail)
	{
		return DB::select('*')
					->from('dt_user')
					->where('mail', '=', $mail)
					->and_where('del_flg', '<>', 1)
					->and_where('onetime_flg', '<>', 1);
	}
	 * 
	 */

	/**
	 * update_flg
	 * 
	 * @param uid, flg_name, flg = 1
	 * @return bool
	public static function update_flg ($uid, $flg_name, $flg = 1)
	{
		$query = DB::update(self::$table_name)
					->set(array($flg_name => $flg))
					->where('id', '=', $uid);
		return (bool)DbHelper::query_exec($query, false, false, 'master');
	}
	 * 
	 */

	/**
	 * update_passwd
	 * 
	 * パスワードをupdateします
	 * 
	public static function update_passwd ($uid, $hash_passwd)
	{
		$query = DB::update(self::$table_name)
					->set(array('passwd' => $hash_passwd))
					->where('id', '=', $uid);
		return DbHelper::query_exec($query, false, false, 'master');
	}
	 * 
	 */

	/**
	 * select_user_by_uid_and_passwd ()
	 * 
	 * パスワード変更validatationで使用している
	 * 
	 * @param  $mail, $hash_passwd
	 * @return user_data_row

	public static function select_user_by_uid_and_passwd ($uid, $hash_passwd)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('id','=', $uid)
					->and_where('passwd',     '=', $hash_passwd)
					->and_where('del_flg',    '=', 0)
					->and_where('onetime_flg','=', 0);
		return DbHelper::query_exec($query, true);
	}
	 * 
	 */
}