<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The User Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Dt_User extends \Orm\Model
{

	public static $table_name = 'dt_user';

	protected static $_properties = array(
		'mail' => array(
			'label' => 'メールアドレス',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('255'),
			),
		),
		//------パスワードリセットの際に使用------
		'repwd_mail' => array(
			'label' => 'メールアドレス',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('255'),
				'checkUniquUserMail' => array(1)
			),
		),
		//------会員情報変更の際に使用------
		'mail_change' => array(
			'label' => 'メールアドレス',
			'validation' => array(
				'required',
				'max_length' => array('255'),
				'checkEmail',
				'checkIsSame'=>array('mail_change_confirm','『メールアドレス(確認用)』'),
				'checkChangeMailDb',
			),
		),
		'mail_signup' => array(
			'label' => 'メールアドレス',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('255'),
				'checkUniquUserMail',
				'checkIsSame'=>array('mail_confirm','メールアドレス(確認用)'),
			),
		),
		'corporate_name' => array(
			'label' => '法人名/団体名',
			'validation' => array(
				'checkCharNumJp'=> array('30'),
			),
		),
		'corporate_name_kana' => array(
			'label' => '法人名/団体名(かな)',
			'validation' => array(
				'checkCharNumJp'=> array('30'),
			),
		),
		'last_name' => array(
			'label' => 'お名前(姓)',
			'validation' => array(
				'required',
				'checkCharNumJp'=> array('30'),
			),
		),
		'first_name' => array(
			'label' => 'お名前(名)',
			'validation' => array(
				'required',
				'checkCharNumJp'=> array('30'),
			),
		),
		'last_name_kana' => array(
			'label' => 'お名前(姓)かな',
			'validation' => array(
				'checkCharNumJp'=> array('30'),
			),
		),
		'first_name_kana' => array(
			'label' => 'お名前(名)かな',
			'validation' => array(
				'checkCharNumJp'=> array('30'),
			),
		),
		'passwd' => array(
			'label' => 'パスワード',
			'validation' => array(
				'required',
				'checkIsSame'=>array('passwd_confirm','パスワード(確認用)'),
				'checkAlphaNumeric',
				'min_length' => array('6'),
				'max_length' => array('10'),
			),
		),

		//-----ログインの際に使用する-----
		'passwd_auth' => array(
			'label' => 'パスワード',
			'validation' => array(
				'required',
				'checkAlphaNumeric',
				'min_length' => array('6'),
				'max_length' => array('10'),
			),
		),
		//-----パスワードを変更する際に使う--------
		'current_passwd' => array(
			'label' => '現在のパスワード',
			'validation' => array(
				'required',
				'checkAlphaNumeric',
				'min_length' => array('6'),
				'max_length' => array('10'),
				'checklogin',
				'checkcurrentpasswd',
			),
		),
		'new_passwd' => array(
			'label' => '変更後のパスワード',
			'validation' => array(
				'required',
				'checkIsSame'=>array('new_passwd_confirm','変更後のパスワード(確認用)'),
				'checkAlphaNumeric',
				'min_length' => array('6'),
				'max_length' => array('10'),
			),
		),

		//-----退会の際に使用する-----
		'password_withdrawal' => array(
			'label' => 'パスワード',
			'validation' => array(
				'required',
				'min_length' => array('6'),
				'max_length' => array('10'),
				'checkAlphaNumeric',
				'checkwithdrawalpasswd'
			),
		),
		//----退会用同意チェックボックス
		'consent_withdrawal' => array(
			'label' => '注意事項のご確認チェック',
			'validation' => array(
				'required',
			),
		),

		'zip' => array(
			'label' => '郵便番号',
			'validation' => array(
				'required',
				'checkZip',
			),
		),
		'address1' => array(
			'label' => '市区郡',
			'validation' => array(
				'required',
				'checkCharNumJp'=> array('100'),
			),
		),
		'address2' => array(
			'label' => '町村字番地',
			'validation' => array(
				'required',
				'checkCharNumJp'=> array('100'),
			),
		),
		'address3' => array(
			'label' => '建物名',
			'validation' => array(
				'checkCharNumJp'=> array('100'),
			),
		),

		'tel1' => array(
			'label' => '電話番号',
			'validation' => array(
				'checkrequiredmult' => array(
					array ('tel1', 'tel2', 'tel3')
				),
				'checktel' => array ('tel1', 'tel2', 'tel3'),
			),
		),
		'fax1' => array(
			'label' => 'ファックス',
			'validation' => array(
				'checktel' => array ('fax1', 'fax2', 'fax3', true),
			),
		),
		'birth_year' => array(
			'label' => 'お誕生日',
			'validation' => array(
//				'checkrequiredmult' => array(
//					array ('birth_year', 'birth_month', 'birth_day',),
//					true,
//				),
				'checkdate' => array(
					'birth_year',
					'birth_month',
					'birth_day',
					true,
				),
			),
		),
		'job_cd' => array(
			'label' => 'ご職業',
			'validation' => array(
				'checkmtvalue' => array('mt_job',true),
			),
		),
		'sex_cd' => array(
			'label' => '性別',
			'validation' => array(
				'checkmtvalue' => array('mt_sex', true),
			),
		),
		'state_cd' => array(
			'label' => '都道府県',
			'validation' => array(
				'required',
				'checkmtvalue' => array('mt_state'),
			),
		),
//		'age_cd' => array(
//			'label' => 'ご年代',
//			'validation' => array(
//				'checkisnumeric',
//				'checkmtvalue' => array('mt_age'),
//			),
//		),
		'magazine_cd' => array(
			'label' => 'メールマガジン利用の区分',
			'validation' => array(
				'checkmtvalue' => array('mt_magazine'),
			),
		),
		'consent' => array(
			'label' => '同意',
			'validation' => array(
				'required',
			),
		),
		'auto_login' => array(
			'label' => '自動ログイン利用',
			'validation' => array(
				'checkisnumeric',
				'checkflg' => array('auto_login',true)
			),
		),
		//-----購入処理の際に使用--------
		'mail_purchase' => array(
			'label' => 'mail',
			'validation' => array(
				'required',
				'checkEmail',
				'max_length' => array('255'),
				'checkIsSame'=>array('mail_purchase_confirm','メール(確認用)'),
			),
		),
		'payment_cd' => array(
			'label' => '決済方法',
			'validation' => array(
				'required',
				'checkisnumeric',
				'checkmtvalue' => array('mt_payment'),
			),
		),
		'payment_process_cd' => array(
			'label' => '使用カードについて',
			'validation' => array(
				'checkisnumeric' => array(true),
				'checkpaymentprocess'
			),
		),
//------------------------------------------//
	);

	/**
	 * get_query_update_user
	 * 
	 */
	public static function get_query_update_user ($update_array, $uid)
	{
		return $query = DB::update(self::$table_name)
							->set($update_array)
							->where('id', '=', $uid);
	}

	/**
	 * get_query_insert_user
	 * 
	 * 新規登録userのsql
	 * 
	 * @param
	 * @return queryObject
	 */
	public static function get_query_insert_user ($data)
	{
		return $query = DB::insert('dt_user')->set($data);
	}

	/**
	 * get_query_insert_user
	 * 
	 * 新規登録userのsql
	 * 
	 * @param
	 * @return queryObject
	 */
	public static function get_query_update_login_flg ($id ,$column, $value)
	{
		//return $query = DB::insert('dt_user')->set($data);

		return $query = DB::update('mt_options')
					->set(
							array(
								$column => $value,
								)
							)
					->where('id', '=', $id);
	}

	/**
	 * get_query_select_user_by_userid
	 * 
	 * userid を キーにしてuserデータを取得します。
	 * 
	 * @access public
	 * @param  $uid, $del_flg = 0, $auto_login = null, $role_cd = null
	 * @return queryObject
	 */
	public static function get_query_select_user_by_userid($uid, $del_flg = 0, $auto_login = null, $role_cd = null)
	{
		return $query = DB::select(
							'id',
							'mail',
							'corporate_name',
							'first_name',
							'last_name',
							'auto_login',
							'passwd',
							'role_cd'
						)
						->from(self::$table_name)
						->where('id', '=', $uid)
						->and_where('del_flg',     '=', $del_flg);

		if (!is_null($role_cd))
			$query->and_where('role_cd', '=', $role_cd);

		if (!is_null($auto_login))
			$query->and_where('auto_login', '=', $auto_login);
	}

	/**
	 * get_query_select_user_by_mail
	 * 
	 * @return queryObject

	public static function get_query_select_user_by_mail($mail)
	{
		return $query = DB::select('*')
				->from(self::$table_name)
				->where('mail', '=', $mail)
				->and_where('del_flg', '<>', 1);
	}
	 */

	/**
	 * get_query_select_user_by_mail
	 * 
	 * mailをキーにuserデータを引きます
	 * @param $mail, $del_flg = 0, $auto_login = null, $role_cd = null
	 * @return queryObject
	 */
	public static function get_query_select_user_by_mail($mail, $del_flg = 0, $auto_login = null, $role_cd = null)
	{
		$query = DB::select(
						'id',
						'mail',
						'corporate_name',
						'first_name',
						'last_name',
						'auto_login',
						'passwd',
						'role_cd'
					)
					->from(self::$table_name)
					->where('mail', '=', $mail)
					->and_where('del_flg','=', $del_flg);

		if (!is_null($role_cd))
			$query->and_where('role_cd', '=', $role_cd);

		if (!is_null($auto_login))
			$query->and_where('auto_login', '=', $auto_login);

		return $query;
	}

	/**
	 * get_login_query
	 * メールとパスワード入力でのログイン時に使用する
	 * @return login query
	 */
	public static function get_login_query($mail)
	{
		return DB::select(
						'id',
						'mail',
						'corporate_name',
						'first_name',
						'last_name',
						'auto_login',
						'passwd',
						'role_cd'
					)
					->from('dt_user')
					->where('mail', '=', $mail)
					->and_where('del_flg', '<>', 1);
	}

	/**
	 * update_flg
	 * 
	 * @param uid, flg_name, flg = 1
	 * @return bool
	 */
	public static function update_flg ($uid, $flg_name, $flg = 1)
	{
		$query = DB::update(self::$table_name)
					->set(array($flg_name => $flg))
					->where('id', '=', $uid);
		return (bool)DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * update_passwd
	 * 
	 * パスワードをupdateします
	 * 
	 */
	public static function update_passwd ($uid, $hash_passwd)
	{
		$query = DB::update(self::$table_name)
					->set(array('passwd' => $hash_passwd))
					->where('id', '=', $uid);
		return DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * select_user_by_uid_and_passwd ()
	 * 
	 * パスワード変更validatationで使用している
	 * 
	 * @param  $mail, $hash_passwd
	 * @return user_data_row
	 */
	public static function select_user_by_uid_and_passwd ($uid, $hash_passwd)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('id','=', $uid)
					->and_where('passwd',     '=', $hash_passwd)
					->and_where('del_flg',    '=', 0);
		return DbHelper::query_exec($query, true);
	}

	/**
	 * select_user_by_uid
	 * 
	 * uidをキーにしてselectします
	 * 
	 */
	public static function select_user_by_uid($uid)
	{
		$query = DB::select('*')
					->from(self::$table_name)
					->where('id', '=', $uid)
					->and_where('del_flg','=', 0);
		return DbHelper::query_exec($query, true);
	}

	/**
	 * select_point_started_by_uid
	 * 
	 */
	public static function select_point_started_by_uid ($uid)
	{
		$query = DB::select('point_started')
					->from(self::$table_name)
					->where('id', '=', $uid)
					->and_where('del_flg', '=', 0);	

		$result = DbHelper::query_exec($query, true)['point_started'];

		return (is_null($result) || empty($result))? false: $result;
	}
	
}