<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Template Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Job extends \Orm\Model
{
	protected static $table_name = 'mt_job';

	protected static $_properties = array(
	);

	/**
	 * get_job
	 * 
	 * @return mt_job data
	 */
	public static function get_job ()
	{
		$redis_key = 'mt_job';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id', 'name')
					->from(self::$table_name)
					->where('del_flg', '<>', '1')
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->order_by('id', 'asc');

			$result = DbHelper::query_exec($query, 'id');

			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
}