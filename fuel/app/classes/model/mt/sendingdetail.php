<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The SendingDetail Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Sendingdetail extends \Orm\Model
{
	protected static $table_name = 'mt_sendingdetail';

	protected static $_properties = array(
	);

	/**
	 * select
	 * 
	 * 有効なマスタを全て取得します
	 * 
	 * @access public
	 * @pamarm none
	 * @return mix array
	 */
	public static function select()
	{
		$redis_key = self::$table_name;
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('sending_id','description', 'tracking_url')
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, 'sending_id');

			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * select_by_sending_id
	 * 
	 * sending_idをキーにselectします
	 * 
	 * @parma int sending_id
	 */
	public static function select_by_sending_id ($sending_id)
	{
		$result = false;
		$details = self::select();

		isset($details[$sending_id]) and $result = $details[$sending_id];

		return $result;
	}
}