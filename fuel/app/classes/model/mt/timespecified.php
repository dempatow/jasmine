<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Timespacified Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Timespecified extends \Orm\Model
{
	protected static $table_name = 'mt_timespecified';

	protected static $_properties = array(
	);

	/**
	 * select
	 * 
	 * 発送時間帯マスタを取得します
	 * 
	 * @access public
	 * @return array
	 */
	public static function select ()
	{
		$redis_key = 'mt_timespecified';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id','name')
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, 'id');

			RedisHelper::set_master($redis_key, $result);
		}

		return $result;
	}

	/**
	 * select_by_id
	 * 
	 * @parma int id
	 * @return mix array
	 * 
	 */
	public static function select_by_id ($id)
	{
		$mt = self::select();
		return (isset($mt[$id]))? $mt[$id]: false;
	}
}