<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Character Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Character extends \Orm\Model
{
	protected static $table_name = 'mt_character';

	protected static $_properties = array(
	);

	/**
	 * select
	 *
	 * 一度DBから取得したものはredisから取得します
	 *
	 * @access public
	 * @return data array
	 */
	public static function select()
	{
		$redis_key = 'mt_character';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select(
							'id',
							'name',
							'description',
							'open_date',
							'close_date'
						)
						->from(self::$table_name)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"))
						->and_where('del_flg', '=', 0);
			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);

		}
		return $result;
	}

	/**
	 * select_by_id
	 * 
	 * @param int character_id
	 * @return mix array
	 */
	public static function select_by_id ($id)
	{
		$result = false;
		$mt_character = self::select();

		if (
			isset($mt_character[$id]) &&
			$mt_character[$id]['open_date'] <= date("Y-m-d H:i:s") &&
			$mt_character[$id]['close_date'] >= date("Y-m-d H:i:s")
		)
		{
			unset($mt_character[$id]['open_date']);
			unset($mt_character[$id]['close_date']);
			$result = $mt_character[$id];
		}

		return $result;
	}
}