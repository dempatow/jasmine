<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Message Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Message extends \Orm\Model
{
	protected static $table_name = 'mt_message';

	protected static $_properties = array(
	);

	/**
	 * select_message
	 * 
	 * messageマスタをDBから取得します
	 * 
	 * @access public
	 * @return message data
	 */
	public static function select_message()
	{
		$redis_key = 'messages';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select(
							'id',
							'message',
							'open_date',
							'close_date'
						)
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query);
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
}