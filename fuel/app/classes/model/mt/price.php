<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Template Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Template extends \Orm\Model
{
	protected static $table_name = 'mt_template';

	protected static $_properties = array(
		'id' => array(
			'label' => 'id',
			'validation'=> array(
				'checkAjaxId',
			),
		),
	);

	/**
	 * get_query_select_menu
	 * 
	 * メニューをselectするqueyrオブジェクトを返します
	 * 
	 * @access public
	 * @return query object
	 */
	public static function get_query_select_menu()
	{
		$query = DB::select('month')
					->from(self::$table_name)
					->where('del_flg', 0)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->group_by('month')
					->order_by('open_date', 'asc');
		return $query;
	}
}