<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The PointAddReason Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Pointaddreason extends \Orm\Model
{
	protected static $table_name = 'mt_pointaddreason';

	protected static $_properties = array(
	);

	/**
	 * select
	 *
	 * 一度DBから取得したものはredisから取得します
	 *
	 * @access public
	 * @return data array
	 */
	public static function select()
	{
		$redis_key = self::$table_name;
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select(
							'id',
							'name'
						)
						->from(self::$table_name)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"))
						->and_where('del_flg', '=', 0);

			$result = DbHelper::query_exec($query,'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
}