<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Purchasestatus Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Purchasestatus extends \Orm\Model
{
	public static $table_name = 'mt_purchasestatus';
	protected static $_properties = array();

	/**
	 * select
	 * @return mix arrays
	 */
	public static function select ()
	{
		$redis_key = self::$table_name;
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = Db::select('id','name')
						->from(self::$table_name)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
}