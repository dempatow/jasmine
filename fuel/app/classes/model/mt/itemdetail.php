<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Itemdetail Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Itemdetail extends \Orm\Model
{
	protected static $table_name = 'mt_itemdetail';

	protected static $_properties = array(
	);

	/**
	 * select
	 * 
	 * @param none
	 */
	public static function select ()
	{
		$redis_key = 'mt_itemdetail';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select(
						'id',
						'view_no',
						'img_cnt',
						'catch',
						'description1',
						'description2',
						'description3',
						'description4',
						'type1',
						'type2',
						'type3',
						'type4'
					)
				->from(self::$table_name)
				->where('del_flg', 0)
				->and_where('open_date', '<=', date("Y-m-d H:i:s"))
				->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * select_by_item_id
	 * 
	 * @param item_id
	 * 
	 */
	public static function select_by_item_id ($item_id)
	{
		$result = self::select();
		return isset($result[$item_id])? $result[$item_id]: false;
	}
}