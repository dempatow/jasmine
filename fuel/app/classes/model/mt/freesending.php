<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Freesending Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Freesending extends \Orm\Model
{
	protected static $table_name = 'mt_freesending';

	protected static $_properties = array(
	);

	/**
	 * select_freesending
	 * 
	 * 送料無料マスタを取得します
	 * 
	 * @access public
	 * @return freesending master
	 */
	public static function select_freesending()
	{
		$redis_key = 'mt_freesending';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('total_amount')
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));

			$result = DbHelper::query_exec($query, true);
			$result = isset($result['total_amount']) && !empty($result['total_amount'])? (Int)$result['total_amount']: false;
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
}