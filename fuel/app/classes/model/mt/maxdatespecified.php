<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Maxdatespecified Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Maxdatespecified extends \Orm\Model
{
	protected static $table_name = 'mt_maxdatespecified';

	protected static $_properties = array(
	);

	/**
	 * select
	 * 
	 * 最大日付指定マスタを取得します
	 * 
	 * @access public
	 * @return array
	 */
	public static function select ()
	{
		$redis_key = 'mt_maxdatespecified';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('day')
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, true);

			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * get_day
	 * 
	 * 最大日付指定マスタに設定されている現在の最大日を取得します
	 * @param none
	 * @return int max date
	 */
	public static function get_day ()
	{
		$result = 0;
		try
		{
			$result = (Int)self::select()['day'];
		}
		catch(Exception $e)
		{
			//TODO: mail処理用実装
			Func::write_exception_log($e);
		}
		return $result;
	}
}