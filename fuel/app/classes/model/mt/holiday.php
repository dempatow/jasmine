<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Holiday Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Holiday extends \Orm\Model
{
	protected static $table_name = 'mt_holiday';

	protected static $_properties = array(
		'id' => array(
			'label' => 'id',
			'validation'=> array(
				'checkAjaxId',
			),
		),
	);

	/**
	 * get_holiday
	 * 
	 * 休日マスタを取得します
	 * キーを年月日に指定します
	 * redis利用
	 * 
	 * @access public
	 * @return array ex)20140212
	 */
	public static function get_holiday()
	{
		$result = RedisHelper::get_master('holiday');

		//redisにあればその値を返す
		if ($result)
			return $result;

		//dbから取得する
		$now = date("Y-m-d H:i:s");
		$query = DB::select(DB::expr('concat(year,month,day) as datetime'))
					->from(self::$table_name)
					->where('del_flg','!=', 1)
					->and_where('open_date', '<=', $now)
					->and_where('close_date', '>=', $now);

		$result = DbHelper::query_exec($query,'datetime');
		RedisHelper::set_master('holiday', $result);
		return $result;
	}
}