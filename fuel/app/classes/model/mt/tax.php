<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Tax Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Tax extends \Orm\Model
{
	protected static $table_name = 'mt_tax';

	protected static $_properties = array(
	);

	/**
	 * select_current_tax
	 * 
	 * 現在の消費税マスタを返却します
	 * 
	 * @access public
	 * @return (Int)tax
	 */
	public static function select_current_tax()
	{
		$redis_key = self::$table_name;
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id','tax')
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, true);

			//TODO: メール追加!!
			if (!isset($result['id']) || !isset($result['tax']))
				Func::error_log ('消費税マスタが正しく取得出来ませんでした'.__FILE__.'/['.__line__.']', true);

			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * select_current_tax_value
	 * 
	 * 現在の消費税を返却します
	 * 
	 * @access public
	 * @return (Int)tax
	 */
	public static function select_current_tax_value ()
	{
		$tax_mt = self::select_current_tax();
		return (Int)$tax_mt['tax'];
	}
}