<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Whatday Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Whatday extends \Orm\Model
{
	protected static $_properties = array(

		'month' => array(
			'data_type' => 'int',
			'label' => '月',
			'validation' => array(
				/*	validation見本
				'min_length' => array(8),
				'max_lingth' => array(8),
				'match' => array('month_confirm'),
				'' => array(),
				'' => array(),
				 */
				),
		),
		'day',
		'del_flg',
		'open_date',
		'close_date',
		'updated',
		'created',
	);

	/**
	 * select_avail_month
	 * 
	 * @access public
	 * @return queryobject
	 */
	 public static function  select_avail_month ()
	 {
	 	$query = DB::select('month')
	 				->from('mt_whatday')
	 				->where('del_flg', 0)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->group_by('month')
					->order_by('open_date', 'asc');
		return DbHelper::query_exec($query);
	 }

	/**
	 * get_query_select_whatday action
	 * 
	 * @access public
	 * @return queryObject
	 */
	 public static function get_query_select_whatday ($month)
	 {
	 	$query = DB::select('*')
	 				->from('mt_whatday')
	 				->where('month', '=', $month)
					->where('del_flg', 0)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"));
		return $query;
	 }
}