<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Magazine Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Magazine extends \Orm\Model
{
	protected static $_properties = array(
	);

	private static $table_name = 'mt_magazine';

	/**
	 * select
	 * 
	 * @return mix array mt_magazin
	 */
	public static function select ()
	{
		$redis_key = 'mt_magazine';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id', 'name')
					->from(self::$table_name)
					->where('del_flg', '<>', '1')
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->order_by('id', 'asc');
			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
}