<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The User Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Withdrawal extends \Orm\Model
{

	protected static $_properties = array();

	/**
	 * get_query_select_withdrawal
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_withdrawal_data_array()
	{
		$result_array = array();

		$data = DB::select('id','reason')->from('mt_withdrawal')
						->and_where('del_flg', '<>', 1)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"))
						->execute()
						->as_array();

		//初期表示のメッセージ
		$result_array[0] = "退会理由をご選択ください";

		foreach ($data as $value)
		{
			$result_array[$value['id']] = $value['reason'];
		}
		return $result_array;
	}

	/**
	 * get_query_select_id_without_1
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_select_id_without_1()
	{
		return $query = DB::select('id')->from('mt_withdrawal')
					->and_where('id', '<>', 1)
					->and_where('del_flg', '<>', 1)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"));
	}

	/**
	 * get_query_select_withdrawal_reason
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_select_withdrawal_reson_by_id($id)
	{
		return $query = DB::select('reason')->from('mt_withdrawal')
					->and_where('id', '=', $id)
					->and_where('del_flg', '<>', 1)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"));
	}
}