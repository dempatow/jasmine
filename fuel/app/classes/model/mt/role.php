<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Role Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Role extends \Orm\Model
{
	protected static $table_name = 'mt_role';

	/**
	 * user role 定数
	 */
	const NO_LOGIN_USER = 0;  //非ログイン
	const GENERAL_USER  = 1;  //一般ユーザー
	const SYSTEM_UESR   = 10; //システムユーザー

	protected static $_properties = array();
}