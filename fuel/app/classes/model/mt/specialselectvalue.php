<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * Model_Mt_Specialselectvalue.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
//
class Model_Mt_Specialselectvalue extends \Orm\Model
{
	protected static $table_name = 'mt_specialselectvalue';

	protected static $_properties = array(
	);

	/**
	 * get_job
	 * 
	 * @return mt_job data
	 */
/**
	public static function select_by_item_id ($item_id)
	{
		$query = DB::select(
						'id',
						'item_id',
						'name'
					)
					->from(self::$table_name)
					->where('item_id', '=', $item_id)
					//期限チェック
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					//削除flgチェック
					->and_where('del_flg', '=', 0);

		return DbHelper::query_exec($query, true, false, 'master');
	}
 */

	/**
	 * select_by_id
	 * 
	 * @param  int id
	 * @return mix array
	 */
	public static function select_by_id ($id)
	{
		$redis_key    = self::$table_name;
		$redis_result = RedisHelper::get_master($redis_key);
		$result       = false;

		if (!$redis_result)
		{
			$query = DB::select(
							'id',
							'value'
						)
						->from(self::$table_name)
						//期限チェック
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"))
						//削除flgチェック
						->and_where('del_flg', '=', 0);

			$db_result = DbHelper::query_exec($query, 'id', false, 'master');

			//redisにセットする
			RedisHelper::set_master($redis_key, $db_result);

			if (isset($db_result[$id]))
				$result = $db_result[$id];
		}
		else
		{
			if (isset($redis_result[$id]))
				$result = $redis_result[$id];
		}
		return $result;
	}
}