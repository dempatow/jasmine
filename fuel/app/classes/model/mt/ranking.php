<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Ranking Model
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Ranking extends \Orm\Model
{
	protected static $table_name = 'mt_ranking';
	protected static $_properties = array();

	/**
	 * select
	 * 
	 * ranking historyを取得します
	 * 
	 * @param $item_id
	 * @return mix data array
	 */
	public static function select()
	{
		$result = array();
		$redis_key = 'mt_ranking';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('from','to')
					->from(self::$table_name)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->and_where('del_flg', '=', 0);
			$result = DbHelper::query_exec($query, true);
			RedisHelper::set_master($redis_key, $result);
		}

		/**
		 * ランキングマスタ取得に失敗した時
		 */
		if (is_null($result))
		{
			//TODO: メール実装!!
			Func::error_log('ランキングデータ取得に失敗しました'.__FILE__.'/['.__line__.']',true);
		}
		return $result;
	}

	/**
	 * insert_next_mt
	 * 次回のマスタをinsertします
	 * 
	 * @param int term day
	 * @return mix $insert_array
	 */
	public static function insert_next_mt ($term_day)
	{
		//集計開始日
		$from_day = date(
			'Y-m-d H:i:s',
			mktime(
				date('H'),
				date('i'),
				date('s'),
				date('m'),
				date('d') - $term_day,
				date('Y')
			)
		);
		//修正終了日
		$to_day = date(
			'Y-m-d H:i:s',
			mktime(
				date('H'),
				date('i'),
				date('s'),
				date('m'),
				date('d') - 1,
				date('Y')
			)
		);

		//使用終了日
		//とりあえず使用期限は無期限に設定
		$close_date = '2100-01-01 00:00:00';

//		$close_date = date(
//			'Y-m-d H:i:s',
//			mktime(
//				date('H'),
//				date('i'),
//				date('s'),
//				date('m'),
//				date('d') + 6,
//				date('Y')
//			)
//		);

		$insert_array = array(
			'from' => $from_day,
			'to' => $to_day,
			'del_flg' => 0,
			'open_date' => date("Y-m-d H:i:s"),
			'close_date' => $close_date,
		);

		$query = DB::insert(self::$table_name)->set($insert_array);
		DbHelper::query_exec($query, false, false, 'master');

		return $insert_array;
	}

	/**
	 * update_del_flg_all
	 * 
	 * 全てのdel_flgに1をたてる
	 */
	public static function update_del_flg_all ()
	{
		$query = DB::update(self::$table_name)
					->set(array('del_flg'=>1));
		DbHelper::query_exec($query, false, false, 'master');
	}

/*************つかってない↓**********/
	/**
	 * update_by_id
	 * 
	 * @param mix array update_array
	 * @param int id
	 */
	public static function update_by_id ($update_array, $id = null)
	{
		//Debug::dump('OK');exit;
		$query = DB::update(self::$table_name)
					->set($update_array)
					->where('id', '=', $id);
		DbHelper::query_exec($query, false, false, 'master');
	}

	/**
	 * insert
	 * 
	 * @param mix array insert_array
	 */
	public static function insert ($insert_array)
	{
		$query = DB::insert(self::$table_name)->set($insert_array);
		DbHelper::query_exec($query, false, false, 'master');
	}

}