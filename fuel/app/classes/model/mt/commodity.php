<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Commodity Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Commodity extends \Orm\Model
{
	protected static $_properties = array(
		'id' => array(
			'label' => 'id',
			'validation'=> array(
				'checkAjaxId',
			),
		),
		'name' => array(
			'label' => '商品名',
			'validation'=> array(
				'required',
				'checkcharNumJp' => array('60'),
			),
		),
		'price' => array(
			'label' => '価格',
			'validation'=> array(
				'required',
				'checkisnumeric',
				'max_length' => array('6'),
			),
		),
		'description' => array(
			'label' => '商品説明',
			'validation'=> array(
				'required',
				'checkcharNumJp'=> array('300'),
			),
		),
		'max_order' => array(
			'label' => '1日の注文最大数',
			'validation'=> array(
				'required',
				'checkisnumeric',
				'max_length' => array('6'),
			),
		),
		'opendate' => array(
			'label' => '使用開始日',
			'validation'=> array(
				'required',
				'max_length' => array('19'),
				'checkDbDate'
			),
		),
		'closedate' => array(
			'label' => '使用終了日',
			'validation'=> array(
				'required',
				'max_length' => array('19'),
				'checkDbDate'
			),
		),
	);

	/**
	 * get_query_select_menu
	 * 
	 * @access public
	 * @return 
	 * 最大受注数未満の商品とそのオプションを返却します。
	 * 
	 * commodity_idを指定した場合は購入可能な商品を取得します。
	 * 
	 * @param $id => commodity_id
	 */
	public static function get_menu($id = null)
	{
		/*
		 * commodity 取得
		 */
		$query = DB::select (
					'id',
					'name',
					'price',
					'description',
					'max_order',
					'extension'
				)
				->from('mt_commodity')
				->and_where('del_flg','!=','1')
				->and_where('opendate','<=',date("Y-m-d H:i:s"))
				->and_where('closedate','>=', date("Y-m-d H:i :s"));

		//commodity_idが指定してある時
		if (!is_null($id))
		{
			$query->and_where('id', '=', $id);
		}
		$query->order_by('updated','desc');
		$mt_commodity = DbHelper::query_exec($query);

		/*
		 * 現在の受注数を取得
		 */
/*
		table定義変更の為コメントアウト
		$query = DB::select (
					'commodity.commodity_id',
					DB::expr('SUM(order.count) as order_count')
				)
				->from(array('dt_order_commodity','commodity'))
				->join(array('dt_order','order'),'LEFT')
				->on('commodity.order_id','=','order.id')
				->and_where('commodity.del_flg','!=','1')
				->and_where('order.del_flg','!=','1')
				->and_where('order.status', '=','0')
				->group_by('commodity.commodity_id');
 */
		$query = DB::select (
					'commodity_id',
					DB::expr('SUM(count) as order_count')
				)
				->from('dt_order')
				->and_where('del_flg','!=','1')
				->and_where('status', '=','0')
				->group_by('commodity_id');
		$dt_order = DbHelper::query_exec($query);

		//受注数の配列形式を commodity_id => order_countにする
		$order_count = array();
		foreach ($dt_order as $value)
		{
			$order_count[$value['commodity_id']] = $value['order_count'];
		}

		/*
		 * commdoity と optionsを結合する
		 */
		//for commodity
		for ($i=0; $i<count($mt_commodity); $i++)
		{
			//一回あたりのとりまとめ最大数に達していた場合は表示しない
			if (isset($order_count[$mt_commodity[$i]['id']]) &&
				$order_count[$mt_commodity[$i]['id']] == $mt_commodity[$i]['max_order']
				)
			{
				unset($mt_commodity[$i]);
				continue;
			}
			else if (isset($order_count[$mt_commodity[$i]['id']]) &&
					$order_count[$mt_commodity[$i]['id']] < $mt_commodity[$i]['max_order'] 
				)
			{
				$mt_commodity[$i]['order_count'] = $order_count[$mt_commodity[$i]['id']];
			}
		}

		//id指定の時 配列の形を整形する
		if (!is_null($id) && isset($mt_commodity[0]))
			$mt_commodity = $mt_commodity[0];

		return $mt_commodity;
	}

	/**
	 * get_menu_options
	 * 
	 * @return 当該commodityの現在有効なレコード配列
	 */
	public static function get_menu_options ($commodity_id, $option_ids = null)
	{

			$query = DB::select (
						'aop.commodity_id',
						array('op.id','option_id'),
						'op.name',
						'op.price'
					)
					->from(array('mt_availoptions','aop'))
					->join(array('mt_options','op'),'LEFT')
					->on('aop.option_id','=','op.id')
					->and_where('aop.del_flg','!=','1')
					->and_where('op.del_flg','!=','1')
					->and_where('aop.opendate','<=',date("Y-m-d H:i:s"))
					->and_where('aop.closedate','>=', date("Y-m-d H:i:s"))
					->and_where('op.opendate','<=',date("Y-m-d H:i:s"))
					->and_where('op.closedate','>=', date("Y-m-d H:i:s"))
					->and_where('aop.commodity_id', '=', $commodity_id);

					//options array
					if (!is_null($option_ids) && is_array($option_ids))
						$query->and_where('op.id', 'in', $option_ids);

					$query->order_by('aop.updated','desc')
							->order_by('op.updated','desc')
							->distinct(true);

			return DbHelper::query_exec($query);
	}

	/*
	 * get_query_update_commodity
	 * 
	 * @access public
	 * @return queryObject
	 * @param data
	 */
	public static function get_query_update_commodity($data)
	{
		$update_data = array(
				'name' => $data['name'],
				'price' => $data['price'],
				'description' => $data['description'],
				'opendate' => $data['opendate'],
				'closedate' => $data['closedate']
				);

		if (isset($data['extension']))
			$update_data['extension'] = $data['extension'];

		return $query = DB::update('mt_commodity')
					->set($update_data)
					->where('id', '=', $data['id']);
	}

	/**
	 * get_query_delete_commodity
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_update_commodity_del_flg($id)
	{
		return $query = DB::update('mt_commodity')
					->set(array('del_flg' => 1))
					->where('id', '=', $id);
	}

	/**
	 * get_query_insert_commodity
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_insert_commodity($data)
	{
		return $query = DB::insert('mt_commodity')
							->set(
								array(
									'name' => $data['name'],
									'price' => $data['price'],
									'description' => $data['description'],
									'max_order' => $data['max_order'],
									'extension' => $data['image']['extension'],
									'opendate' => $data['opendate'],
									'closedate' => $data['closedate'],
								)
							);
	}

	/**
	 * get_query_select_commodity 
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_select_commodity ($check_date = true)
	{
		$query = DB::select (
					'id',
					'name',
					'price',
					'description',
					'max_order',
					'extension',
					'opendate',
					'closedate'
				)
				->from('mt_commodity')
				->where('del_flg','!=','1');

				if ($check_date)
				{
					$query->and_where('opendate','<=',date("Y-m-d H:i:s"))
						->and_where('closedate','>=', date("Y-m-d H:i:s"));
				}

		return $query;
	}

	/**
	 * get_query_select_whatday action
	 * 
	 * @access public
	 * @return queryObject
	 */
	 public static function get_query_select_option ($id = null)
	 {
		$query = DB::select('id', 'name', 'price', 'opendate', 'closedate')
					->from('mt_options')
					->where('del_flg', 0)
					->and_where('opendate', '<=', date("Y-m-d H:i:s"))
					->and_where('closedate', '>=', date("Y-m-d H:i:s"));

		//id指定のときはwhere句に条件付加
		if(!is_null($id) && is_numeric($id))
			$query->and_where('id', '=', $id);

		return $query;
	 }
}