<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Item Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Item extends \Orm\Model
{
	protected static $table_name = 'mt_item';

	protected static $_properties = array(
		'id' => array(
			'label' => 'id',
			'validation'=> array(
				'checkAjaxId',
			),
		),
	);

	/**
	 * select_item_detail_by_id
	 * 
	 * 商品の詳細情報を取得します
	 * 
	 * @param $item_id $is_omission(trueの場合はredisのデータだけ返却します)
	 * @return item_detail_data array
	 */
	public static function select_item_by_id ($item_id, $is_omission = false, $is_stock = false)
	{
		$redis_key = 'item_detail_'.$item_id;
		$result = RedisHelper::get_master($redis_key);
		$item_id = (Int)$item_id;

		if (!$result)
		{
			$query = DB::select(
							'item.id',
							'item.name',
							'item.new_flg',
							'item.selling_date',
							'item.no_specified',
							'item.specialselect_flg',
							'item.individual_send_flg',
							'item.sending_ms_flg',
							'item.hide_price_flg',
							'item.external_url',
							'reco_flg',
							'price.price'
						)
						->from(array(self::$table_name,'item'))
						//価格マスタ
						->join(array('mt_price', 'price'), 'left')
						->on('item.id', '=', 'price.item_id')
						//商品id
						->and_where('item.id', '=', $item_id)
						//期限チェック
						->and_where('item.open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('item.close_date', '>=', date("Y-m-d H:i:s"))
						->and_where('price.open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('price.close_date', '>=', date("Y-m-d H:i:s"))
						//削除flgチェック
						->and_where('item.del_flg', '=', 0)
						->and_where('price.del_flg', '=', 0)
						//表示順
						->order_by('item.order', 'desc')
						->order_by('item.created', 'desc');

			$result = DbHelper::query_exec($query,true);

			if (is_null($result) || empty($result))
				return false;

			//発送方法データを取得する
			$sendings = Model_Dt_Itemavailsending::select_by_item_id($item_id);
			if (count($sendings)>0)
				$result['sendings'] = $sendings;

			//new_flgが立っていない場合はunset
			if(!isset($result['new_flg']) || (Int)$result['new_flg'] === 0)
				unset($result['new_flg']);

			//reco_flgが立っていない場合はunset
			if(!isset($result['reco_flg']) || (Int)$result['reco_flg'] === 0)
				unset($result['reco_flg']);

			//キャラクター
			$character = Model_Dt_Character::select_by_item_id($item_id);
			if ($character)
				$result['chara'] = $character;

			//カテゴリ
			$category = Model_Dt_Category::select_category_by_item_id($item_id);
			if (count($category)>0)
				$result['category'] = $category;

			if (isset($result['category']))
			{
				//--商品カテゴリーマスタを取得する
				$mt_category = Model_Mt_Category::select_category();
				//--商品カテゴリーデータと商品カテゴリーマスタを結合する
				foreach ($result['category'] as $category_cd => &$val)
				{
					$val = $mt_category[$category_cd];
				}
				unset($val);
			}

			if (isset($result['chara']))
			{
				//--キャラクターマスタを取得する
				$mt_character = Model_Mt_Character::select();
				//--キャラクターデータとキャラクターマスタを結合する
				//Debug::dump($result);exit;
				foreach ($result['chara'] as $character_cd => &$val)
				{
					$val = $mt_character[$character_cd];
				}
				unset($val);
			}

			$item_key_str = '';
			//キャラクタを設定する
			//Debug::dump($item['chara']);exit;
			if (isset($result['chara']))
			{
				foreach ($result['chara'] as $val)
				{
					$item_key_str .= $val['name'].',';
				}
			}
			unset($val);

			//カテゴリ文字列を設定する
			if ($result['category'])
			{
				$categories = Model_Mt_Category::get_item_categories_by_childs($result['category']);
				for ($i=0; $i<count($categories); $i++)
				{
					for ($j=0; $j<count($categories[$i]); $j++)
					{
						$item_key_str .= ($j < count($categories[$i])-1)? $categories[$i][$j]['name'].',': $categories[$i][$j]['name'];
					}
				}
			}
			!empty($item_key_str) and $result['meta_key'] = $item_key_str;

			//特殊選択商品の場合
			if (isset($result['specialselect_flg']) && $result['specialselect_flg'])
			{
				$special_select = array();
				$special_select = Model_Mt_Specialselectkey::select_by_item_id($item_id);

				if (isset($special_select['id']))
					$special_values = Model_Dt_Specialselect::select_join_mt_by_key_id($special_select['id']);

				if (!empty($special_values))
					$special_select['values'] = $special_values;

				$result['special_select'] = $special_select;
			}

			//発送方法注意
			if (isset($result['sending_ms_flg']) && $result['sending_ms_flg'])
			{
				$ms_ids = Model_Dt_Sendingmessage::select_message_ids_by_item_id($item_id);
				!empty($ms_ids) and $result['sending_ms_ids'] = $ms_ids;
			}

			//ここまでのデータをredisにセットする
			RedisHelper::set_master($redis_key, $result);
		}

		//アイテムポイント
		$item_point = Model_Mt_Itempoint::select_point_by_item_id($item_id);
		if ($item_point)
			$result['point'] = $item_point;

		//割引情報
		$discount = Model_Mt_Discount::select_by_item_id($item_id);
		if ($discount)
			$result['discount'] = $discount;

		//送料無料情報
		$item_free_sending = Model_Mt_Freesendingitem::select_by_item_id($item_id);
		if ($item_free_sending)
			$result['free_sending'] = $item_free_sending;

		/************
		 * 在庫数データ
		 ************/
		if ($is_stock)
		{
			//通常在庫
			$result['stock'] = Model_Dt_Stock::select_stock_by_item_id($item_id);

			//特殊選択在庫
			if (isset($result['special_select']) && count($result['special_select'])>0)
			{
				$values_tmp = array();
				if (isset($result['special_select']['values']))
				{
					//特殊選択value
					for ($i=0; $i<count($result['special_select']['values']); $i++)
					{
						//特殊選択ごとの在庫を取得する
						if (isset($result['special_select']['values'][$i]['value_id']))
						{
							$count = Model_Dt_Specialselectstock::select_stock_by_value_id($result['special_select']['values'][$i]['value_id']);
							//在庫がある場合
							if ($count>0)
							{
								$result['special_select']['values'][$i]['stock'] = $count;
								$values_tmp[] = $result['special_select']['values'][$i];
							}
						}
					}
				}
				$result['special_select']['values'] = $values_tmp;
			}
		}

		/**************
		 * 省略データ返却
		 **************/
		if ($is_omission)
			return is_null($result)? false: $result;

		//--発送方法マスタ 発送料金マスタ エリアマスタを結合したもの
		$mt_sending_rec = Model_Mt_Sending::select_sending_recursive();

		//--発送料金を取得する
		foreach ($result['sendings'] as $sending_cd => &$val)
		{
			$mt_sending_rec_tmp = $mt_sending_rec[$sending_cd];
			$mt_sending_rec_tmp['priority_flg'] = $val['priority_flg'];
			$val = $mt_sending_rec_tmp;
		}
		unset($val);

		return is_null($result)? false: $result;
	}

	/**
	 * select_item_for_db
	 * 
	 * dbからselectします
	 * 
	 * @access public
	 * @return item data
	 */
	public static function select_item_for_db()
	{
		$query = DB::select('id','name')
					->from(self::$table_name)
					->where('del_flg', '=', 0)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"));
		return DbHelper::query_exec($query);
	}

	/**
	 * select_item_pagenate
	 *
	 * 商品をselectします
	 * 一度DBから取得したものはredisから取得します(pagenate版です)
	 *s
	 * @access public
	 * @return query object
	 */
	public static function select_item_pagenate()
	{
		$result = RedisHelper::get_master('item');

		if (!$result)
		{
			$query = DB::select(
							'item.id',
							'item.name',
							'price.price'
						)
						->from(array(self::$table_name,'item'))
						//価格
						->join(array('mt_price', 'price'))
						->on('item.id', '=', 'price.item_id')
						//商品ポイント
						//期限チェック
						->and_where('item.open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('item.close_date', '>=', date("Y-m-d H:i:s"))
						->and_where('price.open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('price.close_date', '>=', date("Y-m-d H:i:s"))
						//削除flgチェック
						->and_where('item.del_flg', '=', 0)
						->and_where('price.del_flg', '=', 0)
						//表示順
						->order_by('item.order', 'desc')
						->order_by('item.created', 'desc');

			$result = DbHelper::query_exec($query);

			$item_point_mt = Model_Mt_Itempoint::select();

			for ($i=0; $i<count($result); $i++)
			{
				if (isset($item_point_mt[$result[$i]['id']]['point']))
					$result[$i]['point'] = $item_point_mt[$result[$i]['id']]['point'];
			}
			RedisHelper::set_master('item', $result);
		}

		return $result;
	}

	/**
	 * select_ids
	 * 
	 * item_idを取得します
	 * 
	 * @access public
	 * @return ids array
	 */
	public static function select_ids()
	{
		$result = '';
		$redis_key = 'mt_items_ids';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id')
				->from(self::$table_name)
				->where('del_flg', 0)
				->and_where('open_date', '<=', date("Y-m-d H:i:s"))
				->and_where('close_date', '>=', date("Y-m-d H:i:s"))

				->order_by('order', 'desc')
				->order_by('id', 'desc');


			$result_db = DbHelper::query_exec($query);
			for ($i=0; $i<count($result_db); $i++)
			{
				$result[] = $result_db[$i]['id'];
			}

			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * select_new_ids
	 * 
	 * item_idを取得します
	 * 
	 * @access public
	 * @return ids array
	 */
	public static function select_new_ids()
	{
		$result = '';
		$redis_key = 'mt_items_new_ids';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id')
				->from(self::$table_name)
				->where('del_flg', 0)
				->and_where('new_flg', '=', 1)
				->and_where('open_date', '<=', date("Y-m-d H:i:s"))
				->and_where('close_date', '>=', date("Y-m-d H:i:s"))
				->order_by('order', 'desc');

			$result_db = DbHelper::query_exec($query);
			for ($i=0; $i<count($result_db); $i++)
			{
				$result[] = $result_db[$i]['id'];
			}

			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
/**
	public static function select_ids()
	{
		$result = '';
		$redis_key = 'mt_items_ids';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id')
				->from(self::$table_name)
				->where('del_flg', 0)
				->and_where('open_date', '<=', date("Y-m-d H:i:s"))
				->and_where('close_date', '>=', date("Y-m-d H:i:s"));

			$result = DbHelper::query_exec($query);
			RedisHelper::set_master($redis_key, $result);
		}
		Debug::dump($result);
		return $result;
	}
 * 
 */

	/**
	 * select_for_index_page
	 * 
	 * 引数の件数分新商品をselectします
	 */
	public static function select_for_index_page ($count = 5)
	{
		$ids = self::select_new_ids();
		$select_ids = array();
		$result = array ();

		(count($ids) < $count) and $count = count($ids);

		//---引数の件数分商品IDを取得する
		for ($i=0; $i<$count; $i++)
		{
			$select_ids[] = $ids[$i];
		}

		//idをキーに商品マスタを取得する
		for ($i=0; $i<count($select_ids); $i++)
		{
			$result[] = self::select_item_by_id($select_ids[$i], true);
		}

		return $result;
	}

	/**
	 * validation_reserve
	 * 予約用validation
	 * @param mix input data
	 */
	public static function validation_reserve($input_data)
	{
		$result = true;

		/**************
		 * 必須パラメータチェック
		 **************/
		if (!isset($input_data['count']) || !isset($input_data['item_id']))
			$result = false;

		/**************
		 * 対象商品チェック
		 **************/
		$item_data = Model_Mt_Item::select_item_by_id($input_data['item_id'], true);
		if (
			!$result ||
			!(
				isset($item_data['selling_date']) &&
				!empty($item_data['selling_date']) &&
				Func::check_reserve_date($item_data['selling_date'])
			)
		)
			$result = false;

		/**************
		 * 在庫チェック
		 **************/
		if (
			!$result ||
			Model_Dt_Stock::select_stock_by_item_id($input_data['item_id']) < $input_data['count']
		)
			$result = false;

		return $result;
	}

/**

$val = Validation::forge();
$val->add_model('Model_Dt_Campaign');

//同意チェック
$val->add('consent', '規約の同意')
	->add_rule('required');

$check_item = array(
	'order_no',
	'mail',
	'text',
	'consent'
);
$val->run($value, $check_item);
return $val;

 */
}