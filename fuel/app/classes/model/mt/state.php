<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The State Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_State extends \Orm\Model
{
	protected static $_properties = array(
	);

	private static $table_name = 'mt_state';

	/**
	 * get_state
	 * 
	 * redis_key => get_state
	 * 
	 * @return mt_state data
	 */
	public static function get_state ()
	{
		$redis_key = 'mt_state';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id', 'name', 'area_cd')
					->from(self::$table_name)
					->where('del_flg', '<>', '1')
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->order_by('id', 'asc');

			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
}