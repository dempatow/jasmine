<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The UserAction Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Useraction extends \Orm\Model
{
	protected static $table_name = 'mt_useraction';

	protected static $_properties = array(
	);

	/**
	 * get_user_action_master
	 * 
	 * ユーザー行動マスタを取得します
	 * 
	 * @access public
	 * @return master
	 */
	public static function get_user_action_master()
	{
		$key = 'user_action';
		$result = RedisHelper::get_master($key);

		if (!$result)
		{
			$query = DB::select('id','name')
						->from(self::$table_name)
						->where('del_flg', '<>', 1)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));

			$result = DbHelper::query_exec($query, 'id');
			Debug::dump($result);exit;
			RedisHelper::set_master($key, $result);
		}
		return $result;
	}
}