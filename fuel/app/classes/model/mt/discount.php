<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Discount Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Discount extends \Orm\Model
{
	protected static $table_name = 'mt_discount';

	protected static $_properties = array(
	);

	/**
	 * select
	 *
	 * 一度DBから取得したものはredisから取得します
	 *
	 * @access public
	 * @return data array
	 */
	public static function select()
	{
		$redis_key = 'mt_discount';
		$result = RedisHelper::get_master($redis_key);

		if (!$result && !is_array($result))
		{
			$query = DB::select(
							'id',
							'item_id',
							'rate',
							'open_date',
							'close_date'
						)
						->from(self::$table_name)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"))
						->and_where('del_flg', '=', 0);

			$result = DbHelper::query_exec($query, 'item_id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * select_by_item_id
	 * 
	 * @param item_id
	 * @return mix array
	 */
	public static function select_by_item_id ($item_id)
	{
		$mt_discount = self::select();
		$result = false;
		if (
			isset($mt_discount[$item_id]) &&
			$mt_discount[$item_id]['open_date'] <= date("Y-m-d H:i:s") &&
			$mt_discount[$item_id]['close_date'] >= date("Y-m-d H:i:s")
			)
		{
			$result = array(
				'id'=> $mt_discount[$item_id]['id'],
				'item_id'=> $mt_discount[$item_id]['item_id'],
				'rate' => (Int)$mt_discount[$item_id]['rate']
			);
		}
		return $result;
	}

	/**
	 * select_rate_by_item_id
	 * 
	 * @parma int item_id
	 * @return int rate
	 */
	public static function select_rate_by_item_id ($item_id)
	{
		$mt_discount = self::select();
		$result = false;
		if (
			isset($mt_discount[$item_id]) &&
			$mt_discount[$item_id]['open_date'] <= date("Y-m-d H:i:s") &&
			$mt_discount[$item_id]['close_date'] >= date("Y-m-d H:i:s")
			)
		{
			$result = (Int)$mt_discount[$item_id]['rate'];
		}
		return $result;
	}
}