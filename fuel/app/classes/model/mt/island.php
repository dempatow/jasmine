<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Island Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Island extends \Orm\Model
{
	protected static $table_name = 'mt_island';

	protected static $_properties = array(
	);

	/**
	 * get_island
	 * 
	 * 離島マスタを取得する
	 * 
	 * @access public
	 * @return query object
	 */
	public static function get_island()
	{
		$key = 'island_mt';
		$result = RedisHelper::get_master($key);

		if (!$result)
		{
			$query = DB::select('zip')
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, 'zip', false);
			RedisHelper::set_master($key, $result);
		}
		return $result;
	}

	/**
	 * check_island
	 * 
	 * 離島かどうか判定する
	 * 
	 * @access public
	 * @return query object
	 */
	public static function check_island($zip)
	{
		$mt = self::get_island();
		return (isset($mt[$zip]))? true: false;
	}
}