<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The ItemPointRate Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Pointrate extends \Orm\Model
{
	protected static $table_name = 'mt_pointrate';

	protected static $_properties = array(
	);

	/**
	 * select
	 *
	 * 一度DBから取得したものはredisから取得します
	 *
	 * @access public
	 * @return data array
	 */
	public static function select()
	{
		$redis_key = 'pointrate';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select(
							'id',
							'rate'
						)
						->from(self::$table_name)
						//期限チェック
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"))
						//削除flgチェック
						->and_where('del_flg', '=', 0);

			$result = DbHelper::query_exec($query,true);
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * select_rate
	 * 
	 * 現在のrateを取得する
	 * 
	 */
	public static function select_rate ()
	{
		$result = 0;
		$point_rate_mt = self::select();

		if (isset($point_rate_mt['rate']))
		{
			$result = (Int)$point_rate_mt['rate'];
		}
		else
		{
			/**
			 * TODO:
			 */
			Func::error_log('ポイントレートが正しく取得出来ませんでした'.__FILE__.'/['.__line__.']',true);
		}

		return $result;
	}
}