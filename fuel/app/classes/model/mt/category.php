<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Category Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Category extends \Orm\Model
{
	protected static $table_name = 'mt_category';

	protected static $_properties = array(
	);

	/**
	 * select_category_by_recursive_no
	 * 
	 * recursive_noをキーにselectします
	 */
	public static function select_category_by_recursive_no ($recursive_no)
	{
		$redis_key = 'mt_category_'.$recursive_no;
		$result = false;;
		$result = RedisHelper::get_master($redis_key);

		if (!$result && !is_array($result))
		{
			$query = DB::select('id','recursive_no','parent_no','name')
						->from(self::$table_name)
						->where('recursive_no', $recursive_no)
						->and_where('del_flg', '=', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result_db = DbHelper::query_exec($query, 'id');

			//if ((count($result)>0))

			RedisHelper::set_master($redis_key, $result_db);
			if ((count($result_db)>0))
			{
				$result = $result_db;
			}
		}
		return $result;
	}

	/**
	 * select_category
	 * 
	 * カテゴリーマスターを取得します
	 * キーにcategory_idを設定します
	 * 
	 * @access public
	 * @return category_data
	 */
	public static function select_category()
	{
		$redis_key = 'category_mt';
		$result = RedisHelper::get_master($redis_key);
		if (!$result)
		{
			$query = DB::select('id','name','recursive_no','parent_no')
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * select_category_by_id
	 * 
	 * category_idをキーにcategory_mtを取得します
	 * 
	 * @category_id category_id
	 * @return mix array category_mt
	 */
	public static function select_category_by_id ($category_id)
	{
		$mt_category = self::select_category();
		return isset($mt_category[$category_id])? $mt_category[$category_id]: false;
	}

	/**
	 * get_item_categories
	 * 
	 * 子要素が持つカテゴリを配列に入れて返却します
	 * 一番親のカテゴリまで取得します
	 * 
	 * @param  int item id
	 * @return mix category array
	 */
	public static function get_item_categories_by_childs ($child_categories = null)
	{
		$result = array();
		foreach ($child_categories as $val)
		{
			$result_row = array();
			$result_row[] = $val;

			//root要素までのカテゴリを取得する
			$parent = $val['parent_no'];

			while (true)
			{
					$parent  = isset($parent) ? self::select_category_by_id((Int)$parent): false;
					if ($parent)
					{
						array_unshift($result_row, $parent);
					}
					else
					{
						break;
					}
			}
			$result[] = $result_row;
		}
		return $result;
	}
}