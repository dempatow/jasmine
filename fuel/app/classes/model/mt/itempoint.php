<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The ItemPoint Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Itempoint extends \Orm\Model
{
	protected static $table_name = 'mt_itempoint';

	protected static $_properties = array(
	);

	/**
	 * select
	 *
	 * 商品をselectします
	 * 一度DBから取得したものはredisから取得します(pagenate版です)
	 *
	 * @access public
	 * @return query object
	 */
	public static function select()
	{
		$redis_key = 'itempoint';
		$result = RedisHelper::get_master($redis_key);

		if (!$result && !is_array($result))
		{
			$query = DB::select(
							'item_id',
							'point',
							'open_date',
							'close_date'
						)
						->from(self::$table_name)
						//期限チェック
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"))
						//削除flgチェック
						->and_where('del_flg', '=', 0);

			$result = DbHelper::query_exec($query,'item_id');

			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * select_point_by_item_id
	 * 
	 * item_id をキーにpoint
	 * 
	 */
	public static function select_point_by_item_id ($item_id)
	{
		$result = false;
		$item_point_mt = self::select();

		if (
			isset($item_point_mt[$item_id]) &&
			$item_point_mt[$item_id]['open_date'] <= date("Y-m-d H:i:s") &&
			$item_point_mt[$item_id]['close_date'] >= date("Y-m-d H:i:s")
		)
			$result = (Int)$item_point_mt[$item_id]['point'];

		return $result;
	}
}