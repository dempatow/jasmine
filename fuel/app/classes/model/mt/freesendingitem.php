<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Freesendingitem Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Freesendingitem extends \Orm\Model
{
	protected static $table_name = 'mt_freesendingitem';

	protected static $_properties = array(
	);

	/**
	 * select
	 * 
	 * 無料発送商品マスタを取得します
	 * item_id => item_id の形で返却します
	 * 
	 * @access public
	 * @return array
	 */
	public static function select ()
	{
		$redis_key = 'mt_freesendingitem';
		$result = RedisHelper::get_master($redis_key);

		if (!$result && !is_array($result))
		{
			$query = DB::select('item_id','open_date','close_date')
				->from(self::$table_name)
				->where('del_flg', 0)
				->and_where('open_date', '<=', date("Y-m-d H:i:s"))
				->and_where('close_date', '>=', date("Y-m-d H:i:s"));

			$result = DbHelper::query_exec($query, 'item_id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
	
	/**
	 * select_by_item_id
	 * 
	 * item_idをキーに取得します
	 * @parma int item_id
	 * @return mid array
	 */
	public static function select_by_item_id ($item_id)
	{
		$result = false;
		$mt_freesending = self::select();

		//item_idがある事かつ有効期限内である事を確認して返却する
		if (
			isset($mt_freesending[$item_id]) &&
			$mt_freesending[$item_id]['open_date'] <= date("Y-m-d H:i:s") &&
			$mt_freesending[$item_id]['close_date'] >= date("Y-m-d H:i:s"))
		{
			$result = $mt_freesending[$item_id];
		}
		return $result;
	}
}