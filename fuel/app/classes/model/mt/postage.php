<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Postage Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Postage extends \Orm\Model
{
	protected static $table_name = 'mt_postage';

	protected static $_properties = array(
	);

	/**
	 * select_postage_join_area
	 * 
	 * @return mt_postageとmt_areaをjoinしたデータを返却する
	 */
	public static function select_postage_join_area()
	{
		//---エリアごとに料金が異なる(mt_areaとjoinする)
		$query = DB::select(
						'postage.sending_cd',
						array('area.name', 'area_name'),
						array('area.id', 'area_cd'),
						'postage.postage'
					)
					->from(array(self::$table_name,'postage'))
					//--エリアマスタ
					->join(array('mt_area', 'area'), 'left')
					->on('postage.area_cd', '=', 'area.id')
					//期限チェック
					->and_where('postage.open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('postage.close_date', '>=', date("Y-m-d H:i:s"))
					->and_where('area.open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('area.close_date', '>=', date("Y-m-d H:i:s"))
					//削除flgチェック
					->and_where('postage.del_flg', '=', 0)
					->and_where('area.del_flg', '=', 0);
					//表示順
					//->order_by('item.order', 'desc')
					//->order_by('item.created', 'desc');
		$result_area = DbHelper::query_exec($query);

		//---全国一律料金
		$query = DB::select('sending_cd', 'postage')
					->from(self::$table_name)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->and_where('del_flg', '=', 0)
					->and_where('area_cd', '=', null);
		$result_no_area = DbHelper::query_exec($query);
		//---全国一律料金とエリア別料金の物をマージして返却する
		return array_merge($result_area, $result_no_area);
	}
}