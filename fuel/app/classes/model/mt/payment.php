<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Payment Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Payment extends \Orm\Model
{
	const CARD_PAYMENT_ID = 1;
	const CASH_ON_DELIVERY_CD = 3;

	protected static $table_name = 'mt_payment';
	protected static $_properties = array();

	/**
	 * select_payment
	 * 
	 * 決済方法を取得します
	 * 
	 * @access public
	 * @return payment data array
	 */
	public static function select_payment()
	{
		$redis_key = 'mt_payment';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select(
							'id',
							'agent_cd',
							'name'
						)
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
}