<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Whatday Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Options extends \Orm\Model
{
	protected static $_properties = array(
		'id' => array(
			'label' => 'id',
			'validation'=> array(
				'required',
				'checkisnumeric',
			),
		),
		'name' => array(
			'label' => 'オプション名',
			'validation'=> array(
				'required',
				'checkcharNumJp' => array('60'),
			),
		),
		'price' => array(
			'label' => '価格',
			'validation'=> array(
				'required',
				'checkisnumeric',
				'max_length' => array('6'),
			),
		),
		'opendate' => array(
			'label' => '使用開始日',
			'validation'=> array(
				'required',
				'max_length' => array('19'),
				'checkDbDate'
			),
		),
		'closedate' => array(
			'label' => '使用終了日',
			'validation'=> array(
				'required',
				'max_length' => array('19'),
				'checkDbDate'
			),
		),
	);

	/**
	 * get_query_select_whatday action
	 * 
	 * @access public
	 * @return queryObject
	 */
	 public static function get_query_select_option ($id = null, $check_date = true)
	 {
		$query = DB::select('id', 'name', 'price', 'opendate', 'closedate')
					->from('mt_options')
					->where('del_flg', 0);
		if ($check_date)
		{
			$query->and_where('opendate', '<=', date("Y-m-d H:i:s"))
				->and_where('closedate', '>=', date("Y-m-d H:i:s"));
		}

		//id指定のときはwhere句に条件付加
		if(!is_null($id) && is_numeric($id))
			$query->and_where('id', '=', $id);

		return $query;
	 }

	/**
	 * select_avail_month
	 * 
	 * @access public
	 * @return queryobject
	 */
	 public static function  select_avail_month ()
	 {
	 	$query = DB::select('month')
	 				->from('mt_whatday')
	 				->where('del_flg', 0)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->group_by('month')
					->order_by('open_date', 'asc');
		return DbHelper::query_exec($query);
	 }

	/**
	 * get_query_select_whatday action
	 * 
	 * @access public
	 * @return queryObject
	 */
	 public static function get_query_select_whatday ($month)
	 {
	 	$query = DB::select('*')
	 				->from('mt_whatday')
	 				->where('month', '=', $month)
					->where('del_flg', 0)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"));
		return $query;
	 }

	/**
	 * get_query_insert_options
	 * 
	 * @access public
	 * @return queryObject
	 */
	public static function get_query_insert_options($data)
	{
		return $query = DB::insert('mt_options')
							->set(
								array(
									'name' => $data['name'],
									'price' => $data['price'],
									'opendate' => $data['opendate'],
									'closedate' => $data['closedate'],
								)
							);
	}

	/*
	 * get_query_insert_options
	 * 
	 * @access public
	 * @return queryObject
	 * @param data
	 */
	public static function get_query_update_options($data)
	{
		return $query = DB::update('mt_options')
					->set(
							array(
								'name' => $data['name'],
								'price' => $data['price'],
								'opendate' => $data['opendate'],
								'closedate' => $data['closedate']
								)
							)
					->where('id', '=', $data['id']);
	}

	/*
	 * get_query_insert_delflg_options
	 * 
	 * @access public
	 * @return queryObject
	 * @param  id
	 */
	public static function get_query_update_delflg_options($id)
	{
		return $query = DB::update('mt_options')
					->set(array('del_flg' => 1))
					->where('id', '=', $id);
	}

	/**
	 * get_query_select_option_not_del
	 * 
	 * @access public
	 * @return queryObject
	 */
	 public static function get_query_select_option_not_del ($id)
	 {
		$query = DB::select('*')
					->from('mt_options')
					->and_where('id', '=', $id);
		return $query;
	 }
}