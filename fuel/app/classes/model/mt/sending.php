<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Sending Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Sending extends \Orm\Model
{
	protected static $table_name = 'mt_sending';

	protected static $_properties = array(
		//--- 購入処理の際に使用する
		'sending_cd_first' => array(
			'label' => '発送方法',
			'validation' => array(
				'required',
				'checkisnumeric',
				'checkmtvalue' => array('mt_sending'),
				'checkavailsending',
			),
		),
		'date_specified' => array(
			'label' => '日時指定',
			'validation' => array(
				'checkdatespecified',
			),
		),
	);

	/**
	 * get_query_select_menu
	 * 
	 * メニューをselectするqueyrオブジェクトを返します
	 * 
	 * @access public
	 * @return query object
	 */
	public static function get_query_select_menu()
	{
		$query = DB::select('month')
					->from(self::$table_name)
					->where('del_flg', 0)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->group_by('month')
					->order_by('open_date', 'asc');
		return $query;
	}

	/**
	 * select_all_id
	 * 
	 * mt_sendingの全てのidを取得します
	 * 
	 * @return ids
	 */
	public static function select_all_id ()
	{
		$redis_key = self::$table_name;
		$result = RedisHelper::get_master($redis_key);
		if (!$result)
		{
			$query = DB::select('id')
						->from(self::$table_name)
						->and_where('del_flg', '=', 0);
			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * select_sending_recursive
	 * 
	 * 発送方法マスタ/発送料金マスタ/エリアマスタを結合したデータを返却する
	 * キーは発送方法id
	 * 
	 * @return mt_sending join mt_postage join mt_area
	 */
	public static function select_sending_recursive ()
	{
		$redis_key = 'sending_recursive';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			//--発送方法マスタを取得する
			$query = DB::select('id','name', 'has_area', 'is_cash')
						->from(self::$table_name)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"))
						->and_where('del_flg', '=', 0);
			$mt_sending = DbHelper::query_exec($query, 'id');

			//--発送料金マスタとエリアマスタをjoin
			$postage_join_area = Model_Mt_Postage::select_postage_join_area();

			//発送方法マスタ
			foreach ($mt_sending as $id => &$val)
			{
				//発送料金マスタとエリアマスタ
				for ($j=0;$j<count($postage_join_area); $j++)
				{
					if (
						isset($postage_join_area[$j]['sending_cd']) &&
						$id == $postage_join_area[$j]['sending_cd']
					)
					{
						unset($postage_join_area[$j]['sending_cd']);
						$val['postages'][] = $postage_join_area[$j];
					}
				}
			}
			$result = $mt_sending;
			RedisHelper::set_master($redis_key, $result);
		}

		return $result;
	}
}