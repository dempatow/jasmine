<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Sendingmessage Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Sendingmessage extends \Orm\Model
{
	protected static $table_name = 'mt_sendingmessage';
	protected static $_properties = array();

	public static function select ()
	{
		$redis_key = self::$table_name;
		$result    = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$now   = date("Y-m-d H:i:s");
			$query = DB::select('id', array('message', 'ms'))
						->from(self::$table_name)
						//->where('item_id', '=', $item_id)
						->and_where('del_flg','<>', 1)
						->and_where('open_date', '<=', $now)
						->and_where('close_date', '>=', $now);
			$result = DbHelper::query_exec($query, 'id');

			RedisHelper::set_master($redis_key, $result);
		}

		return $result;
	}

	/**
	 * get_holiday
	 * 
	 * @access public
	 * @return array ex)20140212
	 */
	public static function select_by_id($id)
	{
		$messages = self::select();
		return isset($messages[$id])? array($messages[$id]['id'] => $messages[$id]['ms']): false;
	}

	/**
	 * select_by_ids
	 * 
	 * @param type $ids
	 */
	public static function select_by_ids ($ids)
	{
		$messages = self::select();
		$result = array();

		for ($i=0; $i<count($ids); $i++)
			$result[$messages[$ids[$i]]['id']] = $messages[$ids[$i]]['ms'];

		return $result;
	}
}