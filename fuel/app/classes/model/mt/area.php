<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Template Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Area extends \Orm\Model
{
	protected static $table_name = 'mt_area';

	const ISLAND_AREA_CD  = 99;   //離島コード
	const OKINAWA_AREA_CD = 11;  //沖縄エリア

	protected static $_properties = array(
		'id' => array(
			'label' => 'id',
			'validation'=> array(
				'checkAjaxId',
			),
		),
	);

	/**
	 * get_query_select_menu
	 * 
	 * メニューをselectするqueyrオブジェクトを返します
	 * 
	 * @access public
	 * @return query object
	 */
	public static function get_query_select_menu()
	{
		$query = DB::select('month')
					->from(self::$table_name)
					->where('del_flg', 0)
					->and_where('open_date', '<=', date("Y-m-d H:i:s"))
					->and_where('close_date', '>=', date("Y-m-d H:i:s"))
					->group_by('month')
					->order_by('open_date', 'asc');
		return $query;
	}

	/**
	 * get_area
	 * 
	 * area マスタを取得します
	 * @return area master
	 */
	public static function get_area ()
	{
		$redis_key = 'area';
		$result = '';

		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id','name')
						->from(self::$table_name)
						->where('del_flg', '=', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));

			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * get_area_by_state_cd
	 * 
	 * エリアコードを返却します(離島判定あり)
	 * 
	 * @param int state int $zip
	 * @param mix $area array
	 */
	public static function get_area_by_state($state_cd, $zip = null)
	{
		$area_cd = '';
		//離島であれば
		if (!is_null($zip) && Model_Mt_Island::check_island($zip))
		{
			$area_cd = self::ISLAND_AREA_CD;
		}
		else
		{
			$mt_state = Model_Mt_State::get_state();
			$area_cd = $mt_state[$state_cd]['area_cd'];
		}
		return self::get_area()[$area_cd];
	}
}