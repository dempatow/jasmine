<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Itemspeckey Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Itemspeckey extends \Orm\Model
{
	protected static $table_name = 'mt_itemspeckey';

	protected static $_properties = array(
	);

	/**
	 * select
	 * 
	 * @param none
	 */
	public static function select ()
	{
		$redis_key = self::$table_name;
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id', 'name')
				->from(self::$table_name)
				->where('del_flg', 0)
				->and_where('open_date', '<=', date("Y-m-d H:i:s"))
				->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}

	/**
	 * select_by_id
	 * 
	 * @param int key
	 * @retrn mix array
	 */
	public static function select_by_id ($key)
	{
		$mt = self::select();
		return isset($mt[$key]) ? $mt[$key]: false;
	}
}