<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Template Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_News extends \Orm\Model
{
	protected static $table_name = 'mt_news';

	protected static $_properties = array(
	);

	/**
	 * select
	 * 
	 * @access public
	 * @return mix array
	 */
	public static function select()
	{
		$redis_key = self::$table_name;
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select(
							'id',
							'catch',
							'category_id',
							'target_url',
							'is_fqdn',
							'open_date',
							'close_date'
						)
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"))
						->order_by('id', 'desc');
			$result = DbHelper::query_exec($query);
			RedisHelper::set_master($redis_key, $result);
		}

		return $result;
	}

	/**
	 * select_join_category
	 * 
	 * @return mix array join mt_newscategory
	 */
	public static function select_join_category ()
	{
		$today       = date("Y-m-d H:i:s");
		$mt_news     = self::select();
		$mt_category = Model_Mt_Newscategory::select();
		$result      = array();

		for ($i=0; $i<count($mt_news); $i++)
		{
			if (
				$mt_news[$i]['open_date'] <= $today &&
				$today <= $mt_news[$i]['close_date'] &&
				isset($mt_category[$mt_news[$i]['category_id']])
			)
			{
				$mt_news[$i]['category'] = $mt_category[$mt_news[$i]['category_id']];
				$result[] = $mt_news[$i];
			}
		}
		return $result;
	}
}
