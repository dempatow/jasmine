<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Template Model.
 * 
 * @package  app
 * @extends  \Orm\Model
 */
class Model_Mt_Newscategory extends \Orm\Model
{
	protected static $table_name = 'mt_newscategory';

	protected static $_properties = array(
		'id' => array(
			'label' => 'id',
			'validation'=> array(
				'checkAjaxId',
			),
		),
	);

	/**
	 * select
	 * 
	 * @access public
	 * @return mix array
	 */
	public static function select()
	{
		$result = array();
		$redis_key = 'mt_news_category';
		$result = RedisHelper::get_master($redis_key);

		if (!$result)
		{
			$query = DB::select('id','name')
						->from(self::$table_name)
						->where('del_flg', 0)
						->and_where('open_date', '<=', date("Y-m-d H:i:s"))
						->and_where('close_date', '>=', date("Y-m-d H:i:s"));
			$result = DbHelper::query_exec($query, 'id');
			RedisHelper::set_master($redis_key, $result);
		}
		return $result;
	}
}