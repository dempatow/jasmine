<?php
/**
 * the FaceBookAPI class
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P advertising Co.,Ltd.
 * @link       http://sp-k.co.jp
 */

class FacebookAPI
{
	/**
	 * facebook config defaults.
	 */
	protected static $_defaults;
	private static   $sdk_instance = null;

	/**
	 * コンストラクタ
	 */
	public static function _init()
	{
		//config読み込み
		Config::load('facebook', true);
		//memberにconfigをセットする
		static::$_defaults = \Config::get('facebook');
		//sdk instanceを生成する
		require_once("sdk/facebook.php");
		//sdk に渡す設定
		$config = array (
			'appId'=>self::$_defaults['app_id'],        //app id
			'secret' => self::$_defaults['app_secret']  //app secret
		);

		self::$sdk_instance = new Facebook ($config);
	}

	/**
	 * get_login_url
	 *
	 * facebookログインのurlを返します
	 *
	 * @return  Email_Driver    one of the email drivers
	 */
	public static function get_login_url ($scope = array('user_about_me','email'), $callback_url = null)
	{
		if (is_null($callback_url))
			$callback_url = Config::get('custom_config.url.https_domain').'/'.self::$_defaults['call_back_method'];

		return self::$sdk_instance->getLoginUrl(
						array(
							'scope' => $scope,
							'redirect_uri' => $callback_url
						)
				);
	}

	/**
	 * get_sigin_up_user_data
	 *
	 * facebookログインのurlを返します
	 *
	 * @return  Email_Driver    one of the email drivers
	 */
	public static function get_sigin_up_user_data ()
	{
		
		return self::$sdk_instance->getUser();
		
		
	}

	/**
	 * get_user
	 *
	 * facebookログインのurlを返します
	 *
	 * @return  Email_Driver    one of the email drivers
	 */
	public static function get_user ()
	{
		if (self::$sdk_instance->getUser())
		{
			return self::$sdk_instance->api('/me','GET');
			//Debug::dump(self::$sdk_instance->api('/me','GET'));exit;
		}

		return false;
	}

	/**
	 * getAccessToken
	 *
	 * @return AccessToken
	 */
	public static function getAccessToken ()
	{
		return self::$sdk_instance->getAccessToken();
	}

	/**
	 * get_logout_url
	 *
	 * facebookログアウトurlを返します
	 *
	 * @return string 
	 */
	public static function get_logout_url ()
	{
		return self::$sdk_instance->getLogoutUrl();
	}

	/**
	 * get_login_url
	 *
	 * facebookログインのurlを返します
	 *
	 * @return  Email_Driver    one of the email drivers
	 */
	public static function get_access_token ()
	{
		return self::$sdk_instance->getAccessToken();
	}
}