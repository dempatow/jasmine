<?php
/**
 * Image class
 *
 * image関係のクラスです
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */

class UploadImage
{
	//public $template = 'template/template_login';
	private static $exts = array(
		'gif' => 'image/gif',
		'jpg' => 'image/jpeg',
		'jpeg'=> 'image/jpeg',
		'png' => 'image/png',
	);

	/**
	 * image_uploadメソッド
	 *
	 * response json.
	 *
	 * @package  app
	 * @extends  Controller_Common
	 */
	static public function image_upload ()
	{
		Config::load('custom_config', true);
		$view_base_url = Config::get('custom_config.url.assets_url');
		$result_array = array('result' => false, 'info' => array());
		$data = Input::post();

		if (isset($data, $data['status']))
		{
			//$result_array['input'] = $data;

			//Uploadクラスの設定
			$config=array(
				'path'=>'',
				'auto_rename'=>false,
				'overwrite' => true,
				'ext_whitelist'=>array('img','jpg','jpeg','gif','png'),
			);

			/*
			 * tmp 処理
			 */
			if ($data['status'] == 'tmp')
			{
				//画像保存パスの設定
				$path='assets/img/item/tmp/';
				$config['path'] = DOCROOT.$path;
				//保存ファイル名の設定
				$config['new_name'] = uniqid().date('_ymdhis');

				//uploadクラスに設定ファイルを読み込ませる
				self::make_process($config);

				if(Upload::is_valid())
				{
					//thumbnailの場合original画像をtmpに保存する
					Upload::save();
					$file=Upload::get_files(0);
					$file['name'] = $config['new_name'];

					//サムネイル用の画像を作成する
					$thumbnail = self::sava_resize_image($file, $config['new_name'], $config['path'], 59, 38, true);
					//返却用json配列を作成する

					//オリジナル画像の生成結果をセットする
					$origin_result = (is_file($config['path'].$config['new_name'].".".$file['extension']))?true: false;
					$result_array['info'] = array(
						'origin' => array(
							'result' => $origin_result, 
							'path' => $config['path'],
							'view_path' => $view_base_url.self::image_path_helper($config['path']),
							'name' => $file["name"].".".$file['extension']
						), 
						'thum' => $thumbnail
					);

					//オリジナル画像とサムネイル画像生成の結果を格納する
					$result_array['result'] = ($thumbnail['result'] && $origin_result)? true: false;
				}
			}
			/*
			 * 本登録 処理
			 */
			else if ($data['status'] == 'regist')
			{
				
			}

		}
		/*
		 * jsonでresponseを返却する 
		 * 
		 * mimeがtext/htmlなのはjquery uploadの仕様である為
		 */
		//$result_array['test'] = self::image_path_helper($config['path']);

		header('Content-type: text/html');
		echo json_encode($result_array);
		exit;
	}

	/**
	 * make_processメソッド
	 * 
	 * Uploadクラスに設定ファイルを読み込ませます
	 * 
	 */
	static public function  make_process ($config)
	{
		Upload::process($config);
	}

	/**
	 * sava_resize_image
	 *
	 * width と height を設定して
	 * サムネイルを作成します。
	 * @param $file array()
	 * 
	 */
	static public function sava_resize_image($origin_path, $origin_name, $new_path, $new_name, $extension, $width, $height, $is_tmp = false)
	{
		//リサイズ画像名の作成
		$new_name = ($is_tmp)? $new_name.'_thumb': $new_name;
		$new_file =$new_name.'.'.$extension;

		//今アップしたばかりの元画像データをロードして処理します
		$image=Image::load($origin_path.$origin_name.".".$extension);
		//画像のリサイズ
		$image->crop_resize($width, $height)->save($new_path.$new_file);
		//ファイルの有無で成功を判断
		$result = is_file($new_path.$new_file)? true: false;

		return array(
				'result' => $result,
				'path' => $new_path,
				'view_path' => Config::get('custom_config.url.assets_url').'/'.self::image_path_helper($new_path),
				'name' => $new_name,
				'extension' => $extension
			);
	}
/*
	static public function sava_resize_image($file, $file_name, $path, $width, $height, $is_tmp = false)
	{
		//リサイズ画像名の作成
		if ($is_tmp)
		{
			$new_file=$file_name.'_thumb.'.$file['extension'];
		}
		else
		{
			$new_file=$file_name.'.'.$file['extension'];
		}

		//今アップしたばかりの元画像データをロードして処理します
		$image=Image::load($file['saved_to'].$file['name'].".".$file['extension']);
		//画像のリサイズ
		$image->crop_resize($width, $height)->save($path.$new_file);

		//ファイルの有無で成功を判断
		$result = is_file($path.$new_file)? true: false;

		return array('result' => $result, 'path' => $path,'view_path' => Config::get('custom_config.url.assets_url').self::image_path_helper($path), 'name' => $new_file);
	}
*/
	/**
	 * サーバ側のパスからURL(ディレクトリまで)を取得するメソッド
	 * 
	 * /Applications/MAMP/htdocs/ginza/public/assets/img/item/tmp/
	 * 　↓
	 * http://ginza.localhost/assets/img/item/tmp/
	 */
	static public function image_path_helper ($path)
	{
		$add_flg = false;
		$image_path = "";
		$exploaded = explode("/", $path);

		for($i=0; $i<count($exploaded); $i++)
		{
			if ($exploaded[$i] == 'img')
				$add_flg = true;

			if ($add_flg && $i != count($exploaded)-1)
			{
				$image_path .= $exploaded[$i].'/';
			}
			else if ($add_flg)
			{
				$image_path .= $exploaded[$i];
			}
		}
		return $image_path;
	}
}