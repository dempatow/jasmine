<?php
/**
 * RedisHelper class
 *
 * Redis補助クラスです
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */
class RedisHelper
{
	private static $config    = null;
	private static $is_enable = null;
	private static $redis     = null;

	/**
	 * コンストラクタ
	 */
	public static function _init()
	{
		Config::load('redis',true);
		self::$config    = Config::get('redis');
		self::$is_enable = self::$config['is_enable'];
		self::$redis     = \Fuel\Core\Redis::forge();
	}

	/**
	 * get_config
	 */
	public static function get_config ()
	{
		return self::$config;
	}

	/**
	 * get_master
	 * キーに対するマスタを取得する
	 * 
	 * @param $key
	 * @return redis keys data
	 */
	public static function get_master ($key)
	{
		$result = false;

		if (self::$is_enable)
		{
			$result = unserialize(self::$redis->get($key));
		}

		return $result;
	}

	/**
	 * キーに対するマスタをセットする
	 * 
	 * @param $key , $value
	 * @return boolean
	 */
	public static function set_master ($key, $value)
	{
		$result = false;
		if (self::$is_enable)
		{
//			if (self::$redis->get($key))
//				self::$redis->del($key);

			$result = self::$redis->set($key, serialize($value));
		}
		return $result;
	}

	/**
	 * del
	 * 
	 * キーのvalueを削除する
	 * @return void
	 */
	public static function del ($key)
	{
		self::$redis->del($key);
	}

	/**
	 * flushall
	 * 
	 * キャッシュをクリアする
	 * 
	 */
	public static function flush_all ()
	{
		self::$redis->flushall();
	}
}