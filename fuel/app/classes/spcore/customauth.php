<?php
/**
 * CustomAuth class
 *
 * 共通認証クラスです
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */
class CustomAuth
{
	public static $config = array();

	/**
	 * コンストラクタ
	 */
	public static function _init()
	{
		//認証設定ファイル読み込み
		config::load('auth_config',true);
		self::$config = Config::get('auth_config');
	}

	/**
	 * delete_passport
	 * 
	 * passportを削除する
	 * 
	 * @param uid
	 */
	public static function delete_passport ($uid)
	{
		//既存のパスポート削除
		Model_Dt_Passport::set_del_flg_by_uid($uid);
		Cookie::delete(self::$config['passport_key']);
	}

	/**
	 * check_passport
	 * 
	 * ユーザーが自動ログインユーザーかつ有効なパスポートかどうか判定する
	 * 有効であった場合はログイン処理も一緒に行う
	 * 
	 * @return bool
	 */
	public static function check_and_login_passport ()
	{
		$passport = Cookie::get(self::$config['passport_key'], false);

		//passportが取得出来なかった場合はfalseを返す
		if (!$passport)
			return $passport;

		//passport取得した場合DBから取得する
		$query = Model_Dt_Passport::get_query_select_availdata_by_passport($passport, self::$config['expiration']);
		$result = DbHelper::query_exec($query, true);

		//dbpassportから取得したuidでdt_userを引く
		if (isset($result['uid']))
		{
			$query = Model_Dt_User::get_query_select_user_by_userid(
						$result['uid'],
						0,     //del_flg
						1      //auto_login
					);
			$result = DbHelper::query_exec($query, true);

			//Cookie passportとDBpassportが一致したらログイン状態にする
			if (count($result)>0)
			{
				//--ログイン処理--
				$result['access_time'] = strtotime('now');
				unset($result['passwd']);
				Session::set('login_info', $result);
				//passportの更新
				self::create_passport($result['id']);
			}
		}

		if(is_null($result))
			$result = false;

		return (bool)$result;
	}

	/**
	 * create_passport
	 * 
	 * 自動ログイン用のpassport生成しCookieをセット及び,
	 * DBにinsertする
	 * 
	 * @access  private
	 * @param   $uid
	 *
	 */
	public static function create_passport ($uid)
	{
		$result = false;

		//Cookie key
		$key        = self::$config['passport_key'];
		//Cookie 有効期限
		$expiration = self::$config['expiration'];
		//passport(Cookie value)生成
		$passport   = Auth::instance()
						->hash_password(
							(Int)rand(0,1000).
							date('YmdHis').
							self::$config['token_salt']
						);
		//Cookieのセット
		Cookie::set($key, $passport, $expiration);

		$insert_data_array = array (
			'uid'      => $uid,
			'passport' => $passport
		);

		//既存のパスポート削除
		Model_Dt_Passport::set_del_flg_by_uid($uid);
		//passport tableにinsertする
		$query = Model_Dt_Passport::get_query_insert_passport($insert_data_array);
		$result = DbHelper::query_exec($query, null, null, 'master');

		//TODO: パスポート更新
		return (bool)count($result);
	}

	/**
	 * login method
	 *
	 * @access  public
	 * @param   mail password
	 * @return  bool
	 */
	public static function login($mail, $passwd)
	{
		$result = false;

		$db_data = DbHelper::query_exec(
			Model_Dt_User::get_login_query($mail),
			true
		);

		if (
			isset($db_data['mail'],$db_data['passwd']) &&
			$db_data['mail'] == $mail &&
			$db_data['passwd'] == Auth::instance()->hash_password($passwd)
		)
		{
			//--ログイン処理--
			$db_data['access_time'] = strtotime('now');
			unset($db_data['passwd']);
			Session::set('login_info', $db_data);
			//auto_login_flgが1の場合
			if (isset($db_data['auto_login']) && (Int)$db_data['auto_login'] === (Int)1)
			{
				//新規自動ログインパスポート作成
				self::create_passport($db_data['id']);
			}

			$result = true;
		}
		return $result;
	}

	/**
	 *  login_passport
	 *  Cookieから自動ログインを判定し、
	 */

	/**
	 * logout method
	 * 
	 * login情報を削除し、toppegeへredirectさせる
	 * 
	 * @access  public
	 * @param $no_redirect defalut false
	 */
	public static function logout($no_redirect = false, $is_leave_cart = false)
	{
		$uid = Session::get('login_info.id', false);

		if ($uid)
			Model_Dt_Passport::set_del_flg_by_uid($uid);

		Session::delete('login_info');
		Cookie::delete(self::$config['passport_key']);
		Session::destroy();

		if (!$no_redirect)
			Response::redirect(Config::get('custom_config.url.http_domain'));
	}

	/**
	 * check_access_term
	 * 
	 * access時間をcheckするmothod
	 * 
	 * @access  public
	 * @return  bool
	 */
	public static function check_access_term()
	{
		//最終アクセス時間を取得する
		$access_time = Session::get('login_info.access_time', false);

		//最後のアクセスから設定ファイル以上の時間が経過した場合
		if (
			$access_time &&
			((strtotime('now')-$access_time) > ((Int)self::$config['logout_term'] * 60))
		)
		{
			//時間切れだが、自動ログインが有効だった場合
			if(self::check_and_login_passport())
				return;

			//強制logoutさせる
			self::logout();
		}
		else if ($access_time)
		{
			//時間が経過していない場合last_accessを現在時間で上書きする
			Session::set('login_info.access_time', strtotime('now'));
		}
		else
		{
			//時間設定ないけど、自動ログインが有効だった場合
			self::check_and_login_passport();
		}
	}

	/**
	 * アクセス権限のcheck method
	 *
	 * @param check_controller,check_method,role =default0
	 * @access  public
	 * @return  bool
	 */
	public static function check_access_auth($req_cont, $req_meth, $role = 0)
	{
		//configからACLを取得する
		$ACL    = self::$config['ACL'][$role];
		//返却結果
		$result = false;

		/*
		 * roleをチェックする
		 */
		foreach ($ACL as $config_cont => $config_meth_array)
		{
			if ($config_cont === '*' || $config_cont == $req_cont)
			{
				foreach ($config_meth_array as $config_meth)
				{
					$result = ($config_meth === '*') || ($config_meth === $req_meth)? true: $result;
					if($result)
						break;
				}
			}
			if($result)
				break;
		}
		return $result;
	}

	/**
	 * get_login_info
	 * 
	 * session login_info を取得します。
	 * loginしていない場合はfalseを返却します。
	 * 
	 * is_detail を指定した場合はログインuidをキーにdbから
	 * password以外のデータをselecして返却します。
	 * 
	 * @param $is_detail default false
	 * @return user data array
	 */
	public static function get_login_info ($is_detail = false)
	{
		$result = Session::get('login_info', false);

		if ($is_detail && $result)
		{
			$result = Model_Dt_User::select_user_by_uid($result['id']);
			unset($result['passwd']);

			if (empty($result))
				Log::error('user detail info cannot get');
		}
		return $result;
	}
}