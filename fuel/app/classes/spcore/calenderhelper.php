<?php
/**
 * CalenderHelper class
 *
 * Calenderヘルパークラスです
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */
class CalenderHelper
{
	//viewに渡すデータ
	private static $calender_data  = array(
		'weekday' => array('日', '月', '火', '水', '木', '金', '土')
	);
	private static $today_day = null;        //本日の日(桁数調整あり)
	private static $today_year_month = null; //本日の年月(桁数調整あり)20130101
	private static $args_year_month = null;  //引数の年月(桁数調整あり)20130101
	public  static $holiday = null;

	private static $designated_date_array = array(); //発送可能日配列

	/**
	 * コンストラクタ
	 */
	public static function _init()
	{
		self::$today_day = date('d');//本日の日(桁数調整あり)
		self::$holiday = Model_Mt_Holiday::get_holiday();
	}

	/**
	 * get_calender
	 * 
	 * 通常リクエスト時にカレンダーを作成するメソッド
	 * 設置するviewにrenderメソッドを書く必要があります。
	 * 
	 * @param type $template template, $year_month 表示する年月
	 */
	public static function get_calender ($template, $year_month = null)
	{
		self::set_getparam($year_month);
		$template->set_global(self::$calender_data);
	}

	/**
	 * get_first_blank
	 * カレンダー最初のblank数を返却します
	 * @return int
	 */
	private static function get_first_blank ()
	{
		return date("w", mktime(0,0,0,self::$calender_data['month'], 1, self::$calender_data['year']));
	}

	/**
	 * get_calender_ajax
	 * 
	 * ajaxリクエスト時にカレンダーhtmlを作成するメソッド
	 * viewを返却します。
	 * 
	 * @param $yearmonth ex)201312
	 * @return redis keys data
	 */
	public static function get_calender_ajax ($year_month)
	{
		self::set_getparam($year_month);
		return View::forge('calender/calender',self::$calender_data);
	}

	/**
	 * set_getparam
	 * 
	 * getパラメータからカレンダーから表示する年と月を算出し
	 * メンバ$calender_dataにセットする
	 */
	private static function set_getparam ($year_month)
	{
		//checkdate ( int $month , int $day , int $year )
		if (!checkdate ((int)substr($year_month, 4, 2), 1, (int)substr($year_month,0,4)) || is_null($year_month))
		{
			self::$calender_data['year']  = date('Y');
			self::$calender_data['month'] = date('n');
		}
		else
		{
			self::$calender_data['year']  = date('Y', strtotime($year_month . '01'));
			self::$calender_data['month'] = date('n', strtotime($year_month . '01'));
		}

		self::$today_year_month = date('Y').date('m');//本日の年月(桁数調整あり)
		self::$args_year_month = date(self::$calender_data['year'].sprintf("%02d",self::$calender_data['month']));//引数の年月(桁数調整あり)
		self::$calender_data['first_blank'] = self::get_first_blank();
	}

	/**
	 * get_color
	 * 
	 * 日にちごとの文字の色を取得します
	 * 
	 * @param int $day
	 * @return string 文字色
	 */
	public static function get_string_color ($day ,$offset)
	{
		$day = sprintf("%02d",$day);
		$color = '#000000';

		if ((Int)$offset === 0 || (Int)$offset === 6)
		{
			//土日の場合
			$color = '#000000';
		}
		else if (
				isset(self::$holiday[self::$args_year_month.$day])
		)
		{
			//定休日の場合
			$color = '#000000';
		}
		else if (
				(Int)$day === (Int)self::$today_day && 
				(Int)self::$today_year_month === (Int)self::$args_year_month
		)
		{
			//本日の場合
			$color = '#000000';
		}

		return $color;
	}

	/**
	 * get_bg_color
	 * 
	 * 日にちごとの背景色を取得します
	 * 
	 * @param int $day
	 * @return string 背景色
	 */
	public static function get_bg_color ($day, $offset)
	{
		$day = sprintf("%02d",$day);
		$color = '#ffffff';

		if ((Int)$offset === 0 || (Int)$offset === 6)
		{
			//土日の場合
			$color = '#3A75F1';
		}
		else if (
				isset(self::$holiday[self::$args_year_month.$day])
		)
		{
			//定休日の場合
			$color = '#3A75F1';
		}
		else if (
				(Int)$day === (Int)self::$today_day && 
				(Int)self::$today_year_month === (Int)self::$args_year_month
		)
		{
			//本日の場合
			$color = '#FCC6B9';
		}

		return $color;
	}

	/**
	 * check_holiday
	 * 
	 * 土日休日であるかどうかbooleanで返却します
	 */
	public static function check_holiday ($day, $offset)
	{
		$result = false;

		if ((Int)$offset === 0 || (Int)$offset === 6)
		{
			$result = true;
		}
		else if (isset(self::$holiday[self::$args_year_month.sprintf("%02d",$day)]))
		{
			$result = true;
		}

		return $result;
	}

	/**
	 * get_day_class
	 */
	public static function get_day_class ($day, $offset)
	{
		$result ='';
		$classes = '';

		if ((Int)$offset === 0 || (Int)$offset === 6 || isset(self::$holiday[self::$args_year_month.sprintf("%02d",$day)]))
		{
			//土日の場合
			$classes .= 'holiday';
		}

		if (
			(Int)$day === (Int)self::$today_day && 
			(Int)self::$today_year_month === (Int)self::$args_year_month
		)
		{
			//本日の場合
			$classes .= ' today';
		}

		if ($classes != '')
		{
			$result = 'class="'.$classes.'"';
		}
		return $result;
	}

	/**
	 * get_designated_date_array
	 * 
	 * 指定可能な日時を配列で返却します
	 * 
	 * @param String $from
	 * @param String $to
	 */
	public static function get_designated_date_array ($from, $to)
	{
		$result = array();

		//開始日(実行日)
		$year  = date('Y');
		$month = date('m');
		$day   = date('d');

		//指定可能開始日
		$designated_year  = '';
		$designated_month = '';
		$designated_day   = '';

		/**********************
		 *  指定開始可能日を求める
		 **********************/
		$counter = 0;
		while ($counter < $from)
		{
			//for文日にちが月末を超えた場合
			//開始日時を再設定する(設定済み開始日に一日追加する)
			if ($counter > 0)
			{
				$d = new DateTime($year.'-'.$month.'-'.sprintf("%02d", (--$days)));
				$d->modify('+1 day');
				$year       = $d->format('Y');
				$month      = $d->format('m');
				$day        = $d->format('d');
			}

			for ($days = $day; checkdate($month, $days, $year); $days++)
			{
				//曜日取得
				$week_no = date("w", mktime(0,0,0,$month, $days, $year));

				//土日定休日でない場合カウンターをカウントアップ
				($week_no!= 6 && $week_no != 0 && !isset(self::$holiday[$year.$month.sprintf("%02d", $days)])) and $counter++;

				//指定開始日が判断した時
				if ($counter === (Int)$from)
				{
					$designated_year = $year;
					$designated_month = $month;
					$designated_day = sprintf("%02d", $days);
					break;
				}
			}
		}

		/******************************
		 *  選択可能な日時を配列にセットする
		 ******************************/
		$counter = 0;
		while ($counter < $to)
		{
			//for文日にちが月末を超えた場合
			//開始日時を再設定する(設定済み開始日に一日追加する)
			if ($counter > 0)
			{
				$d = new DateTime($designated_year.'-'.$designated_month.'-'.sprintf("%02d", (--$days)));
				$d->modify('+1 day');
				$designated_year  = $d->format('Y');
				$designated_month = $d->format('m');
				$designated_day   = $d->format('d');
			}

			for ($days = $designated_day; checkdate($designated_month, $days, $designated_year); $days++)
			{
				$counter++;
				//曜日取得
				$week_no = date("w", mktime(0,0,0,$designated_month, $days, $designated_year));
				//返却配列に代入する
				$result[] = array(
					'disp' =>$designated_year.'年'.$designated_month.'月'.sprintf("%02d", $days).'日'.'('.self::$calender_data['weekday'][$week_no].')',
					'value'=>$designated_year.$designated_month.sprintf("%02d", $days)
				);
				//指定開始日が判明した時
				if ($counter === (Int)$to)
					break;
			}
		}

		// "選択しない" を挿入する
		if (count($result)>0)
		{
			$first_row = array(
				'disp' => '指定しない',
				'value'   => 0,
			);
			array_unshift($result, $first_row);
		}
		return $result;
	}
}