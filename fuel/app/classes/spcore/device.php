<?php
/**
 * Device
 *
 * Deviceクラスです
 * 
 * Deviceの判定等を行うクラスです
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */
class Device
{
	public static $code = null;
	public static $PC   = null;
	public static $FP   = null;
	public static $SP   = null;
	public static $TB   = null;

//	public static $template = null;
//	public static $header   = null;
//	public static $footer   = null;

	/**
	 * コンストラクタ
	 */
	public static function _init()
	{
		//設定ファイルのdevice_codeをセットする
		//Config::load('device_config', true);
		self::$SP = 1;//(Int)Config::get('device_config.sp_cd');
		self::$PC = 2;//(Int)Config::get('device_config.pc_cd');
		self::$FP = 3;//(Int)Config::get('device_config.fp_cd');
		self::$TB = 1;//(Int)Config::get('device_config.tb_cd');

		//device code をセットする
		self::$code = (Int)self::get_device_code();
	}

	/**
	 * get_template_path
	 * 
	 * template path 取得メソッド
	 * @return string
	 */
/*
	public static function get_template_path ()
	{
		return self::$template;
	}
 */

	/**
	 * get_header_path
	 */
/*
	public static function get_header_path ()
	{
		return self::$header;
	}
 */


	/**
	 * get_footer_path
	 */
/*
	public static function get_footer_path ()
	{
		return self::$footer;
	}
 */


	/**
	 * is_sp
	 * 
	 * SP判定メソッド
	 */
	public static function is_sp ()
	{
		return self::$SP === self::$code;
	}

	/**
	 * is_pc
	 * 
	 * PC判定メソッド
	 */
	public static function is_pc ()
	{
		return self::$PC === self::$code;
	}

	/**
	 * is_pc
	 * 
	 * FP判定メソッド
	 */
	public static function is_fp ()
	{
		return self::$FP === self::$code;
	}

	/**
	 * is_tb
	 * 
	 * tb判定メソッド
	 */
	public static function is_tb ()
	{
		return self::$TB === self::$code;
	}

	/**
	 * get_device_code
	 *
	 * @access  public
	 * @param
	 * @return  int device code by device_config
	 */
	public static function get_device_code()
	{
		$ua = \Input::server('http_user_agent', '');
		if (
				(strpos($ua, 'Android')       !== false) &&
				(strpos($ua, 'Mobile')        !== false) ||
				(strpos($ua, 'iPhone')        !== false) ||
				(strpos($ua, 'Windows Phone') !== false)
		)
		{
			//-----スマートフォンからアクセスされた場合
			self::$template = 'template/template_sp';
			self::$header   = 'layout/header_sp';
			self::$footer   = 'layout/footer_sp';
			return self::$SP;
		}
		else if (
					(strpos($ua, 'Android') !== false) ||
					(strpos($ua, 'iPad')    !== false)
				)
		{
			//-----タブレットからアクセスされた場合
			return self::$TB;
		}
		else if (
					(strpos($ua, 'DoCoMo')   !== false) ||
					(strpos($ua, 'KDDI')     !== false) ||
					(strpos($ua, 'SoftBank') !== false) ||
					(strpos($ua, 'Vodafone') !== false) ||
					(strpos($ua, 'J-PHONE')  !== false)
				)
		{
			//-----携帯からアクセスされた場合
			return self::$FP;
		}
		else
		{
			//-----その他（PC）からアクセスされた場合
			return self::$PC;
		}
	}
}