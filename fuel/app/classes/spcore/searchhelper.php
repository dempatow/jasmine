<?php
/**
 * SearchHelper class
 *
 * SerchHelperクラスです
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */
class SearchHelper
{
	/**
	 * コンストラクタ
	 */
	public static function _init()
	{
	}

	/**
	 * search_item
	 * 
	 * 検索条件から該当するitem_idを返却します
	 * 
	 * @param mix array item_id_array
	 * @return 
	 * 
	 */
	public static function search_item ($key_array)
	{
		//Debug::dump('検索パラメータ',$key_array);

		$result       = array();
		$item_ids     = array();
		$item_ids_cnt = 0;
		$single_category = array();

		//--- category 検索で必要な変数初期化
		if (isset($key_array['cate']))
		{
			$single_category = Model_Mt_Category::select_category_by_recursive_no(0);
		}

		/*
		 * キーワード検索の場合はまず先にviewから検索処理を行う
		 */
		if (isset($key_array['word']) && !empty($key_array['word']))
		{
			$item_ids_db = Model_View_Searchword::select_item_id_by_word($key_array['word']);
			for ($i=0; $i<count($item_ids_db); $i++)
			{
				$item_ids[] = $item_ids_db[$i]['id'];
			}
		}
		else
		{
			$item_ids = Model_Mt_Item::select_ids();
		}

		//--- idsのカウントをとる
		$item_ids_cnt = count($item_ids);

		/*
		 * item idを回す
		 */
		for ($i=0; $i<$item_ids_cnt; $i++)
		{
			//一件ずつマスタ取得
			$item_mt = Model_Mt_Item::select_item_by_id($item_ids[$i],true);
			//比較に失敗した場合false
			$is_much = true;

			/*
			 * itemごとに検索キーをチェックする
			 */
			foreach ($key_array as $type => $param)
			{
				switch ($type)
				{
					/***************
					 * character_id
					 ***************/
					case 'char':
						if (!isset($item_mt['chara']))
							$is_much = false;//商品マスタがキャラクタデータを持ってないとき

						if (!$is_much)
							break;

						for ($j=0; $j<count($param);$j++)
						{
							!isset($item_mt['chara'][$param[$j]]) and $is_much = false;
						}

						break;
					/***************
					 * category_id
					 ***************/
					case 'cate':
						if (!isset($item_mt['category']))
							$is_much = false;//商品マスタがカテゴリデータを持ってないとき

						if (!$is_much)
							break;

						for ($j=0; $j<count($param);$j++)
						{
							if (isset($single_category[$param[$j]]))
							{
								//--- 検索キーが単独カテゴリの場合はitemマスタとそのまま照合する
								!isset($item_mt['category'][$param[$j]]) and $is_much = false;
							}
							else
							{
								//--- 検索キーが親子関係にある場合
								$is_much_parent = false;
								foreach ($item_mt['category'] as $key => $val)
								{
									$is_much_parent = self::check_parent($param[$j] , $val);
									//合致した場合はforeachを抜ける
									if ($is_much_parent)
										break;
								}
								if (!$is_much_parent)
									$is_much = false;
							}
						}
						break;
				}
			}
			//チェック不一致でなかった場合
			$is_much and $result[] = $item_ids[$i];
		}
		return $result;
	}

	/**
	 * search_item_ajax
	 * 
	 * 検索条件から該当するitem_idを返却します
	 * 
	 * @param mix array item_id_array
	 * @return 
	 * 
	 */
	public static function search_item_ajax ($key_array)
	{
		//Debug::dump('検索パラメータ',$key_array);

		$result       = array();
		$item_ids     = '';
		$item_ids_cnt = 0;
		$single_category = array();

		//--- category 検索で必要な変数初期化
		if (isset($key_array['cate']))
		{
			$single_category = Model_Mt_Category::select_category_by_recursive_no(0);
		}

		/*
		 * キーワード検索の場合はまず先にviewから検索処理を行う
		 */
		if (isset($key_array['word']))
		{
			$item_ids = Model_View_Searchword::select_item_id_by_word($key_array['word']);
		}
		else
		{
			$item_ids = Model_Mt_Item::select_ids();
		}

		//--- idsのカウントをとる
		$item_ids_cnt = count($item_ids);

		/*
		 * item idを回す
		 */
		for ($i=0; $i<$item_ids_cnt; $i++)
		{
			//一件ずつマスタ取得
			$item_mt = Model_Mt_Item::select_item_by_id($item_ids[$i]['id'],true);
			//比較に失敗した場合false
			$is_much = true;

			/*
			 * itemごとに検索キーをチェックする
			 */
			foreach ($key_array as $type => $param)
			{
				switch ($type)
				{
					/***************
					 * character_id
					 ***************/
					case 'char':
						if (!isset($item_mt['chara']))
							$is_much = false;//商品マスタがキャラクタデータを持ってないとき

						if (!$is_much)
							break;

						for ($j=0; $j<count($param);$j++)
						{
							!isset($item_mt['chara'][$param[$j]]) and $is_much = false;
						}

						break;
					/***************
					 * category_id
					 ***************/
					case 'cate':
						if (!isset($item_mt['category']))
							$is_much = false;//商品マスタがカテゴリデータを持ってないとき

						if (!$is_much)
							break;

						for ($j=0; $j<count($param);$j++)
						{
							if (isset($single_category[$param[$j]]))
							{
								//--- 検索キーが単独カテゴリの場合はitemマスタとそのまま照合する
								!isset($item_mt['category'][$param[$j]]) and $is_much = false;
							}
							else
							{
								//--- 検索キーが親子関係にある場合
								$is_much_parent = false;
								foreach ($item_mt['category'] as $key => $val)
								{
									$is_much_parent = self::check_parent($param[$j] , $val);
									//合致した場合はforeachを抜ける
									if ($is_much_parent)
										break;
								}
								if (!$is_much_parent)
									$is_much = false;
							}
						}
						break;
				}
			}
			//チェック不一致でなかった場合
			$is_much and $result[] = $item_ids[$i]['id'];
		}
		return $result;
	}

	/**
	 * check_parent
	 * 
	 * 引数に与えたcategory_idからrootまで一致するまで再帰的に検索する
	 * @param int search_key
	 * @param mix array item_mt(category部分のみ)
	 * @return bool result
	 */
	public static function check_parent ($key, $item_mt)
	{
		$result    = false;
		$recursive = $item_mt['recursive_no'];//深さ
		$next_id   = '';//検査中のmt_id

		while ($recursive >= 1 && !$result)
		{
			if ($next_id != '')
			{
				//---二回目以降の比較の場合
				$item_mt = Model_Mt_Category::select_category_by_id($next_id);
			}

			if (isset($item_mt['id']) && (Int)$item_mt['id'] === (Int)$key)
			{
				//---一致した時
				$result = true;
			}
			else
			{
				//---一致しなかった時
				$next_id = isset($item_mt['parent_no'])?$item_mt['parent_no']: null;
			}

			$recursive--;//次回検索深度をカウントダウン
		}
		return $result;
	}

	/**
	 * get_child_node_by_parent_id
	 * 
	 * 引数の要素の子ノードを取得します
	 * 
	 * @param int parent id
	 * @param mix array child nods
	 */
	public static function get_child_node_by_parent_id ($parent_id)
	{
		$result = array();
		$parent_node = Model_Mt_Category::select_category_by_id($parent_id);
		$all_child_nods = Model_Mt_Category::select_category_by_recursive_no(++$parent_node['recursive_no']);

		if ($all_child_nods)
		{
			foreach ($all_child_nods as $key => $val)
			{
				if ($val['parent_no'] === $parent_node['id'])
					$result[$key] = $val;
			}
		}
		return count($result)>0? $result: false;
	}
}