<?php
/**
 * Purchase class
 *
 * 購入クラス
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */
class Purchase
{
	//エラー定数
	const DEFAULT_ERROR_MESSAGE          = '購入処理に失敗しました。しばらくたってから再度お試しください。';

	/*
	 * purchase status用定数
	 * 
	 * 運用ルール
	 * ☆ userの履歴に表示する物は1からカウントアップ
	 * ☆ userの履歴に表示させないものは(管理系コード)100からカウントアップ
	 */
	//通常コード
	const NEW_PURCHASE                   = 1;   //購入申し込み
	const ALREADY_SENDING                = 2;   //発送済み
	const CANCEL_USER                    = 3;   //ユーザー申し出による取消
	const CANCEL_SYSTEM                  = 4;   //管理者による取り消し
	const SENDING_PREPARATION            = 5;   //発送準備中

	//管理系コード
	const CANCEL_AGENT_AUTO              = 101; //代行決済途中自動取り消し(時間経過バッチ処理による)
	const CANCEL_AGNET_BACK_BUTTON       = 102; //代行決済戻りボタン取り消し
	const CANCEL_AGNET_BAD_RESPONSE      = 103; //決済代行エラーレスポンス取り消し(決済画面から)
	const CANCEL_AGNET_BAD_RESPONSE_URL  = 104; //決済代行エラーレスポンス取り消し(リダイレクトURL取得)
	const SCREEN_TRANSITION_GMO          = 105; //決済代行画面遷移中

	/*
	 * payment_status_cd用定数
	 */
	const ZERO_YEN_PAYMENT          = 10;        //決済金額が0円の時に使用する

	//その他
	const NO_LOGIN_USERE_ID           = 111111111;  //非ログインユーザー用固定ID
	private static $error_flg         = 0;          //処理途中のエラー状況
	private static $insert_tables     = '';         //insertしたデータのtable nameをセットします
	private static $purchase_id       = 0;          //購入ID
	private static $payment_amount    = 0;          //決済金額

	private static $cart_dt           = '';         //cart
	private static $cart_sum_info     = '';         //cart合計データ
	private static $purchase_info     = '';         //購入者
	private static $receiver_info     = '';         //お届け先
	private static $sending_cd        = 0;          //発送コード
	private static $sending_info      = '';         //発送方法データ
	private static $login_info        = '';         //ログインデータ
	private static $point_rate_id     = 0;          //ポイントレートID
	private static $point_rate_info   = '';         //ポイントレート情報

	private static $reserve_dt        = null;       //予約データ(session)
	private static $reserve_dt_db     = null;       //予約データ(データベース)

	private static $special_select_dt = array();    //特殊表示session
	private static $insert_purchase_item_results = array();//dt_purchase_itemテーブルのinsert結果

	private static $tax_cd            = 0;          //消費税コード
	private static $payment_cd        = 0;          //決済コード
	private static $total_point       = 0;          //獲得ポイント合計
	private static $use_point         = 0;          //使用ポイント
	private static $error_message     = '';         //エラーメッセージ

	private static $gmo_response      = '';         //gmo resopnse data(代行処理後のあと処理に使用します)

	/**
	 * forge_regist
	 * 新規購入申し込みの際に使用する
	 * @param type $cart_dt
	 * @param type $purchase_info
	 * @param type $receiver_info
	 * @param type $sending_cd
	 * @return \static
	 */
	public static function forge_regist($cart_dt, $purchase_info, $receiver_info, $sending_info, $sending_cd, $point_rate_info, $special_select_dt)
	{
		return new static(1, $cart_dt, $purchase_info, $receiver_info, $sending_info, $sending_cd, $point_rate_info, null,$special_select_dt);
	}

	/**
	 * forge
	 * 初回登録以外の際に使用する
	 * @parma purchase_id
	 */
	public static function forge ($purchase_id = null)
	{
		//return new static (2, $purchase_id);
		// (2, $purchase_id);
		return new static(2, null, null, null, null, null, null,$purchase_id);
	}

	/**
	 * constractor
	 * 
	 * forge の引数を class 変数にセットします
	 * 
	 * @param type $cart_dt
	 * @param type $purchase_info
	 * @param type $receiver_info
	 * @param type $sending_cd
	 */
	public function __construct($status, $cart_dt = null, $purchase_info = null, $receiver_info = null, $sending_info = null, $sending_cd = null, $point_rate_info = null ,$purchase_id = null, $special_select_dt = array())
	{
		/*
		 * 初回購入処理の際のみ
		 */
		if ((Int)$status === 1)
		{
			self::$cart_dt           = $cart_dt;
			self::$special_select_dt = $special_select_dt;

			//予約レコードの取得
			if (!empty(self::$reserve_dt))
				self::set_reserve_dt_db (Model_Dt_Reserve::select_by_session_dt(self::$reserve_dt));

			//在庫の減算
			self::stock_subtraction();

			if (!self::$error_flg)
			{
				self::$cart_sum_info = Func::get_cart_info_sum($cart_dt);
				self::$purchase_info = $purchase_info;
				self::$receiver_info = $receiver_info;
				self::$sending_cd    = (Int)$sending_cd;
				self::$sending_info  = $sending_info;

				//消費税CD取得
				self::$tax_cd        = (Int)Model_Mt_Tax::select_current_tax()['id'];
				//payment_cd設定
				self::$payment_cd    = (Int)self::$purchase_info['payment_cd'];
				//ログインユーザー情報設定
				self::$login_info    = CustomAuth::get_login_info();
				//ポイントレート設定
				self::$point_rate_id = (Int)$point_rate_info['id'];
				//ポイントレート情報
				self::$point_rate_info = $point_rate_info;
				//獲得ポイントの計算
				self::$total_point   = Func::get_total_point(self::$cart_dt, (Int)$point_rate_info['rate']);
				//使用ポイント
				self::$use_point     = isset(self::$purchase_info['use_point']) && self::$purchase_info['use_point']>0? (Int)self::$purchase_info['use_point']: 0;
			}

			/********************************
			 * ここからDB処理 (在庫以外)
			 ********************************/
			//--- dt_purchase insert
			if (!self::$error_flg)
				self::insert_inital_purchase_user();

			//--- dt_purchaseitemhisotry
			if (!self::$error_flg)
				self::insert_purchase_item_histories();

			//-- dt_purchasespecialselect
			if (!self::$error_flg)
				self::insert_purchase_special_select ();

			//--- dt_purchaseaddresshistory
			if (!self::$error_flg)
				self::insert_purchase_address_history();

			//--- dt_pointhistory
			if (!self::$error_flg);
				self::insert_initial_pointhistory();
		}
		else if ((Int)$status === 2)
		{
			self::$purchase_id = $purchase_id;
		}

	}

	/**
	 * set_reserve_dt_db
	 */
	public static function set_reserve_dt_db ($reserve_dt_db)
	{
		self::$reserve_dt_db = $reserve_dt_db;
	}

	/**
	 * get_reserve_dt_db
	 */
	public static function get_reserve_dt_db ()
	{
		return self::$reserve_dt_db;
	}

	/**
	 * set_reserve_dt
	 */
	public static function set_reserve_dt ($reserve_dt)
	{
		self::$reserve_dt = $reserve_dt;
	}

	/**
	 * get_reserve_dt
	 */
	public static function get_reserve_dt ()
	{
		return self::$reserve_dt;
	}

	/**
	 * set_gmo_response
	 */
	public static function set_gmo_response($response_data)
	{
		self::$gmo_response = $response_data;
	}

	/**
	 * get_payment_process_cd
	 * カード決済のプロセスコードを取得します
	 * @return payment_process_cd
	 */
	public static function get_payment_process_cd ()
	{
		return (Int)(isset(self::$purchase_info['payment_process_cd']))? self::$purchase_info['payment_process_cd']: 1;
	}

	/**
	 * get_user_mail
	 * 購入者のメールを取得します
	 * @return user_mail
	 */
	public static function get_user_mail ()
	{
		return (isset(self::$purchase_info['mail_purchase']))? self::$purchase_info['mail_purchase']: '';
	}

	/**
	 * get_payment_amount
	 * 決済総額を返却します
	 * @return payment_amount
	 */
	public static function get_payment_amount()
	{
		return self::$payment_amount;
	}

	/**
	 * get_user_id
	 * ログインしていない場合は定数を返却します
	 * @return user_id
	 */
	public static function get_user_id ()
	{
		return (Int)(isset(self::$login_info['id']))? self::$login_info['id']: self::NO_LOGIN_USERE_ID;
	}

	/**
	 * get_user_name
	 * 購入者名の姓と名を結合して返却します
	 * 
	 * @return last_name + first_name
	 */
	public static function get_user_name()
	{
		$result = '';
		if (isset(self::$purchase_info['first_name']) && isset(self::$purchase_info['last_name']))
			$result = self::$purchase_info['last_name'].''.self::$purchase_info['first_name'];

		return $result;
	}

	/**
	 * get_purchase_id
	 * @return self::$error_message
	 */
	public static function get_purchase_id ()
	{
		return self::$purchase_id;
	}

	/**
	 * set_purchase_id
	 * @return self::$error_message
	 */
	public static function set_purchase_id ($purchase_id)
	{
		self::$purchase_id = $purchase_id;
	}

	/**
	 * get_error_message
	 * @return self::$error_message
	 */
	public static function get_error_message ()
	{
		return self::$error_message;
	}

	/**
	 * get_error_flg
	 * @return self::$error_flg
	 */
	public static function get_error_flg ()
	{
		return self::$error_flg;
	}

	/**
	 * get_payment_cd
	 * 
	 * @return self::$payment_cd
	 */
	public static function get_payment_cd ()
	{
		return self::$payment_cd;
	}

	/**
	 * get_purchase_info
	 */
	public static function get_purchase_info ()
	{
		return self::$purchase_info;
	}

	/**
	 * get_reciever_info
	 */
	public static function get_reciever_info ()
	{
		return self::$receiver_info;
	}

	/**
	 * get_cart_dt
	 */
	public static function get_cart_dt ()
	{
		return self::$cart_dt;
	}

	/**
	 * get_sending_info
	 */
	public static function get_sending_info ()
	{
		return self::$sending_info;
	}

	/**
	 * get_ponit_rate_info
	 */
	public static function get_point_rate_info ()
	{
		return self::$point_rate_info;
	}

	/**
	 * insert_initial_pointhistory
	 * 
	 * dt_pointhistoryにinsertします
	 * 
	 */
	private static function insert_initial_pointhistory ()
	{
		//ログインユーザーで無い場合
		if (!self::$login_info)
			return;

		//--- 使用ポイントありの場合
		if (self::$use_point >0 && Point::get_point() >= self::$use_point)
			Point::add_point(-(self::$use_point), self::$purchase_id, Point::POINT_SUBTRACT_PURCHASE, 1);

		//--- 取得ポイントありの場合
		if (self::$total_point > 0)
				Point::add_point(self::$total_point, self::$purchase_id, Point::POINT_ADD_PURCHASE, 0);
	}

	/**
	 * stock_subtraction
	 * 
	 * 在庫数をチェックし、減算します
	 * 
	 */
	private static function stock_subtraction ()
	{
		$result             = false;
		$checke_data        = Model_Dt_Stock::check_stock(self::$cart_dt, self::$special_select_dt);
		$special_select_tmp = self::$special_select_dt;

		if ($checke_data['result'])
		{
			$query_array = array();
			for ($i=0; $i<count(self::$cart_dt);$i++)
			{
				$count_tmp = self::$cart_dt[$i]['count'];

				/***********************************************
				 * 予約商品がカートにはいっていた場合
				 ***********************************************/
				if (isset(self::$reserve_dt[self::$cart_dt[$i]['id']]))
				{
					for ($j=0; $j<count(self::$reserve_dt[self::$cart_dt[$i]['id']]); $j++)
					{
						$count_tmp -= self::$reserve_dt_db[self::$reserve_dt[self::$cart_dt[$i]['id']][$j]]['count'];
					}
				}

				$query_array[] = DB::query('update `dt_stock` set `count`= `count`-'.$count_tmp.', `updated`="'.date('Y-m-d H:i:s').'" where `item_id` ='. self::$cart_dt[$i]['id'] .' and `del_flg` = 0');

				/***********************************************
				 * 特殊表示アイテムの在庫減算sql生成
				 ***********************************************/
				if (!empty($special_select_tmp))
				{
					foreach ($special_select_tmp as $key => $value)
					{
						if ((Int)$value['item_id'] === (Int)self::$cart_dt[$i]['id'])
						{
							$query_array[] = DB::query(
								'update
									`dt_specialselectstock`
								set
									`count`= `count`-'.$value['count'].',
									`updated`="'.date('Y-m-d H:i:s').'"
								where
									`id` ='. $value['value'] .'
								and
									`del_flg` = 0'
							);
							unset($special_select_tmp[$key]);
						}
					}
				}
			}

			$result = DbHelper::query_exec($query_array, false, false, Fuel::DB_MASTER);
		}

		//*******************************
		// エラー処理
		//*******************************
		$error_str = '';
		if (!$checke_data['result'])
		{
			//処理失敗
			self::error(array('在庫の減算に失敗しました(在庫チェックエラー)', self::DEFAULT_ERROR_MESSAGE));
		}
		else if (!$result)
		{
			//処理失敗
			self::error(array('在庫の減算に失敗しました(DBエラー)', self::DEFAULT_ERROR_MESSAGE));
		}
		//*******************************
	}

	/**
	 * insert_purchse_address_history
	 * 
	 * dt_purchaseaddress_historyにinsertします
	 */
	private static function insert_purchase_address_history ()
	{
		//table名セット(エラー処理用)
		self::$insert_tables[] = Model_Dt_Purchaseaddresshistory::$table_name;

		//お届け先が購入者と同じ場合は2回回す でなければ 1回回す
		$count = isset(self::$purchase_info['receiver']) && self::$purchase_info['receiver'] == 'own' ? 2: 1;

		for ($i=1; $i<=$count; $i++)
		{
			$insert_array = array(
				'purchase_id'         => self::$purchase_id,
				'type_cd'             => (Int)$i,
			);
			isset(self::$purchase_info['corporate_name'])      and $insert_array['corporate_name'] = self::$purchase_info['corporate_name'];
			isset(self::$purchase_info['corporate_name_kana']) and $insert_array['corporate_name_kana'] = self::$purchase_info['corporate_name_kana'];
			isset(self::$purchase_info['first_name'])          and $insert_array['first_name'] = self::$purchase_info['first_name'];
			isset(self::$purchase_info['last_name'])           and $insert_array['last_name'] = self::$purchase_info['last_name'];
			isset(self::$purchase_info['first_name_kana'])     and $insert_array['first_name_kana'] = self::$purchase_info['first_name_kana'];
			isset(self::$purchase_info['last_name_kana'])      and $insert_array['last_name_kana'] = self::$purchase_info['last_name_kana'];
			isset(self::$purchase_info['zip'])                 and $insert_array['zip'] = (String)self::$purchase_info['zip'];
			isset(self::$purchase_info['state_cd'])            and $insert_array['state_cd'] = (Int)self::$purchase_info['state_cd'];
			isset(self::$purchase_info['address1'])            and $insert_array['address1'] = self::$purchase_info['address1'];
			isset(self::$purchase_info['address2'])            and $insert_array['address2'] = self::$purchase_info['address2'];
			isset(self::$purchase_info['address3'])            and $insert_array['address3'] = self::$purchase_info['address3'];
			isset(self::$purchase_info['tel1'])                and $insert_array['tel1'] = self::$purchase_info['tel1'];
			isset(self::$purchase_info['tel2'])                and $insert_array['tel2'] = self::$purchase_info['tel2'];
			isset(self::$purchase_info['tel3'])                and $insert_array['tel3'] = self::$purchase_info['tel3'];
			isset(self::$purchase_info['fax1'])                and $insert_array['fax1'] = self::$purchase_info['fax1'];
			isset(self::$purchase_info['fax2'])                and $insert_array['fax2'] = self::$purchase_info['fax2'];
			isset(self::$purchase_info['fax3'])                and $insert_array['fax3'] = self::$purchase_info['fax3'];
			isset(self::$purchase_info['mail_purchase'])       and $insert_array['mail'] = self::$purchase_info['mail_purchase'];

			//area_cd
			$insert_array['area_cd'] = Model_Mt_Area::get_area_by_state((Int)self::$purchase_info['state_cd'], (String)self::$purchase_info['zip'])['id'];
			$query  = Model_Dt_Purchaseaddresshistory::get_query_insert($insert_array);
			$result = DbHelper::query_exec($query, false, false, 'master');
			//error
			if (!isset($result['insert_id']))
				self::error (array('dt_purchaseaddressのinsertに失敗しました うえ', self::DEFAULT_ERROR_MESSAGE));
		}

		//購入者とお届け先が違う時(お届け先)
		if (!(isset(self::$purchase_info['receiver']) && self::$purchase_info['receiver'] == 'own'))
		{
			$insert_array = array(
				'purchase_id'         => self::$purchase_id,
				'type_cd'             => 2,
			);

			isset(self::$receiver_info['corporate_name'])      and $insert_array['corporate_name'] = self::$receiver_info['corporate_name'];
			isset(self::$receiver_info['corporate_name_kana']) and $insert_array['corporate_name_kana'] = self::$receiver_info['corporate_name_kana'];
			isset(self::$receiver_info['first_name'])          and $insert_array['first_name'] = self::$receiver_info['first_name'];
			isset(self::$receiver_info['last_name'])           and $insert_array['last_name'] = self::$receiver_info['last_name'];
			isset(self::$receiver_info['first_name_kana'])     and $insert_array['first_name_kana'] = self::$receiver_info['first_name_kana'];
			isset(self::$receiver_info['last_name_kana'])      and $insert_array['last_name_kana'] = self::$receiver_info['last_name_kana'];
			isset(self::$receiver_info['zip'])                 and $insert_array['zip'] = (String)self::$receiver_info['zip'];
			isset(self::$receiver_info['state_cd'])            and $insert_array['state_cd'] = (Int)self::$receiver_info['state_cd'];
			isset(self::$receiver_info['address1'])            and $insert_array['address1'] = self::$receiver_info['address1'];
			isset(self::$receiver_info['address2'])            and $insert_array['address2'] = self::$receiver_info['address2'];
			isset(self::$receiver_info['address3'])            and $insert_array['address3'] = self::$receiver_info['address3'];
			isset(self::$receiver_info['tel1'])                and $insert_array['tel1'] = self::$receiver_info['tel1'];
			isset(self::$receiver_info['tel2'])                and $insert_array['tel2'] = self::$receiver_info['tel2'];
			isset(self::$receiver_info['tel3'])                and $insert_array['tel3'] = self::$receiver_info['tel3'];
			isset(self::$receiver_info['fax1'])                and $insert_array['fax1'] = self::$receiver_info['fax1'];
			isset(self::$receiver_info['fax2'])                and $insert_array['fax2'] = self::$receiver_info['fax2'];
			isset(self::$receiver_info['fax3'])                and $insert_array['fax3'] = self::$receiver_info['fax3'];
			isset(self::$receiver_info['mail_purchase'])       and $insert_array['mail'] = self::$receiver_info['mail_purchase'];

			//area_cd
			$insert_array['area_cd'] = Model_Mt_Area::get_area_by_state((Int)self::$purchase_info['state_cd'], (String)self::$purchase_info['zip'])['id'];
			$query  = Model_Dt_Purchaseaddresshistory::get_query_insert($insert_array);
			$result = DbHelper::query_exec($query, false, false, 'master');
			//error
			if (!isset($result['insert_id']))
				self::error (array('dt_purchaseaddressのinsertに失敗しました (した)', self::DEFAULT_ERROR_MESSAGE));
		}
	}

	/**
	 * insert_purchase_item_histories
	 * 
	 * dt_purchaseitemhistory にinsertします
	 * もし、特殊表示sessionが存在する場合は、該当するsession情報から
	 * dt_purchasespecialselectにもinsertする
	 */
	private static function insert_purchase_item_histories ()
	{
		//table名セット(エラー処理用)
		self::$insert_tables[] = Model_Dt_Purchaseitemhistory::$table_name;

		//cart item 種類分insertする
		for ($i =0; $i<count(self::$cart_dt); $i++)
		{
			$insert_array = array ();

			$insert_array['purchase_id'] = self::$purchase_id;
			$insert_array['item_id']     = (Int)self::$cart_dt[$i]['id'];
			$insert_array['count']       = (Int)self::$cart_dt[$i]['count'];
			$insert_array['price']       = (Int)self::$cart_dt[$i]['price'];
			$insert_array['point']       = isset(self::$cart_dt[$i]['point'])? (Int)self::$cart_dt[$i]['point']: 0;
			$insert_array['name']        = self::$cart_dt[$i]['name'];
			$insert_array['rate']        = (isset(self::$cart_dt[$i]['discount']['rate']) && !empty(self::$cart_dt[$i]['discount']['rate']))? self::$cart_dt[$i]['discount']['rate']: 0;
			$result = DbHelper::query_exec(
				Model_Dt_Purchaseitemhistory::get_query_insert($insert_array),
				false,
				false,
				Fuel::DB_MASTER
			);

			if (!isset($result['insert_id']))
			{
				//--エラー
				self::error(array('insert_purchase_item_historys insert失敗', self::DEFAULT_ERROR_MESSAGE));
			}
			else
			{
				self::$insert_purchase_item_results[] = array(
					'insert_id' => $result['insert_id'],
					'item_id' => (Int)self::$cart_dt[$i]['id'],
				);
			}
		}
	}

	/**
	 * insert_purchase_special_select
	 * 
	 * special_select_dt sessioを持っている商品については
	 * dt_purchasespecialselectへinsertします
	 */
	public static function insert_purchase_special_select ()
	{
		//特殊表示Sessionがからの場合は以下の処理をしない
		if (empty(self::$special_select_dt))
			return;

		$special_select_tmp = self::$special_select_dt;

		//-- dt_purchaseitemhistory
		for ($i=0; $i<count(self::$insert_purchase_item_results); $i++)
		{
			//-- special_select_dt
			foreach ($special_select_tmp as $key => $value)
			{
				if ((Int)self::$insert_purchase_item_results[$i]['item_id'] === (Int)$value['item_id'])
				{
					//special_select key value のマスタを取得する
					$mt_key = Model_Mt_Specialselectkey::select_by_id($value['key']);//name
					$mt_val = Model_Mt_Specialselectvalue::select_by_id($value['value']);//value

					/*
					 * ここでtableにinsert する
					 */
					$insert_array = array();
					$insert_array['history_id']  = self::$insert_purchase_item_results[$i]['insert_id'];
					$insert_array['purchase_id'] = self::$purchase_id;
					$insert_array['key_id']      = $value['key'];
					$insert_array['item_id']     = $value['item_id'];
					$insert_array['value_id']    = $value['value'];
					$insert_array['key']         = isset($mt_key['name'])?  $mt_key['name'] : '';
					$insert_array['value']       = isset($mt_val['value'])? $mt_val['value']: '';
					$insert_array['count']       = $value['count'];
					unset($special_select_tmp[$key]);

					$result = DbHelper::query_exec(
						Model_Dt_Purchasespecialselect::get_query_insert($insert_array),
						false,
						false,
						Fuel::DB_MASTER
					);

					//--エラー
					if (!isset($result['insert_id']))
					{
						self::error(array('insert_purchase_special_select insert失敗', self::DEFAULT_ERROR_MESSAGE));
						return;
					}
				}
			}
		}
	}

	/**
	 * insert_inital_purchase_user
	 * 
	 * dt_purchaseuserにインサートします
	 * insert_id を プロパティーにセットします
	 */
	private static function insert_inital_purchase_user ()
	{
		//table名セット(エラー処理用)
		self::$insert_tables[] = Model_Dt_Purchasehistory::$table_name;

		$insert_array = array(
			'payment_cd'     => self::$payment_cd,
			'sending_cd'     => self::$sending_cd,
			'tax_cd'         => self::$tax_cd,
			'point_rate_id'  => self::$point_rate_id,
			'total_point'    => self::$total_point,
			'use_point'      => self::$use_point,
		);

		//お届け先が自分か
		$insert_array['myself_flg'] = isset(self::$purchase_info['receiver']) && self::$purchase_info['receiver'] == 'own' ? 1: 0;

		//送料
		if (isset(self::$sending_info['is_free']) && self::$sending_info['is_free'])
		{
			//--- 無料
			$insert_array['sending_amount'] = 0;
		}
		else
		{
			//--- 通常
			for ($i=0; $i<count(self::$sending_info['postages']); $i++)
			{
				if ((Int)self::$sending_info['postages'][$i]['id'] === (Int)self::$sending_cd)
				{
					$insert_array['sending_amount'] = (Int)self::$sending_info['postages'][$i]['postage'];
				}
			}
		}

		//日にち指定
		if (isset(self::$purchase_info['date_specified']) && (Int)self::$purchase_info['date_specified'] != 0)
			$insert_array['date_specified'] = self::$purchase_info['date_specified'];

		//時間帯指定
		if (isset(self::$purchase_info['time_specified']) && (Int)self::$purchase_info['time_specified'] != 0)
			$insert_array['time_specified'] = (Int)self::$purchase_info['time_specified'];

		//自由欄その1
		if (isset(self::$purchase_info['free1']) && !empty(self::$purchase_info['free1']))
			$insert_array['free1'] = self::$purchase_info['free1'];

		//商品税込み総額
		$insert_array['item_amount'] = self::$cart_sum_info['total_amount'];

		//決済合計金額をプロパティーにセット
		self::$payment_amount = (Int)$insert_array['item_amount'] + (Int) $insert_array['sending_amount'] - $insert_array['use_point'];

		if ((Int)self::$payment_amount === 0)
		{
			//決済金額が0円の場合payment_statusに10をたてる
			$insert_array['payment_status'] = self::ZERO_YEN_PAYMENT;
			$insert_array['payment_date'] = date("Y-m-d H:i:s");
		}
		elseif (self::$payment_cd == Model_Mt_Payment::CARD_PAYMENT_ID)
		{
			//GMOの時は画面遷移中のstatus_cdをたてる
			$insert_array['status_cd'] = self::SCREEN_TRANSITION_GMO;
		}

		//uid and onetime_flg
		if (self::$login_info)
		{
			$insert_array['uid'] = (Int)self::$login_info['id'];
		}
		else
		{
			$insert_array['uid'] = self::NO_LOGIN_USERE_ID;
			$insert_array['onetime_flg'] = 1;
		}

		$query = Model_Dt_Purchasehistory::get_query_insert($insert_array);
		$result = DbHelper::query_exec($query, false, false, Fuel::DB_MASTER);

		if (isset($result['insert_id']) && is_numeric($result['insert_id']))
		{
			//--purchase_idをプロパティーにセット
			self::$purchase_id = $result['insert_id'];
			Session::set('purchase_info.purchase_id', $result['insert_id']);
		}
		else
		{
			//--エラー
			self::error(array('insert_inital_purchase_user 失敗', self::DEFAULT_ERROR_MESSAGE));
		}
	}

	/**
	 * roll_back_purchase
	 * 
	 * 購入情報を全て取り消します
	 * 
	 * 1:基本的にはdel_flgをたてる/在庫数を戻す
	 * 2:dt_purchasehistoryのstatus_cdに引数のstatusコードをたてる
	 */
	public static function roll_back_purchase ($status_cd)
	{
		$user_id = '';

		//purchase idのチェック
		if (!self::$purchase_id || empty(self::$purchase_id))
		{
			self::error(array('roll_back_purchase','プロパティーにpurchase idがセットされていません'));
			return;
		}

		//user_idを取得する
		$result = Model_Dt_Purchasehistory::select_by_id(self::$purchase_id);
		$user_id = (isset($result['uid']) && !empty($result['uid']))? $result['uid']: false;

		/*
		 * dt_purchasehistory
		 * del_flg
		 */
		$update_array = array(
			'del_flg' => 1,
			'status_cd' => $status_cd,
		);
		$result = Model_Dt_Purchasehistory::update_by_purchase_id($update_array, self::$purchase_id);
		unset($update_array);

		/*
		 * dt_stock
		 * dt_purchaseitemhistoryから購入予定だった商品の数を
		 * dt_stockに戻す
		 */
		//dt_purchaseitemhistoryデータを取得
		if ((Int)$result >0)
		{
			$item_data = Model_Dt_Purchaseitemhistory::select_by_purchase_id(self::$purchase_id, 0);
			if (count($item_data)>0)
			{
				$query = array();
				for($i=0; $i<count($item_data); $i++)
				{
					$query[] = 
						DB::query(
								'update `dt_stock` set `count`=`count`+'.$item_data[$i]['count'].', `updated`="'.date('Y-m-d H:i:s').'" where `item_id` ='. $item_data[$i]['item_id'] .' and `del_flg` = 0'
							);
				}
				DbHelper::query_exec($query, false, true, 'master');
				unset($query);
			}
		}

		/*
		 * dt_purchasespecialselect
		 * dt_specialselectstock
		 */
		$dt_purchasespecialselect = Model_Dt_Purchasespecialselect::select_by_purchase_id(self::$purchase_id);
		if (count($dt_purchasespecialselect)>0)
		{
			$dt_purchasespecialselect_ids = array();
			$query = array();
			for ($i=0; $i<count($dt_purchasespecialselect); $i++)
			{
				//dt_purchasespecialselectにまとめてdel_flgをたてる為,退避
				$dt_purchasespecialselect_ids[] = $dt_purchasespecialselect[$i]['id'];
				$query[] = DB::query('update `dt_specialselectstock` set `count`=`count`+'.$dt_purchasespecialselect[$i]['count'].',`updated`="'.date('Y-m-d H:i:s').'" where`id` ='. $dt_purchasespecialselect[$i]['value_id'] .' and `del_flg` = 0');
			}
			$query[] = Model_Dt_Purchasespecialselect::get_query_update_del_flg_by_id($dt_purchasespecialselect_ids);
			DbHelper::query_exec($query, false, false, 'master');
			unset($query);
		}

		/*
		 * dt_gmoresponse
		 * del_flg
		 */
		$update_array = array(
			'del_flg' => 1,
		);
		Model_Dt_Gmoresponse::update_by_purchase_id($update_array, self::$purchase_id);
		unset($update_array);

		/*
		 * ポイント処理
		 */
		if ($user_id)
		{
			//有効期限切れの処理を行う
			Point::set_user_id($user_id);
			/**
			 * pinthisoryにdel_flgをたてる
			 */
			Point::cancel_point_del_flg_by_purchase(self::$purchase_id);
			Point::get_point();
		}

		/*
		 * dt_purchaseaddresshistory
		 * del_flg
		 */
		$update_array = array(
			'del_flg' => 1,
		);
		Model_Dt_Purchaseaddresshistory::update_by_purchase_id($update_array, self::$purchase_id);
		unset($update_array);

		/*
		 * dt_purchaseitemhistory
		 * del_flg
		 */
		$update_array = array(
			'del_flg' => 1,
		);
		Model_Dt_Purchaseitemhistory::update_by_purchase_id($update_array, self::$purchase_id);
	}

	/**
	 * commit_gmo_purchase
	 * 
	 * gmo決済を確定させます
	 */
	public static function commit_gmo_purchase ()
	{
		self::$purchase_id = (Int)self::$gmo_response['order_number'];

		/*
		 * dt_gmoresponse
		 * gmoレスポンスのinsert
		 */
		$insert_array = array (
			'purchase_id' => self::$purchase_id,
			'trans_code'  => (Int)self::$gmo_response['trans_code'],
			'result'      => (Int)self::$gmo_response['result'],
		);
		$result = Model_Dt_Gmoresponse::insert($insert_array);

		if (!isset($result['insert_id']))
			self::error (array('gmo_response insert失敗',self::DEFAULT_ERROR_MESSAGE));

		/*
		 * dt_pointhistory
		 * avail_flg をたてる
		 */
		//購入時にinsertしたpoint hisotory を有効にする
//		$result = Point::commit_point(self::$purchase_id);
//		if ($result <=0)
//			self::error (array('gmo_response pointhisotry update失敗',self::DEFAULT_ERROR_MESSAGE));

		if (!self::$error_flg)
		{
			/*
			 * dt_purchasehistory
			 */
			unset($update_array);
			$update_array = array (
				'payment_status' => 1,
				'payment_date' => date("Y-m-d H:i:s"),
				'status_cd' => self::NEW_PURCHASE,
			);

			if (Model_Dt_Purchasehistory::update_by_purchase_id($update_array,self::$purchase_id) < 1)
				self::error (array('dt_purchasehistoryの決済フラグと決済dateの更新に失敗しました',self::DEFAULT_ERROR_MESSAGE));
		}
	}

	/**
	 * error
	 * 
	 * エラー処理メソッド
	 * 
	 * @param type $error_array 要素1は内部メッセージ/要素2はユーザー向けメッセージ
	 */
	public static function error ($error_array)
	{

		self::$error_flg     = 1;
		self::$error_message = $error_array[1];
		$error_array[] = __FILE__.'/['.__line__.']';
		Func::error_log($error_array, true);

//		Debug::dump($error_array);
//		Debug::dump(self::$insert_tables);
//		Debug::dump('---------失敗---------');
	}
}