<?php
/**
 * DbHelper class
 *
 * db周りの共通処理を行うクラスです
 *
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 * @extend     Model
 */
class DbHelper extends Model
{
	/**
	 * query_exec
	 *
	 * 	queryオブジェクトをexecuteするメソッドです
	 * 
	 * @access  public
	 * @param	array queryObject or queryObject
	 * @return  mysql execute result array
	 */
	 public static function query_exec ($query, $current = false ,$transaction = false, $db ='slave')
	 {
		try
		{
			$result = true;

			//transaction start
			if($transaction)
				DB::start_transaction($db);

			if (is_array($query))
			{
				$insert_result = null;
				$update_result = null;
				
				//---if array---
				foreach($query as $query_no => $execut_query)
				{
					//get type {insert|update|select}
					$type = explode(" ", $execut_query->__toString());
					//query execute
					if($type[0] != 'SELECT')
					{
						$execut_query = self::set_trigger($type[0], $execut_query);
						$exec_result = $execut_query->execute($db);
					}
					else
					{
						//sqlがselectの場合はrollbackさせます
						throw new Exception('複数例外no1');
					}

					//insertが成功した場合はinsert_idとrow_affectedを結果返却用配列に入れる
					if($type[0] == 'INSERT' && $exec_result[1])
					{	//insertが成功した場合はinsert_idとrow_affectedを結果返却用配列に入れる
						$insert_result[$query_no]['insert_id']     = $exec_result[0];
						$insert_result[$query_no]['rows_affected'] = $exec_result[1];
					}
					else if($type[0] == 'INSERT' && !$exec_result[1])
					{//insertに失敗した時
						throw new Exception('複数例外no3');
}
					else if (($type[0] == 'UPDATE' || $type[0] == 'update') && $exec_result > 0)
					{//update affectedrowを入れる
						$update_result[$query_no] = $exec_result;
					}
				}
				//成功したinssertがあった場合$insert_resultを返す
				if( !empty($insert_result) )
					$result = $insert_result;
			}
			else
			{
				//---if not array---
				//get type {insert|update|select}
				$type = explode(" ", $query->__toString());

				//-----insert|update時のtrigger method呼び出し-----
				if ($type[0] == 'INSERT' || $type[0] ==  'UPDATE')
					$query = self::set_trigger($type[0], $query);
				/* --------------Debug用--------------
				Debug::dump($query->__toString());exit;
				exit;
				 */
				$exec_result = $query->execute($db);

				//処理結果とsqltypeごとの処理
				// if( $type[0] == 'UPDATE' && !$exec_result )
				// {   //UPDATAに失敗した時
					// throw new Exception('個別例外no4');
				// }

				if (($type[0] == 'UPDATE' || $type[0] == 'update') && $exec_result>0)
				{
					$result = $exec_result;
				}
				else if ( $type[0] == 'INSERT' && $exec_result[1] )
				{	//insertが成功した場合はinsert_idとrow_affectedを結果返却用配列に入れる
					$insert_result['insert_id']     = $exec_result[0];
					$insert_result['rows_affected'] = $exec_result[1];
					//結果返却用変数に配列を代入
					$result = $insert_result;
				}
				else if( $type[0] == 'INSERT' && !$exec_result[1] )
				{//insertに失敗した時
					throw new Exception('複数例外no3');
				}
				else if ( $type[0] == 'SELECT' && $current && is_string($current))
				{
					//selectの場合かつcurrentの中身がカラム名が指定してあった時
					$result = $exec_result->as_array($current);
				}
				else if( $type[0] == 'SELECT' && $current )
				{//selectの場合かつcurrentの場合は　->current()を実行 
					$result = $exec_result->current();
				}
				else if( $type[0] == 'SELECT' && !$current )
				{//selectの場合かつcurrentではない場合
					$result = $exec_result->as_array();
				}
			}

			//例外が投げられなければコミット
			if($transaction)
				DB::commit_transaction($db);

			return $result;
		}
		catch(Exception $e)
		{
			//Debug::dump(DB::last_query());
			//Debug::dump($e);
			DB::rollback_transaction($db);
			Func::write_exception_log($e, true);
			return false;
		}
	}

	/**
	 * trigger_date
	 *
	 * query_execでupdate|insert query が投げられた時に
	 * updated|updatedとcreatedをセットします
	 * 
	 * @access  private
	 * @param   String 'UPDATE' | 'INSERT' , QueryObject
	 * @return  QueryObject
	 */
	private static function set_trigger ($type, $query)
	{
		$date = date('Y-m-d H:i:s'); //created updated用

		if ($type == 'INSERT')
		{
			$query->set_trigger(array('updated' => $date, 'created' => $date));
		}
		else if ($type == 'UPDATE')
		{
			$query->set(array('updated' => $date));
		}

		return $query;
	}

	/**
	 * array_filter
	 * 
	 * name_arrayに入っている要素のみの配列にして返却します。
	 * 
	 * @param  $name_array $data
	 * @return $result
	 */
	public static function array_filter ($name_array, $data)
	{
		$result = array();

		for ($i=0; $i<count($name_array); $i++)
		{
			$result[$name_array[$i]] = (isset($data[$name_array[$i]]))?$data[$name_array[$i]]:'';
		}
		return $result;
	}
}