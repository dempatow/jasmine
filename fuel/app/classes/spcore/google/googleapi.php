<?php
/**
 * the GoogleAPI class
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P advertising Co.,Ltd.
 * @link       http://sp-k.co.jp
 */

class GoogleAPI
{
	/**
	 * Google config defaults.
	 */
	protected static $_defaults;
	private static   $sdk_instance = null;

	/**
	 * コンストラクタ
	 */
	public static function _init()
	{
		//config読み込み
		Config::load('google', true);
		//memberにconfigをセットする
		static::$_defaults = \Config::get('google');
	}

	/**
	 * 
	 * 
	 */
	public static function test ()
	{
		$baseURL = self::$_defaults['api_auth_url'].'auth?';
		$scope = array(
			'https://www.googleapis.com/auth/userinfo.profile', // 基本情報(名前とか画像とか)
			'https://www.googleapis.com/auth/userinfo.email',   // メールアドレス
			);

		// 認証用URL生成
		$authURL = $baseURL . 'scope=' . urlencode(implode(' ', $scope))
			.'&redirect_uri=' . urlencode(self::$_defaults['callback_url'])
			.'&response_type=code'
			.'&client_id=' . self::$_defaults['client_id'];
		//Redirect
		Response::redirect($authURL);
	}
	
	/**
	 * 
	 * 
	 */
	public static function call_back ()
	{
		$code = $_REQUEST['code'];

		// access_token 取得
		$baseURL = self::$_defaults['api_auth_url'].'token';
		$params = array(
			'code'          => $code,
			'client_id'     => self::$_defaults['client_id'],
			'client_secret' => self::$_defaults['client_secret'],
			'redirect_uri'  => self::$_defaults['callback_url'],
			'grant_type'    => 'authorization_code'
		);

		$req = new HttpRequest($baseURL, HttpRequest::METH_POST);
		$req->setPostFields($params);
		$req->send();
		$response = json_decode($req->getResponseBody());

		if(isset($response->error)){
			// getCodeへ戻す
			echo 'エラー発生。<a href="getCode.php">最初からやりなおす</a>';
			exit;
		}
		$access_token = $response->access_token;

		// ユーザ情報取得
		$userInfo = json_decode(
			file_get_contents(self::$_defaults['api_get_data_url'].'v1/userinfo?'.
			'access_token=' . $access_token)
		);

		// google の id + name(表示名)をセット
		Debug::dump($userInfo);
//		$user_id = $userInfo->id;
//		$name    = $userInfo->name;
	}
}