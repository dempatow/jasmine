<?php
/**
 * Point class
 *
 * Point クラス
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */
class Point
{
	//--- 加減算理由
	const POINT_ADD_PURCHASE        = 1;//商品購入付与ポイント
	const POINT_SUBTRACT_PURCHASE   = 2;//商品購入によるポイント使用
	const POINT_SUBTRACT_EXPIRATION = 3;//有効期限切れ
	const POINT_SUBTRACT_CANCEL     = 4;//キャンセル
	const POINT_ADD_SYSTEM          = 5;//システムユーザーによる付与
	const POINT_SUBTRACT_SYSTEM     = 6;//システムユーザーによる減算
	//エラー定数
	const NO_LOGIN_USER = '非ログインユーザーです';

	private static $cart_dt        = '';//cart情報
	private static $user_id        = '';//ログインユーザーid
	private static $execute_status = true;
	private static $_config        = '';

	/**
	 * コンストラクタ
	 */
	public static function _init()
	{
		$user_info = CustomAuth::get_login_info();
		self::$user_id = isset($user_info['id'])? $user_info['id']: false;
		//設定ファイル読み込み
		config::load('point_config',true);
		self::$_config = Config::get('point_config');
	}

	/**
	 * set_user_id
	 * 
	 * user_idをプロパティーにセットします
	 * (通常はconstractorでsessionから直接取得するので使用しない)
	 * 
	 * @parma user_id
	 */
	public static function set_user_id ($user_id)
	{
		self::$user_id = $user_id;
	}

	/**
	 * get_point
	 * 現在のポイントを取得します
	 * 有効期限切れのポイントがあった場合は各種レコードを更新します
	 * 
	 * @param is_update_only default = false 有効期限切れポイントの更新処理のみ行う場合
	 */
	public static function get_point ()
	{
		if (!self::$user_id)
			self::error (array(self::NO_LOGIN_USER, 'get point'));

		//現在のポイント
		$result = Model_Dt_Pointhistory::select_current_point_by_uid(self::$user_id);

		if ($result < 0)
			Func::error_log ('合計ポイントがマイナスのユーザーがいます。確認してください uid はこちら => '.(self::$user_id), true);

		//有効期限確認
		$expiration_result = self::check_expiration();

		/*
		 * 有効期限が切れていた場合はポイント保有ポイント分をマイナスポイントとして加算する
		 */
		if (!$expiration_result)
		{
			if ($result > 0)
			{
				//マイナスポイント加算
				Point::add_point(-$result, NULL, self::POINT_SUBTRACT_EXPIRATION, 1);
			}
			return 0;
		}

		return $result;
	}

	/**
	 * ポイント加算メソッド
	 * 
	 * @param type $name Description $point
	 * @param type $name Description $purchase_id
	 * @param type $name Description $type_cd
	 * @param type $name Description $avail_flg
	 * @param type $name Description $uid (基本的には入れない system userの場合使う)
	 * 
	 */
	public static function add_point($point, $purchase_id = null, $type_cd = 0, $avail_flg = 0, $uid = null)
	{
		$insert_array = array();
		if (!is_null($uid))
		{
			//uidが指定してあった場合(system userを想定)
			$insert_array['uid'] = $uid;
		}
		else if (!is_null(self::$user_id) || !empty(self::$user_id))
		{
			//session idを使用
			$insert_array['uid'] = self::$user_id;
		}
		else
		{
			Func::error_log('uid指定がないポイント追加がありました', true);
		}

		!is_null($purchase_id) and $insert_array['purchase_id'] = $purchase_id;
		$insert_array['point']     = $point;
		$insert_array['type_cd']   = $type_cd;
		$insert_array['avail_flg'] = $avail_flg;

		$query  = Model_Dt_Pointhistory::get_query_insert($insert_array);
		$result = DbHelper::query_exec($query, false, false, 'master');

		if (!$result || !isset($result['insert_id']))
			Func::error_log ('pointの追加に失敗しました'.$query->__toString(), true);
	}
/**
	 * check_expiration
	 * ユーザーが保有しているポイントの有効期限をチェックします
	 * 期限切れている場合は現時点で保有しているポイントの合計数を
	 * マイナスポイントとしてaddする
 */

	/**
	 * check_expiration
	 * 
	 * 有効期限チェックメソッド
	 * 
	 * dt_user point_staredから設定期間分の期間が
	 * 経過したかどうか確認する
	 * 
	 * dt_userにpoint_staredが設定されていない場合はtrueを返却する
	 */
	public static function check_expiration ()
	{
		$result = true;
		$start_date = Model_Dt_User::select_point_started_by_uid(self::$user_id);

		if (!$start_date || is_null($start_date) || empty($start_date))
		{
			return $result;
		}

		$now   = date('Y-m-d H:i:s');
		$limit = date('Y-m-d H:i:s', strtotime($start_date." +".self::$_config['expiration']." month"));

		if ($now > $limit)
			$result = false;

		return $result;
	}

	/**
	 * add_point
	 * 
	 * 確定ポイントを加減算する
	 * 
	 * @param point
	 * @param reason_code (このクラスの定数を渡す)
	 * @param purchase_id default = null
	 * 
	 */
/**
	public static function add_point($point, $type_code , $purchase_id = null)
	{
		if (!self::$user_id)
			self::error (array(self::NO_LOGIN_USER, 'add point'));
 
		//--- dt_pointhistoryへのinsert
		$insert_array = array(
			'uid'         => self::$user_id,
			'type_cd'     => $type_code,
			'point'       => $point,
			'avail_flg'   => 1,
		);

		!is_null($purchase_id) and $insert_array['purchase_id'] = $purchase_id;

		$query  = Model_Dt_Pointhistory::get_query_insert($insert_array);
		$result = Dbhelper::query_exec($query, false, false, 'master');

		if (!$result['insert_id'])
			self::error('add_point dt_pointhistory へのinsertに失敗しました');

		return self::$execute_status;
	}
 */


	/**
	 * commit_point
	 * 未確定ポイントを確定させます
	 */
	public static function commit_point ($purchase_id)
	{
		$update_array = array (
			'avail_flg' => 1,
		);
		return Model_Dt_Pointhistory::update_by_purchase_id($update_array, $purchase_id, 0, 0);
	}

	/**
	 * cancel_point_by_purchase
	 * 
	 * ポイントをキャンセル(減算)します
	 */
	public static function cancel_point_by_purchase ($purchase_id)
	{
		//pointhistoryの取得
		$data = Model_Dt_Pointhistory::select_by_purchase_id($purchase_id, null, 0);
		for ($i=0; $i<count($data); $i++)
		{
			if ((Int)$data[$i]['avail_flg'] === 0)
			{
				//--- 未確定ポイントの場合 del_flgをたてる
				$update_array = array(
					'del_flg' => 1,
				);
				Model_Dt_Pointhistory::update_by_purchase_id($update_array, $purchase_id);
			}
			else if ((Int)$data[$i]['avail_flg'] === 1)
			{
				//--- 確定ポイントの場合 減算用のレコードを挿入する
				$reason_cd = ((-$data[$i]['point']) < 0)? self::POINT_SUBTRACT_CANCEL: self::POINT_ADD_SYSTEM;
				self::add_point(-($data[$i]['point']), $reason_cd, $purchase_id);
			}
		}
	}

	/**
	 * cancel_point_del_flg_by_purchase
	 * 
	 * ポイントをキャンセルします (全てdel_flg処理)
	 */
	public static function cancel_point_del_flg_by_purchase ($purchase_id)
	{
		//pointhistoryの取得
		$data = Model_Dt_Pointhistory::select_by_purchase_id($purchase_id, null, 0);
		for ($i=0; $i<count($data); $i++)
		{
			$update_array = array(
				'del_flg' => 1,
			);
			Model_Dt_Pointhistory::update_by_purchase_id($update_array, $purchase_id);
		}
	}

	/**
	 * error
	 * 
	 * エラー処理メソッド
	 * 
	 * @param type array or string
	 */
	public static function error ($error_message)
	{
		self::$execute_status = false;
		//TODO : メール実装する!!
		Func::error_log($error_message, true);
		Debug::dump('---------失敗---------');
	}
}