<?php
/**
 * ViewHelper class
 *
 * view helper クラスです
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */
class ViewHelper
{

	/**
	 * コンストラクタ
	 */
	public static function _init()
	{

	}

	/*
	 * 
	 * side_menu
	 * 
	 */
	
	/**
	 * get_chara_data
	 * 
	 * キャラクターから選ぶ表示用
	 * 
	 * @return mix array
	 */
	public static function get_chara_data ()
	{
		$result = '';
		$result = Model_Mt_Character::select();
		return $result;
	}

	/**
	 * get_side_menu_category_data
	 * 
	 * side_menuのカテゴリーから選ぶ表示用
	 * 
	 * @return mix array
	 */
	public static function get_side_menu_category_data()
	{
		//親子関係ありのカテゴリを取得

		//大カテゴリと親子関係なしカテゴリを取得
		$result_rec_0 = Model_Mt_Category::select_category_by_recursive_no(0);
		$result_rec_1 = Model_Mt_Category::select_category_by_recursive_no(1);
		//2つの配列をマージして返却する

		if ($result_rec_0 && $result_rec_1)
		{
			$result = array_merge($result_rec_1,$result_rec_0);
		}
		else if ($result_rec_1)
		{
			$result = $result_rec_1;
		}
		else
		{
			$result = $result_rec_0;
		}
		return $result;
	}
}