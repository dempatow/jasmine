<?php
/**
 * SiteMapHelper class
 *
 * SiteMapHelperクラスです
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */
class SiteMapHelper
{
	public static function get_site_map_xml ($urls)
	{
		$dom = new DOMDocument('1.0', 'UTF-8');
		$dom->preserveWhiteSpace = false;
		$dom->formatOutput = true;

		$urlset = $dom->createElement('urlset');
		$urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

		foreach($urls as $target_url) {

			$url = $dom->createElement('url');

			//loc
			$url->appendChild($dom->createElement('loc', $target_url));

			//changefreq
			$url->appendChild($dom->createElement('changefreq', 'weekly'));

			//priority
			if ($target_url === \Config::get('custom_config.url.http_domain'))
			{
				$url->appendChild($dom->createElement('priority', '1.0'));
			}
			else
			{
				$url->appendChild($dom->createElement('priority', '0.5'));
			}

			$urlset->appendChild($url);
		}

		$dom->appendChild($urlset);

		return $dom->saveXML();
	}
}