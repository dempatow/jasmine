<?php
/**
 * Common controller
 *
 * 全controller共通で継承するcontrollerです
 *
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 * @extends    Controller_Hybrid
 */
class Common extends Controller_Hybrid
{
	//-----画面に渡す変数-----
	public $assets_path_array = null;

	//-----request情報-----
	public $request_controller = null;
	public $request_method     = null;
	public $request_protocol   = null;

	//-----その他-----
	public $_config     = null; //custom_config_array
	public $assets_path = null;
	public $is_ssl      = false;
	public $base_url    = null;
	public $is_login    = false;
	public $uid         = null;

	const AVAIL_PURCASE_TERM = 900; //purchase系セッション保持時間

	/*
	 * constructor
	 */
	public function before ()
	{
		//request情報をメンバーにセットする
		self::set_prop_request();

		//各種config fileをloadする
		self::load_config();

		//device_cdをセットする
		//self::$device_cd = Device::$code;

		//debug表示をdefaultで展開する
		Debug::$js_toggle_open = true;

		if (Input::is_ajax() || Input::is_api())
		{
			/********************************
			 *
			 *      Rest通信 初期処理
			 *
			 ********************************/
			if (Input::protocol() === 'https')
				$this->is_ssl = true;

			//memberにassets_path_arrayをセットする
			self::set_view_value();

			//Hybrid_controllerのbefore呼び出し
			parent::before();
		}
		else
		{
			/********************************
			 *
			 *      default 初期処理
			 *
			 ********************************/

			//Hybrid_controllerのbefore呼び出し
			parent::before();

			$this->template->overlay = View::forge('layout/overlay');

			//Viewにassets pathをset
			$this->template->set_global($this->assets_path_array);

			//View titleのset
			self::set_view_title();

			//View keywordのset
			self::set_view_keywords();

			//View descriptionのset
			self::set_view_description();

			//ログイン失敗時のerrorメッセージと入力値表示処理
			self::set_login_login_parts();

			//会員向けメッセージを取得する
			//self::get_message ();

			/**
			 * check系
			 */
			//controller/methodの存在check
			self::check404();

			//protocolのcheck及びredirect
			self::check_protocol();

			//memberにassets_path_arrayをセットする
			self::set_view_value();

			/*
			 * auth系
			 */
			//自動ログアウト確認method
			CustomAuth::check_access_term();

			//アクセス権限のチェック
			self::access_check();

			//CustomAuth::check_access_auth($this->request_controller, $this->request_method ,Config::get('auth_config.role.general'));

			//購入関係のsessionを確認する
			self::check_purchase_sessions();

/*********
 * 開発中
 *********/
//Debug::dump($ids);

/**
id
uid
item_id
count

purchase_id
del_flg
created
updated
 */

//Debug::dump(Session::get());




		}
	}

	/**
	 * check_purchase_sessions
	 * 
	 * 購入関連のsessionをチェックします
	 */
	private function check_purchase_sessions ()
	{
		$session_key = 'purcahse_start';
		$started = Session::get($session_key, false);

		//購入コントローラーでない場合 || 有効期限が超えている場合
		if (
			!($this->request_controller === 'Controller_User_Purchase' || $this->request_controller === 'Controller_Item') ||
			($started && ((time() - $started) > self::AVAIL_PURCASE_TERM))
		)
		{
			self::delete_purcahse_sessions(true);
		}
		else
		{
			//新timestampをセットする
			Session::set($session_key, time());
		}
	}

	/**
	 * delete_purcahse_sessions
	 * 
	 * 購入関連のsessionを削除します
	 */
	public function delete_purcahse_sessions ($not_cart = false)
	{
		Session::delete('purcahse_start');      //購入手続き開始日
		Session::delete('purchase_info');       //購入者情報
		Session::delete('receiver_info');       //お届け先情報
		Session::delete('sending_cd');          //発送コード
		Session::delete('sending_info');        //発送情報
		Session::delete('point_rate_info');     //ポイントレート情報

		if (!$not_cart)
		{
			Session::delete('cart_dt');
			Session::delete('special_select_dt');
			Session::delete('reserve_dt');
		}
	}

	/**
	 * TODO:使ってない!!
	 * get_message
	 * 
	 * 会員向けメッセージを取得する
	 */
	private function get_message ()
	{
		$result      = false;
		$login       = Session::get('login_info', false);
		$message     = Model_Mt_Message::select_message();
		$message_cnt = count($message);

		if ($login && count($message)>0)
		{
			$uid = $login['id'];

			//-- マスターにある既読message_idを取得する
			$readed_ids = Model_Dt_Readmessage::select_by_uid($uid, $message);

			for ($i=0; $i<$message_cnt; $i++)
			{
				for ($j=0; $j<count($readed_ids); $j++)
				{
					if (isset($message[$i]) && (Int)$message[$i]['id'] === (Int)$readed_ids[$j]['message_id'])
					{
						//既読
						unset($message[$i]);
					}
				}
			}

			//-- 未読のメッセージを既読にしreturnする
			$unread_message_cnt = count($message);
			if ($unread_message_cnt>0)
			{
				//既読メッセージがある場合
				for ($i=0; $i<$unread_message_cnt; $i++)
				{
					Model_Dt_Readmessage::insert_dt($uid, $message[$i]['id']);
				}
				$result = $message;
			}
		}
		return $result;
	}

	/**
	 * set_login_login_parts
	 * 
	 * ログイン失敗時のerrorメッセージと入力値表示処理
	 * set globalします
	 */
	private function set_login_login_parts()
	{
		$error = Session::get('login_parts', false);
		$auto_login = Session::get('login_info.auto_login',false);

		if ($error)
		{
			$this->template->set_global('login_parts', $error);
			Session::delete('login_parts');
		}
		else if ($auto_login && $auto_login == 1)
		{
			$this->template->set_global('login_parts', array('auto_login' => 1));
		}
	}

	/**
	 * tempate_device_filter
	 * 
	 * pc以外のtemplate 及びviewを設定します
	 * 
	 * @param device_code
	 */
	public function create_device_view ($template, $layout_array = null, $device_code = 1)
	{
		//templateの設定
		$this->template = $template;

		//Rest before呼び出し
		parent::before();
		//layout(footerやheaderがあればセットする)
		if (!is_null($layout_array))
		{
			foreach ($layout_array as $key => $value)
			{
				$this->template->$key = View::forge($value);
			}
		}
	}

	/**
	 * tempate_device_filter
	 * 
	 * spからのアクセスの場合は
	 * templateファイル名の最後に'_sp'を付加します。
	 * 
	 * @param device_code
	 */
	private function template_device_filter ($device_code)
	{
		if($device_code == Config::get('custom_config.device.device_code.sp'))
			$this->template.='_sp';
	}

	/**
	 * load_config
	 *
	 * @access  private
	 */
	private function load_config ()
	{
		//独自設定file load
		Config::load('custom_config', true);
		$this->_config = Config::get('custom_config');

		//SEO設定ファイル読み込み
		Config::load('seo_config', true);

		//ポイントconfig
		config::load('point_config',true);

		//共通message設定file load
		Lang::load('common_message');
	}

	/**
	 * access_check method
	 * 
	 * @access  private
	 */
	private function access_check ()
	{
		$role = Session::get('login_info.role_cd',0);

		//debug用
		//Debug::dump(CustomAuth::check_access_auth($this->request_controller, $this->request_method, $role));exit;

		if(!CustomAuth::check_access_auth($this->request_controller, $this->request_method, $role))
				Func::show_overlay_message('アクセスの権限がありません。', $this->base_url);
	}

	/**
	 * set_prop_request
	 *
	 * @access  private
	 */
	private function set_prop_request ()
	{
		//request controller
		$this->request_controller = Request::main()->active()->controller;
		//method
		$this->request_method = Request::main()->active()->action;
		//protocol
		$this->request_protocol = Input::protocol();
	}

	/**
	 * set_view_title
	 *
	 * @access  public
	 * @param   $title
	 */
	public function set_view_title ($title = null)
	{
		if (empty($title))
		{
			$default_titile = Config::get('seo_config.title.default');

			$set_title = Config::get(
										'seo_config.title.'.$this->request_controller.'.'.$this->request_method,
										$default_titile
							);
		}
		else
		{
			$set_title = $title;
		}
		$this->template->set_global('title', $set_title);
	}

	/**
	 * set_view_keywords
	 *
	 * @access  public
	 * @param   $keywords
	 */
	public function set_view_keywords ($keywords = null)
	{
		if (empty($keywords))
		{
			$default_keywords = Config::get('seo_config.keywords.default');
			$set_keywords = Config::get(
											'seo_config.keywords.'.$this->request_controller.'.'.$this->request_method,
											$default_keywords
								);
		}
		else
		{
			$set_keywords = $keywords;
		}
		$this->template->set_global('keywords', $set_keywords);
	}

	/**
	 * set_view_description
	 *
	 * @access  public
	 * @param   $description
	 */
	public function set_view_description ($description = null)
	{
		if (empty($description))
		{
			$default_description = Config::get('seo_config.description.default');
			$set_description = Config::get(
												'seo_config.description.'.$this->request_controller.'.'.$this->request_method,
												$default_description
									);
		}
		else
		{
			$set_description = $description;
		}
		$this->template->set_global('description', $set_description);
	}

	/**
	 * set_prop_pankuzu
	 *
	 * @access  public
	 * @return  img path
	 */
	public function set_prop_pankuzu ($pankuzu_array)
	{
		foreach ($pankuzu_array as $path => $name)
		{
			$this->pankuzu[$path] = $name;
		}
		$this->template->set_global('pankuzu', $this->pankuzu);
	}

	/**
	 * check404
	 *
	 * @access  public
	 */
	public function check404 ()
	{
		$result = true;
		//リクエスト先コントローラにメソッドが存在しない場合は404ページに遷移させる
		if(!method_exists($this->request_controller, 'action_'.$this->request_method))
		{
			throw new HttpNotFoundException;
			$result = false;
		}
		return true;
	}

	/**
	 * check_ssl
	 *
	 * @access  public
	 */
	public function check_protocol ()
	{
		//request controllerとmethod名を取得する
		$req_controller = $this->request_controller;
		$req_method = $this->request_method;
		//request protocolの取得
		$input_protocol = $this->request_protocol;
		//protocol初期値
		$config_protocol = 'http';

		if ($input_protocol === 'https')
		{
			Func::$is_ssl = $this->is_ssl = true;
			return;
		}

		//request controller/methodとcustom_configに記載されているものを比較する
		foreach (Config::get('custom_config.ssl.force') as $controller => $method) 
		{
			//controller chekck
			if ($controller == $req_controller)
			{
				//method 全指定
				if ($method == '*')
				{
					$config_protocol = 'https';
				}
				else
				{//method 個別指定
					for ($i=0; $i < count($method); $i++)
					{
						if($method[$i] == $req_method)
						{
							$config_protocol = 'https';
							break;
						}
					}
				}
				if ($config_protocol == 'https')
					break;
			}
		}

		//configがhttpsではないがログイン中であった場合
		if ($config_protocol != "https" && CustomAuth::get_login_info())
		{
			$config_protocol="https";
		}

		//configのプロトコルがsslになっていた場合またはログイン中の場合
		//assethpathをhttpsを付加して上書きする
		if($config_protocol=="https")
			Func::$is_ssl = $this->is_ssl = true;

		//requestのprotocolと設定ファイルのprotocolが違う場合はredirectさせる
		if (($config_protocol != $input_protocol))
		{
			//リクエストがitemページの場合
			if ((String)$req_controller === 'Controller_Item')
			{
				$redirect_url = Config::get('custom_config.url.'.$config_protocol.'_domain').Fuel\Core\Input::uri();
			}
			else
			{
				$redirect_url = Config::get('custom_config.url.'.$config_protocol.'_domain').'/'.implode('/', Uri::segments());
			}
			Response::redirect($redirect_url);
		}
	}

	/**
	 * sslのassetspahtをセットする
	 */
	public function set_view_value ()
	{
		if ($this->is_ssl)
		{
			$this->assets_path = Config::get('custom_config.url.ssl_assets_url');
			$this->base_url = Config::get('custom_config.url.https_domain');
			if (!Input::is_ajax() && !Input::is_api())
			{
				$this->template->set_global('is_ssl',true);
			}
			else
			{
				View::set_global('is_ssl',true);
			}
		}
		else
		{
			$this->assets_path = Config::get('custom_config.url.assets_url');
			$this->base_url = Config::get('custom_config.url.http_domain');
		}

		/************
		 * user info
		 ************/
		$user_info = CustomAuth::get_login_info();
		$this->is_login = ($user_info)? true: false;
		if ($this->is_login && isset($user_info['id']))
			$this->uid = (Int)$user_info['id'];

		$this->assets_path_array = array('assets_path' => $this->assets_path);

		if (!Input::is_ajax() && !Input::is_api())
		{
			$this->template->set_global($this->assets_path_array);
			$this->template->set_global('is_login', $this->is_login);
			$this->template->set_global('base_url', $this->base_url);

//			//ログイン中の場合
//			if (CustomAuth::get_login_info())
//			{
//				//ポイント
//				$this->template->set_global('point', Point::get_point());
//			}

			//overlay メッセージの表示
			$overlay_message = Session::get_flash('overlay_message', false);
			if ($overlay_message)
				$this->template->set_global('overlay_message',$overlay_message, false);

			/****************************
			 * over lay 表示のログイン表示
			 ****************************/
			if (Session::get_flash('show_overlaylogin', false))
				$this->template->set_global('overlay_login', true, false);

			/****************************
			 * reserve dataをviewにセットする
			 ****************************/
/**
			$no_login_reserve = Session::get_flash('reserve_no_login',false);
			if ($no_login_reserve)
				$this->template->set_global('no_login_reserve', $no_login_reserve, false);
 */
		}
		else
		{
			View::set_global($this->assets_path_array);
			View::set_global('is_login', $this->is_login);
			View::set_global('base_url', $this->base_url);
		}
	}
}