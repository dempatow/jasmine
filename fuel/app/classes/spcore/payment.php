<?php
/**
 * Payment class
 *
 * 決済クラス
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @package    app
 */
class Payment
{
	public static $_config = array();
	public static $PROSESS_FIRST = 1;            //1=>初回課金
	public static $PROSESS_REGISTERED = 2;       //2=>登録済み課金
	public static $PROSESS_REGISTERED_CHANGE = 4;//4=>登録内容変更

	/**
	 * コンストラクタ
	 */
	public static function _init()
	{
		//設定ファイル読み込み
		config::load('payment_config',true);
		self::$_config = Config::get('payment_config');
		//プロセスコードをクラス変数にセットする
		self::$PROSESS_FIRST             = self::$_config['PROSESS_FIRST'];            //初回課金
		self::$PROSESS_REGISTERED        = self::$_config['PROSESS_REGISTERED'];       //登録済み課金
		self::$PROSESS_REGISTERED_CHANGE = self::$_config['PROSESS_REGISTERED_CHANGE'];//登録内容変更
	}

	public static function get_config ()
	{
		return self::$_config;
	}

	/**
	 * get_redirect_url
	 * ユーザーにridirectさせるurlを取得します
	 * 
	 * @param $user_name
	 * @param $user_mail_add
	 * @param $user_id
	 * @param $price
	 * @param $process_code
	 * @param $order_number
	 * 
	 * @return redirect url
	 */
	public static function get_redirect_url ($user_name, $user_mail_add, $user_id, $price, $process_code, $order_number)
//	public static function get_redirect_url ($args_array)
	{
		//post値
		$post_array = array(
			//設定ファイルから設定
			'contract_code' => self::$_config['CONTRACT_CODE'],   //契約者コード
			'item_name'     => Config::get('custom_config.shop_name').self::$_config['ITEM_NAME'],       //商品名
			'st_code'       => self::$_config['ST_CODE'],         //決済区分(カード固定)
			'mission_code'  => self::$_config['MISSION_CODE'],    //課金区分(1回課金固定)
			'xml'           => self::$_config['RESPONSE_FORMAT'], //応答形式(1固定)
			'item_code'     => self::$_config['ITME_CODE'],       //商品番号
			//引数から設定
			'user_id'       => $user_id,                          //ユーザーid
			'user_name'     => $user_name,                        //ユーザー名
			'user_mail_add' => $user_mail_add,                    //ユーザーメール
			'order_number'  => $order_number,                     //オーダー番号(ユニーク)
			'item_price'    => $price,                            //決済総額
			'process_code'  => $process_code,                     //処理区分(1=>初回課金,2=>登録済み課金,4=>登録内容変更)
			'memo1'         => $order_number,                     //オーダー番号(エラーコールバックメソッドで使用する為)
		);

		//sjis に変換する
		$post_array = self::decode_and_change_char_encording($post_array, 1);
		//gmoにリクエストを投げる
		return $response = self::post_request(self::$_config['TARGET_URL'], $post_array);
	}

	/**
	 * decode_and_change_char_encording
	 * 
	 * urlでコードと文字コードを変換するメソッドです
	 * gmo からのレスポンスデータ
	 * 
	 * @param  param array
	 * @param  change code 
	 *         1:utf8 => sjis-win 2:sjis-win => utf8
	 * @param  is_decode default false
	 * 
	 * @return change char encording array
	 * 
	 */
	private static function decode_and_change_char_encording ($target_array, $change_code, $is_decode = false)
	{
		$char_set_array = array(
			1 => array('to' => 'SJIS-win', 'from' => 'UTF-8'),
			2 => array('to' => 'UTF-8', 'from' =>'SJIS-win')
		);
		$result = array();

		foreach ($target_array as $key => $val)
		{
			//url decode
			$decoded = ($is_decode)? urldecode($val): $val;
			//change char encording
			$converted    = mb_convert_encoding(
					$decoded,
					$char_set_array[$change_code]['to'],
					$char_set_array[$change_code]['from']
			);

			//set result
			$result[$key] = $converted;
		}
		return $result;
	}


	/**
	 * post_request
	 * 
	 * postリクエストを投げるクラスです
	 * 
	 * @param target_url
	 * @param post data
	 * 
	 * @return redirect url
	 */
	public static function post_request ($target_url, $post_data)
	{
		$result = false;

		//curlインスタンス作成
		$curl = \Fuel\Core\Request::forge($target_url,'curl');
		//method にpostを設定
		$curl->set_method('post');
		//postデータを設定
		$curl->set_params($post_data);

		try
		{
			//レスポンス取得
			$response_body = $curl->execute()->response()->body();

			//成功=>1/失敗=>0

			for ($i=0; $i<count($response_body['result']); $i++)
			{
				foreach ($response_body['result'][$i] as $res_data)
				{
					foreach ($res_data as $res_key => $res_data)
					{
						//url decode
						$decoded = urldecode($res_data);
						//change char encording
						$converted    = mb_convert_encoding(
								$decoded,
								'UTF-8',
								'SJIS-win'
						);
						$result[$res_key] = $converted;
					}
				}
			}
		}
		catch (Exception $e)
		{
			$result = false;
			Debug::dump($e);
			Func::write_exception_log($e);
		}
		return $result;
	}
}