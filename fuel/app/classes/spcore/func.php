<?php
/**
 * Func class
 *
 * 共通で使用するメソッドを集めたクラスです
 * 
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 * @extends  Validatio
 * @package  app
 */
class Func
{
	/**
	 * commonの中でssl判定しsslの場合はtrueが入る
	 */
	static public $is_ssl = false;

	/**
	 * show_overlay_message
	 *
	 * @param type $message 表示させるメッセージ
	 * @param type $url リダイレクト先 default = https top
	 */
	static public function show_overlay_message ($message, $url = null)
	{
		Session::set_flash('overlay_message', $message);
		$redirect_url = (is_null($url))? Config::get('custom_config.url.http_domain'): $url;
		Response::redirect($redirect_url);
	}

	/**
	 * get_mail_subject
	 * 
	 * @parma string $subject
	 */
	static public function get_mail_subject($subject)
	{
		return $subject.'【'.Config::get('custom_config.shop_name').'】';
	}

	/**
	 * get_total_point
	 * 
	 * @param cart_dt
	 * @param point_rate
	 * 
	 * @return total_point
	 */
	static public function get_cart_total_info ($cart_dt)
	{

		//送料無料金額マスタ
		$mt_free_sending = Model_Mt_Freesending::select_freesending();
		//合計獲得金額
		$total_price = self::get_cart_info_sum($cart_dt)['total_amount'];

		//送料
		if ((count($cart_dt) === 1 && isset($cart_dt[0]['free_sending'])) || ($total_price >= $mt_free_sending))
		{
			//個別商品送料無料 || 合計金額送料無料
			$free_sending = true;
		}
		else
		{
			$free_sending = $mt_free_sending - $total_price;
		}

		return $total_info = array(
			'total_price'  => $total_price,
			'free_sending' => $free_sending,
			'get_point'    => Func::get_total_point($cart_dt, Model_Mt_Pointrate::select_rate()),
		);
	}

	/**
	 * get_total_point
	 * 
	 * @param cart_dt
	 * @param point_rate
	 * 
	 * @return total_point
	 */
	static public function get_total_point ($cart_dt, $rate)
	{
		$total_point         = 0; //ポイント合計
		$no_has_point_amount = 0; //ポイントを持っていない商品金額(税込み)

		//dt_cart
		for ($i=0; $i<count($cart_dt); $i++)
		{
			if (isset($cart_dt[$i]['point']) && count($cart_dt[$i]['point'])>0)
			{
				//商品ポイントを持っている場合
				$total_point += ($cart_dt[$i]['point'] * $cart_dt[$i]['count']);
			}
			else
			{
				//ポイントを持っていない商品
				$no_has_point_amount += (self::get_item_value($cart_dt[$i]) * (Int)$cart_dt[$i]['count']);
			}
		}
		$total_point += (Int)($no_has_point_amount * ($rate/100));
		return $total_point;
	}

	/**
	 * get_tax_inclueded_price
	 * 
	 * 税込金額を返却します(端数が出た場合は切り捨て)
	 * @param int price
	 * @param int tax included price
	 */
	static public function get_tax_inclueded_price ($price)
	{
		$result = 0;
		$tax    = Model_Mt_Tax::select_current_tax_value();
		return (Int)($price * (1+ $tax/100));
	}

	/**
	 * get_pagenate_data
	 * 
	 * @param $data, $page_no, $limit
	 * @return array('current_page_data', =>data, 'current_no' => no, 'page_count' => $page_count)
	 */
	static public function get_pagenate_data ($data, $page_no, $limit)
	{
		$result = array('current_page_data' => null, 'page_no' => 0, 'page_count' => 0);

		//引数チェック
		if (!is_numeric($page_no) || $page_no <= 0)
			return false;
			//self::redirect(Config::get('custom_config.url.http_domain').'errors/error', '指定された値が不正です');

		//データのカウントを取得する
		$data_count = count($data);

		if ($data_count > 0)
		{
			//ページ数を取得する
			$page_count = (Int)ceil($data_count / $limit);

			//ページ総数より次ぎのページが大きい時は強制的に1ページ目を出力する
			if ($page_count < $page_no)
			{
				$page_no = 1;
				$current_page_data = array_slice($data, 0, $limit);
				return  array('current_page_data' =>$current_page_data, 'page_no' => $page_no, 'page_count' => $page_count);
				//self::redirect('errors/error', '指定された値が不正です');
			}
			//表示するページのデータを取得する
			$current_page_data = array_slice($data,  $limit * ($page_no - 1), $limit);
			$result = array('current_page_data' =>$current_page_data, 'page_no' => $page_no, 'page_count' => $page_count);
		}
		return $result;
	}

	/**
	 * write_exception_log
	 * 
	 * exceptionの内容をログを書き込むメソッド
	 * 
	 * @param exeption message / alert_mail_flg
	 */
	static public function write_exception_log ($e, $alert_mail_flg = false)
	{
		if (isset($e))
		{
			$error = PHP_EOL;
			$error .= '----------------exeption!!----------------'.PHP_EOL;
			$error .= $e->getMessage().PHP_EOL;
			$error .= $e->getFile().PHP_EOL;
			$error .= $e->getLine().PHP_EOL;
			$error .= '------------------------------------------'.PHP_EOL;
			Log::error($error);
		}

		if ($alert_mail_flg)
		{
			$mail_data = array(
				'text' => $error,
			);

			$target_address = (\Fuel\Core\Fuel::$env === \Fuel\Core\Fuel::DEVELOPMENT)? 'y.funayose@gmail.com': 'dev@sp-k.co.jp';

			$mail_info = array(
					'admin' => array(
							'param' => array(
									'to'        => $target_address,
									'subject'   => 'エラーレポート',
									'view'      => 'mail/admin/alert',
									'from'      => 'contact@shopmail.panpikids.com',
									'from_name' => Config::get('custom_config.shop_name'),
							),
							'email_data' => $mail_data,
					),
			);

			$mail_info = \Func::prepare_forSendmail($mail_info);
			//メールを非同期で送信する
			$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
			exec($command, $output, $return_code);
		}
	}

	/**
	 * redirect
	 * 
	 * redirect させる
	 * 
	 * @param url message
	 */
	static public function redirect ($url = null, $message = null)
	{
		//urlがnullの時トップページをセットする
		if (is_null($url))
			$url = Config::get('custom_config.url.http_domain');

		//messageがからでない場合はset_flashを設定する
		if (!is_null($message))
			Session::set_flash('message', $message);

		Response::redirect($url);
	}

	/**
	 * redirect_error
	 * 
	 * errorが発生時にredirect させる
	 * 
	 * @param url message
	 */
	static public function redirect_error ($message = null)
	{
		//urlがnullの時エラーページをセットする
		$url = self::$is_ssl ? Config::get('custom_config.url.https_domain'): Config::get('custom_config.url.http_domain');
		//メッセージの表示
		is_null($message) and $message = '処理に失敗しました。<br />しばらく時間を置いてから再度お試し下さい。';
		Session::set_flash('error_message',$message);
		Response::redirect($url.'/errors/error');
	}

	/**
	 * assetsパスのgetter action
	 *
	 * @access  public
	 * @return  img path
	 */
	static public function get_assets_path ()
	{
		return Config::get('custom_config.url.assets_url');
	}

	/**
	 *  Sendmailで渡す引数をJSON形式に整形
	 *
	 *  @param  array $params (渡す引数の配列)
	 *  @return json  $params (整形後のJSON形式のデータ)    
	 *
	 */
	public static function prepare_forSendmail( $params )
	{
		// 特殊文字のエスケープ
		array_walk_recursive($params, 
			create_function('&$val', '
				$val = htmlspecialchars($val, ENT_QUOTES, "UTF-8");
			')
		);
		// JSONエンコード
		$params = json_encode($params);
		return $params;
}

	/**
	 * メール送信(SMTP)
	 * 設定ファイルは app/config/email.php
	 *@param  array   $param 各種設定
	 * # to      : 宛先
	 * # subject : 件名
	 * # veiw    : テンプレートView
	 * array   $data   Viewに渡すデータ
	 * @return boolean $result 送信結果
	 */
	public static function sendMail( $param, $data, $type = "text" )
	{
		// Create shorter varibales
		foreach ( $param as $k => $v ) {
			$$k = $v;
		}

		// Send
		$email = Email::forge();

		//configからを取得する
		Config::load('custom_config',true);
		//$from      = Config::get('custom_config.email.contact', false);  //送信元メールアドレス
		//$from_name = Config::get('custom_config.email.contact_name', false); //送信者名


		//Emailプロパティに各変数をセット
		$email->from( $from, $from_name );
		$email->to( $to );

		if (isset($cc))
			$email->cc( $cc );

		$email->subject( $subject );

		if($type == "html"){
			$email->html_body( View::forge($view, $data, false) );
		} else {
			$email->body( View::forge($view, array('data'=> $data), false) );
		}

		//一応公式documentのtry-chthを加えます
		try
		{
			// send and return Result
			return ($email->send())? true: false;
		}
		catch( \EmailValidationFailedException $e )
		{
			// The validation failed
			\Log::error($e);
			return false;
		}
		catch( \EmailSendingFailedException $e )
		{
			// The driver could not send the email
			\Log::error($e);
			return false;
		}
		catch(\SmtpCommandFailureException $e)
		{
			\Log::error($e);
			return false;
		}
		catch( Exception $e )
		{
			\Log::error($e);
			return false;
		}
	}

	/**
	 * pankuzu_helper
	 *
	 * @access  public
	 * @return  pankuzu tag
	 */
	static public function pankuzu_helper ($data_array)
	{
		$return_str = '';
		$count = 1;

		foreach ($data_array as $path => $name)
		{
			$array_count = count($data_array);
			if ($array_count != $count)
			{
				$return_str .= '<a href="'.Uri::create($path).'">'.$name.'</a>';
				if($array_count >= 3 && $count != $array_count-1)
					$return_str .= '>';
			}
			else
			{
				$return_str .= '>'.$name;
			}
			$count++;
		}
		return $return_str;
	}

	/**
	 * get_keys_value
	 * 
	 * 引数に渡されたキーは列に合致した値のみにします
	 * 
	 * @parama $keys, $values
	 * @return array
	 */
	static public function get_keys_value ($keys, $values)
	{
		$return_array = array();

		for ($i=0; $i<count($keys); $i++)
		{
			isset($values[$keys[$i]]) and $return_array[$keys[$i]] = $values[$keys[$i]];
		}
		return $return_array;
	}

	/**
	 * get_array_diff
	 * 
	 * !!このメソッドを使用する前に2つの配列に対してget_keys_valueを行ってください!!
	 * 返却する値はsrcのものになります
	 * 
	 * @parama src_array, target_array
	 * @return diff key
	 */
	static public function get_array_diff ($src, $target, $is_key_only = false)
	{
		$diff = array();

		foreach ($src as $key => $src_val)
		{
			if (!isset($target[$key]) || $src_val != $target[$key])
			{
				$diff[] = $key;
			}
		}
		return $diff;
	}

	/**
	 * get_cart_info_sum
	 * 
	 * カートの合計金額(税込み)/合計購入個数
	 * 
	 * @param cart_dt
	 * @return sum data array
	 */
	public static function get_cart_info_sum($cart_dt)
	{
		//結果返却
		$result_array = array(
			'total_amount' => 0, //合計金額
			'total_count'  => 0, //合計個数
		);
		if (!$cart_dt)
			return $result_array;

		for ($i=0; $i<count($cart_dt); $i++)
		{
			if (isset($cart_dt[$i]['discount']['rate']))
			{
				//商品個別割引があった時
				$result_array['total_amount'] += Func::get_tax_inclueded_price((Int)$cart_dt[$i]['price'] * (1- (Int)$cart_dt[$i]['discount']['rate']/100)) * (Int)$cart_dt[$i]['count'];
			}
			else
			{
				//通常料金の時
				$result_array['total_amount'] += (Int)self::get_tax_inclueded_price($cart_dt[$i]['price']) * (Int)$cart_dt[$i]['count'];
			}
			$result_array['total_count']  += $cart_dt[$i]['count'];
		}
		return $result_array;
	}

	/**
	 * get_item_value
	 * 
	 * 引数がitem_idの場合はデータを取得してから結果を返却します
	 * 引数が配列の場合は配列を検査して結果を返却します
	 * 
	 * @param int or mix array
	 * 
	 */
	public static function get_item_value($item_data)
	{
		$result = false;

		if (!is_array($item_data))
			$item_data = Model_Mt_Item::select_item_by_id($item_data, true);

		if (isset($item_data['discount']['rate']))
		{
			//商品個別割引があった時(カートデータが引数の場合使用)
			$result = Func::get_tax_inclueded_price((Int)$item_data['price'] * (1- (Int)$item_data['discount']['rate']/100));
		}
		else if (isset($item_data['rate']))
		{
			//商品個別割引があった時(商品購入履歴の時使用)
			$result = Func::get_tax_inclueded_price((Int)$item_data['price'] * (1- (Int)$item_data['rate']/100));
		}
		else
		{
			//通常料金の時
			$result = (Int)self::get_tax_inclueded_price((Int)$item_data['price']);
		}
		return $result;
	}

	/**
	 * 
	 * error_log
	 * 
	 * error log を書き込むメソッドです
	 * 
	 * @param $message, $is_mail(メール送信するかどうか)
	 * @return none
	 */
	public static function error_log($message, $is_mail = false)
	{
		$error = '大変、エラーをキャッチしたお!!(´･ヮ･`)';
		try
		{
			$error.= PHP_EOL;
			$error.='------------------------------------------'.PHP_EOL;
			if (is_array($message))
			{
				foreach ($message as $key => $val)
				{
					$error .= $key.' =>'.$val.PHP_EOL;
				}
			}
			else
			{
				$error .= $message.PHP_EOL;
			}
			$error.='------------------------------------------'.PHP_EOL;
			Log::error($error);

			if ($is_mail)
			{
				$mail_data = array(
					'text' => $error,
				);

				$target_address = (\Fuel\Core\Fuel::$env === \Fuel\Core\Fuel::DEVELOPMENT)? 'y.funayose@gmail.com': 'dev@sp-k.co.jp';

				$mail_info = array(
					'admin' => array(
							'param' => array(
									'to'        => $target_address,
									'subject'   => 'エラーレポート['.Fuel::$env.'環境]',
									'view'      => 'mail/admin/alert',
									'from'      => 'contact@shopmail.panpikids.com',
									'from_name' => Config::get('custom_config.shop_name'),
							),
							'email_data' => $mail_data,
					),
				);

				$mail_info = \Func::prepare_forSendmail($mail_info);
				//メールを非同期で送信する
				$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
				exec($command, $output, $return_code);
			}
		}
		catch (Exception$e)
		{
			Log::error('error_log書き込み失敗'.json_encode($message));
		}
	}

	/**
	 * get_avail_sending_by_cart_and_state
	 * 
	 * 有効な発送方法データを返却します
	 * 
	 * @param cart_dt array
	 * @return avali sending array
	 */
	public static function get_avail_sending ($cart_dt, $state_cd, $zip , $payment_cd = null, $sending_cd = null)
	{
		$result      = array(); //結果返却用
		$sending_cds = array(); //dt_cartから取得した発送方法CD
		$mt_area     = Model_Mt_Area::get_area_by_state($state_cd, $zip); //area(state_cdで絞ったデータ)
		$mt_sending  = Model_Mt_Sending::select_sending_recursive(); //発送方法 料金を取得する
		$is_priority = false; //優先発送方法を持つ商品があった場合はtrue

		/*
		 * カートに入っている商品に対応する
		 * 発送方法コードを重複なく配列にsetする
		 */
		for ($i=0; $i<count($cart_dt); $i++)
		{
			//商品ごとの発送方法をまわす
			foreach ($cart_dt[$i]['sendings'] as $value)
			{
				(isset($value['priority_flg']) && (Int)$value['priority_flg'] === 1) and $is_priority = true;
				$sending_cds[$value['sending_cd']] = $value['sending_cd'];
			}
		}

		foreach ($sending_cds as $value)
		{
			try
			{
				//決済方法が代引き(payment_cd => 3)の時はcontinue
				if (!is_null($payment_cd) && (Int)$payment_cd === Model_Mt_Payment::CASH_ON_DELIVERY_CD && isset($mt_sending[$value]['is_cash']) && (Int)$mt_sending[$value]['is_cash'] === 0)
					continue;

				if ($mt_sending[$value]['has_area'] == 1)
				{
					$count_tmp = count($mt_sending[$value]['postages']);
					for ($i=0; $i<$count_tmp; $i++)
					{
						if ((Int)$mt_sending[$value]['postages'][$i]['area_cd'] != (Int)$mt_area['id'])
						{
							unset($mt_sending[$value]['postages'][$i]);
						}
					}
					$mt_sending[$value]['postages'] = array_values($mt_sending[$value]['postages']);
				}
				$sending_tmp = array(
					'id' => $mt_sending[$value]['id'],
					'name' => $mt_sending[$value]['name'],
					'postage' => $mt_sending[$value]['postages'][0]['postage']
				);
				isset($mt_sending[$value]['postages'][0]['area_name']) and $sending_tmp['area_name'] = $mt_sending[$value]['postages'][0]['area_name'];
				$result['postages'][] = $sending_tmp;
			}
			catch (Exception $e)
			{
				Func::write_exception_log($e);
			}
		}

		/*
		 * 選択商品が複数あった場合かつ優先発送方法がtrueの時
		 * 発送方法の中から一番高い方法を検索し
		 * $result['highest_cd'] = sending_cdを設定する
		 */
		if (count($cart_dt)>1 && $is_priority)
		{
			// 比較対象のrowをとった配列を作成する
			foreach ($result['postages'] as $key => $row)
			{
				$postage[$key] = (Int)$row['postage'];
			}
			$sort_tmp = $result['postages'];
			// データを送料['postage']の降順にソートする。
			if (!array_multisort($postage, SORT_DESC, $sort_tmp) || !isset($sort_tmp[0]['id']))
			{
				Func::error_log('送料最大値比較失敗!!'.__FILE__.'/['.__line__.']', true);
			}
			$result['highest_cd'] = $sort_tmp[0]['id'];
			unset($result['postages']);
			$result['postages'][] = $sort_tmp[0];
		}

		/*
		 * 送料無料処理
		 * 商品合計値 && 離島でない場合
		 * $result['is_free'] = true を設定する
		 * $result['free_amount'] = (送料無料料金)
		 */
		$free_amount = Model_Mt_Freesending::select_freesending(); //送料無料マスタ取得
		$cart_info = self::get_cart_info_sum($cart_dt);

		//--- 合計金額が送料無料マスタ以上であった場合
		if (
			(
				(Int)$mt_area['id'] != Model_Mt_Area::ISLAND_AREA_CD &&
				(Int)$mt_area['id'] != Model_Mt_Area::OKINAWA_AREA_CD
			) &&
			($cart_info['total_amount'] >= $free_amount)
		)
		{
			$result['is_free'] = true;
			$result['free_amount'] = $free_amount;
		}

		//--- 商品個別送料無料かどうか確認する
		if (
			!isset($result['is_free']) &&
			count($cart_dt) === 1 &&
			(Int)$mt_area['id'] != Model_Mt_Area::ISLAND_AREA_CD &&
			(Int)$mt_area['id'] != Model_Mt_Area::OKINAWA_AREA_CD &&
			isset($cart_dt[0]['id']) &&
			Model_Mt_Freesendingitem::select_by_item_id($cart_dt[0]['id'])
		)
		{
			$result['is_free'] = true;
			$result['free_item'] = true;
		}

		/*
		 * 引数にsending_cdが指定してあった時
		 * はpostagesをsending_cdだけにして返却する
		 */
		if (!is_null($sending_cd))
		{
			$postae_cnt = count($result['postages']);
			for ($i=0; $i<$postae_cnt; $i++)
			{
				if ($result['postages'][$i]['id'] != $sending_cd)
					unset($result['postages'][$i]);
			}
			$result['postages'] = array_values($result['postages']);
		}
		return $result;
	}

	/**
	 * check_date_passed
	 * 
	 * 現在がmysqlのdatetimeカラムから取得した日時
	 * 以上かどうかチェックします
	 * 
	 * @param type string (mysql datetime str)
	 */
	public static function check_date_passed ($date_str)
	{
		return strtotime($date_str) <= time();
	}

	/**
	 * check_date_passed
	 * 
	 * mt_itemのselling_dateをもとに
	 * 予約期間中かどうかチェックします
	 * 
	 * @param type string (mysql datetime str)
	 */
	public static function check_reserve_date ($date_str)
	{
		return (!empty($date_str) && strtotime($date_str) > time())? true: false;
	}

	/**
	 * send_reserve_mail
	 * 
	 * 予約内容をメールします。
	 */
	public static function send_reserve_mail ($input_data)
	{
		$item_mt = Model_Mt_Item::select_item_by_id($input_data['item_id'], true);
		$data = array();
		$data = CustomAuth::get_login_info();
		$data['name']         = $item_mt['name'];
		$data['count']        = $input_data['count'];
		$data['reserve_id']   = $input_data['reserve_id'];
		$data['item_id']      = $input_data['item_id'];
		$data['date']         = date('Y年m月d日H時i分s秒');
		$data['selling_date'] = $item_mt['selling_date'];

		//adminとuserにメール送信
		$mail_info = array(
				'user' => array(
						'param' => array(
								'to'        => $data['mail'],
								'subject'   => Func::get_mail_subject(Config::get('custom_config.mail.title.reserve.user')),
								'view'      => 'mail/user/reserve/user',
								'from'      => Config::get('custom_config.mail.from.contact'),
								'from_name' => Config::get('custom_config.shop_name'),
						),
						'email_data' => $data
				),
				'admin' => array(
						'param' => array(
								'to'        => Config::get('custom_config.mail.from.admin'),
								'subject'   => Config::get('custom_config.mail.title.reserve.admin'),
								'view'      => 'mail/user/reserve/admin',
								'from'      => Config::get('custom_config.mail.from.contact'),
								'from_name' => Config::get('custom_config.shop_name'),
						),
						'email_data' => $data
				)
		);
		$mail_info = Func::prepare_forSendmail($mail_info);
		//メールを非同期で送信する
		$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
		exec($command, $output, $return_code);
	}

	/**
	 * delelte_special_select_by_item_id
	 * 
	 * item_idに紐づいたspecial_select sessionを削除します。
	 * 
	 * @param item_id
	 */
	public static function delete_special_select_by_item_id ($item_id)
	{
		$session_dt = Session::get('special_select_dt', array());

		foreach ($session_dt as $key => $val)
			if ((Int)$val['item_id'] === (Int)$item_id)
				unset($session_dt[$key]);

		if (count($session_dt) === 0)
		{
			Session::delete('special_select_dt');
		}
		else
		{
			Session::set('special_select_dt', array_values($session_dt));
		}
	}

	/**
	 * check_integrity_item_and_special_session
	 * 
	 * 商品とspecial_select_dtとの整合性もチェックします
	 * 
	 * @param int $item_id 購入item id
	 * @param mix array $special_select_session 特殊選択session
	 * @param int $purchase_num 購入個数
	 */
	public static function check_integrity_item_and_special_session ($item_id, $special_select_session, $purchase_num)
	{
		$result           = false;
		$item_mt          = Model_Mt_Item::select_item_by_id($item_id);
		$purchase_counter = 0;

		if (isset($item_mt['special_select']['values']))
		{
			//-------------------------------------------------------
			// 商品マスタで特殊商品が設定されている時
			//-------------------------------------------------------

			//セッションデータをまわす
			foreach ($special_select_session as $value)
			{
				if ((Int)$value['item_id'] === (Int)$item_id)
				{
					$result = false;

					//itemマスタの特殊選択設定をまわす
					for ($i=0; $i<count($item_mt['special_select']['values']); $i++)
					{
						if ((Int)$value['value'] === (Int)$item_mt['special_select']['values'][$i]['value_id'])
						{
							$purchase_counter+=$value['count'];
							$result = true;
							break;
						}
					}

					/**
					 * チェックに失敗した場合はfalseを返却する
					 */
					if (!$result)
						return false;
				}
			}

			/**
			 * item購入個数とspecial_select_sessionが持っている個数の合計が合致するかどうか
			 * チェックする
			 */
			if ((Int)$purchase_num !== (Int)$purchase_counter)
				$result = false;
		}
		else
		{
			//-------------------------------------------------------
			// 検査不要の時
			//-------------------------------------------------------
			$result = true;
		}
		return $result;
	}

	/**
	 * join_dt_cart_dn_special_select
	 * 
	 * @param type $cart_dt
	 * @param type $special_session_dt
	 */
	public static function join_dt_cart_and_special_select ($cart_dt, $special_select_dt)
	{
		/******************************
		 * 特殊選択sessionがあった場合
		 ******************************/
		if (!empty($special_select_dt))
		{
			//cart_dt
			for ($i=0; $i<count($cart_dt); $i++)
			{
				$special_select_tmp = array();
				foreach ($special_select_dt as $key => $val)
				{
					if ((Int)$cart_dt[$i]['id'] === (Int)$val['item_id'])
					{
						//--- 特殊選択keyの取得
						$mt_key = Model_Mt_Specialselectkey::select_by_id($val['key']);
						$val['key_name'] = isset($mt_key['name'])? $mt_key['name']: false;

						//--- 特殊選択valueの取得
						$mt_value = Model_Mt_Specialselectvalue::select_by_id($val['value']);
						$val['value_name'] = isset($mt_value['value'])? $mt_value['value']: false;

						$special_select_tmp[] = $val;
						unset($special_select_dt[$key]);
					}
				}
				if (count($special_select_tmp)>0)
					$cart_dt[$i]['special_select'] = $special_select_tmp;
			}
		}
		return $cart_dt;
	}
}