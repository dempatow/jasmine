<?php
/**
 * the TwitterAPI class
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P advertising Co.,Ltd.
 * @link       http://sp-k.co.jp
 */

class TwitterAPI
{
	/**
	 * facebook config defaults.
	 */
	protected static $_defaults;
	private static   $sdk_instance = null;

	/**
	 * priorities
	 */

	/**
	 * コンストラクタ
	 */
	public static function _init()
	{
		//config読み込み
		Config::load('twitter', true);
		//memberにconfigをセットする
		static::$_defaults = \Config::get('twitter');

		//sdk instanceを生成する
		require_once("twitteroauth/twitteroauth.php");
	}


	/**
	 * get_auth_url
	 * 
	 * twitter 認証用のurlを取得します
	 * 
	 * @return redirect url
	 */
	public static function get_auth_url ()
	{
		if (!Input::get('oauth_token') && !Input::get('oauth_verifier'))
		{
			/* Build TwitterOAuth object with client credentials. */
			$connection = new TwitterOAuth(self::$_defaults['consumer_key'], self::$_defaults['consumer_secret']);

			/* Get temporary credentials. */
			$request_token = $connection->getRequestToken(self::$_defaults['call_back']);

			/* Save temporary credentials to session. */
			$token = $request_token['oauth_token'];

			Session::set('oauth_token', $request_token['oauth_token']);
			Session::set('oauth_token_secret', $request_token['oauth_token_secret']);

			/* If last connection failed don't display authorization link. */

			switch ($connection->http_code)
			{
				case 200:
					/* Build authorize URL and redirect user to Twitter. */
					$is_select_url = (Session::get('oauth_token') && Session::get('oauth_token_secret'))?true:false;
					$url = $connection->getAuthorizeURL($token, $is_select_url);

					//return autentication url
					return $url;

				default:
					//status code に 200が返ってこなかった時
					Session::delete('oauth_token');
					Session::delete('oauth_token_secret');
					Func::redirect('errors/error', '処理に失敗しました');
			}
		}
	}

	/**
	 * get_call_back_data
	 * 
	 * twitter認証urlからのcallbackメソッド
	 * twitterユーザーデータを返却します
	 * 
	 * @return user_data_array
	 */
	public static function get_call_back_data ()
	{

		try
		{
			if (Input::get('oauth_token') && Input::get('oauth_verifier'))
			{
				// access token 取得
				$tw = new TwitterOAuth
						(
							self::$_defaults['consumer_key'],
							self::$_defaults['consumer_secret'],
							Session::get('oauth_token'),
							Session::get('oauth_token_secret')
						);
				Debug::dump($_REQUEST['oauth_verifier']);
				$access_token = $tw->getAccessToken($_REQUEST['oauth_verifier']);
				Debug::dump($access_token);

				// Twitter の user_id + screen_name(表示名)
				Debug::dump($access_token['user_id']);
				Debug::dump($access_token['screen_name']);
				Debug::dump($access_token);
				$user = $tw->get('account/verify_credentials');

				//Objectでとれるので必要な要素だけ取得して配列に詰める
				$want_item = array('');
				array_flip($want_item);
				foreach ($user as $key => $value)
				{
					Debug::dump($key, $value);
//					if ()
//					{
//						
//					}
				}

				//Sessionを消す
				Session::delete('oauth_token');
				Session::delete('oauth_token_secret');

				return $user_data;
			}
			//パラメタ不足エラー
			throw new Exception('Insufficient parameters');

		}
		catch (Exception $e)
		{
			//Sessionを消す
			Session::delete('oauth_token');
			Session::delete('oauth_token_secret');
			Func::write_exception_log($e);
			Func::redirect(null, '処理に失敗しました');
		}
	}

	/**
	 * tweet連投テストメソッド
	 * 
	 */
	public static function test ()
	{
		$tw = new TwitterOAuth
			(
				self::$_defaults['consumer_key'],
				self::$_defaults['consumer_secret'],
				'357531364-wfJbvo6FbrIhvDmYXpVwI3VyghPbeOsGbFTc5AC4',
				'NyNQGMw515GLidDWID6jfzIsIMZlRL8enWQM0xTW8o'
			);
		$tw->post('statuses/update', array('status'=>'テスト'));

//		for ($i=0; $i<1000; $i++)
//		{
//			$count = $i + 1;
//			$tweet = "api test !{$count}回目";
//			$tw->post('statuses/update', array('status'=>$tweet));
//		}

	}
}