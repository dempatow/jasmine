<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Index Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_Index extends Common
{
	public $template = 'template/template_index';

	public function before()
	{
		if (Device::is_sp())
			$this->template = Device::get_template_path ();

		parent::before();
	}

	/**
	 * index action
	 * 
	 * トップ画面の表示
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		$data = array();
		//--- 新着商品データ
		$data['new_item'] = Model_Mt_Item::select_for_index_page(5);
		//$data['new_item'] = Model_Mt_Item::select_for_index_page();
		//--- ニュースデータ
		$data['news'] = Model_Mt_News::select_join_category();

		//--- viewの作成
		if (Device::is_sp())
		{
			//css
			Asset::css(array($this->assets_path.'/css/index_sp.css'), array(), 'add_css', false);
			$this->template->header = View::forge(Device::get_header_path());
			$this->template->footer = View::forge(Device::get_footer_path());
			$this->template->content= View::forge('index/index_sp');
		}
		else
		{
			//初回だけ固定出力
			//$data['ranking']  = Model_Dt_Purchasehistory::get_ranking_data();
			//--- ランキングデータ
			$data['ranking'] = array(
				0 => Model_Mt_Item::select_item_by_id(10015, true),
				1 => Model_Mt_Item::select_item_by_id(10013, true),
				2 => Model_Mt_Item::select_item_by_id(10031, true),
				3 => Model_Mt_Item::select_item_by_id(10010, true),
				4 => Model_Mt_Item::select_item_by_id(10001, true),
			);

			CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
			$this->template->footer = View::forge('layout/footer_common');
			$this->template->header = View::forge('layout/header_common');
			$this->template->content= View::forge('index/index');
			$this->template->left   = View::forge('layout/side_menu');
			$this->template->menu   = View::forge('layout/main_menu');
		}

		$this->template->set_global('data', $data);
	}

	/**
	 * calender ajax
	 * カレンダー
	 */
	public function post_calender()
	{
		$month_year = Input::post('month_year');
		if (isset($month_year))
		{
			echo CalenderHelper::get_calender_ajax($month_year);
		}
	}
}
