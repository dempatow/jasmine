<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Purchasehistory Controller.
 * 
 * 購入履歴
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_User_Withdrawal extends Common
{
	public $template = 'template/template_common';

	public function before()
	{
		parent::before();

		if (!$this->is_login)
			Func::redirect_error (null, 'ログインしていません');

		if (Input::is_ajax())
			return;

		//基本viewの作成
		//viewの作成
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');
		$this->template->mypage_header = View::forge('layout/mypage_header');
		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
		//css
		Asset::css(array($this->assets_path.'/css/mypage.css', $this->assets_path.'/css/common.css'), array(), 'add_css', false);
	}

	/**
	 * index action
	 * 
	 * 退会処理
	 * 
	 * @access public
	 */
	public function action_index ()
	{
		$data = array();
		$input_data = Input::post();

		/***********
		 * 退会処理
		 ***********/
		if (Security::check_token() && !empty($input_data))
		{
			$val = Validation::forge('Model_Dt_User');
			$val->add_model('Model_Dt_User');
			$item = array(
				'password_withdrawal',
				'consent_withdrawal',
			);

			if ($val->run($input_data, $item))
			{
				//DB処理
				$user_dt = CustomAuth::get_login_info();
				$query = '';
				if (isset($user_dt['id']))
					$query = Model_Dt_User::get_query_update_user(array('del_flg' => 1), $user_dt['id']);

				if (DbHelper::query_exec($query, false, false, 'master'))
				{
					//ログを残す
					Model_Dt_Useractionhistory::insert_history(106, $user_dt['id']);
					//セッション削除
					CustomAuth::logout(true);
					//トップページにリダイレクト
					Func::show_overlay_message("ご利用頂きましてありがとうございました。<br />退会処理が完了しました。", $this->base_url);
				}
				Func::redirect_error (null);
			}
			else
			{
				$data = $input_data;
				$this->template->set_global('val', $val);
			}
		}
		$this->template->set_global('data', $data);
		$this->template->content = View::forge('withdrawal/index');
	}
}