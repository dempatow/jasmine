<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Mypage Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_User_Mypage extends Common
{
	public $template = 'template/template_common';

	public function before()
	{
		if (Device::is_sp())
			$this->template = Device::get_template_path ();

		parent::before();

		if (Device::is_sp())
		{
			//css
			Asset::css(array($this->assets_path.'/css/mypage_sp.css'), array(), 'add_css', false);
			$this->template->header = View::forge(Device::get_header_path());
			$this->template->footer = View::forge(Device::get_footer_path());
		}
		else
		{
			//基本viewの作成
			//viewの作成
			$this->template->footer        = View::forge('layout/footer_common');
			$this->template->header        = View::forge('layout/header_common');
			$this->template->left          = View::forge('layout/side_menu');
			$this->template->menu          = View::forge('layout/main_menu');
			$this->template->mypage_header = View::forge('layout/mypage_header');

			//css
			Asset::css(array($this->assets_path.'/css/mypage.css'), array(), 'add_css', false);

			//--- viewの作成
			CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
		}
	}

	/**
	 * form action
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{

		$data = array();
		$data = CustomAuth::get_login_info(true);
		$state = Model_Mt_State::get_state();

		if (isset($data['state_cd']) && isset($state[$data['state_cd']]))
		{
			$data['state_cd'] = $state[$data['state_cd']]['name'];
		}

		//カード利用が過去にあるかどうか
		if (isset($data['id']) && count(Model_Dt_Purchasehistory::select_credit_data ($data['id'])) > 0)
			$data['is_use_card'] = true;

		//予約商品一覧
		$reserve_dt = Model_Dt_Reserve::get_reserve_items($data['id'], Model_Dt_Reserve::RESERVE);

		if (!empty($reserve_dt))
		{
			$data['reserve'] = $reserve_dt;
			$reserve_session = Session::get('reserve_dt',null);
			if (!is_null($reserve_session))
			{
				for ($i=0; $i<count($data['reserve']); $i++)
				{
					if (isset($reserve_session[$data['reserve'][$i]['item_id']]))
					{
						for ($j=0; $j<count($reserve_session[$data['reserve'][$i]['item_id']]); $j++)
						{
							if ((Int)$reserve_session[$data['reserve'][$i]['item_id']][$j] === (Int)$data['reserve'][$i]['id'])
							{
								$data['reserve'][$i]['in_cart'] = true;
								break;
							}
						}
					}
				}
			}
		}

		if (Device::is_sp())
		{
			Asset::js(array($this->assets_path.'/js/mypage/index_sp.js'), array(), 'add_js', false);
			$this->template->content = View::forge('mypage/index_sp');
		}
		else
		{
			Asset::js(array($this->assets_path.'/js/mypage/index.js'), array(), 'add_js', false);
			$this->template->content = View::forge('mypage/index');
		}
		$this->template->set_global('data', $data);
	}
}