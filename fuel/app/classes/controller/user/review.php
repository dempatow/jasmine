<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Purchasehistory Controller.
 * 
 * レビューコントローラー
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_User_Review extends Common
{
	public $template = 'template/template_common';
	const  EVALUATION_MAX = 5;

	public function before()
	{
		parent::before();

		if (!$this->is_login)
			Func::redirect_error (null, 'ログインしていません');

		if (Input::is_ajax())
			return;

		//基本viewの作成
		//viewの作成
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');
		$this->template->mypage_header = View::forge('layout/mypage_header');
		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
		//css
		Asset::css(array($this->assets_path.'/css/mypage.css', $this->assets_path.'/css/common.css'), array(), 'add_css', false);
	}

	/**
	 * index action
	 * 
	 * レビュー一覧
	 * 
	 * @access public
	 */
	public function action_index ($page_no = 1)
	{
		//pager定数
		$max_list_cnt     = 3;//listページの表示件数(奇数でお願いします)
		$disp_item_cnt    = 5;//1ページの最大表示件数

		//pagerその他
		$record_cnt       = 0;//レコード合計件数
		$page_count       = 0;//ページ総数
		$db_offset        = 0;//db offset

		//その他
		$uid              = (Int)CustomAuth::get_login_info()['id'];//uid
		$review_flg       = 0;

		//レコードのデータ
		$query_search_data = Model_Dt_Purchaseitemhistory::get_query_select_for_review_list($uid, $review_flg);

		//ページ件数取得
		$record_cnt = Model_Dt_Purchaseitemhistory::select_count_for_review_list($uid,$review_flg);

		$page_count = (Int)ceil($record_cnt / $disp_item_cnt);
		$max_list_cnt>$page_count and $max_list_cnt=$page_count;

		//パラメータが不正の時は現在ページに1を設定する
		((Int)$page_no <1 || (Int)$page_no > $page_count) and $page_no = 1;

		//db offset 取得
		(Int)$page_no !== 1 and $db_offset = ($page_no-1) * $disp_item_cnt;

		//レコード取得
		$record_data = DbHelper::query_exec($query_search_data->offset($db_offset)->limit($disp_item_cnt)->order_by('purchase.created', 'desc'));

		//画面にセットするもの
		$data['record_data']  = $record_data;
		$data['page_count']   = $page_count;
		$data['page_no']      = $page_no;
		$data['max_list_cnt'] = $max_list_cnt;

		$this->template->set_global('data', $data);
		$this->template->content = View::forge('review/index');
	}

	/**
	 * detail form
	 * 
	 * レビュー書き込み
	 * 
	 * @access public
	 */
	public function action_form ($history_id = null)
	{
		$data = array();
		$input_data = Input::post();
		$uid = (Int)CustomAuth::get_login_info()['id'];//uid

		if (Fuel\Core\Security::check_token() && isset($input_data['purchase_id']))
		{
			//---submit押下時(validation)
			//ユーザーが投稿出来るレビューかどうかチェックする
			$dt_purchae = DbHelper::query_exec(Model_Dt_Purchaseitemhistory::get_query_select_for_review_list($uid, 0)->and_where('item.id', '=', $input_data['purchase_id']), true);

			if (empty($dt_purchae))
				Func::redirect_error ();

			!isset($input_data['evaluation']) and $input_data['evaluation'] = 0;

			$val = Validation::forge('Model_Dt_Review');
			$val->add_model('Model_Dt_Review');
			$item = array(
				'evaluation',
				'contributor',
				'title',
				'comment',
			);

			/**
			 * validation
			 */
			if ($val->run($input_data, $item))
			{
				$update_array = array(
					'review_flg' => 1,
				);

				$insert_array = array(
					'uid'          => $uid,
					'item_id'      => $dt_purchae['item_id'],
					'contributor'   => $input_data['contributor'],
					'title'        => $input_data['title'],
					'evaluation'   => $input_data['evaluation'],
					'comment'      => $input_data['comment'],
					'approval_flg' => 0,
				);

				//--- dt_purchaseitemhistory update and dt_review insert
				if (
					!Model_Dt_Purchaseitemhistory::update_by_id($update_array ,$input_data['purchase_id']) ||
					!Model_Dt_Review::insert($insert_array)
				)
				{
					Func::error_log ('review 書き込み失敗!!', true);
					Func::redirect_error();
				}
				Session::set_flash('overlay_message', 'レビューの投稿が完了しました');
				Response::redirect($this->base_url.'/user/review');
			}
			else
			{
				isset($dt_purchae['id']) and $history_id = $dt_purchae['id'];
				$data = $input_data;
				$this->template->set_global('val', $val);
			}
		}
		else if (empty($history_id) || !is_numeric($history_id))
		{
			//---初回idパラメタチェック
			Func::redirect_error (null);
		}

		$dt_purchae = DbHelper::query_exec(Model_Dt_Purchaseitemhistory::get_query_select_for_review_list($uid, 0)->and_where('item.id', '=', $history_id), true);
		//未登録レビューがない場合
		if (empty($dt_purchae))
			Func::redirect_error (null);

		$data['dt_purchase'] = $dt_purchae;
		$this->template->set_global('data', $data);
		Asset::js(array($this->assets_path.'/js/mypage/review.js',), array(), 'add_js', false);
		$this->template->content = View::forge('review/form');
	}
}