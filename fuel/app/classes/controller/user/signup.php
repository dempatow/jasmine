<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Member Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_User_Signup extends Common
{
	public $template = 'template/template_common';

	public function before()
	{
		if (Device::is_sp())
			$this->template = Device::get_template_path ();

		parent::before();

		if (Device::is_sp())
		{
			$this->template->header = View::forge(Device::get_header_path());
			$this->template->footer = View::forge(Device::get_footer_path());
			//css
			Asset::css(array($this->assets_path.'/css/mypage_sp.css'), array(), 'add_js', false);
		}
		else
		{
			//基本viewの作成
			//viewの作成
			$this->template->footer = View::forge('layout/footer_common');
			$this->template->header = View::forge('layout/header_common');
			$this->template->left   = View::forge('layout/side_menu');
			$this->template->menu   = View::forge('layout/main_menu');
			//css
			Asset::css(array($this->assets_path.'/css/mypage.css'), array(), 'add_js', false);
		}
		//formの切り分け用
		$this->template->set_global('form_status', 1);

		//--- viewの作成
		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
		//$this->template->set_global('data', array('state' => Model_Mt_State::get_state()));

		//heder_commonのログイン後redirect先を設定する(権限エラーをなくすため)
		$this->template->set_global('login_redirect_url', Config::get('custom_config.url.https_domain'));
	}

	/**
	 * form action
	 * 
	 * 1.display form view
	 * 2.check and redirect confirm
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_form()
	{
		$session_data = Session::get('form',false);
		Session::delete('form');
		$form_data    = (!$session_data)? Input::post(): $session_data;

		/************************
		 * 入力チェック 確認画面ridirect
		 ************************/
		if (Security::check_token() && count($form_data)>0)
		{
			//validation
			$val = Validation::forge('Model_Dt_User');
			$val->add_model('Model_Dt_User');
			$check_item = array(
				'mail_signup',
				'corporate_name',
				'corporate_name_kana',
				'last_name',
				'first_name',
				'last_name_kana',
				'first_name_kana',
				'passwd',
				'zip',
				'address1',
				'address2',
				'address3',
				'tel1',
				'tel2',
				'tel3',
				'fax1',
				'fax2',
				'fax3',
				'birth_year',
				'birth_month',
				'birth_day',
				'job_cd',
				'sex_cd',
				'state_cd',
				'magazine_cd',
				'consent',
			);

			if ($val->run($form_data, $check_item))
			{
				//redirect
				Session::set('form', $form_data);
				Response::redirect(Config::get('custom_config.url.https_domain').'/user/signup/confirm');
			}
			else
			{
				$this->template->set_global('val', $val);
			}
		}

		/********************
		 * 予約と同時の会員登録
		 ********************/
		$get_param = Input::get();
		if (isset($get_param['r_count']) && isset($get_param['reserve']) && isset($get_param['r_item_id']))
		{
			$form_data['r_count']   = $get_param['r_count'];
			$form_data['reserve']   = $get_param['reserve'];
			$form_data['r_item_id'] = $get_param['r_item_id'];
		}

		/************************
		 * form表示
		 ************************/
		Asset::js(array('https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3-https.js',), array(), 'add_js', false);
		$form_data['mt'] = $this->get_form_mt();
		unset($form_data['passwd'],$form_data['passwd_confirm']);
		$this->template->set_global('data', $form_data);

		//--- viewの作成
		if (Device::is_sp())
		{
			$this->template->content = View::forge('signup/form_sp');
		}
		else
		{
			$this->template->content = View::forge('signup/form');
		}
		return;
	}

	/**
	 * confirm action
	 *
	 * display confirm view
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_confirm()
	{
		$form_data = Session::get('form', false);
		Session::delete('form');

		//sessionがない場合はformにリダイレクト
		if (!$form_data)
			Response::redirect(Config::get('custom_config.url.https_domain').'/user/signup/form');

		//passwdの隠蔽
		if (isset($form_data['passwd']))
		{
			$form_data['passwd_hide'] = '';
			for ($i=0; $i<strlen($form_data['passwd']); $i++)
			{
				$form_data['passwd_hide'].='*';
			}
		}

		$form_data['mt'] = $this->get_form_mt();
		$this->template->set_global('data', $form_data);

		//--- viewの作成
		if (Device::is_sp())
		{
			$this->template->content = View::forge('signup/confirm_sp');
		}
		else
		{
			$this->template->content = View::forge('signup/confirm');
		}
	}

	/**
	 * regist action
	 * 
	 * 1.regist db and disp regist view
	 * 2.redirect form
	 */
	public function action_regist ()
	{
		$form_data = Input::post();

		if (!Security::check_token() || count($form_data)<=0)
			Func::redirect_error ('不正なリクエストです');

		if (isset($form_data['back_singnup']))
		{
			//formに戻る
			Session::set('form',$form_data);
			Response::redirect(Config::get('custom_config.url.https_domain').'/user/signup/form');
		}
		else if (isset($form_data['regist_singnup']))
		{
			Session::delete('form');

			//validation
			$val = Validation::forge('Model_Dt_User');
			$val->add_model('Model_Dt_User');

			$check_item = array(
				'mail_signup',
				'corporate_name',
				'corporate_name_kana',
				'last_name',
				'first_name',
				'last_name_kana',
				'first_name_kana',
				'passwd',
				'zip',
				'address1',
				'address2',
				'address3',
				'tel1',
				'tel2',
				'tel3',
				'fax1',
				'fax2',
				'fax3',
				'birth_year',
				'birth_month',
				'birth_day',
				'job_cd',
				'sex_cd',
				'state_cd',
				'magazine_cd',
			);

			if ($val->run($form_data, $check_item))
			{
				$insert_item = array (
					'corporate_name',
					'corporate_name_kana',
					'first_name',
					'last_name',
					'first_name_kana',
					'last_name_kana',
					'passwd',
					'zip',
					'state_cd',
					'address1',
					'address2',
					'address3',
					'tel1',
					'tel2',
					'tel3',
					'fax1',
					'fax2',
					'fax3',
					'birth_year',
					'birth_month',
					'birth_day',
					'sex_cd',
					'job_cd',
					'magazine_cd',
				);

				$insert_data = DbHelper::array_filter($insert_item, $form_data);
				$insert_data['mail'] = $form_data['mail_signup'];
				$insert_data['auto_login'] = 0;
				$pass_len = strlen($form_data['passwd']);
				$insert_data['passwd'] = Auth::instance()->hash_password($insert_data['passwd']);
				$query = Model_Dt_User::get_query_insert_user($insert_data);

				//DB失敗
				$db_result = DbHelper::query_exec($query, false, false, 'master');
				if (!isset($db_result['insert_id']) || empty($db_result['insert_id']))
					Func::redirect (null, '処理に失敗しました');

				//登録完了メール送信
				//登録日時
				$data = $insert_data;
				$data['regist_date'] = date('Y年m月d日H時i分s秒');
				//passwdの隠蔽
				$data['passwd'] = '';
				for ($i=0; $i<$pass_len; $i++)
				{
					$data['passwd'].='*';
				}

				//adminとuserにメール送信
				$mail_info = array(
						'user' => array(
								'param' => array(
										'to'        => $data['mail'],
										'subject'   => Func::get_mail_subject(Config::get('custom_config.mail.title.signup.user')),
										'view'      => 'mail/user/signup/user',
										'from'      => Config::get('custom_config.mail.from.contact'),
										'from_name' => Config::get('custom_config.shop_name'),
								),
								'email_data' => $data
						),
						'admin' => array(
								'param' => array(
										'to'        => Config::get('custom_config.mail.from.admin'),
										'subject'   => Config::get('custom_config.mail.title.signup.admin'),
										'view'      => 'mail/user/signup/admin',
										'from'      => Config::get('custom_config.mail.from.contact'),
										'from_name' => Config::get('custom_config.shop_name'),
								),
								'email_data' => $data
						)
				);
				$mail_info = Func::prepare_forSendmail($mail_info);
				//メールを非同期で送信する
				$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
				exec($command, $output, $return_code);

				/*****************************
				 * 商品予約と同時の登録があった場合
				 *****************************/
				if (
					(isset($form_data['r_count']) && isset($form_data['reserve']) && isset($form_data['r_item_id'])) &&
					Model_Mt_Item::validation_reserve(array('count' => $form_data['r_count'], 'item_id' => $form_data['r_item_id'])) &&
					($reserve_data['reserve_id'] = Model_Dt_Reserve::reserve_item($db_result['insert_id'], $form_data['r_item_id'], $form_data['r_count'])) &&
					CustomAuth::login($form_data['mail_signup'], $form_data['passwd'])
				)
				{
					$reserve_data['count']   = $form_data['r_count'];
					$reserve_data['item_id'] = $form_data['r_item_id'];
					Func::send_reserve_mail($reserve_data);
					Func::show_overlay_message('商品の予約が完了しました', Config::get('custom_config.url.https_domain').'/user/mypage');
				}

				//--- viewの作成
				if (Device::is_sp())
				{
					$this->template->content = View::forge('signup/regist_sp');
				}
				else
				{
					$this->template->content = View::forge('signup/regist');
				}
				return;
			}
		}
		//例外処理
		Session::delete('form');
		Func::redirect(Config::get('custom_config.url.http_domain').'/errors/error','処理に失敗しました');
	}

	/**
	 * get_form_mt
	 * 
	 * @return form_mt
	 */
	private function get_form_mt ()
	{
		$mt = array();
		//form用マスタ
		$mt['state_cd'] = Model_Mt_State::get_state();//都道府県マスタ
		$mt['sex_cd'] = Model_Mt_Sex::get_sex();//性別マスタ
		$mt['job_cd'] = Model_Mt_Job::get_job();//仕事マスタ
		$mt['magazine_cd'] = Model_Mt_Magazine::select();//メルマガ可否マスタ

		//$mt['age_cd'] = Model_mt_Age::get_age();//年代マスタ
		return $mt;
	}
}