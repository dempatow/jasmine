<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The User Address Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_User_Address extends Common
{
	public $template = 'template/template_common';

	public function before()
	{
		parent::before();

		if (Input::is_ajax())
			return;

		//基本viewの作成
		//viewの作成
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');
		$this->template->mypage_header = View::forge('layout/mypage_header');

		//住所登録formの切り分け用
		$this->template->set_global('form_status', 3);

		//css
		Asset::css(array($this->assets_path.'/css/mypage.css'), array(), 'add_css', false);
		//js
		Asset::js(array($this->assets_path.'/js/mypage/address.js'), array(), 'add_js', false);

		//--- viewの作成
		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
	}

	/**
	 * form action
	 * 
	 * 1.display form view
	 * 2.check and redirect confirm
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_form()
	{
		$overlay_message = Session::get_flash('overlay_message', false);
		if ($overlay_message)
			$this->template->set_global('overlay_message',$overlay_message);

		//ログイン情報取得
		$login_info = CustomAuth::get_login_info();
		$session_data = Session::get('address_form',false);

		if (!$session_data)
		{
			$form_data = Input::post();

			//購入処理終わりからのポストの場合はopen
			if (isset($form_data['regist_address']))
			{
				$this->template->set_global('open_address_form', true);
				$form_data['address_id'] = 'new';
			}
		}
		else
		{
			$form_data = $session_data;
			$this->template->set_global('open_address_form', true);
		}

		/************************
		 * 入力チェック 確認画面ridirect
		 ************************/
		if (Security::check_token() && count($form_data)>0)
		{
			//validation
			$val = Validation::forge('Model_Dt_User');
			$val->add_model('Model_Dt_User');
			$check_item = array(
				'mail_address',
				'corporate_name',
				'corporate_name_kana',
				'last_name',
				'first_name',
				'last_name_kana',
				'first_name_kana',
				'zip',
				'address1',
				'address2',
				'address3',
				'tel1',
				'tel2',
				'tel3',
				'fax1',
				'fax2',
				'fax3',
				'state_cd',
			);

			if ($val->run($form_data, $check_item))
			{
				//redirect
				Session::set('address_form', $form_data);
				Response::redirect(Config::get('custom_config.url.https_domain').'/user/address/confirm');
			}
			else
			{
				$this->template->set_global('open_address_form', true);
				$this->template->set_global('val', $val);
			}
		}

		/************************
		 * form表示
		 ************************/
		Asset::js(array('https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3-https.js',), array(), 'add_js', false);
		$form_data['mt'] = $this->get_form_mt();
		$form_data['address'] = Model_Dt_Receiveraddress::select_by_uid((Int)$login_info['id']);

		$this->template->set_global('data', $form_data);
		$this->template->content = View::forge('address/form');
		$this->template->content->_inner_form = View::forge('signup/form');
		return;
	}

	/**
	 * confirm action
	 *
	 * display confirm view
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_confirm()
	{
		$form_data = Session::get('address_form', false);
		Session::delete('address_form');

		//sessionがない場合はformにリダイレクト
		if (!$form_data)
			Response::redirect(Config::get('custom_config.url.https_domain').'/user/address/form');

		$form_data['mt'] = $this->get_form_mt();

		$this->template->set_global('data', $form_data);
		$this->template->content = View::forge('signup/confirm');
	}

	/**
	 * regist action
	 * 
	 * 1.regist db and disp regist view
	 * 2.redirect form
	 */
	public function action_regist ()
	{
		$form_data  = Input::post();
		$login_info = CustomAuth::get_login_info();//ログイン情報取得

		if (!Security::check_token() || count($form_data)<=0)
			Func::redirect ('errors/error', '不正なリクエストです');

		if (isset($form_data['back_singnup']))
		{
			//formに戻る
			Session::set('address_form',$form_data);
			Response::redirect(Config::get('custom_config.url.https_domain').'/user/address/form');
		}
		else if (isset($form_data['regist_singnup']))
		{
			Session::delete('address_form');

			//validation
			$val = Validation::forge('Model_Dt_User');
			$val->add_model('Model_Dt_User');

			$check_item = array(
				'corporate_name',
				'corporate_name_kana',
				'last_name',
				'first_name',
				'last_name_kana',
				'first_name_kana',
				'zip',
				'address1',
				'address2',
				'address3',
				'tel1',
				'tel2',
				'tel3',
				'fax1',
				'fax2',
				'fax3',
				'state_cd',
			);

			if ($val->run($form_data, $check_item))
			{
				$insert_item = array (
					'corporate_name',
					'corporate_name_kana',
					'first_name',
					'last_name',
					'first_name_kana',
					'last_name_kana',
					'zip',
					'state_cd',
					'address1',
					'address2',
					'address3',
					'tel1',
					'tel2',
					'tel3',
					'fax1',
					'fax2',
					'fax3',
				);

				$address_data = DbHelper::array_filter($insert_item, $form_data);
				$query = '';

//				Debug::dump(
//					$form_data['address_id'],
//					$login_info['id'],
//					count(Model_Dt_Receiveraddress::select_by_uid($login_info['id']))
//				);exit;

				if (
					$form_data['address_id'] === 'new' &&
					!empty($login_info['id']) &&
					count(Model_Dt_Receiveraddress::select_by_uid($login_info['id'])) <= (Model_Dt_Receiveraddress::DISP_COUNT - 1)
				)
				{
					/**********
					 * insert
					 **********/
					$address_data['uid'] = $login_info['id'];
					$query = Model_Dt_Receiveraddress::get_query_insert($address_data);
				}
				else if (
					!empty($form_data['address_id']) &&
					!empty($login_info['id'])
				)
				{
					/**********
					 * update
					 **********/
					$query = Model_Dt_Receiveraddress::get_query_update_by_id_and_uid($address_data, $form_data['address_id'], $login_info['id']);
				}

				//queryが作られていない又DB登録失敗
				if (empty($query) || !DbHelper::query_exec($query, false, false, 'master'))
						Func::redirect (null, '処理に失敗しました');

				Session::set_flash('overlay_message', 'お届け先の編集の登録が終了しました');
				Response::redirect(Config::get('custom_config.url.https_domain').'/user/address/form');
				return;
			}
			$error_array = array();
			$error_array = $val->get_error_array();
			$error_array['error_file'] = 'address/regist';
			Func::error_log($error_array, true);
		}
		//例外処理
		Session::delete('form');
		Func::redirect(Config::get('custom_config.url.http_domain').'/errors/error','処理に失敗しました');
	}

	/**
	 * post_get_address
	 * 
	 * return json
	 */
	public function post_get_address ()
	{
		$id     = Input::post('id',false);
		$result = array(
			'result'  => false,
			'address' => array()
		);
		$login_info = CustomAuth::get_login_info();

		if (isset($login_info['id']) && !empty($login_info['id']) && (Int)$id > 0)
		{
			$result['address'] = Model_Dt_Receiveraddress::select_by_id_and_uid($id, $login_info['id']);
			if (count($result['address'])>0)
				$result['result'] = true;
		}
		$this->response($result);
	}

	/**
	 * post_get_address
	 * 
	 * return json
	 */
	public function post_del_address ()
	{
		$id     = Input::post('id',false);
		$result = array(
			'id' => $id,
			'result'  => false,
		);
		$login_info = CustomAuth::get_login_info();

		if (isset($login_info['id']) && !empty($login_info['id']) && (Int)$id > 0)
		{
			$query = Model_Dt_Receiveraddress::get_query_update_by_id_and_uid(array('del_flg' => 1) ,$id, $login_info['id']);
			$db_reuslt = DbHelper::query_exec($query,false,'master');
			if ((bool)$db_reuslt)
				$result['result'] = true;
		}
		$this->response($result);
	}

	/**
	 * get_form_mt
	 * 
	 * @return form_mt
	 */
	private function get_form_mt ()
	{
		$mt = array();
		//form用マスタ
		$mt['state_cd'] = Model_Mt_State::get_state();   //都道府県マスタ
		return $mt;
	}
}