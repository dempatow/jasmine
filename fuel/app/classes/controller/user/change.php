<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The User Change Controller
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_User_Change extends Common
{
	public $template = 'template/template_common';

	public function before()
	{
		parent::before();

		//基本viewの作成
		//viewの作成
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');
		$this->template->mypage_header = View::forge('layout/mypage_header');
		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));

		//formの切り分け用
		$this->template->set_global('form_status', 2);

		//css
		Asset::css(array($this->assets_path.'/css/mypage.css'), array(), 'add_js', false);
	}

	/**
	 * card action
	 * 
	 * カード情報を変更します
	 */
	public function action_card ()
	{
		$get_data   = Input::get();//get param
		$login_info = CustomAuth::get_login_info();//user info

		if (
			!empty($get_data) && isset($get_data['result']) &&
			(Int)$get_data['result'] === 1 && isset($login_info['id'])
		)
		{
			Model_Dt_Useractionhistory::insert_history(107, $login_info['id']);
			Func::show_overlay_message (
				'処理が完了しました',
				Config::get('custom_config.url.https_domain').'/user/mypage'
			);
		}


		/******************
		 * カード利用が過去にあるかどうか
		 ******************/
		if (
			!isset($login_info['id']) ||
			count(Model_Dt_Purchasehistory::select_credit_data ($login_info['id'])) <= 0
		)
			Func::show_overlay_message ('カード利用がありません', Config::get('custom_config.url.https_domain').'/user/mypage');

		/******************
		 * リダイレクト
		 ******************/
		$response = Payment::get_redirect_url(
									$login_info['last_name'].' '.$login_info['first_name'],
									$login_info['mail'],
									$login_info['id'],
									0,
									Payment::$PROSESS_REGISTERED_CHANGE,
									0
								);

		if (
			isset($response['redirect']) && !empty($response['redirect']) &&
			isset($response['result']) && (Int)$response['result'] === 1
		)
			Response::redirect($response['redirect']);

		/******************
		 * 処理が失敗した時
		 ******************/
		Func::error_log($response, true);
		Func::show_overlay_message (
			'処理に失敗しました。<br />しばらく時間をあけてお試しください。',
					Config::get('custom_config.url.https_domain').'/user/mypage'
		);
	}

	/**
	 * form action
	 * 
	 * 1.display form view
	 * 2.check and redirect confirm
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_form()
	{
		$form_data = array();
		$session_data = array();

		/******************
		 * ユーザー情報の設定
		 ******************/
		$session_data = Session::get('chenge_form',false);
		Session::delete('chenge_form');
		$form_data    = (!$session_data)? Input::post(): $session_data;
		if (!$form_data)
		{
			//DBから設定
			$dt_user = CustomAuth::get_login_info(true);
			$dt_user['mail_change'] =  $dt_user['mail_change_confirm'] = $dt_user['mail'];
			unset(
				$dt_user['mail'],
				$dt_user['role_cd'],
				$dt_user['auto_login'],
				$dt_user['id'],
				$dt_user['del_flg'],
				$dt_user['created'],
				$dt_user['updated']
			);
			$form_data = $dt_user;
		}

		/************************
		 * 入力チェック 確認画面ridirect
		 ************************/
		if (Security::check_token() && count($form_data)>0)
		{
			//validation
			$val = Validation::forge('Model_Dt_User');
			$val->add_model('Model_Dt_User');
			$check_item = array(
				'mail_change',
				'corporate_name',
				'corporate_name_kana',
				'last_name',
				'first_name',
				'last_name_kana',
				'first_name_kana',
				'zip',
				'address1',
				'address2',
				'address3',
				'tel1',
				'tel2',
				'tel3',
				'fax1',
				'fax2',
				'fax3',
				'birth_year',
				'birth_month',
				'birth_day',
				'job_cd',
				'sex_cd',
				'state_cd',
				'magazine_cd',
			);

			if ($val->run($form_data, $check_item))
			{
				//redirect
				Session::set('change_form', $form_data);
				Response::redirect(Config::get('custom_config.url.https_domain').'/user/change/confirm');
			}
			else
			{
				$this->template->set_global('val', $val);
			}
		}

		/***********
		 * form表示
		 ***********/
		Asset::js(array('https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3-https.js',), array(), 'add_js', false);
		$form_data['mt'] = $this->get_form_mt();
		$this->template->set_global('data', $form_data);
		$this->template->content = View::forge('signup/form');
		return;
	}

	/**
	 * confirm action
	 *
	 * display confirm view
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_confirm()
	{
		$form_data = Session::get('change_form', false);
		Session::delete('change_form');

		//sessionがない場合はformにリダイレクト
		if (!$form_data)
			Response::redirect(Config::get('custom_config.url.https_domain').'/user/change/form');

		$form_data['mt'] = $this->get_form_mt();
		$this->template->set_global('data', $form_data);
		$this->template->content = View::forge('signup/confirm');
	}

	/**
	 * regist action
	 * 
	 * 1.regist db and disp regist view
	 * 2.redirect form
	 */
	public function action_regist ()
	{
		$form_data = Input::post();

		if (!Security::check_token() || count($form_data)<=0)
			Func::redirect ('errors/error', '不正なリクエストです');

		if (isset($form_data['back_singnup']))
		{
			//formに戻る
			Session::set('chenge_form',$form_data);
			Response::redirect(Config::get('custom_config.url.https_domain').'/user/change/form');
		}
		else if (isset($form_data['regist_singnup']))
		{
			Session::delete('chenge_form');

			//validation
			$val = Validation::forge('Model_Dt_User');
			$val->add_model('Model_Dt_User');

			$check_item = array(
				'mail_change',
				'corporate_name',
				'corporate_name_kana',
				'last_name',
				'first_name',
				'last_name_kana',
				'first_name_kana',
				'zip',
				'address1',
				'address2',
				'address3',
				'tel1',
				'tel2',
				'tel3',
				'fax1',
				'fax2',
				'fax3',
				'birth_year',
				'birth_month',
				'birth_day',
				'job_cd',
				'sex_cd',
				'state_cd',
				'magazine_cd',
			);

			if ($val->run($form_data, $check_item))
			{
				$update_item = array (
					'corporate_name',
					'corporate_name_kana',
					'first_name',
					'last_name',
					'first_name_kana',
					'last_name_kana',
					'zip',
					'state_cd',
					'address1',
					'address2',
					'address3',
					'tel1',
					'tel2',
					'tel3',
					'fax1',
					'fax2',
					'fax3',
					'birth_year',
					'birth_month',
					'birth_day',
					'sex_cd',
					'job_cd',
					'magazine_cd',
				);

				$update_array = DbHelper::array_filter($update_item, $form_data);
				$update_array['mail'] = $form_data['mail_change'];
				$login_info = CustomAuth::get_login_info();
				$query = Model_Dt_User::get_query_update_user($update_array, (Int)$login_info['id']);

				//DB失敗
				if (!DbHelper::query_exec($query, false, false, 'master'))
						Func::redirect (null, '処理に失敗しました');

				//セッションを入れ替える
				Session::set('login_info.mail',$update_array['mail']);
				Session::set('login_info.corporate_name',$update_array['corporate_name']);
				Session::set('login_info.first_name', $update_array['first_name']);
				Session::set('login_info.last_name', $update_array['last_name']);


				//変更完了メール送信
				//登録日時
				$update_array['regist_date'] = date('Y年m月d日H時i分s秒');

				//adminとuserにメール送信
				$mail_info = array(
						'user' => array(
								'param' => array(
										'to'        => $update_array['mail'],
										'subject'   => Func::get_mail_subject(Config::get('custom_config.mail.title.change.user')),
										'view'      => 'mail/user/change/user',
										'from'      => Config::get('custom_config.mail.from.contact'),
										'from_name' => Config::get('custom_config.shop_name'),
								),
								'email_data' => $update_array
						),
						'admin' => array(
								'param' => array(
										'to'        => Config::get('custom_config.mail.from.admin'),
										'subject'   => Config::get('custom_config.mail.title.change.admin'),
										'view'      => 'mail/user/change/admin',
										'from'      => Config::get('custom_config.mail.from.contact'),
										'from_name' => Config::get('custom_config.shop_name'),
								),
								'email_data' => $update_array
						)
				);

				$mail_info = Func::prepare_forSendmail($mail_info);
				//メールを非同期で送信する
				$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
				exec($command, $output, $return_code);
				$this->template->content = View::forge('signup/regist');
				return;
			}
			$error_array = $val->get_error_array();
			$error_array[] = __FILE__.'/['.__line__.']';
			Func::error_log($error_array, true);
		}

		//例外処理
		Session::delete('change_form');
		Func::redirect(Config::get('custom_config.url.http_domain').'/errors/error','処理に失敗しました');
	}

	/**
	 * get_form_mt
	 * 
	 * @return form_mt
	 */
	private function get_form_mt ()
	{
		$mt = array();
		//form用マスタ
		$mt['state_cd'] = Model_Mt_State::get_state();//都道府県マスタ
		$mt['sex_cd'] = Model_Mt_Sex::get_sex();//性別マスタ
		$mt['job_cd'] = Model_Mt_Job::get_job();//仕事マスタ
		$mt['magazine_cd'] = Model_Mt_Magazine::select();//メルマガ可否マスタ

		//$mt['age_cd'] = Model_mt_Age::get_age();//年代マスタ
		return $mt;
	}
}