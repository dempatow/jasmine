<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Point Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_User_Point extends Common
{
	public $template = 'template/template_common';

	public function before()
	{
		parent::before();

		if (Input::is_ajax())
			return;

		//基本viewの作成
		//viewの作成
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');
		$this->template->mypage_header = View::forge('layout/mypage_header');

		//formの切り分け用
		$this->template->set_global('form_status', 1);

		//css
		Asset::css(array($this->assets_path.'/css/mypage.css'), array(), 'add_js', false);

		//--- viewの作成
		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
		//$this->template->set_global('data', array('state' => Model_Mt_State::get_state()));
	}

	/**
	 * list action
	 * 
	 * ポイント履歴ページ
	 * 
	 * @access  public
	 * @param int page_no current pageno
	 * @return  Response
	 */
	public function action_list($page_no = 1)
	{
		//pager定数
		$max_list_cnt     = 7;//listページの表示件数(奇数でお願いします)
		$disp_item_cnt    = 20;//1ページの最大表示件数
		//pagerその他
		$point_record_cnt = 0;//レコード合計件数
		$page_count       = 0;//ページ総数
		$db_offset        = 0;//db offset
		//その他
		$point_data       = array();//dt_pointhistoryレコードdt用
		$uid              = (Int)CustomAuth::get_login_info()['id'];//uid
		$mt_point_reason  = array();//ポイント増減理由マスタ

		//レコード合計件数取得
		$point_record_cnt = (Int)DbHelper::query_exec(Model_Dt_Pointhistory::get_query_selec_count_avail_by_uid($uid), true)['count'];

		//ページ件数取得
		$page_count = (Int)ceil($point_record_cnt / $disp_item_cnt);
		$max_list_cnt>$page_count and $max_list_cnt=$page_count;
		//パラメータが不正の時は現在ページに1を設定する
		((Int)$page_no <1 || (Int)$page_no > $page_count) and $page_no = 1;
		//db offset 取得
		(Int)$page_no !== 1 and $db_offset = ($page_no-1) * $disp_item_cnt;
		//レコード取得
		$point_data = DbHelper::query_exec(Model_Dt_Pointhistory::get_query_select_avail_by_uid($uid)->offset($db_offset)->limit($disp_item_cnt));
		$mt_point_reason = Model_Mt_Pointaddreason::select();
		//ポイントマスタを結合する
		for ($i=0; $i<count($point_data); $i++)
		{
			if (isset($point_data[$i]['type_cd']) && isset($mt_point_reason[$point_data[$i]['type_cd']]))
				$point_data[$i]['reason'] = $mt_point_reason[$point_data[$i]['type_cd']];
		}
		$data = array(
			'point_list'      => $point_data,
			'page_count'      => $page_count,
			'page_no'         => $page_no,
			'max_list_cnt'    => $max_list_cnt
		);
		//現在ポイントを取得
		$current_point = Point::get_point();
		if ($current_point >= 0 )
			$this->template->set_global('current_point', $current_point);

		$this->template->set_global('data', $data);
		$this->template->content = View::forge('point/list');
	}
}