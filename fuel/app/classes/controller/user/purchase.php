<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Purchase Controller.
 * 
 * 購入処理
 *
 * 購入者画面(兼 決済決済方法) => お届け先画面 => 発送方法画面 =>  確認画面 => 決済(代行時のみ) => 完了画面
 *
 * @package  app
 * @extends  Controller_Comgit mon
 */
class Controller_User_Purchase extends Common
{
	public  $template          = 'template/template_common';
	private $error_message     = '';

	public function before()
	{
		parent::before();

		if (Input::is_ajax())
			return;

		//基本viewの作成
		//viewの作成
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');

		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
	}

	/**
	 * reserve
	 * 予約前処理メソッド
	 * 前処理を行った上、通常のpurchaserアクションに
	 * リダイレクトさせます。
	 */
	public function action_reserve ()
	{
		$input_data = Input::post();

		if (
			Security::check_token() &&
			$this->is_login &&
			isset($input_data['id']) &&
			$db_data = Model_Dt_Reserve::select_by_id_and_uid($input_data['id'], $this->uid)
		)
		{
			$item_data = Model_Mt_Item::select_item_by_id($db_data['item_id'], true);
			unset($item_data['category'],$item_data['chara'],$item_data['new_flg'],$item_data['reco_flg'], $item_data['meta_key']);
			$item_data['count'] = $db_data['count'];

			//カート情報をsessionにセットする
			Session::set('cart_dt', array($item_data));
			Session::set('reserve_dt', $input_data['id']);

			//purchaserにリダイレクトさせる
			Response::redirect(Config::get('custom_config.url.https_domain').'/user/purchase/purchaser');
		}
		//error
		Func::redirect_error();
	}

	/**
	 * purchaser action
	 * 
	 * 購入者情報入力画面
	 * (決済方法選択兼)
	 * 
	 * @access public
	 * 
	 */
	public function action_purchaser ()
	{
		$special_select_dt = Session::get('special_select_dt', array());
		$post_data         = Input::post();//post
		$cart_dt           = Session::get('cart_dt', false);//カート
		$data              = array();//Viewデータ用
		$login_info        = CustomAuth::get_login_info(true);//ログイン情報
		$stock_check       = Model_Dt_Stock::check_stock($cart_dt, $special_select_dt);

		/**
		 * 初期エラー処理
		 */
		if (!$cart_dt)
		{
			//カート空
			$this->error_message = 'カートに商品がありません';
		}
		else if (!$stock_check['result'])
		{
			//在庫不足
			$this->error_message = '在庫不足が発生しました。カートをご確認下さい。';
		}
		$this->alert_cart_page();

		/**
		 * 各種条件分岐
		 */
		if (Security::check_token() && isset($post_data['check_purchaser']) && !empty($post_data['check_purchaser']))
		{
			/**
			 * 2回目以降の表示
			 * validationの実施/データ不正がある場合は入力画面を再度表示させる
			 */
			$val = Validation::forge('Model_Dt_User');
			$val->add_model('Model_Dt_User');
			$check_item = array(
				'mail_purchase',
				'mail_purchase_confirm',
				'corporate_name',
				'corporate_name_kana',
				'last_name',
				'first_name',
				'last_name_kana',
				'first_name_kana',
				'zip',
				'state_cd',
				'address1',
				'address2',
				'address3',
				'tel1',
				'tel2',
				'tel3',
				'fax1',
				'fax2',
				'fax3',
				'payment_cd',
				'payment_process_cd',
			);
			!$this->is_login and $check_item[] = 'consent';

			/**
			 * validation
			 */
			if ($val->run($post_data, $check_item) && isset($post_data['receiver']))
			{
				//---- validation OK
				unset($post_data[Config::get('security.csrf_token_key')],$post_data['check_purchaser'],$post_data['is_card_used'], $post_data['consent']);
				Session::set('purchase_info', $post_data);

				if ($post_data['receiver'] == 'own')
				{
					//--- 送り先を購入者にする場合
					//receiver_infoが残っていた場合はSession delete
					if (Session::get('receiver_info', false))
						Session::delete('receiver_info');
					Response::redirect(Config::get('custom_config.url.https_domain').'/user/purchase/sending');
				}
				else
				{
					//--- 送り先を別の指定にする場合
					Response::redirect(Config::get('custom_config.url.https_domain').'/user/purchase/receiver');
				}
			}
			else
			{
				//---- validation NG
				$this->template->set_global('val', $val);
				$data = $post_data;
			}
		}
		else
		{
			/**
			 * 初期表示
			 */
			$data['is_login'] = false;

			$purchase = Session::get('purchase_info', false);
			if ($purchase)
			{
				$data = $purchase;
				Session::delete('purchase_info');
			}
			else if ($login_info)
			{
				$data = $login_info;
				$data['mail_purchase'] = $login_info['mail'];
				$data['mail_purchase_confirm'] = $login_info['mail'];
				$data['is_login'] = $this->is_login;
			}
		}

		/**
		 * 購入者入力画面
		 */
		//カート情報
		//tmp $data['cart_dt'] = $this->set_special_select_mt($cart_dt, $special
		$data['cart_dt'] = Func::join_dt_cart_and_special_select($cart_dt, $special_select_dt);

		//都道府県マスタ
		$data['mt']['state_cd'] = Model_Mt_State::get_state();
		//決済方法マスタ
		$data['mt']['payment_cd'] = Model_Mt_Payment::select_payment();
		(
			isset($post_data['payment_cd']) &&
			!empty($post_data['payment_cd']) &&
			isset($data['mt']['payment_cd'][$post_data['payment_cd']])
		)
			and $data['mt']['payment_cd'][$post_data['payment_cd']]['checked'] = true;

		//決済方法詳細マスタ
		$this->template->set_global('payment_detail', Model_Mt_Paymentdetail::select(), false);

		//カード利用が過去にあるかどうか
		if (
			$this->is_login &&
			count(Model_Dt_Purchasehistory::select_credit_data ($login_info['id'])) > 0
		)
			$data['is_card_used'] = true;

		Asset::js(array('https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3-https.js',), array(), 'add_js', false);
		Asset::css(array($this->assets_path.'/css/cart.css'), array(), 'add_css', false);
		$this->template->set_global('data', $data);
		$this->template->content = View::forge('purchase/purchaser');
	}

	/**
	 * receiver action
	 * 
	 * 送り先情報入力画面
	 * 
	 * @access public
	 */
	public function action_receiver ()
	{
		$special_select_dt = Session::get('special_select_dt', array());
		$cart_dt       = Session::get('cart_dt', false);//カート
		$data          = array(); //Viewデータ用
		$login_info    = CustomAuth::get_login_info();//ログイン情報
		$purchase_info = Session::get('purchase_info', false);
		$receiver_info = Session::get('receiver_info', false);
		$post_data     = Input::post();
		$stock_check   = Model_Dt_Stock::check_stock($cart_dt, $special_select_dt);

		/**
		 * 初期エラー処理
		 */
		if (!$cart_dt)
		{
			//カート空
			$this->error_message = 'カートに商品がありません';
		}
		else if (!$stock_check['result'])
		{
			//在庫不足
			$this->error_message = '在庫不足が発生しました。カートをご確認下さい。';
		}
		else if (!$purchase_info)
		{
			//購入者情報なし
			Func::show_overlay_message('再度、購入者情報からご入力ください。', $this->base_url.'/user/purchase/purchaser');
		}
		$this->alert_cart_page();

		/*
		 * 戻りの場合
		 */
		//
		//purchase_info.receiver
		//
		if (isset($purchase_info['receiver']) && $purchase_info['receiver'] == 'own')
		{
			Session::delete('receiver_info');
		}
		else if (isset($purchase_info['receiver']) && $purchase_info['receiver'] == 'other' && $receiver_info)
		{
			$data = $receiver_info;
		}

		/**
		 * 各種条件分岐
		 */
		if (isset($post_data['back']) && !empty($post_data['back']))
		{
			//--- 戻るボタン
			Response::redirect(Config::get('custom_config.url.https_domain').'/user/purchase/purchaser');
		}
		else if (isset($post_data['check_receiver']) && !empty($post_data['check_receiver']))
		{
			$val = Validation::forge('Model_Dt_User');
			$val->add_model('Model_Dt_User');
			$check_item = array(
				'corporate_name',
				'corporate_name_kana',
				'last_name',
				'first_name',
				'last_name_kana',
				'first_name_kana',
				'zip',
				'state_cd',
				'address1',
				'address2',
				'address3',
				'tel1',
				'tel2',
				'tel3',
				'fax1',
				'fax2',
				'fax3',
			);

			/**
			 * 2回目以降の表示
			 */
			if ($val->run($post_data, $check_item))
			{
				//--- validation OK
				//確認画面
				unset($post_data[Config::get('security.csrf_token_key')]);
				unset($post_data['check_receiver']);
				Session::set('receiver_info', $post_data);
				Response::redirect(Config::get('custom_config.url.https_domain').'/user/purchase/sending');
			}
			else
			{
				//--- validation NG
				$this->template->set_global('val', $val);
				$data = $post_data;
			}
		}

		//都道府県マスタ
		$data['mt']['state_cd'] = Model_Mt_State::get_state();
		//選択された決済情報
		$data['mt']['payment'] = Model_Mt_Payment::select_payment()[$purchase_info['payment_cd']];
		//ログイン中であれば登録済アドレス帳データを取得する
		$this->is_login and $data['address_data'] = Model_Dt_Receiveraddress::select_by_uid($login_info['id']);

		Asset::js(array('https://ajaxzip3.googlecode.com/svn/trunk/ajaxzip3/ajaxzip3-https.js',$this->assets_path.'/js/cart.js'), array(), 'add_js', false);
		Asset::css(array($this->assets_path.'/css/cart.css',$this->assets_path.'/css/common.css'), array(), 'add_css', false);
		$this->template->set_global('data', $data);
		$this->template->content = View::forge('purchase/receiver');
	}

	/**
	 * sending action
	 * 
	 * 発送方法選択画面
	 * 
	 * @access public
	 */
	public function action_sending ()
	{
		$data              = array();                                    //Viewデータ用
		$cart_dt           = Session::get('cart_dt', array());           //カート

		$special_select_dt = Session::get('special_select_dt', array());   //特殊選択情報
		$purchase_info     = Session::get('purchase_info', false);       //購入者
		$receiver_info     = Session::get('receiver_info', false);       //送信先情報
		$post_data         = Input::post();                              //postデータ
		$stock_check       = Model_Dt_Stock::check_stock($cart_dt, $special_select_dt);//在庫検査
		$validation_result = true;                                       //validation の通し結果を設定する
		//--- 発送先郵便番号
		$receiver_zip      = isset($receiver_info['zip'])? $receiver_info['zip']: $purchase_info['zip'];
		//--- 発送先都道府県番号
		$receiver_state_cd = isset($receiver_info['state_cd'])? $receiver_info['state_cd']: $purchase_info['state_cd'];

		/**
		 * 初期エラー処理
		 */
		if (!$cart_dt)
		{
			//カート空
			$this->error_message = 'カートに商品がありません';
		}
		else if (!$stock_check['result'])
		{
			//在庫不足
			$this->error_message = '商品に在庫が不足してしまいました';
		}
		else if (
			!(
				($purchase_info && $receiver_info) ||
				(!$receiver_info && $purchase_info && isset($purchase_info['receiver']) && $purchase_info['receiver'] == 'own')
			)
		)
		{
			//購入者情報なし
			Func::show_overlay_message('再度、購入者情報からご入力ください。', $this->base_url.'/user/purchase/purchaser');
		}
		$this->alert_cart_page();

		//Viewコンテンツとassetsのセット
		Asset::js(array($this->assets_path.'/js/cart.js'), array(), 'add_js', false);
		Asset::css(array($this->assets_path.'/css/cart.css',$this->assets_path.'/css/common.css'), array(), 'add_css', false);
		$this->template->content = View::forge('purchase/sending');

		if (isset($post_data['back']) && !empty($post_data['back']))
		{
			/*
			 * 戻るボタン
			 */
			$redirect_method = ($receiver_info)? 'receiver': 'purchaser';
			Response::redirect(Config::get('custom_config.url.https_domain').'/user/purchase/'.$redirect_method);
		}
		else if (isset($post_data['check_sending']) && !empty($post_data['check_sending']))
		{
			/*
			 * 2回目以降(発送方法/日時指定入力確認)
			 */
			$val = Validation::forge('Model_Mt_Sending');
			$val->add_model('Model_Mt_Sending');
			$check_item = array(
				'sending_cd_first',
				'date_specified',
			);

			$val_purchase = Validation::forge('Model_Dt_Purchasehistory');
			$val_purchase->add_model('Model_Dt_Purchasehistory');
			$check_item_purchase = array(
				'free1',
			);

			/*
			 * validationの実施
			 */
			$post_data = Input::post();
			$post_data['sending_cd_first'] = (isset($post_data['sending_cd']))? $post_data['sending_cd']: '';

			//日時指定validation
			if (!$val->run($post_data, $check_item))
					$validation_result = false;

			//自由欄
			if (!$val_purchase->run($post_data, $check_item_purchase))
				$validation_result = false;

			if ($validation_result)
			{
				//--- validation OK
				//確認画面
				if (isset($post_data['date_specified']))
				{
					Session::set('purchase_info.date_specified',$post_data['date_specified']);
				}
				else
				{
					Session::delete('purchase_info.date_specified');
				}

				if (isset($post_data['time_specified']))
				{
					Session::set('purchase_info.time_specified',$post_data['time_specified']);
				}
				else
				{
					Session::delete('purchase_info.time_specified');
				}

				if (isset($post_data['free1']))
				{
					Session::set('purchase_info.free1', $post_data['free1']);
				}
				else
				{
					Session::delete('purchase_info.free1');
				}

				Session::set('sending_cd', $post_data['sending_cd']);
				Response::redirect(Config::get('custom_config.url.https_domain').'/user/purchase/confirm');
			}
			else
			{
				$this->template->set_global('val_purchase', $val_purchase);
				$this->template->set_global('val', $val);
			}
		}

		/************************************************
		 * validationに失敗した時または
		 * この画面に戻って来た時にinnerformをviewにセットする
		 ************************************************/
		$session_sendig_cd      = Session::get('sending_cd', false);
		$session_time_specifeid = isset($purchase_info['time_specified'])? $purchase_info['time_specified']: false;
		$session_date_specifeid = isset($purchase_info['date_specified'])? $purchase_info['date_specified']: false;

		if (
			!$validation_result ||
			($session_sendig_cd)
		)
		{
			if (!isset($post_data) || empty($post_data))
			{
				$session_sendig_cd and $post_data['sending_cd'] = $session_sendig_cd;
				$session_time_specifeid and $post_data['time_specified'] = $session_time_specifeid;
				$session_date_specifeid and $post_data['date_specified'] = $session_date_specifeid;
			}
			if (isset($post_data['sending_cd']))
			{
				//--- validation NG
				//日時指定のフォームをviewにセットする
				$specified = $this->get_date_specified ((Int)Model_Mt_Area::get_area_by_state($receiver_state_cd, $receiver_zip)['id'],$post_data['sending_cd']);
				$this->template->set_global('error_date', $specified['date']);
				$this->template->set_global('error_time', $specified['time']);
				if ($specified['date'] || $specified['time'])
					$this->template->content->_inner_date_specified = View::forge('purchase/_inner_date_specified', $post_data);
			}
			unset($post_data['check_sending']);
			$data = $post_data;
		}

		//--- 発送情報データ
		$data['mt']['sending'] = Func::get_avail_sending ($cart_dt, $receiver_state_cd, $receiver_zip ,(Int)$purchase_info['payment_cd']);

		//お届け先area_cd
		$data['mt']['sending']['area_cd'] = (Int)Model_Mt_Area::get_area_by_state($receiver_state_cd, $receiver_zip)['id'];

		//取得した発送情報データの詳細情報を取得する
		$sending_detail = array();
		for ($i=0; $i<count($data['mt']['sending']['postages']); $i++)
		{
			$sending_id = $data['mt']['sending']['postages'][$i]['id'];
			//日にち指定ができるか
			$data['mt']['sending']['postages'][$i]['date_specified'] = (!Model_Mt_Datespecified::select_by_sending_cd_and_area_cd($sending_id, $data['mt']['sending']['area_cd']))?false: true;
			//発送方法詳細マスタの取得
			$sending_detail[$sending_id] = Model_Mt_Sendingdetail::select_by_sending_id($sending_id);
		}
		if (count($sending_detail)>0)
			$this->template->set_global('sending_detail',$sending_detail, false);

		//--- 発送方法注意メッセージをセットする
		$sending_message = $this->get_sending_message_array($cart_dt);
		if (!empty($sending_message))
			$this->template->set_global('sending_message', $sending_message);

		Session::set('sending_info', $data['mt']['sending']);

		//free1テキスト戻り値
		$data['free1'] = isset($post_data['free1'])? $post_data['free1']: Session::get('purchase_info.free1', '');
		$this->template->set_global('data', $data);
	}

	/**
	 * confirm action
	 * 確認画面
	 * 
	 * @access public
	 */
	public function action_confirm ()
	{
		$cart_dt           = Session::get('cart_dt', false);             //カート
		$special_select_dt = Session::get('special_select_dt', array()); //特殊選択情報
		$data              = array();                                    //Viewデータ用
		$purchase_info     = Session::get('purchase_info', false);       //購入者
		$receiver_info     = Session::get('receiver_info', false);       //送信先情報
		$sending_info      = Session::get('sending_info', false);        //発送情報
		$sending_cd        = Session::get('sending_cd', false);          //発送方法コード
		$post_data         = Input::post();                              //post data
		$stock_check       = Model_Dt_Stock::check_stock($cart_dt, $special_select_dt);

		$point_rete_info   = Model_Mt_Pointrate::select();
		Session::set('point_rate_info', $point_rete_info);
		$point_rate        = (Int)$point_rete_info['rate'];

		/**
		 * 初期エラー処理
		 */
		if (!$cart_dt)
		{
			//カート空
			$this->error_message = 'カートに商品がありません';
		}
		else if (!$stock_check['result'])
		{
			//在庫不足
			$this->error_message = '商品に在庫が不足してしまいました';
		}
		else if (
					(!$sending_info) ||
					(!$sending_cd)   ||
					(!$purchase_info)||
					(!$purchase_info && !$receiver_info)||
					(!$receiver_info && $purchase_info && !(isset($purchase_info['receiver']) && $purchase_info['receiver'] == 'own'))
		)
		{
			//必要sessionなし
			$this->error_message = ' 処理に失敗しました。';
		}
		$this->alert_cart_page();

		if (isset($post_data['back']) && !empty($post_data['back']))
		{
			/**
			 * 戻るボタン
			 */
			Response::redirect(Config::get('custom_config.url.https_domain').'/user/purchase/sending');
		}
		else if (isset($post_data['check_confirm']) && !empty($post_data['check_confirm']) && Security::check_token())
		{
			/**
			 * 次へ進むボタン(完了 OR 決済画面)
			 */
			$is_error = false;

			//Dt_UserとMt_Sending validationインスタンス作成
			$val_user    = Validation::forge('Model_Dt_User');
			$val_sending = Validation::forge('Model_Mt_Sending');
			$val_point   = Validation::forge('Model_Dt_Pointhistory');
			$val_user->add_model('Model_Dt_User');
			$val_sending->add_model('Model_Mt_Sending');
			$val_point->add_model('Model_Dt_Pointhistory');

			//--- 購入者/決済方法/発送方法のvalidation
			$check_purchase_item = array(
				'mail_purchase',
				'mail_purchase_confirm',
				'corporate_name',
				'corporate_name_kana',
				'last_name',
				'first_name',
				'last_name_kana',
				'first_name_kana',
				'zip',
				'state_cd',
				'address1',
				'address2',
				'address3',
				'tel1',
				'tel2',
				'tel3',
				'fax1',
				'fax2',
				'fax3',
				'payment_cd',
				'payment_process_cd',
			);
			$check_sending_item = array(
				'sending_cd',
			);
			//--- 使用ポイントのvalidation
			$check_point_item = array(
				'use_point_hidden'
			);

			//ポイントvalidation
			if ($val_point->run($post_data, $check_point_item))
			{
				//割引ポイント入力があればSessionに追加する
				if (isset($post_data['use_point_hidden']) && !empty($post_data['use_point_hidden']) && (Int)$post_data['use_point_hidden'] != 0)
				{
					Session::set('purchase_info.use_point', (Int)$post_data['use_point_hidden']);
					$purchase_info['use_point'] = (Int)$post_data['use_point_hidden'];
				}
				else
				{
					Session::delete('purchase_info.use_point');
					unset($purchase_info['use_point']);
				}
			}
			else
			{
				//error
				$is_error = true;
				$this->template->set_global('val_point', $val_point);
				if (isset($post_data['use_point']))
					$purchase_info['use_point'] = $post_data['use_point'];
			}

			if (
				!$val_user->run($purchase_info, $check_purchase_item) ||
				!$val_sending->run(array('sending_cd' => $sending_cd), $check_sending_item)
			)
			{
				$is_error = true;
				//--- 致命的エラー
				parent::delete_purcahse_sessions();
				Func::error_log('購入確定ボタン押下後(購入者とお届け先が一緒)'.__FILE__.'/['.__line__.']',true);
				Response::redirect('cart/index');
			}

			//--- 購入者とお届け先が違う時
			if ($purchase_info['receiver'] == 'other')
			{
				$check_receiver_item = array(
					'corporate_name',
					'corporate_name_kana',
					'last_name',
					'first_name',
					'last_name_kana',
					'first_name_kana',
					'zip',
					'state_cd',
					'address1',
					'address2',
					'address3',
					'tel1',
					'tel2',
					'tel3',
					'fax1',
					'fax2',
					'fax3',
				);
				//--- お届け先情報validation
				if (!$val_user->run($receiver_info, $check_receiver_item))
				{
					$is_error = true;
					//--- 致命的エラー
					Func::error_log('購入確定ボタン押下後(購入者とお届け先が違う時)'.__FILE__.'/['.__line__.']',true);
					parent::delete_purcahse_sessions();
					Session::set_flash('overlay_message', '処理に失敗しました');
					Response::redirect('cart/index');
				}
			}

			/***************************
			 * validationに全て成功した時 *
			 ***************************/
			if (!$is_error)
			{
				/**
				 * 各種DB処理
				 */
				empty($receiver_info) and $receiver_info = null;
				$point_rate_info = Session::get('point_rate_info', Model_Mt_Pointrate::select());

				/********************************
				 * 予約商品購入の時は
				 * インスタンス変数にデータをセットする
				 ********************************/
				$reserve_dt = Session::get('reserve_dt', null);
				if (!empty($reserve_dt))
					Purchase::set_reserve_dt ($reserve_dt);

				$Purchase = Purchase::forge_regist($cart_dt, $purchase_info, $receiver_info, $sending_info, $sending_cd, $point_rate_info, $special_select_dt);

				//処理失敗の場合はエラー画面に遷移させる
				if ($Purchase->get_error_flg())
				{
					parent::delete_purcahse_sessions();
					Func::redirect_error();
				}

				/*
				 * 決済処理
				 */
				if (1 === (Int)$Purchase->get_payment_cd())
				{
					//決済金額が0円の時
					if ((Int)$Purchase->get_payment_amount() === 0)
					{
						$this->send_confirm_mail();
						Response::redirect(Config::get('custom_config.url.https_domain').'/user/purchase/complete');
					}

					/**********************
					 *   決済代行処理の場合
					 **********************/
					$user_name     = $Purchase->get_user_name();         //ユーザー名
					$user_mail_add = $Purchase->get_user_mail();         //ユーザーメール
					$user_id       = ((Int)$Purchase->get_user_id() == (Int)Purchase::NO_LOGIN_USERE_ID)? 'd'.rand(1000, 9999).'-'.time(): $Purchase->get_user_id(); //ユーザーid
					$price         = $Purchase->get_payment_amount();    //決済総額
					$process_code  = $Purchase->get_payment_process_cd();//処理区分(1=>初回課金,2=>登録済み課金/登録と同時の課金,4=>登録内容変更)
					$order_number  = $Purchase->get_purchase_id();       //オーダー番号(ユニーク)
					$gmo_response  = Payment::get_redirect_url($user_name, $user_mail_add, $user_id, $price, $process_code, $order_number);

					if (
						isset($gmo_response['result']) &&
						isset($gmo_response['redirect']) && 
						(Int)$gmo_response['result'] === (Int) 1 &&
						!empty($gmo_response['redirect'])
					)
					{
						//--- 決済画面に遷移させる
						Response::redirect($gmo_response['redirect']);
					}
					else
					{
						//--- 失敗ステータスが返ってきた時(取消)
						$Purchase = Purchase::forge((Int)$gmo_response['memo1']);
						$Purchase->roll_back_purchase(Purchase::CANCEL_AGNET_BAD_RESPONSE_URL);
						parent::delete_purcahse_sessions();
						Func::error_log($gmo_response, true);
						Func::redirect_error('決済処理エラー<br />処理は取り消しされました。');
					}
				}
				else
				{
					/**********************
					 *   その他の決済方法の場合
					 **********************/
					$this->send_confirm_mail();
					Response::redirect(Config::get('custom_config.url.https_domain').'/user/purchase/complete');
				}
			}
		}
		else if (
			isset($purchase_info['payment_cd'])     &&
			$purchase_info['payment_cd']            &&
			(Int)$purchase_info['payment_cd'] === 1 &&
			Input::get('order_number', false)
		)
		{
			/*******************************
			 * 決済代行画面で戻るボタンを押した時
			 *******************************/
			//purchase_idをキーに全てのテーブルにdel_flgをたてる
			$purchase_id = Input::get('order_number');
			$Purchase = Purchase::forge((Int)$purchase_id);
			$Purchase->roll_back_purchase(Purchase::CANCEL_AGNET_BACK_BUTTON);
		}

		//発送先都道府県番号
		$receiver_state_cd = isset($receiver_info['state_cd'])? $receiver_info['state_cd']: $purchase_info['state_cd'];
		//購入者情報session
		$data['purchase_info'] = $purchase_info;
		//発送先情報session
		$data['receiver_info'] = $receiver_info;
		//購入者都道府県情報
		$data['purchase_info']['state'] = Model_Mt_State::get_state()[$purchase_info['state_cd']];
		//送り先都道府県情報
		$data['receiver_info']['state'] = Model_Mt_State::get_state()[$receiver_state_cd];
		//発送方法情報
		$selected_sending = '';
		for ($i =0; $i<count($sending_info['postages']); $i++)
		{
			if ((Int)$sending_info['postages'][$i]['id'] === (Int)$sending_cd)
				$selected_sending = $sending_info['postages'][$i];
		}
		if (isset($sending_info['is_free']) && !empty($sending_info['is_free']))
		{
			//送料無料0円表示
			$selected_sending['postage'] = 0;
			$selected_sending['is_free'] = true;

			if (isset($sending_info['free_amount']))
			{
				$selected_sending['free_amount'] = $sending_info['free_amount'];
			}
			else
			{
				$selected_sending['free_item'] = $sending_info['free_item'];
			}
		}
		$data['sending_info'] = $selected_sending;

		//商品合計金額/商品合計購入総数/獲得個別ポイントを取得する
		$data['sum'] = Func::get_cart_info_sum($cart_dt);

		//商品合計金額と送料の合計値
		$data['total_amount'] = (Int)$data['sum']['total_amount']+(Int)$data['sending_info']['postage'];

		//合計取得ポイント(個別とレート含む)
		$data['total_point'] = Func::get_total_point($cart_dt, $point_rate);

		//現在保持ポイント
		if ($this->is_login)
			$data['has_point'] = Point::get_point();

		//ポイント使用ボタンが押された時
		if (isset($post_data['use_point_button']) && !empty($post_data['use_point_button']))
		{
			/**
			 * ポイント使用ボタン
			 */
			//--- 使用ポイントのvalidation
			$check_point_item = array('use_point');
			$val_point   = Validation::forge('Model_Dt_Pointhistory');
			$val_point->add_model('Model_Dt_Pointhistory');

			if ($val_point->run($post_data, $check_point_item))
			{
				if ($post_data['use_point']>0)
				{
					$data['total_amount']-=$post_data['use_point'];
					$data['use_point'] = $post_data['use_point'];
				}
			}
			else
			{
				$data['use_point'] = 0;
				$this->template->set_global('val_point', $val_point);
			}
		}

		//選択された決済情報
		$data['payment'] = Model_Mt_Payment::select_payment()[$purchase_info['payment_cd']]['name'];

		//カートデータ
		$data['cart_dt'] = Func::join_dt_cart_and_special_select($cart_dt, $special_select_dt);

		//時間帯指定
		if (isset($data['purchase_info']['time_specified']) && !empty($data['purchase_info']['time_specified']))
		{
			$time_mt = Model_Mt_Timespecified::select_by_id((Int)$data['purchase_info']['time_specified']);
			$data['purchase_info']['time_specified_name'] = $time_mt['name'];
		}
		//日にち指定
		if (isset($data['purchase_info']['date_specified']) && !empty($data['purchase_info']['date_specified']))
		{
			$data['purchase_info']['date_specified_name'] = 
					substr($data['purchase_info']['date_specified'], 0,4).'年'.
					substr($data['purchase_info']['date_specified'], 4,2).'月'.
					substr($data['purchase_info']['date_specified'], 6,2).'日';
		}

		//dataをset_global
		$this->template->set_global('data', $data);

		Asset::js(array($this->assets_path.'/js/cart.js'), array(), 'add_js', false);
		Asset::css(array($this->assets_path.'/css/cart.css',$this->assets_path.'/css/common.css'), array(), 'add_css', false);
		$this->template->content = View::forge('purchase/confirm');
	}

	/**
	 * complete action
	 * 
	 * 購入処理完了画面
	 * 
	 * @access public
	 */
	public function action_complete ()
	{
		$check_session = true;
		$purchase_info = Session::get('purchase_info', false);
		$receiver_info = Session::get('receiver_info', false);
		$reserve_dt    = Session::get('reserve_dt', false); //予約購入中
		$data = array();

		//セッションの削除
		parent::delete_purcahse_sessions();

		if (!$purchase_info)
		{
			Func::redirect_error(null, '不正な画面遷移です');
		}
		else
		{
			isset($purchase_info['payment_cd']) and $data['payment_cd'] = $purchase_info['payment_cd'];
			if (
				$this->is_login &&
				isset($purchase_info['receiver']) && $purchase_info['receiver'] === 'other' &&
				count(Model_Dt_Receiveraddress::select_by_uid(Session::get('login_info.id'))) <= (Model_Dt_Receiveraddress::DISP_COUNT - 1)
			)
			{
					$data['is_other_address'] = true;
					isset($receiver_info['last_name']) and $data['last_name'] = $receiver_info['last_name'];
					isset($receiver_info['first_name']) and $data['first_name'] = $receiver_info['first_name'];
					isset($receiver_info['last_name_kana']) and $data['last_name_kana'] = $receiver_info['last_name_kana'];
					isset($receiver_info['first_name_kana']) and $data['first_name_kana'] = $receiver_info['first_name_kana'];
					isset($receiver_info['corporate_name']) and $data['corporate_name'] = $receiver_info['corporate_name'];
					isset($receiver_info['corporate_name_kana']) and $data['corporate_name_kana'] = $receiver_info['corporate_name_kana'];
					isset($receiver_info['zip']) and $data['zip'] = $receiver_info['zip'];
					isset($receiver_info['state_cd']) and $data['state_cd'] = $receiver_info['state_cd'];
					isset($receiver_info['address1']) and $data['address1'] = $receiver_info['address1'];
					isset($receiver_info['address2']) and $data['address2'] = $receiver_info['address2'];
					isset($receiver_info['address3']) and $data['address3'] = $receiver_info['address3'];
					isset($receiver_info['tel1']) and $data['tel1'] = $receiver_info['tel1'];
					isset($receiver_info['tel2']) and $data['tel2'] = $receiver_info['tel2'];
					isset($receiver_info['tel3']) and $data['tel3'] = $receiver_info['tel3'];
					isset($receiver_info['fax1']) and $data['fax1'] = $receiver_info['fax1'];
					isset($receiver_info['fax2']) and $data['fax2'] = $receiver_info['fax2'];
					isset($receiver_info['fax3']) and $data['fax3'] = $receiver_info['fax3'];
			}
		}

		/*****************************
		 * 予約商品の場合はセッションの削除
		 * と予約レコードの処理をする
		 *****************************/
		if ($reserve_dt)
		{
			$reserve_ids = array();
			foreach ($reserve_dt as $val)
			{
				$reserve_ids = array_merge($reserve_ids, $val);
			}
			$update_data = array(
				'status_cd' => Model_Dt_Reserve::PURCHASE_COMPLATE,
				'purchase_id' => $purchase_info['purchase_id'],
			);
			Model_Dt_Reserve::update_by_ids($update_data, $reserve_ids);
		}

		//js/common.js
		Asset::js(array($this->assets_path.'/js/cart.js'), array(), 'add_js', false);
		Asset::css(array($this->assets_path.'/css/cart.css',$this->assets_path.'/css/common.css'), array(), 'add_css', false);
		$this->template->set_global('data', $data);
		$this->template->content = View::forge('purchase/complete');
	}

	/**
	 * 日時指定セレクトボックスを画面に表示する時に使用する
	 * 
	 * @param int area_cd
	 * @parma int sending_cd
	 * @return mix result array
	 */
	private function get_date_specified ($area_cd, $sending_cd)
	{
		$specified_end_day = Model_Mt_Maxdatespecified::get_day();//最大日付指定
		$result = array(
			'date'   => false,
			'time'   => false,
		);

		if (empty($area_cd) || empty($sending_cd))
		{
			return $result;
		}

		//発送方法とエリアコードをキーに日付マスタを取得する
		$mt_date = Model_Mt_Datespecified::select_by_sending_cd_and_area_cd($sending_cd, $area_cd);

		//日付表示データを取得する
		if (isset($mt_date['from_day']))
		{
			$result['date'] = CalenderHelper::get_designated_date_array($mt_date['from_day'], $specified_end_day);
		}
		else
		{
			return $result;
		}

		//時間帯指定データを取得する
		$time_mt = Model_Dt_Timespecified::select_by_date_id_join_mt ($mt_date['date_id']);
		if ($time_mt)
		{
			//時間帯を指定しないを追加する
			array_unshift($time_mt ,array('id' => 0,'name'=>'指定しない'));
			$result['time'] = $time_mt;
		}
		return $result;
	}

	/**
	 * post get_date_specified
	 * 
	 * 日にち指定部品を取得するメソッド
	 * @param sending_cd, area_cd
	 * @return mix result json
	 */
	public function post_get_date_specified ()
	{
		$result = array(
			'result' => true,
			'date'   => false,
			'time'   => false,
			'th_str' => '',
		);

		$specified_end_day = Model_Mt_Maxdatespecified::get_day();//最大日付指定
		$area_cd           = Input::post('area_cd');//エリアコード
		$sending_cd        = Input::post('sending_cd');//発送方法コード

		//エリアコードと発送方法コードが渡されなかった場合はfalseを返却
		if (empty($area_cd) || empty($sending_cd))
		{
			$result['result'] = false;
			return $this->response($result);
		}

		//カートに配達日時指定不可商品が入っていた場合はfalseを返却する
		$cart_dt = array();
		if (($cart_dt = Session::get('cart_dt', false)) && count($cart_dt)>0)
		{
			for ($i=0; $i<count($cart_dt); $i++)
			{
				$item_dt = Model_Mt_Item::select_item_by_id($cart_dt[$i]['id'], true, false);
				if (isset($item_dt['no_specified']) && $item_dt['no_specified'])
					return $this->response($result);
			}
		}
		else
		{
			return $this->response($result);
		}

		//発送方法とエリアコードをキーに日付マスタを取得する
		$mt_date = Model_Mt_Datespecified::select_by_sending_cd_and_area_cd($sending_cd, $area_cd);

		//日付表示データを取得する
		if (isset($mt_date['from_day']))
		{
			$result['date'] = CalenderHelper::get_designated_date_array($mt_date['from_day'], $specified_end_day);
		}
		else
		{
			$result['result'] = false;
		}

		//時間帯指定データを取得する
		$time_mt = Model_Dt_Timespecified::select_by_date_id_join_mt ($mt_date['date_id']);
		if ($time_mt)
		{
			//時間帯を指定しないを追加する
			array_unshift($time_mt ,array('id' => 0,'name'=>'指定しない'));
		}
		$result['time'] = $time_mt;

		//th に設定する項目名を決定する
		if ($result['result'] && $result['date'] && $result['time'])
		{
			$result['th_str'] = 'お届け希望日時';
		}
		else if ($result['result'] && $result['date'])
		{
			$result['th_str'] = 'お届け希望日';
		}
		return $this->response($result);
	}

	/***********************************************
	 * 以下のメソッドはこのcontrollerで使用する処理メソッド
	 ***********************************************/
	/**
	 * 購入確認メール送信メソッド
	 * 
	 */
	private function send_confirm_mail ()
	{
		//payment_cd
		//各種セッションを取得する
		$data['purchase_info']   = Session::get('purchase_info',  false);
		$data['receiver_info']   = Session::get('receiver_info',  false);
		$data['cart_dt']         = Session::get('cart_dt',        false);
		$data['sending_info']    = Session::get('sending_info',   false);
		$data['point_rate_info'] = Session::get('point_rate_info',false);
		$data['sending_cd']      = Session::get('sending_cd',     false);
		$data['total_info']      = Func::get_cart_total_info($data['cart_dt']);

		//check session
		$check_result = true;

		//取得したsessionをチェックする
		if ($data['purchase_info'])
		{
			//お届け先が購入者と違うのにセッションが無い
			//カート情報/発送可能方法情報/選択発送方法コード/ポイントレート情報のどれかがかけた場合
			if(
				($data['purchase_info']['receiver'] === 'other' && !$data['receiver_info']) ||
				(!$data['cart_dt'] || !$data['sending_info'] || !$data['sending_info'] || !$data['sending_cd'] || !$data['point_rate_info'])
			)
			{
				$check_result = false;

				/************
				 * エラー記録
				 ************/
				$error_msg = '';

				if ($data['purchase_info']['receiver'] === 'other' && !$data['receiver_info'])
				{
					$error_msg = '購入前エラー1';
				}
				elseif (!$data['cart_dt'])
				{
					$error_msg = '購入前エラー2';
				}
				elseif (!$data['sending_info'])
				{
					$error_msg = '購入前エラー3';
				}
				elseif (!$data['sending_cd'])
				{
					$error_msg = '購入前エラー4';
				}
				elseif (!$data['point_rate_info'])
				{
					$error_msg = '購入前エラー5';
				}
				else
				{
					$error_msg = '購入前エラー6';
				}
				Func::error_log($error_msg, true);
			}
		}
		else
		{
			//購入情報が無いエラー
			Func::error_log('購入情報が無いエラー', true);
			$check_result = false;
		}
		//チェック判定
		if (!$check_result)
		{
			Func::error_log('購入処理は成功(確定)したがメール処理直前で失敗', true);
		}

		//カート情報に各、特殊選択情報を結合する
		for ($i=0; $i<count($data['cart_dt']); $i++)
		{
			/******************************************************
			 * レプリケーションが完了してい可能性がある為、masterから取得する
			 ******************************************************/
			$purchase_special_select_dt = 
				DbHelper::query_exec(
					Model_Dt_Purchasespecialselect::get_query_select_by_purchase_id_and_item_id($data['purchase_info']['purchase_id'], $data['cart_dt'][$i]['id']),
					false,
					false,
					Fuel::DB_MASTER
				);

			if (!empty($purchase_special_select_dt))
				$data['cart_dt'][$i]['special_select'] = $purchase_special_select_dt;
		}

		/******************
		 * ここからメール処理
		 ******************/
		//選択された発送方法データを整形する
		$sending_info_new = array();

		for ($i=0; $i<count($data['sending_info']['postages']); $i++)
		{
			if ((Int)$data['sending_info']['postages'][$i]['id'] === (Int)$data['sending_cd'])
			{
				//Debug::dump($data['sending_info']['postages'][$i]);exit;
				$sending_info_new = $data['sending_info']['postages'][$i];

				//無料の場合
				if (isset($data['sending_info']['is_free']) && $data['sending_info']['is_free'])
					$sending_info_new['postage'] = 0;

				//日にち指定
				if (isset($data['purchase_info']['date_specified']) && $data['purchase_info']['date_specified'])
				{
					$sending_info_new['date_specified'] = 
						substr($data['purchase_info']['date_specified'], 0,4).'年'.
						substr($data['purchase_info']['date_specified'], 4,2).'月'.
						substr($data['purchase_info']['date_specified'], 6,2).'日';
				}
				else
				{
					$sending_info_new['date_specified'] = false;
				}



				//時間帯指定
				if (isset($data['purchase_info']['time_specified']) && $data['purchase_info']['time_specified'])
				{
					$time_mt = Model_Mt_Timespecified::select_by_id((Int)$data['purchase_info']['time_specified']);
					$sending_info_new['time_specified'] = $time_mt['name'];
				}
			}
		}

		$data['sending_info'] = $sending_info_new;
		//login status
		$data['is_login'] = $this->is_login;
		//登録日
		$data['date'] = date('Y年m月d日H時i分s秒');
		//adminとuserにメール送信
		$mail_info = array(
				'user' => array(
						'param' => array(
								'to'        => $data['purchase_info']['mail_purchase'],
								'subject'   => Func::get_mail_subject(Config::get('custom_config.mail.title.purchase.user')),
								'view'      => 'mail/user/purchase/user',
								'from'      => Config::get('custom_config.mail.from.contact'),
								'from_name' => Config::get('custom_config.shop_name'),
						),
						'email_data' => $data
				),
				'admin' => array(
						'param' => array(
								'to'        => Config::get('custom_config.mail.from.admin'),
								'subject'   => Config::get('custom_config.mail.title.purchase.admin'),
								'view'      => 'mail/user/purchase/admin',
								'from'      => Config::get('custom_config.mail.from.contact'),
								'from_name' => Config::get('custom_config.shop_name'),
						),
						'email_data' => $data
				)
		);
		$mail_info = Func::prepare_forSendmail($mail_info);
		//メールを非同期で送信する
		$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
		exec($command, $output, $return_code);
	}

	/**
	 * redirect_gmo
	 * 
	 * gmoにリダイレクトさせるメソッド
	 * errorがあった場合はerror画面を表示
	 */
/*
	private function redirect_gmo ($post_array)
	{
		//設定パラメータ
		$post_array = array (
			'user_name'     => '船寄良登 test',          //ユーザーメール
			'user_mail_add' => 'y.funayose@sp-k.co.jp', //ユーザー名
			'user_id'       => '103',                   //ユーザーid
			'price'         => '10000',                 //決済総
			'process_code'  => '1',                     //処理区分(1=>初回課金,2=>登録済み課金/登録と同時の課金,4=>登録内容変更)
			'order_number'  => '28'                     //オーダー番号(ユニーク)
		);

		$gmo_response = Payment::get_redirect_url($post_array);

		if (
			isset($gmo_response['result']) &&
			isset($gmo_response['redirect']) && 
			(Int)$gmo_response['result'] === (Int) 1 &&
			!empty($gmo_response['redirect'])
		)
		{
			//--- 決済画面に遷移させる
			Response::redirect($gmo_response['redirect']);
		}

		//--- errorの場合
		$gmo_response['uid'] = $post_array['user_id'];
		//不要な項目をunsetする
		unset($gmo_response['memo1']);
		unset($gmo_response['memo2']);
		Func::error_log($gmo_response);
		//Func::error_log();
	}
 */

	/**
	 * action callback
	 * 
	 * 
	 * 外部決済コールバックメソッド
	 * 
	 */
	public function action_callback ()
	{
		//postデータ取得
		$get_data = Input::get();
		if (isset($get_data['result']) && !empty($get_data['result']))
		{
			//購入処理クラスインスタンス取得
			$Purchase = Purchase::forge();
			//レスポンスのset
			$Purchase->set_gmo_response($get_data);

			if ((Int)$get_data['result'] === 1)
			{
				//--- 成功ステータスが返ってきた時
				//登録データの確定
				$Purchase->commit_gmo_purchase();

				if ($Purchase->get_error_flg())
				{
					parent::delete_purcahse_sessions();
					Func::show_overlay_message ('不正なパラメータです');
				}

				//メール送信
				$this->send_confirm_mail();
				Response::redirect($this->base_url.'/user/purchase/complete');
			}
			else if ((Int)$get_data['result'] === 0)
			{
				//--- 失敗ステータスが返ってきた時(取消)
				$Purchase->set_purchase_id ((Int)$get_data['memo1']);
				$Purchase->roll_back_purchase(Purchase::CANCEL_AGNET_BAD_RESPONSE);
			}
			Func::error_log('コールバックメソッド成功も失敗も判定出来なかった', true);
			Func::redirect_error(null, null);
		}

//エラーの時
//Variable #1:
//  (Array, 6 elements) ↵
//     result (String): "0" (1 characters)
//     err_code (String): "604" (3 characters)
//     err_detail (String): "オーダー番号が既に登録されています" (51 characters)
//     memo1 (String): "42" (2 characters)
//     memo2 (String): "" (0 characters)
//     uid (String): "8" (1 characters)

/**
 //決済成功の時
APPPATH/classes/controller/user/purchase.php @ line: 785
Variable #3:
  (Array, 4 elements) ↵
     trans_code (String): "447809" (6 characters)
     user_id (String): "8" (1 characters)
     result (String): "1" (1 characters)
     order_number (String): "42" (2 characters)
 */
	}

	/**
	 * alert_cart_page
	 * 
	 * 購入処理の途中で問題がある場合はこのメソッドから
	 * カートページにredirectします
	 * カートページではオーバーレイでメッセージを出力します。
	 * 
	 * @return none
	 * 
	 */
	private function alert_cart_page ()
	{
		if (!empty($this->error_message))
		{
			Session::set_flash('overlay_message', $this->error_message);
			Response::redirect($this->base_url.'/cart/index');
		}
	}

	/**
	 * このコントローラーの有効期限をチェックする
	 * //未使用
	 */
/**
	private function check_dete_time ()
	{
		$result = false;
		$session_key = 'purcahse_start';
		//このcontrollerに初めて来た時間を取得する
		$started = Session::get($session_key, false);

		if ($started && ((time() - $started ) > self::AVAIL_PURCASE_TERM))
		{
			//有効期限が超えている場合
			Session::delete($session_key);
			$this->delete_all_sessions(true);
		}
		else
		{
			//超えていない場合または設定がなかった場合
			Session::set($session_key, time());
		}
	}
 */
	/**
	 * get_sending_message_array
	 * 
	 * 発送方法注意メッセージの作成
	 * 
	 * @param mix $cart_dt
	 */
	private function get_sending_message_array ($cart_dt)
	{
		$result = array();

		for ($i=0; $i<count($cart_dt); $i++)
		{
			$mt_item = Model_Mt_Item::select_item_by_id($cart_dt[$i]['id']);
			if (isset($mt_item['sending_ms_ids']) && !empty($mt_item['sending_ms_ids']))
			{
				$message = Model_Mt_Sendingmessage::select_by_ids($mt_item['sending_ms_ids']);
				$result+=$message;
			}
		}
		return $result;
	}
}