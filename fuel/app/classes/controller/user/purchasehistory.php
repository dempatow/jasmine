<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Purchasehistory Controller.
 * 
 * 購入履歴
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_User_Purchasehistory extends Common
{
	public $template = 'template/template_common';

	public function before()
	{
		parent::before();

		if (!$this->is_login)
			Func::redirect_error (null, 'ログインしていません');

		if (Input::is_ajax())
			return;

		//基本viewの作成
		//viewの作成
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');
		$this->template->mypage_header = View::forge('layout/mypage_header');
		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
		//css
		Asset::css(array($this->assets_path.'/css/mypage.css', $this->assets_path.'/css/common.css'), array(), 'add_css', false);
	}

	/**
	 * index action
	 * 
	 * 購入履歴
	 * 
	 * @access public
	 */
	public function action_index ($page_no = 1)
	{
		//pager定数
		$max_list_cnt     = 7;//listページの表示件数(奇数でお願いします)
		$disp_item_cnt    = 20;//1ページの最大表示件数

		//pagerその他
		$record_cnt       = 0;//レコード合計件数
		$page_count       = 0;//ページ総数
		$db_offset        = 0;//db offset

		//その他
		$purchase         = array();//dt_purchasehistoryレコードdt用
		$uid              = (Int)CustomAuth::get_login_info()['id'];//uid
		$mt_status        = Model_Mt_Purchasestatus::select();//購入履歴ステータスマスタ
		$input_data       = Input::post();
		$query_string     = '?';

		if (count($input_data)===0)
		{
			$get_array = Input::get();
			isset($get_array['status_cd']) and $input_data['status_cd'] = $get_array['status_cd'];
			isset($get_array['selected_year']) and $input_data['selected_year'] = $get_array['selected_year'];
		}

		/******************************************
		 * レコード合計件数/最も遅い購入年/購入月を取得する
		 ******************************************/
		$base_data = Model_Dt_Purchasehistory::select_by_history_base_data($uid);
		//レコード合計件数
		$record_cnt = $base_data['count'];
		//最大日にち
		if (!is_null($base_data['max']) || empty($base_data['max']))
		{
			$data['max_year'] = substr($base_data['max'], 0,4);
			//最小日にち
			if ((!is_null($base_data['min']) || empty($base_data['min']))&& (isset($data['max_year']) && $data['max_year'] !== substr($base_data['min'], 0,4)))
			{
				$data['min_year'] = substr($base_data['min'], 0,4);
			}
		}

		if (isset($input_data['status_cd']) || (isset($input_data['selected_year']) && !empty($input_data['selected_year'])))
			$query_search_count = Model_Dt_Purchasehistory::get_query_select_count_for_searched_history($uid);

		$query_search_data  = Model_Dt_Purchasehistory::get_query_select_by_uid($uid);

		/***********
		 * 処理状況欄
		 ***********/
		$status_radio_array = array(
			0 => array(
				'value' => 0,
				'disp' => '全て'
			),
			Purchase::ALREADY_SENDING => array(
				'value' => Purchase::ALREADY_SENDING,
				'disp' => $mt_status[Purchase::ALREADY_SENDING]['name'],
			),
			Purchase::NEW_PURCHASE => array(
				'value' => Purchase::NEW_PURCHASE,
				'disp' => $mt_status[Purchase::NEW_PURCHASE]['name'],
			),
		);

		if (isset($input_data['status_cd']) && !empty($input_data['status_cd']) && isset($status_radio_array[$input_data['status_cd']]))
		{
			$status_radio_array[$input_data['status_cd']]['checked'] = true;
			$query_search_count->and_where('status_cd', '=', $input_data['status_cd']);
			$query_search_data->and_where('status_cd', '=', $input_data['status_cd']);
			$query_string.='status_cd='.$input_data['status_cd'].'&';
		}
		else
		{
			$status_radio_array[0]['checked'] = true;
		}

		/***********
		 * 年selectbox
		 ***********/
		if (isset($input_data['selected_year']) && !empty($input_data['selected_year']))
		{
			$data['selected_year'] = $input_data['selected_year'];

			$start_date_time = $input_data['selected_year'].'-01-01 00:00:00';
			$end_date_time   = $input_data['selected_year'].'-12-31 11:59:59';
			$query_search_count->and_where('created', 'between', array($start_date_time, $end_date_time));
			$query_search_data->and_where('created', 'between', array($start_date_time, $end_date_time));
			$query_string.='selected_year='.$input_data['selected_year'].'&';
		}

		//ページ件数取得
		if (isset($query_search_count))
			$record_cnt = DbHelper::query_exec ($query_search_count, true)['count'];

		$page_count = (Int)ceil($record_cnt / $disp_item_cnt);
		$max_list_cnt>$page_count and $max_list_cnt=$page_count;

		//パラメータが不正の時は現在ページに1を設定する
		((Int)$page_no <1 || (Int)$page_no > $page_count) and $page_no = 1;

		//db offset 取得
		(Int)$page_no !== 1 and $db_offset = ($page_no-1) * $disp_item_cnt;

		//レコード取得
		$record_data = DbHelper::query_exec($query_search_data->offset($db_offset)->limit($disp_item_cnt)->order_by('created', 'desc'));

		//purchase status マスタを結合させる
		for ($i=0; $i<count($record_data); $i++)
		{
			if (isset($record_data[$i]['status_cd']))
			{
				$record_data[$i]['status_cd'] = $mt_status[$record_data[$i]['status_cd']];
			}
		}

		//画面にセットするもの
		$data['record_data']  = $record_data;
		$data['page_count']   = $page_count;
		$data['page_no']      = $page_no;
		$data['max_list_cnt'] = $max_list_cnt;
		$data['status_radio_array'] = $status_radio_array;

		if ($query_string !== "?")
			$this->template->set_global('query_string',$query_string,false);

		$this->template->set_global('data', $data);
		$this->template->content = View::forge('purchasehistory/index');
	}

	/**
	 * detail action
	 * 
	 * 購入履歴
	 * 
	 * @access public
	 */
	public function action_detail ($purchase_id = null)
	{
		$uid      = (Int)CustomAuth::get_login_info()['id'];//uid
		$data     = array ();
		$referrer = Input::referrer();
		$data['back_url'] = !empty($_SERVER['QUERY_STRING'])? $referrer.'?'.$_SERVER['QUERY_STRING']: $referrer;

		/**********
		 * 購入履歴
		 **********/
		$data['dt_purchase'] = Model_Dt_Purchasehistory::select_by_id_and_uid($purchase_id, $uid);

		//レコードなしの場合はエラーページへ
		if (empty($data['dt_purchase']) || isset($data['dt_purchase']['status_cd']) && (Int)$data['dt_purchase']['status_cd']>=100)
			Func::redirect_error (null);

		//支払い方法
		$mt_payment = Model_Mt_Payment::select_payment();
		isset($data['dt_purchase']['payment_cd']) && isset($mt_payment[$data['dt_purchase']['payment_cd']]['name']) and $data['dt_purchase']['payment_name'] = $mt_payment[$data['dt_purchase']['payment_cd']]['name'];

		//ステータス
		$mt_purchase_status = Model_Mt_Purchasestatus::select();
		isset($data['dt_purchase']['status_cd']) && isset($mt_purchase_status[$data['dt_purchase']['status_cd']]['name']) and $data['dt_purchase']['status_name'] = $mt_purchase_status[$data['dt_purchase']['status_cd']]['name'];

		//発送方法
		$mt_sending = Model_Mt_Sending::select_sending_recursive();
		isset($data['dt_purchase']['sending_cd']) && isset($mt_sending[$data['dt_purchase']['sending_cd']]['name']) and $data['dt_purchase']['sending_name'] = $mt_sending[$data['dt_purchase']['sending_cd']]['name'];
		$mt_sending_detail = Model_Mt_Sendingdetail::select();
		isset($data['dt_purchase']['sending_cd']) && isset($mt_sending_detail[$data['dt_purchase']['sending_cd']]['tracking_url']) and $data['dt_purchase']['tracking_url'] = $mt_sending_detail[$data['dt_purchase']['sending_cd']]['tracking_url'];

		if (isset($data['dt_purchase']['time_specified']) && !empty($data['dt_purchase']['time_specified']))
			$data['dt_purchase']['time_specified'] = Model_Mt_Timespecified::select()[$data['dt_purchase']['time_specified']]['name'];

		/**********
		 * 商品履歴
		 **********/
/**
Variable #1:
  (Array, 12 elements) ↵
     id (String): "96" (2 characters)
     item_id (String): "10026" (5 characters)
     purchase_id (String): "69" (2 characters)
     name (String): "単品特殊選択アイテム" (30 characters)
     count (String): "3" (1 characters)
     price (String): "100" (3 characters)
     point (String): "0" (1 characters)
     rate (String): "0" (1 characters)
     review_flg (String): "0" (1 characters)
     del_flg (String): "0" (1 characters)
     created (String): "2014-11-11 19:19:30" (19 characters)
     updated (String): "2014-11-11 19:19:30" (19 characters)

 */
		$dt_purchaseitemhistory = Model_Dt_Purchaseitemhistory::select_by_purchase_id($purchase_id);
		for ($i=0; $i<count($dt_purchaseitemhistory); $i++)
		{
			//特殊表示データを結合する
			$purchase_special_select_dt = 
				Model_Dt_Purchasespecialselect::select_by_purchase_id_and_item_id(
					$dt_purchaseitemhistory[$i]['purchase_id'],
					$dt_purchaseitemhistory[$i]['item_id']
				);

			if (!empty($purchase_special_select_dt))
				$dt_purchaseitemhistory[$i]['special_select'] = $purchase_special_select_dt;
		}
		$data['dt_item'] = $dt_purchaseitemhistory;


		/**********
		 * 住所履歴
		 **********/
		$data['dt_address'] = Model_Dt_Purchaseaddresshistory::select_by_purchase_id($purchase_id);
		//都道府県
		$mt_state = Model_Mt_State::get_state();
		isset($data['dt_address'][1]['state_cd']) && isset($mt_state[$data['dt_address'][1]['state_cd']]) and $data['dt_address'][1]['state_name'] = $mt_state[$data['dt_address'][1]['state_cd']]['name'];
		isset($data['dt_address'][2]['state_cd']) && isset($mt_state[$data['dt_address'][2]['state_cd']]) and $data['dt_address'][2]['state_name'] = $mt_state[$data['dt_address'][2]['state_cd']]['name'];

		$this->template->set_global('data', $data);
		$this->template->content = View::forge('purchasehistory/detail');
	}
}