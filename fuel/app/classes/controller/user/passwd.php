<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The User Passwd Controller
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_User_Passwd extends Common
{
	public $template = 'template/template_common';

	public function before()
	{
		parent::before();

		//基本viewの作成
		//viewの作成
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');

		if ($this->is_login)
			$this->template->mypage_header = View::forge('layout/mypage_header');

		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));

		//formの切り分け用
		$this->template->set_global('form_status', 2);

		//css
		Asset::css(array($this->assets_path.'/css/mypage.css'), array(), 'add_js', false);
		//js
		//Asset::js(array('http://',), array(), 'add_js', false);
	}

	/**
	 * action change
	 *
	 * パスワード変更
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_change()
	{
		$form_data = Input::post();

		if ($form_data && \Security::check_token())
		{
			//validation
			$validation = Validation::forge();
			$validation->add_model('Model_Dt_User');

			if (
				$validation->run($form_data, array('current_passwd', 'new_passwd', 'new_passwd_confirm'))
			)
			{
				$login_info = CustomAuth::get_login_info();

				if ($login_info)
				{
					//新しいpasswdにupdateする
					if(!Model_Dt_User::update_passwd($login_info['id'], Auth::instance()->hash_password($form_data['new_passwd'])))
						Func::redirect (Config::get('custom_config.url.http_domain').'/errors/error', '処理に失敗しました。');

					//登録完了メール送信
					//$dt_user['new_passwd']  = $new_pw;
					$login_info['regist_date'] = date('Y年m月d日H時i分s秒');

					//adminとuserにメール送信
					$mail_info = array(
							'user' => array(
									'param' => array(
											'to'        => $login_info['mail'],
											'subject'   => Func::get_mail_subject(Config::get('custom_config.mail.title.change_passwd.user')),
											'view'      => 'mail/user/passwd/change/user',
											'from'      => Config::get('custom_config.mail.from.contact'),
											'from_name' => Config::get('custom_config.mail.from_name'),
									),
									'email_data' => $login_info
							),
							'admin' => array(
									'param' => array(
											'to'        => Config::get('custom_config.mail.from.admin'),
											'subject'   => Config::get('custom_config.mail.title.change_passwd.admin'),
											'view'      => 'mail/user/passwd/change/admin',
											'from'      => Config::get('custom_config.mail.from.contact'),
											'from_name' => Config::get('custom_config.mail.from_name'),
									),
									'email_data' => $login_info
							)
					);
					//Debug::dump($mail_info);exit;

					$mail_info = Func::prepare_forSendmail($mail_info);
					//メールを非同期で送信する
					$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";

					exec($command, $output, $return_code);

					Model_Dt_Useractionhistory::insert_history(104, $login_info['id']);

					Session::set_flash('overlay_message', 'パスワードの変更が完了しました');
					Response::redirect(Config::get('custom_config.url.https_domain').'/user/mypage');
				}
			}
			else
			{
				/*
				 * validation 失敗
				 */
				$this->template->set_global('val', $validation);
			}
		}

		/***********
		 * form表示
		 ***********/
		$this->template->content = View::forge('passwd/change');
		return;
	}

	/**
	 * reset action
	 *
	 * reset
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_reset()
	{
		$form_data = Input::post();
		//validation
		$validation = Validation::forge();
		$validation->add_model('Model_Dt_User');

		if (\Security::check_token())
		{
			if ($validation->run($form_data, array('repwd_mail')))
			{
				//ログアウトさせる
				CustomAuth::logout(true);

				//mailをキーにdt_userを引く
				$dt_user = DbHelper::query_exec(
						Model_Dt_User::get_query_select_user_by_mail($form_data['repwd_mail']),
						true,
						false
				);

				if (count($dt_user)>0)
				{
					/************************
					 * パスワードのリセット処理
					 ************************/
					$pw_len = 10;
					$new_pw = "";
					$data = Input::post();
					$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
					for ($i=0; $i<$pw_len; $i++)
					{
						$new_pw .= $chars[mt_rand(0, strlen($chars)-1)];
					}
					//新しいpasswdにupdateする
					if(!Model_Dt_User::update_passwd($dt_user['id'], Auth::instance()->hash_password($new_pw)))
						Func::redirect_error ();



					//登録完了メール送信
					$dt_user['new_passwd'] = $new_pw;
					$dt_user['regist_date'] = date('Y年m月d日H時i分s秒');

					//adminとuserにメール送信
					$mail_info = array(
							'user' => array(
									'param' => array(
											'to'        => $dt_user['mail'],
											'subject'   => Func::get_mail_subject(Config::get('custom_config.mail.title.reset_passwd.user')),
											'view'      => 'mail/user/passwd/reset/user',
											'from'      => Config::get('custom_config.mail.from.contact'),
											'from_name' => Config::get('custom_config.mail.from_name'),
									),
									'email_data' => $dt_user
							),
							'admin' => array(
									'param' => array(
											'to'        => Config::get('custom_config.mail.from.admin'),
											'subject'   => Config::get('custom_config.mail.title.reset_passwd.admin'),
											'view'      => 'mail/user/passwd/reset/admin',
											'from'      => Config::get('custom_config.mail.from.contact'),
											'from_name' => Config::get('custom_config.mail.from_name'),
									),
									'email_data' => $dt_user
							)
					);
					$mail_info = Func::prepare_forSendmail($mail_info);
					//メールを非同期で送信する
					$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";

					exec($command, $output, $return_code);
					Model_Dt_Useractionhistory::insert_history(104, $dt_user['id']);

					Session::set_flash('overlay_message', 'パスワードを初期化し、メールにて送信させていただきました。');
					Response::redirect(Config::get('custom_config.url.http_domain'));
				}
			}
			else
			{
				if(isset($form_data['repwd_mail']))
					$this->template->set_global('data', array('repwd_mail' => $form_data['repwd_mail']));

				$this->template->set_global('val', $validation);
			}
		}
		$this->template->content = View::forge('passwd/reset');
	}
}