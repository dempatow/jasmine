<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Static Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_Static extends Common
{
	public $template = 'template/template_common';
	private $main_cont = null;

	public function before()
	{
		parent::before();
		//基本viewの作成
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');
		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
		Asset::css(array($this->assets_path.'/css/common.css',$this->assets_path.'/css/otherpage.css'), array(), 'add_css', false);
	}

	/**
	 * info action
	 * ご利用案内
	 */
	public function action_info ()
	{
		$free_sending = Model_Mt_Freesending::select_freesending();
		if (!empty($free_sending) && $free_sending > 0)
			$this->template->set_global('free_sending', $free_sending);

		$this->template->set_global('content', View::forge('static/info'));
	}

	/**
	 * law action
	 * 特定商取引法に基づく表記
	 */
	public function action_law()
	{
		$this->template->set_global('content', View::forge('static/law'));
	}

	/**
	 * contact action
	 * お問い合わせ
	 */
	public function action_contact()
	{
		$this->template->set_global('content', View::forge('static/contact'));
	}
}