<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Cart Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_Cart extends Common
{
	public $template = 'template/template_common';

	public function before()
	{
		parent::before();

		if (Input::is_ajax())
		{
			//購入系のセッションを削除する
			parent::delete_purcahse_sessions(true);
		}
		else
		{
			CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
		}
	}

	/**
	 * reserve
	 * 
	 * 予約メソッド
	 * 
	 * 1. ログインユーザー => 予約処理後、マイページにredirect
	 * 2. 非ログイン => リファラーのページにリダイレクトし、ログインフォームと新規登録のオーバーレイを表示させる
	 */
	public function action_reserve()
	{
		$input_data = Input::post();
		if (!Security::check_token() && Model_Mt_Item::validation_reserve($input_data))
			Func::redirect_error ();

		if(
			$this->is_login &&
			$input_data['reserve_id'] = Model_Dt_Reserve::reserve_item($this->uid, $input_data['item_id'], $input_data['count'])
		)
		{
			Func::send_reserve_mail($input_data);
			Func::show_overlay_message('商品の予約が完了しました', Config::get('custom_config.url.https_domain').'/user/mypage');
		}
		else if (!$this->is_login)
		{
			/**
			 * 非ログイン時
			 */
			//Session::set_flash('reserve_no_login', $input_data);
			Session::set('login_parts', $input_data);
			Session::set_flash('show_overlaylogin', true);
			Response::redirect(Input::referrer());
		}
		Func::redirect_error('予約の処理に失敗しました。');
	}

	/**
	 * delete_item
	 * 
	 * 予約を削除します
	 * @return bool
	 */
	public function post_delelte_reserve ()
	{
		//get post data
		$input_data = Input::post();
		$res = array(
			'input'  => $input_data,
			'result' => false,
		);

		/*************
		 * 各種チェック
		 *************/
		if (
			!$this->is_login  ||
			empty($this->uid) ||
			!isset($input_data['reserve_id'])||
			!isset($input_data['token'])     ||
			!Security::check_token($input_data['token'])
		)
			return $this->response($res);

		$dt_reserve = Model_Dt_Reserve::select_by_id_and_uid($input_data['reserve_id'], $this->uid);
		if (count($dt_reserve) == 0)
			return $this->response($res);

		$stock_result = Model_Dt_Stock::add_stock($dt_reserve['item_id'], $dt_reserve['count']);

		/*****************************
		 * 在庫加算失敗時はメールを送信する
		 *****************************/
		if (!$stock_result)
			Func::error_log('予約取り消し時の在庫加算に失敗しました', true);

		$db_result = Model_Dt_Reserve::update_del_flg($input_data['reserve_id'], Model_Dt_Reserve::CANCEL_USER);
		if (!empty($db_result) && $db_result>0)
			$res['result'] = true;

		return $this->response($res);
	}

	/**
	 * send_reserve_mail
	 * 
	 * 
	 */
	public function send_reserve_mail ($input_data)
	{
		$item_mt = Model_Mt_Item::select_item_by_id($input_data['item_id'], true);
		$data = array();
		$data = CustomAuth::get_login_info();
		$data['name']         = $item_mt['name'];
		$data['count']        = $input_data['count'];
		$data['reserve_id']   = $input_data['reserve_id'];
		$data['item_id']      = $input_data['item_id'];
		$data['date']         = date('Y年m月d日H時i分s秒');
		$data['selling_date'] = $item_mt['selling_date'];

		//adminとuserにメール送信
		$mail_info = array(
				'user' => array(
						'param' => array(
								'to'        => $data['mail'],
								'subject'   => Func::get_mail_subject(Config::get('custom_config.mail.title.reserve.user')),
								'view'      => 'mail/user/reserve/user',
								'from'      => Config::get('custom_config.mail.from.contact'),
								'from_name' => Config::get('custom_config.shop_name'),
						),
						'email_data' => $data
				),
				'admin' => array(
						'param' => array(
								'to'        => Config::get('custom_config.mail.from.admin'),
								'subject'   => Config::get('custom_config.mail.title.reserve.admin'),
								'view'      => 'mail/user/reserve/admin',
								'from'      => Config::get('custom_config.mail.from.contact'),
								'from_name' => Config::get('custom_config.shop_name'),
						),
						'email_data' => $data
				)
		);
		$mail_info = Func::prepare_forSendmail($mail_info);
		//メールを非同期で送信する
		$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
		exec($command, $output, $return_code);
	}

	/**
	 * index
	 * 
	 * cart画面の表示
	 * @return view response
	 */
	public function action_index ()
	{
		/************
		 * cart info
		 ************/
		$cart_dt           = Session::get('cart_dt', null);
		$session_reserve   = Session::get('reserve_dt', null);
		$special_select_dt = Session::get('special_select_dt', null);

		/******************************
		 * 予約商品がカートに入っている場合
		 ******************************/
		if ($this->is_login && !empty($session_reserve))
		{
			for ($i=0; $i<count($cart_dt); $i++)
			{
				if (isset($session_reserve[$cart_dt[$i]['id']]))
					$cart_dt[$i]['is_reserve'] = true;
			}
		}

		/******************************
		 * 特殊選択sessionがあった場合
		 ******************************/
		if (!empty($special_select_dt))
		{
			//cart_dt
			for ($i=0; $i<count($cart_dt); $i++)
			{
				$special_select_tmp = array();
				foreach ($special_select_dt as $key => $val)
				{
					if ((Int)$cart_dt[$i]['id'] === (Int)$val['item_id'])
					{
						//--- 特殊選択keyの取得
						$mt_key = Model_Mt_Specialselectkey::select_by_id($val['key']);
						$val['key_name'] = isset($mt_key['name'])? $mt_key['name']: false;

						//--- 特殊選択valueの取得
						$mt_value = Model_Mt_Specialselectvalue::select_by_id($val['value']);
						$val['value_name'] = isset($mt_value['value'])? $mt_value['value']: false;

						$special_select_tmp[] = $val;
						unset($special_select_dt[$key]);
					}
				}
				if (count($special_select_tmp)>0)
					$cart_dt[$i]['special_select'] = $special_select_tmp;
			}
		}

		$this->template->set_global('cart_dt', $cart_dt);

		/**********
		 * 合計情報
		 **********/
		if (count($cart_dt)>0)
		{
			$total_info = Func::get_cart_total_info($cart_dt);
			$this->template->set_global('total_info', $total_info);
		}

		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');
		$this->template->content = View::forge('cart/index');
		Asset::js(array($this->assets_path.'/js/cart.js'), array(), 'add_js', false);
		Asset::css(array($this->assets_path.'/css/cart.css'), array(), 'add_css', false);
	}

	/**
	 * change_count
	 */
	public function post_change_count ()
	{
		$result = array(
			'result'  => false,
			'input'   => Input::post('count'),
			'item_id' => (Int)Input::post('item_id'),
			'error'   => '',
		);

		if (empty($result['item_id']))
		{
			$result['error'] = '処理に失敗しました';
		}

		if (isset($result['input']) && is_numeric($result['input']) && $result['input'] != '')
		{
			$result['input'] = (Int)$result['input'];
			if (Model_Dt_Stock::select_stock_by_item_id($result['item_id']) < $result['input'])
			{
				$result['error'] = '在庫数が足りません';
			}
			else
			{
				$cart_dt = Session::get('cart_dt',null);
				$cart_dt_cnt = count($cart_dt);
				for ($i=0; $i<$cart_dt_cnt; $i++)
				{
					if ((Int)$cart_dt[$i]['id'] === $result['item_id'])
					{
						if ($result['input'] === 0)
						{
							//商品を削除する
							unset($cart_dt[$i]);
							$cart_dt = array_values($cart_dt);
							break;
						}
						else
						{
							//カート数を変更する
							$cart_dt[$i]['count'] = $result['input'];
							$result['new_value'] = Func::get_item_value($cart_dt[$i]) * $cart_dt[$i]['count'];
							break;
						}
					}
				}

				Session::delete('cart_dt');
				if (count($cart_dt)>0)
				{
					Session::set('cart_dt', $cart_dt);
				}
				else
				{
					$result['no_cart'] = true;
				}
				$result['result'] = true;
			}
		}
		else
		{
			$result['error'] = '数量は半角数字で入力してください';
		}
		return $this->response($result);
	}

	/**
	 * get_total_info
	 */
	public function post_get_total_info()
	{
		$total_info = array();
		$cart_dt = Session::get('cart_dt', null);
		$mt_free_sending = Model_Mt_Freesending::select_freesending();

		if (count($cart_dt)>0)
		{
			//合計獲得金額
			$total_price = Func::get_cart_info_sum($cart_dt)['total_amount'];
			//送料
			if ((count($cart_dt) === 1 && isset($cart_dt[0]['free_sending'])) || ($total_price >= $mt_free_sending))
			{
				//個別商品送料無料 || 合計金額送料無料
				$free_sending = true;
			}
			else
			{
				$free_sending = $mt_free_sending - $total_price;
			}

			$total_info = array(
				'total_price'  => $total_price,
				'free_sending' => $free_sending,
				'get_point'    => Func::get_total_point($cart_dt, Model_Mt_Pointrate::select_rate()),
			);
		}
		return $this->response($total_info);
	}

	/**
	 * delete_item
	 * 
	 * cartからitemを削除します
	 * @return bool
	 */
	public function post_delete ()
	{
		//get post data
		$post_data = Input::post();
		$item_id = $post_data['item_id'];
		$res = array(
			'input' => $post_data,
			'result'   => false
		);

		//get cart data
		$cart_dt = Session::get('cart_dt', false);

		if (
			isset($item_id) &&
			!empty($item_id) &&
			$cart_dt
		)
		{
			Func::delete_special_select_by_item_id($item_id);

			for ($i=0; $i<count($cart_dt); $i++)
			{
				if ($cart_dt[$i]['id'] == $item_id)
				{
					unset($cart_dt[$i]);
					$cart_dt = array_values($cart_dt);
					break;
				}
			}

			//予約カート追加があった場合
			if (Session::get('reserve_dt.'.$item_id, false))
				Session::delete('reserve_dt.'.$item_id);

			$cart_cnt = count($cart_dt);
			if ($cart_cnt > 0)
			{
				Session::set('cart_dt', $cart_dt);
			}
			else
			{
				Session::delete('cart_dt');
			}
			$res['result'] = true;
			$res['remaining_cnt'] = $cart_cnt;
		}
		return $this->response($res);
	}

	/**
	 * post_add_special_select
	 * 
	 * 特殊表示アイテム在庫チェックメソッド
	 */
	public function check_stock_special_select ($item_id, $select_dt, $count)
	{
		$item_mt = array();
		$item_mt = Model_Mt_Item::select_item_by_id($item_id, true);
		$result  = true;

		if (!empty($item_mt) && isset($item_mt['special_select']['id']) && isset($select_dt[$item_mt['special_select']['id']]))
		{
			//既にカートに入っている商品個数を取得する
			$special_select_dt = Session::get('special_select_dt', array());
			$current_count = 0;
			for ($i=0; $i<count($special_select_dt); $i++)
			{
				if ((Int)$special_select_dt[$i]['key'] === (Int)$item_mt['special_select']['id'] && (Int)$special_select_dt[$i]['value'] === (Int)$select_dt[$item_mt['special_select']['id']])
					$current_count+=$special_select_dt[$i]['count'];
			}

			//在庫数の確認
			$dt_stock = Model_Dt_Specialselectstock::select_stock_by_value_id((Int)$select_dt[(Int)$item_mt['special_select']['id']]);

			if (Model_Dt_Stock::DISP_MAX_COUNT<(Int)$count || $dt_stock<$current_count+$count)
				$result = false;
		}
		else
		{
			$result = false;
		}
		return $result;
	}

	/**
	 * add_cart
	 * 
	 * cartにitemを追加する
	 * @return 成功:商品データ 失敗:false
	 */
	public function post_add ()
	{
		//postデータの取得
		$post_data = Input::post();
		//予約sessionを格納する
		$reserve_session = Session::get('reserve_dt', null);
		//既存のカート情報を取得
		$cart_dt = Session::get('cart_dt', false);
		//商品情報を取得する
		$item_data = Model_Mt_Item::select_item_by_id($post_data['item_id'], true);
		//カート商品種類
		$cart_cnt = count($cart_dt);

		//responseのひな形
		$res = array(
			'status'=> true,
			'input' => $post_data,
			'result'   => null
		);

		/************************
		 * 同一商品一回の最大購入制限チェック
		 ************************/
		$result_check_system_max = true;
		if ((Int)$post_data['count'] > Model_Dt_Stock::DISP_MAX_COUNT)
		{
			$result_check_system_max = false;
		}
		else if (is_array($cart_dt))
		{
			for ($i=0; $i<count($cart_dt); $i++)
			{
				if ((Int)$cart_dt[$i]['id'] === (Int)$post_data['item_id'] && (Int)$cart_dt[$i]['count']+(Int)$post_data['count'] > Model_Dt_Stock::DISP_MAX_COUNT)
				{
					$result_check_system_max = false;
					break;
				}
			}
		}
		if (!$result_check_system_max)
		{
			//1回の最大購入制限チェックに失敗した場合
			return $this->response(
				array(
					'status'=> false,
					'input' => $post_data,
					'result'   => false,
					'ms' => '同一商品で一度にカートに追加出来る商品数量は['.Model_Dt_Stock::DISP_MAX_COUNT.']までです。',
				)
			);
		}

		//--------------------------------------------------------------------------
		// 個別発送商品の検査
		//--------------------------------------------------------------------------
		if (!empty($cart_dt))
		{
			$error_message = null;

			if ((bool)$item_data['individual_send_flg'])
			{
				//追加商品が個別発送商品
				if(
					($cart_cnt === 1 && (Int)$cart_dt[0]['id'] !== (Int)$post_data['item_id']) ||
					$cart_cnt > 1
				)
					//エラー
					$error_message = "<div class='long'>[{$item_data['name']}]は<br>商品の性質上、他の商品と同時にカートに追加する事ができません。<br>この商品をカートに追加する場合は、<br>一度カートの商品を削除してください。</div>";
			}
			else
			{
				//追加商品が個別発送商品ではない
				if ($cart_cnt===1 && (Int)$cart_dt[0]['individual_send_flg'])
					//エラー
					$error_message = "<div class='long'>既にカート内に入っている商品の性質上、<br>[{$item_data['name']}]<br>を追加する事ができませんでした。<br>カートの中の商品を削除してからお試しください。</div>";
			}

			//エラーメッセージ返却
			if (!is_null($error_message))
				return $this->response(
					array(
						'status'=> false,
						'input' => $post_data,
						'result'   => false,
						'ms' => $error_message,
					)
				);
		}

		//--------------------------------------------------------------------------
		// 予約商品のカート追加の場合
		//--------------------------------------------------------------------------
		if (isset($post_data['reserve_id']) && $this->uid)
		{
			$reserve_dt = Model_Dt_Reserve::select_by_id_and_uid($post_data['reserve_id'],$this->uid);

			if (empty($reserve_session))
			{
				$reserve_session = array(
					$reserve_dt['item_id'] => array((Int)$post_data['reserve_id']),
				);
			}
			else if (isset($reserve_session[$reserve_dt['item_id']]))
			{
				foreach ($reserve_session[$reserve_dt['item_id']] as $val)
				{
					if ((Int)$val === (Int)$post_data['reserve_id'])
						return $this->response(
							array(
								'status'=> false,
								'input' => $post_data,
								'result'   => false,
								'ms' => '処理に失敗しました。',
							)
						);
				}
				$reserve_session[$reserve_dt['item_id']][] = (Int)$post_data['reserve_id'];
			}
			else
			{
				$reserve_session[$reserve_dt['item_id']][] = (Int)$post_data['reserve_id'];
			}
			$post_data['item_id'] = $reserve_dt['item_id'];
			$res['input']['count'] = $post_data['count']   = $reserve_dt['count'];

			Session::set('reserve_dt', $reserve_session);
		}
		//--------------------------------------------------------------------------

		if (
			isset($post_data['item_id']) &&
			isset($post_data['count']) &&
			is_numeric($post_data['item_id']) &&
			is_numeric($post_data['count'])
		)
		{
			//get item data
			$item_data_unset = $item_data;

			unset(
				$item_data_unset['category'],
				$item_data_unset['chara'],
				$item_data_unset['new_flg'],
				$item_data_unset['reco_flg'],
				$item_data_unset['meta_key'],
				$item_data_unset['setitems'],
				$item_data_unset['special_select'],
				$item_data_unset['setitem_flg'],
				$item_data_unset['specialselect_flg'],
				$item_data_unset['hide_price_flg'],
				$item_data_unset['external_url'],
				$item_data_unset['meta_key']
			);

			//get stock
			$stock = Model_Dt_Stock::select_stock_by_item_id($post_data['item_id']);
			$res['result'] = $item_data_unset;
			$res['result']['price_tax'] = Func::get_item_value($item_data_unset);

			/********************
			 * 予約カート追加の場合
			 * 在庫数に予約数を追加する
			 ********************/
			if (isset($reserve_session[$post_data['item_id']]))
				$stock += Model_Dt_Reserve::select_ids_count ($reserve_session[$post_data['item_id']]);

			//商品データが取得出来たらカートsessionに詰める
			if (count($item_data_unset)>0 && !empty($stock))
			{
				try
				{
					//既にsessionが存在した場合はitem_idの重複を確認する
					if ($cart_dt)
					{
						for ($i=0; $i<count($cart_dt); $i++)
						{
							if ((Int)$cart_dt[$i]['id'] === (Int)$post_data['item_id'])
							{
								//個数
								$tmp_count = $cart_dt[$i]['count'] + $post_data['count'];
								if ((Int)$tmp_count > $stock)
								{
									Func::error_log('在庫不足エラー', true);
									return $this->response(
										array(
											'status'=> false,
											'input' => $post_data,
											'result'   => false,
											'ms' => '処理に失敗しました。',
										)
									);
								}

								$cart_dt[$i]['count'] = $tmp_count;
								unset($post_data);
								break;
							}
						}
					}
					//既存のitem_idがない場合
					if (isset($post_data))
					{
						if ((Int)$post_data['count'] > $stock)
						{
							Func::error_log('在庫エラー', true);
							return $this->response(
								array(
									'status'=> false,
									'input' => $post_data,
									'result'   => false,
									'ms' => '処理に失敗しました。',
								)
							);
						}

						//購入件数をセット
						$item_data_unset['count'] = (Int)$post_data['count'];
						$cart_dt[] = $item_data_unset;
					}

					//--------------------------------------------------------------------------
					// 特殊選択 (通常商品) ここから
					//--------------------------------------------------------------------------
					$post_data = Input::post();//unsetしてるので再度取得
					$result_check_special_select = array();
					$result_check_special_select['result'] = true;

					if (isset($post_data['special_select']) && $post_data['special_select']>0)
					{
						//特殊商品のチェックを実施する
						foreach ($post_data['special_select'] as $special_key => $special_val)
						{
							$result_check_special_select = 
									Model_Dt_Specialselectstock::check_stock_special_select(
										$post_data['item_id'],
										array($special_key => $special_val),
										$post_data['count'],
										Session::get('special_select_dt', array()));

							if (!$result_check_special_select['result'])
								break;
						}
					}
					else
					{
						if (
							isset($item_data['special_select'])
						)
						{
							//商品自体に特殊選択マスタが設定されているが、ポスト値が無い場合
							return $this->response(
								array(
									'status'=> false,
									'input' => $post_data,
									'result'   => false,
									'ms' => '['.$item_data['special_select']['name'].'] を選択してください。',
								)
							);
						}
					}

					if (!$result_check_special_select['result'])
					{
						/**************************
						 * 特殊商品チェックに失敗した時
						 **************************/
						return $this->response(
							array(
								'status'=> false,
								'input' => $post_data,
								'result'   => false,
								'ms' => $result_check_special_select['message'],
							)
						);
					}
					else if (isset($post_data['special_select']))
					{
						/**************************
						 * 成功した場合はsessionにセットする
						 **************************/
						$special_select_dt = Session::get('special_select_dt', array());

						if (count($special_select_dt) === 0)
						{
							//sessionが存在しない時

							foreach ($post_data['special_select'] as $select_key => $select_value)
							{
								$special_select_dt[] = array(
									'item_id' => $post_data['item_id'],
									'key'     => $select_key,
									'value'   => $select_value,
									'count'   => $post_data['count']
								);
							}
						}
						else
						{
							//sessionが既に存在する時

							$overwrited_flg = false; //上書き完了フラグ

							//--- session data
							for ($i=0; $i<count($special_select_dt); $i++)
							{
								//--- post data
								foreach ($post_data['special_select'] as $select_key => $select_value)
								{
									if ((Int)$special_select_dt[$i]['key'] === (Int)$select_key && (Int)$special_select_dt[$i]['value'] === (Int)$select_value)
									{
										//既存のセッションレコードを上書する
										$special_select_dt[$i]['count'] += $post_data['count'];
										$overwrited_flg = true;
										break;
									}
								}
							}

							//上書きが完了していない場合(sessionはあるが、項目は初めての時)
							if (!$overwrited_flg)
							{
								$special_select_dt[] = array(
									'item_id' => $post_data['item_id'],
									'key'     => $select_key,
									'value'   => $select_value,
									'count'   => $post_data['count']
								);
							}
						}
						Session::set('special_select_dt', $special_select_dt);
					}
					//--------------------------------------------------------------------------

					//カート情報をsessionにセットする
					Session::set('cart_dt', $cart_dt);
					//実行結果をセット
					$res['status'] = true;
				}
				catch( Exception $e)
				{
					$res['status'] = false;
					Func::write_exception_log($e);
				}
			}
			else
			{
				$res['status'] = false;
			}
		}
		return $this->response($res);
	}
}