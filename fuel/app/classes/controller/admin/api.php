<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Admin Api Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_Admin_Api extends Common
{
	public $template = 'template/template_menu';

	public function before()
	{
		parent::before();
	}

	public function post_cache_clear ()
	{
		$command = "cd ".APPPATH."../../ ; php oil refine generatcache";
		exec($command, $output, $return_code);
		return $this->response(array('result'=>'OK'),200);
	}
}