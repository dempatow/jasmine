<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Admin index Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_Admin_Index extends Common
{
	public $template = 'template/template_menu';

	public function before()
	{
		parent::before();
	}

	/**
	 * index action
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		//viewの作成
		$this->template->footer = View::forge('layout/footer_base');
		$this->template->header = View::forge('layout/header_base');
		$this->template->content = View::forge('admin/index/index');
	}
}