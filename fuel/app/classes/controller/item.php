<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Item Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_Item extends Common
{
	public  $template = 'template/template_common';
	private $PAGER_LIMIT = 20;

	public function before()
	{
		parent::before();

		if(!Input::is_ajax())
			CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
	}

	/**
	 * post_get_category
	 * 
	 * 親子カテゴリが選択された時
	 * 表示させるデータを取得する
	 */
	public function action_get_category()
	{
		$result = array();
		$search_keys = array();
		$post_data = Input::post();

		/*
		 * postデータ(選択データ)を整形する
		 */
		foreach ($post_data['input'] as $key => $val)
		{
			if ($key === 'cate')
			{
				for ($i=0; $i<count($val); $i++)
				{
					//単語に区切る
					$exploaded = explode('-', $val[$i]);
					if (count($exploaded) === 2)
					{
						$search_keys[$exploaded[0]][] = $exploaded[1];
					}
				}
			}
		}

		/*
		 * postデータ(選択データ)から
		 * 表示するべき小カテゴリを取得する
		 * recursive_no 2
		 */
		$s_key_cnt = count($search_keys);
		for ($i=1; $i<=$s_key_cnt; $i++)
		{
			if (isset($search_keys[$i]))
			{
				for ($j=0; $j<count($search_keys[$i]); $j++)
				{
					$child_node = SearchHelper::get_child_node_by_parent_id($search_keys[$i][$j]);
					if ($child_node && isset($result['category'.($i+1)]) && !empty($result['category'.($i+1)]))
					{
						$result['category'.($i+1)] = array_values(array_merge($result['category'.($i+1)], $child_node));
					}
					else if ($child_node)
					{
						$result['category'.($i+1)] = array_values($child_node);
					}
				}
			}
		}

		/*
		 * 表示させるべきデータの中に選択データがあった場合は
		 * checkedをつける
		 */
		foreach ($search_keys as $key => $val)
		{
			if (isset($result['category'.$key]))
			{
				for ($i=0; $i<count($val); $i++)
				{
					foreach ($result['category'.$key] as &$new_mt)
					{
						(Int)$val[$i] === (Int)$new_mt['id'] and $new_mt['checked'] = true;
					}
				}
			}
		}
		return $this->response($result);
	}

	public function post_search()
	{
		$post_data   = Input::post();
		$search_key  = array();
		$category_recursive = array();
		$item_ids = '';

		/*
		 * パラメタが不足している場合は失敗を返却する
		 */
		if (
			
			!isset($post_data['curent']) ||
			!isset($post_data['next'])
		)
		{
			$item_ids = array();
		}
		else if (isset($post_data['form']))
		{
			foreach ($post_data['form'] as $key => $val)
			{
				switch ($key)
				{
					/*
					 * category
					 */
					case 'cate':
						for ($i=0; $i<count($val); $i++)
						{
							//単語に区切る
							$exploaded = explode('-', $val[$i]);
							if (count($exploaded) === 2)
							{
								$search_key['cate'][] = $exploaded[1];
							}
						}
						break;
					/*
					 * single category
					 */
					case 's_cate':
						for ($i=0; $i<count($val); $i++)
						{
							$search_key['cate'][] = $val[$i];
						}
						break;
					/*
					 * character
					 */
					case 'char':
						for ($i=0; $i<count($val); $i++)
						{
							$search_key['char'][] = $val[$i];
						}
						break;
					/*
					 * word
					 */
					case 'search_word':
						for ($i=0; $i<count($val); $i++)
						{
							//単語に区切る
							$converted = mb_convert_kana($val[$i], 's');
							$exploaded = explode(' ', $converted);
							$exp_cnt = count($exploaded);
							for ($i=0; $i<$exp_cnt; $i++)
							{
								if (empty($exploaded[$i]))
									unset($exploaded[$i]);
							}
							count($exploaded)>0 and $search_key['word'] = $exploaded;
						}
						break;
				}
			}
			if (count($search_key)>0)
				$item_ids = SearchHelper::search_item($search_key);
		}
		else
		{
			$item_ids = Model_Mt_Item::select_ids();
		}

		$data = array();
		$item_cnt = 0;
		$page_no = 0;


		//item_idからitem情報を取得する
		$item_cnt = ($item_ids)? count($item_ids): 0;
		$data['item_cnt'] = $item_cnt;

		//ページネーション値取得
		if ($item_ids)
		{
			$page_no = 0;

			//表示ページの設定
			if ($post_data['next'] === 'prev')
			{
				$page_no = --$post_data['curent'];
			}
			else if ($post_data['next'] === 'next')
			{
				$page_no = ++$post_data['curent'];
			}
			else
			{
				$page_no = $post_data['next'];
			}

			$pagenate = Func::get_pagenate_data ($item_ids, $page_no, $this->PAGER_LIMIT);

			if ($pagenate)
			{
				$data['page'] = $pagenate;
				for ($i=0; $i<count($data['page']['current_page_data']); $i++)
				{
					$mt_items[] = Model_Mt_Item::select_item_by_id($data['page']['current_page_data'][$i]);
				}
				$data['page']['current_page_data'] = $mt_items;
			}
		}
		echo View::forge('item/_result',array('data' => $data));
	}

	/**
	 * index action
	 * 
	 * 商品詳細画面の表示
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		//post_add_special_select;
		//$template_name = 'item/template_'; //使用するコンテンツtemplate

		//パラメタのチェック
		$item_id = (Int)Uri::segment(3);
		if (is_null($item_id) || $item_id === 0 || empty($item_id))
			Func::redirect_error ();

		/***************
		 * 各種データ取得
		 ***************/
		//商品
		$item = Model_Mt_Item::select_item_by_id($item_id, false, true);

		if (is_null($item) || empty($item))
		{
			//-----------------------------
			//  item情報が取得出来なかった時
			//-----------------------------
			Func::redirect_error ();
		}
		else if (isset($item['external_url']) && !is_null($item['external_url']))
		{
			//-----------------------------
			//  外部サイト商品の場合
			//-----------------------------
			Response::redirect($item['external_url']);
		}

		$this->template->set_global('item', $item);

		//レビュー
		$review = Model_Dt_Review::select_review_by_item_id($item_id);
		$this->template->set_global('review', $review);

		//詳細
		$detail = Model_Mt_Itemdetail::select_by_item_id($item_id);
		$this->template->set_global('detail', $detail, false);

//仕様(未使用だが実装はしている)
//		$spec = Model_Dt_Itemspeckeyvalue::select_by_item_id($item_id);
//		$this->template->set_global('spec', $spec, false);

		//title
		$this->template->set_global('title', Config::get('custom_config.shop_name').' | '.$item['name'].'');
		//description
		$this->template->set_global('description', 'パンピキッズ公式通販サイト '.Config::get('custom_config.shop_name').' |「'.$item['name'].'」の商品紹介ページです。プレゼントやギフトの通販にもおすすめです!');
		//keywords
		if (isset($item['meta_key']))
			$this->template->set_global('keywords', 'パンピキッズ,キャラクター通販,グッズ,'.Config::get('custom_config.shop_name').','.$item['meta_key']);

		/***********************
		 * コンテンツtempalteの振り分け
		 ***********************/
		$template_name = 'item/template_'; //使用するコンテンツtemplate
		$template_no = isset($detail['view_no'])? $detail['view_no']: "1";

		/***********************
		 * デバイスごとの切り分け
		 ***********************/
		if (!Device::is_sp())
		{
			/**
			 * PC画面
			 */
			Asset::js(array($this->assets_path.'/js/cart.js',$this->assets_path.'/js/item/item.js'), array(), 'add_js', false);
			Asset::css(array($this->assets_path.'/css/item.css'), array(), 'add_css', false);

			//viewの作成
			$this->template->footer = View::forge('layout/footer_common');
			$this->template->header = View::forge('layout/header_common');
			$this->template->left   = View::forge('layout/side_menu');
			$this->template->menu   = View::forge('layout/main_menu');

			//template dir の設定
			$template_name .= $template_no;
		}
		else
		{
			/**
			 * SP画面
			 */
			Asset::js(array($this->assets_path.'/js/cart_sp.js'), array(), 'add_js', false);
			Asset::css(array($this->assets_path.'/css/item_sp.css'), array(), 'add_css', false);
			$this->template = \View::forge(Device::get_template_path ());
			$this->template->header = View::forge(Device::get_header_path());
			$this->template->footer = View::forge(Device::get_footer_path());
			$this->template->content= View::forge('item/index_sp');

			//template dir の設定
			$template_name .= 'sp_'.$template_no;
		}

		/***********************
		 * templateのセット
		 ***********************/
		$this->template->content= View::forge($template_name.'/index');

		/***********************
		 * 商品ごとの表示きりかえ
		 ***********************/
		if (isset($detail['type1']) && (Int)$detail['type1']>0)
		{
			$this->template->content->type1 = View::forge($template_name.'/type1_'.$detail['type1']);
		}
		if (isset($detail['type2']) && (Int)$detail['type2']>0)
		{
			$this->template->content->type2 = View::forge($template_name.'/type2_'.$detail['type2']);
		}
		if (isset($detail['type3']) && (Int)$detail['type3']>0)
		{
			$this->template->content->type3 = View::forge($template_name.'/type3_'.$detail['type3']);
		}

		/**********
		 * 特殊表示
		 **********/
		if (isset($detail['type4']) && (Int)$detail['type4']>0)
		{
			if ((Int)$detail['type4'] === 1)
			{
				$this->template->set_global('desc_img_1', true);
			}
		}

//		if (!Device::is_sp())
//		{
//			/**
//			 * PC画面
//			 */
//			/***********************
//			 * コンテンツtempalteの振り分け
//			 ***********************/
//			$template_no = isset($detail['view_no'])? $detail['view_no']: "1";
//			$template_name .= $template_no;
//			$this->template->content= View::forge($template_name.'/index');
//
//			/***********************
//			 * 商品ごとの表示きりかえ
//			 ***********************/
//			if (isset($detail['type1']) && (Int)$detail['type1']>0)
//			{
//				$this->template->content->type1 = View::forge($template_name.'/type1_'.$detail['type1']);
//			}
//			if (isset($detail['type2']) && (Int)$detail['type2']>0)
//			{
//				$this->template->content->type2 = View::forge($template_name.'/type2_'.$detail['type2']);
//			}
//			if (isset($detail['type3']) && (Int)$detail['type3']>0)
//			{
//				$this->template->content->type3 = View::forge($template_name.'/type3_'.$detail['type3']);
//			}
//
//			Asset::js(array($this->assets_path.'/js/cart.js',$this->assets_path.'/js/item/item.js'), array(), 'add_js', false);
//			Asset::css(array($this->assets_path.'/css/item.css'), array(), 'add_css', false);
//
//			//viewの作成
//			$this->template->footer = View::forge('layout/footer_common');
//			$this->template->header = View::forge('layout/header_common');
//			$this->template->left   = View::forge('layout/side_menu');
//			$this->template->menu   = View::forge('layout/main_menu');
//		}
//		else
//		{
//			/**
//			 * SP画面
//			 */
//			Asset::js(array($this->assets_path.'/js/cart_sp.js'), array(), 'add_js', false);
//			Asset::css(array($this->assets_path.'/css/item_sp.css'), array(), 'add_css', false);
//			$this->template = \View::forge(Device::get_template_path ());
//			$this->template->header = View::forge(Device::get_header_path());
//			$this->template->footer = View::forge(Device::get_footer_path());
//			$this->template->content= View::forge('item/index_sp');
//		}
	}

	/**
	 * list action
	 * 
	 * @access public
	 * @return Respones
	 */
	public function action_list ()
	{
		$data         = array();      //view用
		$get_data     = Input::get(); //get  query
		$post_data    = Input::post();//post data
		$item_ids     = array();      //表示するitem_id
		$mt_items     = array();      //表示するitem
		$page_no      = isset($get_data['no'])?$get_data['no']: 1;//現在ページ
		$search_key_array = array();  //検索キー用

		//単独カテゴリ
		$data['single_category'] = Model_Mt_Category::select_category_by_recursive_no(0);
		//大カテゴリ
		$data['category_1'] = Model_Mt_Category::select_category_by_recursive_no(1);
		//キャラクター
		$data['character'] = Model_Mt_Character::select();

		//--- 検索ワード
		if (isset($post_data['search_word']) || isset($get_data['word']))
		{
			$keywords = isset($post_data['search_word'])? $post_data['search_word']: $get_data['word'];
			//単語に区切る
			$converted = mb_convert_kana($keywords, 's');
			$exploaded = explode(' ', $converted);
			$exp_cnt = count($exploaded);
			for ($i=0; $i<$exp_cnt; $i++)
			{
				if (empty($exploaded[$i]))
					unset($exploaded[$i]);
			}
			if (count($exploaded)>0)
				$search_key_array['word'] = $exploaded;

			//画面検索欄表示
			$this->template->set_global('search_word',$converted);
		}

		if (isset($get_data['cate']))
		{
			//--- カテゴリー
			$search_key_array['cate'] = array($get_data['cate']);

			//画面チェック表示用
			if (isset($data['single_category'][$get_data['cate']]))
			{
				$data['single_category'][$get_data['cate']]['checked'] = true;
			}
			else if (isset($data['category_1'][$get_data['cate']]))
			{
				$data['category_1'][$get_data['cate']]['checked'] = true;
			}
		}
		if (isset($get_data['char']) && isset($data['character'][$get_data['char']]))
		{
			//--- キャラクター
			$data['character'][$get_data['char']]['checked'] = true;
			$search_key_array['char'] = array($get_data['char']);
		}

		if (count($search_key_array)>0)
		{
			//getパラメータがあった時
			$item_ids = SearchHelper::search_item($search_key_array);//検索する
		}
		else if (count($get_data) == 0)
		{
			//getパラメータが無い時(直接アクセスされた時)
			$item_ids = Model_Mt_Item::select_ids();
		}

		//item_idからitem情報を取得する
		$item_cnt = ($item_ids)? count($item_ids): 0;
		$data['item_cnt'] = $item_cnt;

		//ページネーション値取得
		if ($item_ids)
		{
			if (isset($get_data['cate']))
			{
				//大カテゴリが選択されていた場合は画面に中カテゴリを出す
				$cate_rec_tmp = Model_Mt_Category::select_category_by_id($get_data['cate']);
				if ($cate_rec_tmp['recursive_no'] && (Int)$cate_rec_tmp['recursive_no'] === 1)
					$this->template->set_global('child_category', SearchHelper::get_child_node_by_parent_id($get_data['cate']));
			}

			$pagenate = Func::get_pagenate_data ($item_ids, $page_no, $this->PAGER_LIMIT);
			if ($pagenate)
			{
				$data['page'] = $pagenate;

				for ($i=0; $i<count($data['page']['current_page_data']); $i++)
				{
					$mt_items[] = Model_Mt_Item::select_item_by_id($data['page']['current_page_data'][$i]);
				}
				$data['page']['current_page_data'] = $mt_items;
			}
		}

		//viewの作成
		Asset::js(array($this->assets_path.'/js/item/list.js',$this->assets_path.'/js/item/search.js'), array(), 'add_js', false);
		Asset::css(array($this->assets_path.'/css/item.css'), array(), 'add_js', false);
		$this->template->set_global('data',$data);
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->content= View::forge('item/list');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');
	}
}