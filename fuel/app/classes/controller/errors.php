<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Errors Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_Errors extends Common
{
	public $template = 'template/template_common';
	private $main_cont = null;

	public function before()
	{
		parent::before();
		//基本viewの作成
		$this->template->footer = View::forge('layout/footer_common');
		$this->template->header = View::forge('layout/header_common');
		$this->template->left   = View::forge('layout/side_menu');
		$this->template->menu   = View::forge('layout/main_menu');
		CalenderHelper::get_calender($this->template, date('Ym',mktime(date('m'),date('d'))));
		Asset::css(array($this->assets_path.'/css/common.css',$this->assets_path.'/css/otherpage.css'), array(), 'add_css', false);

	}

	/**
	 * index action
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		//content viewの作成
		$this->template->set_global('content', View::forge('errors/404'));
	}

	/**
	 * error action
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_error()
	{
		//content viewの作成
		$message = Session::get_flash('error_message', false);
		if ($message)
			$this->template->set_global('error_message', $message, false);

		$this->template->set_global('content', View::forge('errors/error'));
	}
}
