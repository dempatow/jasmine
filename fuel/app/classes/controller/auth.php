<?php
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Auth Controller.
 *
 * response body and status.
 *
 * @package  app
 * @extends  Controller_Common
 */
class Controller_Auth extends Common
{
	public $template = 'template/template_common';

	public function before()
	{
		parent::before();
	}

/*
	public function action_index ()
	{
		Debug::dump('index auth');exit;
	}
*/
	/**
	 * call_back action
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_api_call_back()
	{
		Debug::dump('OK call back');
		GoogleAPI::call_back();
		exit;
		//viewの作成
		$this->template->footer = View::forge('layout/footer_login');
		$this->template->header = View::forge('layout/header_login');
		$this->template->content = View::forge('index/index');
	}

	public function action_log_out ()
	{
		CustomAuth::logout();
		exit;
	}

	/**
	 * login action
	 * 
	 * ログイン処理をするメソッドです。
	 * 成功時に商品リストページにredirectさせます。
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_login()
	{
		//リダイレクト先のurlの決定
		$redirect = '';
		$get_url = Input::get('rd', false);

		$redirect = ($get_url)? $get_url: Input::referrer(false);
		if (!$redirect)
			$redirect = Config::get('custom_config.url.https_domain');

		//postデータの取得
		$data = Input::post();

		//validation
		$validation = Validation::forge();
		$validation->add_model('Model_Dt_User');

		if (!isset($data['auto_login']))
			$data['auto_login'] = 0;

		if (
			(
				\Security::check_token() &&
				$validation->run($data, array('mail', 'passwd_auth', 'auto_login')) &&
				CustomAuth::login($data['mail'], $data['passwd_auth'])
			)
		)
		{
			$uid        = Session::get('login_info.id');
			$auto_login = Session::get('login_info.auto_login', false);

			//自動ログインを有効にする
			if (
				(isset($data['auto_login']) && (Int)$data['auto_login'] === (Int)1)
			)
			{
				//dbの自動ログインflgをたてる
				Model_Dt_User::update_flg($uid, 'auto_login', 1);
				CustomAuth::create_passport($uid);
				Session::set('login_info.auto_login',1);
			}
			else
			{
				//db自動ログインflgをおろす
				Model_Dt_User::update_flg($uid, 'auto_login', 0);
				Model_Dt_Passport::set_del_flg_by_uid($uid);
				Cookie::delete(CustomAuth::$config['passport_key']);
				Session::set('login_info.auto_login',0);
			}
			Model_Dt_Useractionhistory::insert_history(101, $uid);

			/******************************
			 * ログインと同時の商品予約があった場合
			 ******************************/
			if (isset($data['count']) && isset($data['item_id']))
			{
				$reserve_data['reserve_id'] = Model_Dt_Reserve::reserve_item($uid, $data['item_id'], $data['count']);
				if (!Model_Mt_Item::validation_reserve($data) || !$reserve_data['reserve_id'])
					Func::redirect_error ('商品予約の処理に失敗しました。');

				$reserve_data['count']   = $data['count'];
				$reserve_data['item_id'] = $data['item_id'];
				Func::send_reserve_mail($reserve_data);

				Func::show_overlay_message('商品の予約が完了しました', Config::get('custom_config.url.https_domain').'/user/mypage');
			}
		}
		else
		{
			//ログ保存
			if (isset($data['mail']))
			{
				Model_Dt_Useractionhistory::insert_history(301, null, $data['mail']);
			}
			else
			{
				Model_Dt_Useractionhistory::insert_history(301);
			}

			foreach ($validation->get_error_array() as $key => $value)
			{
				$data['error'][$key] = $value;
			}

			//形式エラーがない時(passかidが違う時)
			if (!isset($data['error']))
				$data['error']['passwd_auth'] = '会員IDとパスワードをご確認ください。';

			//ログインメッセージをoverlayで表示させる
			if (isset($data['overlay']))
				$data['overlay'] = true;

			//遷移先で入力値とerrorメッセージを表示させる為
			Session::set('login_parts', $data);
		}
		//画面遷移させる
		Response::redirect($redirect);
	}

	/**
	 * reset_passwd
	 * @access public
	 * @return view Resonse
	 */
	public function action_reset_passwd ()
	{
		//postデータの取得
		$form_data = Input::post();

//デバッグ用
//$form_data['repwd_mail'] = 'y.funayose@gmail.com';

		//validation
		$validation = Validation::forge();
		$validation->add_model('Model_Dt_User');

		if (
			\Security::check_token() &&
			$validation->run($form_data, array('repwd_mail'))
		)
		{
			//ログアウトさせる
			CustomAuth::logout(true);

			//mailをキーにdt_userを引く
			$dt_user = DbHelper::query_exec(
					Model_Dt_User::get_query_select_user_by_mail($form_data['repwd_mail']),
					true,
					false
			);

			if (count($dt_user)>0)
			{
				/************************
				 * パスワードのリセット処理
				 ************************/
				$pw_len = 10;
				$new_pw = "";
				$data = Input::post();
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				for ($i=0; $i<$pw_len; $i++)
				{
					$new_pw .= $chars[mt_rand(0, strlen($chars)-1)];
				}

				//新しいpasswdにupdateする
				if(!Model_Dt_User::update_passwd($dt_user['id'], Auth::instance()->hash_password($new_pw)))
					Func::redirect (Config::get('custom_config.url.http_domain').'/errors/error', '処理に失敗しました。');

				//登録完了メール送信
				$dt_user['new_pw']      = $new_pw;
				$dt_user['regist_date'] = date('Y年m月d日H時i分s秒');

				//adminとuserにメール送信
				$mail_info = array(
						'user' => array(
								'param' => array(
										'to'        => $dt_user['mail'],
										'subject'   => Func::get_mail_subject(Config::get('custom_config.mail.title.reset_passwd.user')),
										'view'      => 'mail/user/reset_passwd/user',
										'from'      => Config::get('custom_config.mail.from.contact'),
										'from_name' => Config::get('custom_config.mail.from_name'),
								),
								'email_data' => $dt_user
						),
						'admin' => array(
								'param' => array(
										'to'        => Config::get('custom_config.mail.from.admin'),
										'subject'   => Config::get('custom_config.mail.title.reset_passwd.admin'),
										'view'      => 'mail/user/reset_passwd/admin',
										'from'      => Config::get('custom_config.mail.from.contact'),
										'from_name' => Config::get('custom_config.mail.from_name'),
								),
								'email_data' => $dt_user
						)
				);
				$mail_info = Func::prepare_forSendmail($mail_info);
				//メールを非同期で送信する
				$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
				exec($command, $output, $return_code);
				Model_Dt_Useractionhistory::insert_history(104, $dt_user['id']);
				Debug::dump('OK reset passwd');
				exit;
			}
			exit;
		}

		Debug::dump($validation->get_error_array());
		//失敗した時通る
		Debug::dump('NG reset passwd');
		exit;
	}

	/**
	 * change_passwd_form
	 * @access public
	 * @return view Response
	 */
	public function action_change_passwd_form ()
	{
		
		$this->template->footer = View::forge('layout/footer_test');
		$this->template->header = View::forge('layout/header_test');
		$this->template->content = View::forge('auth/change_passwd');
		return;
	}

	/**
	 * change_passwd
	 * @access public
	 * @return view Resonse
	 */
	public function action_change_passwd ()
	{
		//postデータの取得
		$form_data = Input::post();
		//Debug::dump($form_data);exit;

//TODO: change_passwdデバッグ用
//$form_data['current_passwd'] = '7bFNAUkkEo';
//$form_data['new_passwd'] = 'funayose4410';
//$form_data['new_passwd_confirm'] = 'funayose4410';

		//validation
		$validation = Validation::forge();
		$validation->add_model('Model_Dt_User');

		if (
			//\Security::check_token() &&
			$validation->run($form_data, array('current_passwd', 'new_passwd', 'new_passwd_confirm'))
		)
		{
			Debug::dump('validation OK');exit;

			//mailをキーにdt_userを引く
			$dt_user = DbHelper::query_exec(
					Model_Dt_User::get_query_select_user_by_mail($form_data['repwd_mail']),
					true,
					false
			);

			if (count($dt_user)>0)
			{
			

				//新しいpasswdにupdateする
				if(!Model_Dt_User::update_passwd($dt_user['id'], Auth::instance()->hash_password($new_pw)))
					Func::redirect (Config::get('custom_config.url.http_domain').'/errors/error', '処理に失敗しました。');

				//登録完了メール送信
				$dt_user['new_passwd']  = $new_pw;
				$dt_user['regist_date'] = date('Y年m月d日H時i分s秒');

				//adminとuserにメール送信
				$mail_info = array(
						'user' => array(
								'param' => array(
										'to'        => $dt_user['mail'],
										'subject'   => Func::get_mail_subject(Config::get('custom_config.mail.title.reset_passwd.user')),
										'view'      => 'mail/user/reset_passwd/user',
										'from'      => Config::get('custom_config.mail.from.contact'),
										'from_name' => Config::get('custom_config.mail.from_name'),
								),
								'email_data' => $dt_user
						),
						'admin' => array(
								'param' => array(
										'to'        => Config::get('custom_config.mail.from.admin'),
										'subject'   => Func::get_mail_subject(Config::get('custom_config.mail.title.reset_passwd.admin')),
										'view'      => 'mail/user/reset_passwd/admin',
										'from'      => Config::get('custom_config.mail.from.contact'),
										'from_name' => Config::get('custom_config.mail.from_name'),
								),
								'email_data' => $dt_user
						)
				);

				$mail_info = Func::prepare_forSendmail($mail_info);
				//メールを非同期で送信する
				$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";

				exec($command, $output, $return_code);

				Model_Dt_Useractionhistory::insert_history(104, $dt_user['id']);

				Debug::dump('OK change passwd');
				exit;
			}
		}
		//失敗した時通る
		Debug::dump('NG change passwd');
		$this->template->footer = View::forge('layout/footer_test');
		$this->template->header = View::forge('layout/header_test');
		$this->template->content = View::forge('auth/change_passwd');
		$this->template->set_global('val', $validation);
	}
}