<?php
namespace Fuel\Tasks;
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Rollbackexpirationpurcahsedata Class
 * 
 * @package  app
 */
class Rollbackexpirationpurcahsedata
{
	/******************************
	 * GMO画面に遷移したまま戻らなかった
	 * ユーザーのデータをロールバックする
	 ******************************/
	public static function run()
	{
		$log_str = '';
		$expiration_data = \Model_Dt_Purchasehistory::get_expiration_gmo_user();
		$dt_count = count($expiration_data);

		$log_str.=PHP_EOL.'------------------------expiration_gmo_data start------------------------'.PHP_EOL;
		if ($dt_count>0)
		{
			/****************************
			 * 削除処理が必要なデータがある場合
			 ****************************/
			for ($i=0; $i<$dt_count; $i++)
			{
				$Purchase = \Purchase::forge((Int)$expiration_data[$i]['id']);
				$Purchase->roll_back_purchase(\Purchase::CANCEL_AGENT_AUTO);
				$log_str.= $expiration_data[$i]['id'].PHP_EOL;
			}
		}
		else
		{
			/****************************
			 * 削除するデータが無い場合
			 ****************************/
			$log_str.='there was no target data(´･ヮ･`)'.PHP_EOL;
		}
		$log_str.='------------------------expiration_gmo_data  end-------------------------'.PHP_EOL;
		\Log::info($log_str);//ログ出力
	}
}