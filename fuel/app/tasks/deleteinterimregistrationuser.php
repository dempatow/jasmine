<?php

namespace Fuel\Tasks;

class Deleteinterimregistrationuser
{
	/**
	 * 仮登録されたUserRecordにdel_flgをたてる
	 */
	public static function run()
	{
		\Config::load('custom_config', true);
		$query = \Model_Dt_Users::get_query_update_del_expiration_user();
		\DbHelper::query_exec($query);
		\Log::info('期限切れユーザーにdel_flgをたてるbachが実行されました');
	}
}