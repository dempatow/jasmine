<?php
namespace Fuel\Tasks;
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Deletepassport Class
 * 
 * @package  app
 */
class Deletepassport
{
	/********************************************************
	 * passport削除バッチ
	 * 
	 * del_flgが立っているレコードを削除します。
	 * 
	 ********************************************************/
	public static function run()
	{
		\Log::info('run delete passport');
		if (\Model_Dt_Passport::delete_by_del_flg_1())
		{
			echo '----------------------------------'.PHP_EOL;
			echo '論理削除済みpassportをdelteしました'.PHP_EOL;
			echo '----------------------------------'.PHP_EOL;
			return;
		}
		else
		{
			echo '----------------------------------'.PHP_EOL;
			echo '論理削除済みpassportのdelteに失敗しました'.PHP_EOL;
			echo '----------------------------------'.PHP_EOL;
		}

		$mail_data = array(
			'text' => '!!passportの削除に失敗しました',
		);
		$mail_info = array(
			'admin' => array(
				'param' => array(
					'to'        => 'dev@sp-k.co.jp',
					'subject'   => 'passportの削除に失敗しました',
					'view'      => 'mail/admin/alert',
					'from'      => 'contact@shopmail.panpikids.com',
					'from_name' => \Config::get('custom_config.shop_name'),
				),
				'email_data' => $mail_data,
			),
		);
		$mail_info = \Func::prepare_forSendmail($mail_info);
		//メールを非同期で送信する
		$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
		\Debug::dump($command);exit;
		exec($command, $output, $return_code);
	}
}