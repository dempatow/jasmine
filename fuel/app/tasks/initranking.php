<?php
namespace Fuel\Tasks;
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Initranking Class
 * 
 * @package  app
 */
class Initranking
{
	const RANKING_TERM = 7;

	/**
	 * ランキング初期化メソッド
	 */
	public static function run()
	{
		\Config::load('custom_config',true);

		try
		{
			//ランキングマスタにdel_flgをたてる
			\Model_Mt_Ranking::update_del_flg_all();

			//新しいランキングマスタを作成する
			\Model_Mt_Ranking::insert_next_mt(self::RANKING_TERM);

			\Log::info('run init ranking');

			echo '----------------------------------'.PHP_EOL;
			echo '新しいランキングマスタを作成したよ!(´･ヮ･`)'.PHP_EOL;
			echo '----------------------------------'.PHP_EOL;

			$mail_data = array(
				'text' => 'ランキングデータを生成しましたよ!!(´･ヮ･`)',
			);
			$mail_info = array(
					'admin' => array(
							'param' => array(
									'to'        => 'dev@sp-k.co.jp',
									'subject'   => 'ランキング期間レコード生成完了!(jasmine)',
									'view'      => 'mail/admin/alert',
									'from'      => 'contact@shopmail.panpikids.com',
									'from_name' => \Config::get('custom_config.shop_name'),
							),
							'email_data' => $mail_data,
					),
			);
			$mail_info = \Func::prepare_forSendmail($mail_info);
			//メールを非同期で送信する
			$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
			exec($command, $output, $return_code);
		}
		catch (\Exception $e)
		{
			\Func::write_exception_log($e, true);
			\Debug::dump('Exep');
		}
	}
}