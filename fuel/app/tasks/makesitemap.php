<?php
namespace Fuel\Tasks;
/**
 * @ex
 * php oil r makesitemap
 */
class Makesitemap
{
	/**
	 * これからリクエストするURL
	 */
	public static $request_array = array();

	/**
	 * この 配列の文字列が含まれているURLは除外
	 */
	public static $ignore_exts = array(
		'css',
		'js',
		'ico',
		'&',
		'twitter',
		'ttrinity',
		'upsold',
	);

	/**
	 * このキーワ−ドがurlに含まれていないといけない
	 */
	public static $common_key_word = 'panpikids';

	//収集したURL
	public static $site_url = array();

	/**
	 * サイトマップ自動生成
	 * メインメソッド
	 */
	public static function run()
	{
		\Config::load('custom_config', true);

		//urlに含まれていないといけないキーワードを変更する
		if (
			\Fuel::$env === \Fuel::DEVELOPMENT ||
			\Fuel::$env === \Fuel::STAGE
		)
			self::$common_key_word = 'jasmine';

		/********************
		 * item url を取得する
		 ********************/
		self::set_item_url(self::get_item_url());

		/********************************
		 * request_arrayの要素分 リクエストを行う
		 ********************************/
		//サイトindex
		self::$request_array[] = \Config::get('custom_config.url.http_domain');
		//サイトitem一覧
		self::$request_array[] = \Config::get('custom_config.url.http_domain').'/item/list';
		for ($i=0; $i<count(self::$request_array); $i++)
		{
			self::set_request_array(self::request(self::$request_array[$i]));
		}

		/*******************
		 * site mapを生成する
		 *******************/
		$xml = \SiteMapHelper::get_site_map_xml(self::$site_url);
		\File::update(APPPATH . '/../../public/', 'sitemap.xml', $xml);

		echo '---------------------------------------------->'.PHP_EOL;
		echo 'site mapの生成が完了しました'.PHP_EOL;
		echo '<----------------------------------------------'.PHP_EOL;
	}

	/**
	 * get_item_url
	 * 
	 */
	private static function get_item_url ()
	{
		return \Model_Mt_Item::select_item_for_db();
	}

	/**
	 * set_item_url
	 * @param type $data
	 */
	private static function set_item_url ($data)
	{
		for ($i=0; $i<count($data); $i++)
		{
			//商品名url
			if (self::check_ignore_url(\Config::get('custom_config.url.http_domain').'/'.$data[$i]['name']))
			{
				self::$site_url[\Config::get('custom_config.url.http_domain').'/'.$data[$i]['name']] = \Config::get('custom_config.url.http_domain').'/'.$data[$i]['name'];
			}

			//商品番号url
			if (self::check_ignore_url(\Config::get('custom_config.url.http_domain').'/item/list/'.$data[$i]['id']))
			{
				self::$site_url[\Config::get('custom_config.url.http_domain').'/item/list/'.$data[$i]['id']] = \Config::get('custom_config.url.http_domain').'/item/list/'.$data[$i]['id'];
			}
		}
	}

	/**
	 * request
	 * 
	 * @param type $target
	 */
	private static function request ($target)
	{
		$result = false;
		//curlインスタンス作成
		$curl = \Fuel\Core\Request::forge($target,'curl');
		//method にpostを設定
		$curl->set_method('get');
		$return_body_str = '';

		try
		{
			$respnse = $curl->execute()->response();

			/**
			 * 通信成功の場合
			 */
			if ($respnse->status == 200)
				self::$site_url[$target] = $target;

			//レスポンス取得
			$return_body_str = $respnse->body();

		}
		catch (Exception $e)
		{
			return $return_body_str;
		}
		return $return_body_str;
	}

	/**
	 * set_request_array
	 * @param string str
	 */
	private static function set_request_array ($str)
	{
		$pattern= '/.+<?\shref=[\'|"](.*?)[\'|"].*/';
		$match = array();

		preg_match_all($pattern, $str, $match);

		foreach ($match[1] as $val)
		{
			if (self::check_ignore_url($val))
			{
				self::$site_url[$val] = $val;
			}
		}
	}

	/**
	 * check_ignore_url
	 * 
	 */
	private static function check_ignore_url ($target_url)
	{
		$result = true;

		if (
			isset(self::$site_url[$target_url]) ||
			(string)$target_url === '#' ||
			!preg_match('/'.self::$common_key_word.'/', $target_url)
		)
		{
			$result = false;
		}
		else
		{
			foreach (self::$ignore_exts as $val)
			{
				if (preg_match('/'.$val.'/', $target_url))
				{
					$result = false;
					break;
				}
			}
		}
		return $result;
	}
}