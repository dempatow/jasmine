<?php
namespace Fuel\Tasks;
/**
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

/**
 * The Generatcache Class
 * 
 * @package  app
 */
class Generatcache
{
	/**
	 * キャッシュの初期化バッチ
	 * 
	 * アプリ全体のキャッシュを初期化し、
	 * 動作に必要なキャッシュを準備します
	 */
	public static function run()
	{
		\Config::load('custom_config',true);
		try
		{
			//cache all clear
			\RedisHelper::flush_all();

			/***********
			 * item
			 ***********/
			// ids
			\Model_Mt_Item::select_ids();

			$items = \Model_Mt_Item::select_item_for_db();

			for ($i=0; $i<count($items); $i++)
			{
				//--item短縮url
				\RedisHelper::set_master(base64_encode('url/'.$items[$i]['name']), 'item/index/'.$items[$i]['id']);

				//--item情報(各種join)
				\Model_Mt_Item::select_item_by_id($items[$i]['id']);

				//--itemspec情報
				\Model_Dt_Itemspeckeyvalue::select_by_item_id($items[$i]['id']);
			}

			//--item詳細情報
			\Model_Mt_Itemdetail::select();

			//--特殊選択key (キャッシュ生成の為 引数0)
			\Model_Mt_Specialselectkey::select_by_id(0);

			//--特殊選択key (キャッシュ生成の為 引数0)
			\Model_Mt_Specialselectvalue::select_by_id(0);

			//--- 送料無料商品マスタ
			\Model_Mt_Freesendingitem::select();

			//--- キャラクターマスタ
			\Model_Mt_Character::select();

			/************
			 * sending
			 ************/
			//--- 日付指定マスタ
			\Model_Mt_Datespecified::select();

			//--- 時間帯マスタ
			\Model_Mt_Timespecified::select();

			/***********
			 * island
			 ***********/
			//-- 離島マスタ
			\Model_Mt_Island::get_island();

			/***********
			 * tax
			 ***********/
			//-- 消費税マスタs
			\Model_Mt_Tax::select_current_tax();

			/***********
			 * category
			 ***********/
			//--カテゴリマスタ(recursive)
			$result = true;
			$i=1;
			while ($result)
			{
				$result = \Model_Mt_Category::select_category_by_recursive_no($i);
				$i++;
			}
			unset($i,$result);

			/***********
			 * message
			 ***********/
			/**
				$messages = \Model_Mt_Message::select_message_for_db();
				if (count($messages)>0)
					\RedisHelper::set_master('messages', $messages);
			**/

			/***********
			 * news
			 ***********/
			//-- news
			\Model_Mt_News::select();
			//-- news category
			\Model_Mt_Newscategory::select();

			/***********
			 * sending
			 ***********/
			//-- sending ids
			\Model_Mt_Sending::select_all_id();

			/***********
			 * purchase
			 ***********/
			//-- purchase status
			\Model_Mt_Purchasestatus::select();

			/***********
			 * other
			 ***********/
			//-- holiday
			\Model_Mt_Holiday::get_holiday();

			/***********
			 * ranking
			 ***********/
			\Model_Dt_Purchasehistory::get_ranking_data();
			echo '----------------------------------'.PHP_EOL;
			echo 'キャッシュ初期化完了したお(´･ヮ･`)'.PHP_EOL;
			echo '----------------------------------'.PHP_EOL;
			\Log::info('run generate cache');

			$mail_data = array(
				'text' => 'キャッシュ初期化完了したお(´･ヮ･`)',
			);
			$mail_info = array(
					'admin' => array(
							'param' => array(
									'to'        => 'dev@sp-k.co.jp',
									'subject'   => 'キャッシュ生成完了!(jasmine)',
									'view'      => 'mail/admin/alert',
									'from'      => 'contact@shopmail.panpikids.com',
									'from_name' => \Config::get('custom_config.shop_name'),
							),
							'email_data' => $mail_data,
					),
			);

			$mail_info = \Func::prepare_forSendmail($mail_info);
			//メールを非同期で送信する
			$command = "cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &";
			exec($command, $output, $return_code);
		}
		catch (\Exception $e)
		{
			\Func::write_exception_log($e, true);
			\Debug::dump('Exep');
		}
	}
}