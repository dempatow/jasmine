<?php
namespace Fuel\Tasks;

class Test
{
	/**
	 * メールを送信するメソッド
	 * このメソッドを実行するためのコマンドは
	 * `php oil refine sendmail`
	 * プログラム側からは
	 * exec("cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &");
	 */
	public static function run()
	{
		\Debug::dump('OK task');
		\Log::error('OK task');
	}
}