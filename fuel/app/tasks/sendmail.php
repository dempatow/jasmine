<?php
namespace Fuel\Tasks;

class Sendmail
{
	/**
	 * メールを送信するメソッド
	 * このメソッドを実行するためのコマンドは
	 * `php oil refine sendmail`
	 * プログラム側からは
	 * exec("cd ".APPPATH."../../ ; php oil refine sendmail '".$mail_info."' >/dev/null &");
	 */
	public static function run($mail_info)
	{
		$recipients = json_decode($mail_info, true);
		array_walk_recursive($recipients, 
			create_function('&$val', '$val = htmlspecialchars_decode($val, ENT_QUOTES);')
		);

		foreach ($recipients as $key => $param)
		{
			\Func::sendMail( $param['param'], $param['email_data'], "text", $key );
		}
	}
}