<?php
/**
 * custom config
 *
 * 独自設定ファイルです
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P.advertising Co.,Ltd.
 * @link       http://sp-k.co.jp/
 * test
 */

return array (
	/**
	 * device cd
	 */
	'pc_cd' => 1,
	'sp_cd' => 2,
	'fp_cd' => 3,
	'tb_cd' => 4,
);