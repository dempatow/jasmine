<?php
/**
 * seo config
 *
 * seo設定ファイルです
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P.advertising Co.,Ltd.
 * @link       http://sp-k.co.jp/
 */

return array (

	//---title
	'title' => array(

		//---設定がない時に適用される
		'default' => 'パンピショップ',

		'Controller_Index' => array(
			'index' => 'パンピショップ'
		),

		'Controller_Cart' => array(
			'index' => 'パンピショップ | カート',
		),
		'Controller_User_Signup' => array(
			'form' => 'パンピショップ | 新規登録',
			'confirm' => 'パンピショップ | 新規登録',
			'regist' => 'パンピショップ | 新規登録',
		),
		'Controller_User_Withdrawal' => array(
			'index' => 'パンピショップ | 退会',
		),
		'Controller_User_Mypage' => array(
			'index' => 'パンピショップ | マイページ',
		),
		'Controller_User_Address' => array(
			'form' => 'パンピショップ | アドレス帳',
			'confirm' => 'パンピショップ | アドレス帳',
			'regist' => 'パンピショップ | アドレス帳',
		),
		'Controller_User_Change' => array(
			'confirm' => 'パンピショップ | 登録情報の変更',
			'form' => 'パンピショップ | 登録情報の変更',
			'regist' => 'パンピショップ | 登録情報の変更',
		),
		'Controller_User_Passwd' => array(
			'change' => 'パンピショップ | パスワードの変更',
			'reset' => 'パンピショップ | パスワードの初期化',
		),
		'Controller_User_Review' => array(
			'index' => 'パンピショップ | レビュー',
			'form' => 'パンピショップ | レビュー',
		),
		'Controller_User_Point' => array(
			'list' => 'パンピショップ | ポイント履歴',
		),
		'Controller_User_Purchasehistory' => array(
			'index' => 'パンピショップ | 注文履歴',
			'detail' => 'パンピショップ | 注文履歴',
		),
		'Controller_User_Purchase' => array(
			'callback' => 'パンピショップ',
			'complete' => 'パンピショップ | 購入手続き完了',
			'confirm' => 'パンピショップ | 確認画面',
			'purchaser' => 'パンピショップ | 購入者様情報',
			'receiver' => 'パンピショップ | お届け先情報',
			'sending' => 'パンピショップ | 発送方法',
		),
		'Controller_Errors' => array(
			'404' => 'パンピショップ | 404 not found',
			'error' => 'パンピショップ | error',
		),
		'Controller_Item' => array(
			'list' => 'パンピショップ | 商品一覧',
		),
		'Controller_Static' => array(
			'info' => 'パンピショップ | ショッピングガイド',
			'law' => 'パンピショップ | 特定商取引法に関する表示',
			'contact' => 'パンピショップ | お問い合わせ先',
		),
	),
	//keywords設定
	'keywords' => array(

		//---設定がない時に適用される
		'default' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',

		'Controller_Index' => array(
			'index' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育'
		),

		'Controller_Cart' => array(
			'index' => '',
		),
		'Controller_User_Signup' => array(
			'form' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'confirm' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'regist' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_User_Withdrawal' => array(
			'index' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_User_Mypage' => array(
			'index' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_User_Address' => array(
			'form' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'confirm' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'regist' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_User_Change' => array(
			'confirm' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'form' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'regist' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_User_Passwd' => array(
			'change' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'reset' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_User_Review' => array(
			'index' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'form' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_User_Point' => array(
			'list' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_User_Purchasehistory' => array(
			'index' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'detail' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_User_Purchase' => array(
			'callback' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'complete' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'confirm' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'purchaser' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'receiver' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'sending' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_Errors' => array(
			'404' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'error' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_Item' => array(
			//'index' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'list' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
		'Controller_Static' => array(
			'info' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'law' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
			'contact' => 'パンピキッズ,パンピショップ,キャラクター,通販,かわいい,食育',
		),
	),
	//description
	'description' => array(

		//---設定がない時に適用される
		'default' => 'パンピキッズ公式通販サイト「パンピショップ」のページです。キャラクターグッズを販売しています！プレゼントやギフトにもおすすめです！',

		'Controller_Index' => array(
			'index' => 'パンピキッズ公式通販サイト「パンピショップ」のページです。キャラクターグッズを販売しています！プレゼントやギフトにもおすすめです！'
		),

		'Controller_Cart' => array(
			'index' => 'パンピキッズ公式通販サイト「パンピショップ」|「お問い合わせ先」のページです。',
		),
		'Controller_User_Signup' => array(
			'form' => 'パンピキッズ公式通販サイト「パンピショップ」|「新規登録」のページです。',
			'confirm' => 'パンピキッズ公式通販サイト「パンピショップ」|「新規登録」のページです。',
			'regist' => 'パンピキッズ公式通販サイト「パンピショップ」|「新規登録」のページです。',
		),
		'Controller_User_Withdrawal' => array(
			'index' => 'パンピキッズ公式通販サイト「パンピショップ」|「退会」のページです。',
		),
		'Controller_User_Mypage' => array(
			'index' => 'パンピキッズ公式通販サイト「パンピショップ」|「マイページ」のページです。',
		),
		'Controller_User_Address' => array(
			'form' => 'パンピキッズ公式通販サイト「パンピショップ」|「アドレス帳」のページです。',
			'confirm' => 'パンピキッズ公式通販サイト「パンピショップ」|「アドレス帳」のページです。',
			'regist' => 'パンピキッズ公式通販サイト「パンピショップ」|「アドレス帳」のページです。',
		),
		'Controller_User_Change' => array(
			'confirm' => 'パンピキッズ公式通販サイト「パンピショップ」|「登録情報変更」のページです。',
			'form' => 'パンピキッズ公式通販サイト「パンピショップ」|「登録情報変更」のページです。',
			'regist' => 'パンピキッズ公式通販サイト「パンピショップ」|「登録情報変更」のページです。',
		),
		'Controller_User_Passwd' => array(
			'change' => 'パンピキッズ公式通販サイト「パンピショップ」|「パスワード変更」のページです。',
			'reset' => 'パンピキッズ公式通販サイト「パンピショップ」|「パスワード初期化」のページです。',
		),
		'Controller_User_Review' => array(
			'index' => 'パンピキッズ公式通販サイト「パンピショップ」|「商品レビュー」のページです。',
			'form' => 'パンピキッズ公式通販サイト「パンピショップ」|「商品レビュー」のページです。',
		),
		'Controller_User_Point' => array(
			'list' => 'パンピキッズ公式通販サイト「パンピショップ」|「ポイント履歴」のページです。',
		),
		'Controller_User_Purchasehistory' => array(
			'index' => 'パンピキッズ公式通販サイト「パンピショップ」|「注文履歴」のページです。',
			'detail' => 'パンピキッズ公式通販サイト「パンピショップ」|「注文履歴詳細」のページです。',
		),
		'Controller_User_Purchase' => array(
			'callback' => 'パンピキッズ公式通販サイト「パンピショップ」|「購入処理」のページです。',
			'complete' => 'パンピキッズ公式通販サイト「パンピショップ」|「購入お申し込み完了」のページです。',
			'confirm' => 'パンピキッズ公式通販サイト「パンピショップ」|「購入確認」のページです。',
			'purchaser' => 'パンピキッズ公式通販サイト「パンピショップ」|「購入者情報入力」のページです。',
			'receiver' => 'パンピキッズ公式通販サイト「パンピショップ」|「お届け先情報入力」のページです。',
			'sending' => 'パンピキッズ公式通販サイト「パンピショップ」|「発送方法選択」のページです。',
		),
		'Controller_Errors' => array(
			'404' => 'パンピキッズ公式通販サイト「パンピショップ」| 404ページです。',
			'error' => 'パンピキッズ公式通販サイト「パンピショップ」|エラーページです。',
		),
		'Controller_Item' => array(
			//'index' => '',
			'list' => 'パンピキッズ公式通販サイト「パンピショップ」|「商品一覧」のページです。',
		),
		'Controller_Static' => array(
			'info' => 'パンピキッズ公式通販サイト「パンピショップ」|「ショッピングガイド」のページです。',
			'law' => 'パンピキッズ公式通販サイト「パンピショップ」|「特定商取引法に関する表示」のページです。',
			'contact' => 'パンピキッズ公式通販サイト「パンピショップ」|「お問い合わせ先」のページです。',
		),
	),
);