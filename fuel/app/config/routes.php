<?php
return array(
	'_root_'  => 'index/index',  // The default route
	'_404_'   =>  function () {
		$redis_result = RedisHelper::get_master(base64_encode('url'.$GLOBALS['_SERVER']['PATH_INFO']));
		$request = ($redis_result)? $redis_result: 'errors/404';
		return \Request::forge($request, false)->execute();
	},
	//ご利用案内
	'info'    => 'static/info',
	//お問い合わせ
	'contact' => 'static/contact',
	//特定商取引法に基づく表記
	'law'     => 'static/law',
);

/**
	'_root_'  => 'welcome/index',  // The default route
	'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),
	'_404_'   => 'errors/404',    // The main 404 route
	//今日は何の日
	'what_day/jan' => 'whatday/daily_list/1',
 */