<?php
/**
 * custom config
 *
 * 独自設定ファイルです
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P.advertising Co.,Ltd.
 * @link       http://sp-k.co.jp/
 * test
 */
 
return array (

	/**
	 * shop name
	 */
	'shop_name' => 'パンピショップ ',

	/**
	 * shop name
	 */
	'shop_address' => '東京都文京区後楽1-4-14 後楽森ビル8階',

	/**
	 * shop_open_time
	 */
	'shop_open_time' => '平日10:30〜19:00',

	/**
	 * shop_tel
	 */
	'shop_tel' => '03-3868-3662',

	/**
	 * corporate_name
	 */
	'corporate_name' => 'エス・ピー広告株式会社',

	/*
	 * url関連
	 */
	'url' => array(
		'domain' => 'shop.panpikids.com',
		'http_domain' => 'http://shop.panpikids.com',
		'https_domain' => 'https://shop.panpikids.com',
		'assets_url' => 'http://shop.panpikids.com/assets',
		'ssl_assets_url' => 'https://shop.panpikids.com/assets',
	),

	/*
	 * user情報関連
	 */
	'user' => array(
		//登録 SMS code
		'sms_code' => array(
			'origin'   => 0, //SMS利用なし
			'facebook' => 1, //facebook利用
			'twitter'  => 2, //Twitter利用
			'google'   => 3, //Google利用
		),
	),

	/*
	 * SSL関連
	 */
	'ssl' => array(
		/*
		 * SSL通信を強制するControllerとmethodを指定する
		 * Controller全体を指定する場合は Controller名と*を指定する
		 * Controllerの一部を指定する場合 Controller名とarray(method名)を指定する
		 * http通信の場合は強制的にリダイレクトされる
		 */
		'force' => array(
			'Controller_Cart'                 => array(
													'index'
												),
			'Controller_User_Signup'          => array(
													'form',
													'confirm',
													'regist'
												),
			'Controller_User_Withdrawal'      => array(
													'index'
												),
			'Controller_User_Mypage'          => array(
													'index'
												),
			'Controller_User_Address'         => array(
													'form',
													'confirm',
													'regist'
												),
			'Controller_User_Change'          => array(
													'confirm',
													'form',
													'regist',
													'card',
												),
			'Controller_User_Passwd'          => array(
													'change',
													'reset'
												),
			'Controller_User_Review'          => array(
													'index',
													'form'
												),
			'Controller_User_Point'           => array(
													'list'
												),
			'Controller_User_Purchasehistory' => array(
													'index',
													'detail'
												),
			'Controller_User_Purchase'        => array(
													'*'
												),
		),
	),

	/*
	 * mail
	 */
	'mail' => array(
		'from_name' => 'パンピショップ',
		'from' => array (
			'contact' => 'contact@shopmail.panpikids.com',
			'admin' => 'admin@shopmail.panpikids.com',
		),

		'title' => array(
			'signup' => array(
				'user'  => '会員登録完了',
				'admin' => '新規登録がありました!!',
			),
			'reset_passwd' => array(
				'user'  => 'パスワード初期化のお知らせ',
				'admin' => 'パスワードの初期化を受けました!!',
			),
			'change_passwd' => array(
				'user'  => 'パスワード変更完了のお知らせ',
				'admin' => 'パスワードの変更を受けました!!',
			),
			'purchase' => array(
				'user'  => '購入完了のお知らせ',
				'admin' => '購入完了がありました!!',
			),
			'reserve' => array(
				'user'  => '予約受付のお知らせ',
				'admin' => '予約がありました!!',
			)
		)
	),
	
);
