<?php
/**
 * Part of the Fuel framework.
 *
 * @package    Fuel
 * @version    1.0
 * @copyright  S.P.advertising Co.,Ltd.
 * @author     yoshito funayose
 * @link       http://sp-k.co.jp/
 */

return array(

	/*
	 * If you don't specify a DB configuration name when you create a connection
	 * the configuration to be used will be determined by the 'active' value
	 */
	'active' => 'slave',

	/**
	 * master
	 */
	'master' => array(
		'type'        => 'mysqli',
		'connection'  => array(
			'hostname'   => '192.168.2.1',
			'username'   => 'panpishopweb',
			'password'   => '1qazse4rfvgy7',
			'database'   => 'jasmine',
			'persistent' => false,
		),
		'identifier'   => '`',
		'table_prefix' => '',
		'charset'      => 'utf8',
		'enable_cache' => true,
		'profiling'    => true,
	),

	/**
	 * slave
	 */
	'slave' => array(
		'type'        => 'mysqli',
		'connection'  => array(
			'hostname'   => '192.168.2.2',
			'username'   => 'panpishopweb',
			'password'   => '1qazse4rfvgy7',
			'database'   => 'jasmine',
			'persistent' => false,
		),
		'identifier'   => '`',
		'table_prefix' => '',
		'charset'      => 'utf8',
		'enable_cache' => true,
		'profiling'    => true,
	),

);
