<?php
/**
 * redis config
 *
 * redis用独自設定ファイルです
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P.advertising Co.,Ltd.
 * @link       http://sp-k.co.jp/
 * test
 */
 
return array (
	/**
	 * redisの有効/無効
	 */
	'is_enable' => true,
	
);
