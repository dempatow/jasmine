<?php
/**
 * auth_config
 *
 * 認証機能設定ファイルです
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P.advertising Co.,Ltd.
 * @link       http://sp-k.co.jp/
 */
return array(

	'token_salt' => 'S.P.advertising Co.,Ltd.1qazse4rfvgy7',

	/*
	 * 強制ログアウトさせる時間(分単位)
	 */
	'logout_term' => 60,

	/*
	 * アクセス権設定
	 * (*)指定で全てのコントローラとメソッドが許可されます!!
	 */
	'ACL' => array(
		/**
		 * 非ログインユーザー
		 */
		0 => array(
			//'Controller_Index' => array('*'),
			//'*'  => array('*'),
			'Controller_Index'  => array('index'),
			'Controller_Cart'  => array('index', 'reserve'),
			'Controller_User_Signup'  => array('form', 'confirm', 'regist'),
			'Controller_User_Passwd'  => array('reset'),
			'Controller_User_Purchase'  => array('*'),
			'Controller_Errors'  => array('*'),
			'Controller_Item'  => array('*'),
			'Controller_Static'  => array('*'),
			'Controller_Auth'   => array('*'),
		),

		/**
		 * 一般ユーザー
		 */
		1 => array(
			'Controller_Auth'   => array('*'),
			'Controller_Index'  => array('index'),
			'Controller_Cart'   => array('index', 'reserve'),
			'Controller_User_Withdrawal'  => array('index'),
			'Controller_User_Mypage'  => array('index'),
			'Controller_User_Address'  => array('form', 'confirm', 'regist'),
			'Controller_User_Change'  => array('form', 'confirm', 'regist', 'card'),
			'Controller_User_Passwd'  => array('change', 'reset'),
			'Controller_User_Review'  => array('index', 'form'),
			'Controller_User_Point'  => array('list'),
			'Controller_User_Purchasehistory'  => array('index', 'detail'),
			'Controller_User_Purchase'  => array('*'),
			'Controller_Errors'  => array('*'),
			'Controller_Item'  => array('*'),
			'Controller_Static'  => array('*'),
		),

//		/**
//		 * hogehoge1ユーザー
//		 */
//		2 => array(
//			''  => array(''),
//		),
//
//		/**
//		 * hogehoge2ユーザー
//		 */
//		3 => array(
//			''  => array(''),
//			''  => array(''),
//		),

		/**
		 * 管理者ユーザー
		 */
		10 => array(
			'*'  => array('*'),
		),
	),

	/****************
	 * 自動ログイン関係 
	 ****************/

	/*
	 * パスポートキー
	 */
	'passport_key' => 'passport',
	/*
	 * パスポート有効期限 (秒単位)
	 */
	'expiration' => 2073600,
);
