<?php
/**
 * point_config
 *
 * ポイント機能設定ファイルです
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P.advertising Co.,Ltd.
 * @link       http://sp-k.co.jp/
 */
return array(
	//ポイント有効期限(月単位)
	'expiration' => 24,
);
