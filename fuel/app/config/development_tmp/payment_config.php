<?php
/**
 * payment_config
 *
 * 決済機能設定ファイルです
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P.advertising Co.,Ltd.
 * @link       http://sp-k.co.jp/
 */
return array(

	//GMO URL
	'TARGET_URL'      => 'https://beta.epsilon.jp/cgi-bin/order/receive_order3.cgi',

	////契約者コード
	'CONTRACT_CODE'   => '60734780',

	//決済区分(カード固定)
	'ST_CODE'         => '10000-0000-00000-00000-00000-00000-00000',

	//課金区分(1回課金固定)
	'MISSION_CODE'    => '1',

	//応答形式(XML固定)
	'RESPONSE_FORMAT' => '1',

	//決済商品名(明細に表示される)
	'ITEM_NAME' => 'ご利用代金',

	//決済アイテムコード(明細に表示される)
	'ITME_CODE' => '1',

	//-----処理区分-----
	//初回課金
	'PROSESS_FIRST'             => '1',
	//登録済み課金
	'PROSESS_REGISTERED'        => '2',
	//登録内容変更課金
	'PROSESS_REGISTERED_CHANGE' => '4',
);
