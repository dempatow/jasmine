<?php
/**
 * the facebook class
 *
 * @package    Fuel
 * @version    1.5
 * @author     yoshito funayose
 * @copyright  S.P advertising Co.,Ltd.
 * @link       http://sp-k.co.jp
 */

namespace FaceBook;

class FaceBook
{

	/**
	 * Instance for singleton usage.
	 */
	public static $_instance = false;

	/**
	 * Driver config defaults.
	 */
	protected static $_defaults;

	/**
	 * Email priorities
	 */
	const P_LOWEST		= '5 (Lowest)';
	const P_LOW			= '4 (Low)';
	const P_NORMAL		= '3 (Normal)';
	const P_HIGH		= '2 (High)';
	const P_HIGHEST		= '1 (Highest)';

	/**
	 * this class forge.
	 *
	 * @return  Email_Driver    one of the email drivers
	 */
	public static function forge()
	{
		\Config::load('facebook', true);
		static::$_defaults = \Config::get('facebook');
		return new FaceBook();
	}

	/**
	 * test method
	 *
	 * @return  Email_Driver    one of the email drivers
	 */
	public function test()
	{
		echo "FB packege";
		\Debug::dump(\Config::get('facebook'));exit;
	}
}